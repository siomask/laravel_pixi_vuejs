<?php

namespace Tests\Utils;

trait AuthHelperTrait
{
    private function login($headers) {
        $response = $this->json('POST','/api/auth/login',
            $headers, array_merge([
                'Accept' => 'application/json, text/plain, */*',
                'Content-Type' => 'application/json',
            ]));

        return $response;
    }

    public function loginByUser($username, $password, $host) {

        $response = $this->login([
            'username' => $username,
            'password' => $password,
            'host'     => $host,
        ]);
        $response->assertStatus(200);
        $data = $response->json();
//        $this->assertTrue(isset($data['status']), 'the status is not found');
//        $this->assertTrue(isset($data['message']), 'the message is not found');
        $this->assertTrue(isset($data['access_token']), 'the token is not found');
        $data['username'] = $username;
        $data['password'] = $password;
        $data['host'] = $host;
        return $data;
    }

    public function loginByConfig(array $config = null) {
        $testConfig = $config;
        if ($testConfig === null) {
            $testConfig = config('test');
        }
        if (!isset($testConfig['host'])) {
            throw new \Exception('host is required an array field in laravel-path/config/test.php');
        }
        if (isset($testConfig['username']) && isset($testConfig['password'])) {
            return $this->loginByUser('LudwigVerschraegen', 'admin', $testConfig['host']);
        }
        throw new \Exception('loginByConfig must have username and password');
    }
}
