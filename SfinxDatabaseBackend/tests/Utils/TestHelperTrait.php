<?php

namespace Tests\Utils;

trait TestHelperTrait
{
    private function assertIsArray($data, string $message = null) {
        $this->assertTrue(is_array($data), $message ?? 'the data is not an array');
    }

    private function assertArrayHasKeys(array $data, array $keys, string $message = null, array $keyMessages = []) {
        foreach ($keys as $key => $type) {
            $this->assertTrue(array_key_exists($key, $data), $message ?? "the array data has no '$key' key");
            if ($type !== null) {
                if ($type == 'null') {
                    $this->assertTrue(is_null($data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a null type');
                }
                if ($type == 'string') {
                    $this->assertTrue(is_string('' . $data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a string type');
                }
                if ($type == 'int') {
                    $this->assertTrue(is_int(+$data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a int type');
                }
                if ($type == 'float') {
                    $this->assertTrue(is_float(+$data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a float type');
                }
                if ($type == 'array') {
                    $this->assertTrue(is_array($data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a array type');
                }
                if ($type == 'object') {
                    $this->assertTrue(is_object($data[$key]),
                        $keyMessages[$key] ?? 'the array key is not a object type');
                }
            }
        }
    }
}
