<?php

namespace Tests\Feature\Contract\Client;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ClientTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;



    public function test_fetch_all()
    {
        echo "\n", 'ClientTest::test_fetch_all()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('GET', '/api/v1/client', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($clientResponse->content());
        var_dump(count($clientResponse->json()['data']));
        $clientResponse->assertStatus(200);

    }
    public function test_store()
    {
        echo "\n", 'ClientTest::test_store()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('POST', '/api/v1/client', [
            'first_name'=>'TEsts',
            'last_name'=>'TEsts'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($clientResponse->content());
        $clientResponse->assertStatus(201);

    }
    public function test_update()
    {
        echo "\n", 'ClientTest::test_update()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('PUT', '/api/v1/client/4', [
            'first_name'=>'TEstsdsd',
            'last_name'=>'TEsts'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($clientResponse->content());
        $clientResponse->assertStatus(200);

    }
    public function test_remove()
    {
        echo "\n", 'ClientTest::test_update()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('DELETE', '/api/v1/client/4', [
            'first_name'=>'TEstsdsd',
            'last_name'=>'TEsts'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($clientResponse->content());
        $clientResponse->assertStatus(200);

    }
    public function test_get_one()
    {
        echo "\n", 'ClientTest::test_update()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('GET', '/api/v1/client/5', [ ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($clientResponse->content());
        $clientResponse->assertStatus(200);

    }
}
