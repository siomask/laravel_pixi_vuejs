<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\TestHelperTrait;
use Tests\Utils\AuthHelperTrait;

class ContactTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_fetch_contacts()
    {
        echo "\n", "ContactTest::test_fetch_contacts()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact?type=2,1&name=gh', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

//        $this->assertJson($response->getContent(), 'the contractor response data is not json');
//
//        $contractorData = $response->json();

    }
}
