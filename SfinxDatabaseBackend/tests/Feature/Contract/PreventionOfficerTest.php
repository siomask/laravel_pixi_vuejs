<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class PreventionOfficerTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetPreventionOfficers()
    {
        echo "\n", 'PreventionOfficerTest::testGetPreventionOfficers()', "\n";

        $authResponse = $this->loginByConfig();

        $preventionOfficerResponse = $this->json('GET','/api/contact/prevention_officers', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $preventionOfficerResponse->assertStatus(200);

        $this->assertJson($preventionOfficerResponse->getContent(), 'the prevention officer data is not json');

        $preventionOfficerData = $preventionOfficerResponse->json();

        $this->assertArrayHasKeys($preventionOfficerData, ['data' => 'array']);

        foreach ($preventionOfficerData['data'] as $preventionOfficerArray) {
            $this->assertIsArray($preventionOfficerArray);
            $this->assertArrayHasKeys($preventionOfficerArray, [
                'contactid'     => 'int',
                'projectnaam'   => 'string',
                'straat'        => 'string',
                'nr'            => 'string',
                'bus'           => 'string',
                'postcode'      => 'string',
                'gemeente'      => 'string',
                'klantenstatus' => 'string',
                'werkkrachten'  => 'string',
            ]);
        }
    }
}
