<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class StabilityEngineerTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetStabilityEngineers()
    {
        echo "\n", 'StabilityEngineerTest::testGetStabilityEngineers()', "\n";

        $authResponse = $this->loginByConfig();

        $stabilityEngineerResponse = $this->json('GET','/api/contact/stability_engineers', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $stabilityEngineerResponse->assertStatus(200);

        $this->assertJson($stabilityEngineerResponse->getContent(), 'the stability engineer data is not json');

        $stabilityEngineerData = $stabilityEngineerResponse->json();

        $this->assertArrayHasKeys($stabilityEngineerData, ['data' => 'array']);

        foreach ($stabilityEngineerData['data'] as $stabilityEngineerArray) {
            $this->assertIsArray($stabilityEngineerArray);
            $this->assertArrayHasKeys($stabilityEngineerArray, [
                'id'           => 'int',
                'street'       => 'string',
                'nr'           => 'string',
                'bus'          => 'string',
                'postalcode'   => 'string',
                'municipality' => 'string',
                'type'         => 'string',
                'projectnr'    => 'string',
                'projectnaam'  => 'string'
            ]);
        }
    }
}