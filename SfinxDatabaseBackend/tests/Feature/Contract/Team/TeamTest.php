<?php

namespace Tests\Feature\Contract\Team;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class TeamTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_store_team()
    {
        echo "\n", 'TeamTest::test_store_team()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/contact/3/team', [
            'name'=>'Test',
            'abbreviation'=>'Test'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }

    public function test_fetch_teams()
    {
        echo "\n", 'TeamTest::test_fetch_teams()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact/3/team', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        $response->assertStatus(200);
        $data = $response->json();
        var_dump($response->content());

        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_taam()
    {
        echo "\n", 'TeamTest::test_update_taam()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/contact/3/team/4', [
            'name'=>'TestUpdatedfg',
            'abbreviation'=>'Test'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_team()
    {
        echo "\n", 'TeamTest::test_delete_team()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/contact/3/team/4', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
