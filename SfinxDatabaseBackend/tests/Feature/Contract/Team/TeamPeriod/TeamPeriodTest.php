<?php

namespace Tests\Feature\Contract\Team;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class TeamPeriodTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_store_team_period()
    {
        echo "\n", 'TeamTest::test_store_team()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/contact/3/team/3/period', [
            'start'=>'2018-02-05',
            'end'=>'2018-02-05'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }

    public function test_fetch_team_periods()
    {
        echo "\n", 'TeamPeriodTest::test_fetch_team_periods()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact/3/team/3/period', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

        $data = $response->json();
        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_tam_period()
    {
        echo "\n", 'TeamPeriodTest::test_update_tam_period()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/contact/3/team/3/period/4', [
            'start'=>'2018-01-05',
            'end'=>'2018-02-05'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_team_period()
    {
        echo "\n", 'TeamPeriodTest::test_delete_team_period()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/contact/3/team/3/period/4', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
