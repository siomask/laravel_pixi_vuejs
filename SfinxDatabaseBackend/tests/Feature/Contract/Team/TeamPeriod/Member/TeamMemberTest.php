<?php

namespace Tests\Feature\Contract\Team;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class TeamMemberTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_store_team_period_member()
    {
        echo "\n", 'TeamMemberTest::test_store_team_member()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/contact/3/team/3/period/3/member', [
            'id'=>'1'//member id
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }

    public function test_fetch_team_period_members()
    {
        echo "\n", 'TeamMemberTest::test_fetch_team_members()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact/3/team/3/period/3/member', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($response->content());

        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_team_period_member()
    {
        echo "\n", 'TeamMemberTest::test_delete_team_period_member()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/contact/3/team/3/period/3/member/1', [], [//member id
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
