<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class EPBReporterTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetEPBReporters()
    {
        echo "\n", 'EPBReporterTest::testGetEPBReporters()', "\n";

        $authResponse = $this->loginByConfig();

        $epbReporterResponse = $this->json('GET','/api/contact/epb_reporters', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $epbReporterResponse->assertStatus(200);

        $this->assertJson($epbReporterResponse->getContent(), 'the EPB reporter data is not json');

        $epbReporterData = $epbReporterResponse->json();

        $this->assertArrayHasKeys($epbReporterData, ['data' => 'array']);

        foreach ($epbReporterData['data'] as $epbReporterArray) {
            $this->assertIsArray($epbReporterArray);
            $this->assertArrayHasKeys($epbReporterArray, [
                'id'           => 'int',
                'street'       => 'string',
                'nr'           => 'string',
                'bus'          => 'string',
                'postalcode'   => 'string',
                'municipality' => 'string',
                'type'         => 'string',
                'projectnr'    => 'string',
                'projectnaam'  => 'string'
            ]);
        }
    }
}
