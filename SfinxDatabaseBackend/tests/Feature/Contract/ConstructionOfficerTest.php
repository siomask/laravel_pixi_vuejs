<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ConstructionOfficerTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetConstructionOfficers()
    {
        echo "\n", 'ConstructionOfficerTest::testGetConstructionOfficers()', "\n";

        $authResponse = $this->loginByConfig();

        $constructionOfficerResponse = $this->json('GET','/api/contact/construction_officers', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $constructionOfficerResponse->assertStatus(200);

        $this->assertJson($constructionOfficerResponse->getContent(), 'the construction officer data is not json');

        $constructionOfficerData = $constructionOfficerResponse->json();

        $this->assertArrayHasKeys($constructionOfficerData, ['data' => 'array']);

        foreach ($constructionOfficerData['data'] as $constructionOfficerArray) {
            $this->assertIsArray($constructionOfficerArray);
            $this->assertArrayHasKeys($constructionOfficerArray, [
                'id'           => 'int',
                'street'       => 'string',
                'nr'           => 'string',
                'bus'          => 'string',
                'postalcode'   => 'string',
                'municipality' => 'string',
                'type'         => 'string',
                'projectnr'    => 'string',
                'projectnaam'  => 'string'
            ]);
        }
    }
}
