<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ClientTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }

    public function testGetClients()
    {
        echo "\n", 'ClientTest::testGetClients()', "\n";

        $authResponse = $this->loginByConfig();

        $clientResponse = $this->json('GET', '/api/contact/clients', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $clientResponse->assertStatus(200);

        $this->assertJson($clientResponse->getContent(), 'the response data is not json');

        $clientData = $clientResponse->json();

        $this->assertIsArray($clientData, 'the response data must be an array');

        $this->assertArrayHasKey('data', $clientData, "the response data must be a 'data' key");

        $arrayData = $clientData['data'];

        $this->assertIsArray($arrayData, 'Data must be an array of data');

        foreach ($arrayData as $itemData) {
            $this->assertArrayHasKeys($itemData, [
                'id'            => 'int',
                'street'        => 'string',
                'nr'            => 'string',
                'bus'           => 'string',
                'postalcode'    => 'int',
                'municipality'  => 'string',
                'type'          => 'int',
                'projectnr'     => 'string',
                'projectnaam'   => 'string',
                'klantenstatus' => 'int',
                'site'          => 'array',
            ]);
            $sites = $itemData['site'];
            $this->assertIsArray($sites, 'the site must be an array');
            if (count($sites) === 0) continue;

            $this->assertArrayHasKeys($sites, [
                'id'          => 'int',
                'contactid'   => 'int',
                'projectLead' => 'array',
            ]);
            $projectLead = $sites['projectLead'];
            if (count($projectLead) === 0) continue;

            foreach($projectLead as $projectLeadArray) {
                $this->assertArrayHasKeys($projectLeadArray, [
                    'id'        => 'int',
                    'firstName' => 'string',
                    'lastName'  => 'string',
                    'type'      => 'int',
                ]);
            }
        }
    }
}
