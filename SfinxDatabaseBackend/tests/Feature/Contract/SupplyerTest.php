<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class SupplyerTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetSupplyers()
    {
        echo "\n", 'SupplyerTest::testGetSupplyers()', "\n";

        $authResponse = $this->loginByConfig();

        $suplyerResponse = $this->json('GET','/api/contact/suppliers', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $suplyerResponse->assertStatus(200);

        $this->assertJson($suplyerResponse->getContent(), 'the contractor response data is not json');

        $contractorData = $suplyerResponse->json();

        $this->assertArrayHasKeys($contractorData, [
            'data' => 'array'
        ]);

        $data = $contractorData['data'];

        foreach ($data as $dataArray) {
            $this->assertArrayHasKeys($dataArray, [
                'id'           => 'int',
                'street'       => 'string',
                'nr'           => 'string',
                'bus'          => 'string',
                'postalcode'   => 'string',
                'municipality' => 'string',
                'type'         => 'string',
                'projectnr'    => 'string',
                'projectnaam'  => 'string'
            ]);
        }
    }
}
