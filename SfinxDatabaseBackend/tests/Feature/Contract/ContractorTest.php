<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ContractorTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetContractors()
    {
        echo "\n", 'ContractorTest::testGetContractors()', "\n";

        $authResponse = $this->loginByConfig();

        $contractorResponse = $this->json('GET','/api/contact/contractors', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $contractorResponse->assertStatus(200);

        $this->assertJson($contractorResponse->getContent(), 'the contractor response data is not json');

        $contractorData = $contractorResponse->json();

        $this->assertArrayHasKeys($contractorData, [
            'data' => 'array'
        ]);

        $data = $contractorData['data'];

        foreach ($data as $dataArray) {
            $this->assertArrayHasKeys($dataArray, [
                'id'           => 'int',
                'street'       => 'string',
                'nr'           => 'string',
                'bus'          => 'string',
                'postalcode'   => 'int',
                'municipality' => 'string',
                'type'         => 'string',
                'projectnr'    => 'string',
                'projectnaam'  => 'string',
            ]);
        }
    }
}
