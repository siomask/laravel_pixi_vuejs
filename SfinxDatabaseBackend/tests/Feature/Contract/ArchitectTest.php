<?php

namespace Tests\Feature\Contract;

use Tests\TestCase;
use Tests\Utils\TestHelperTrait;
use Tests\Utils\AuthHelperTrait;

class ArchitectTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetArchitects()
    {
        echo "\n", "ArchitectTest::testGetArchitects()", "\n";

        $authResponse = $this->loginByConfig();

        $suplyerResponse = $this->get('/api/contact/suppliers', [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $suplyerResponse->assertStatus(200);

        $this->assertJson($suplyerResponse->getContent(), 'the contractor response data is not json');

        $contractorData = $suplyerResponse->json();

        $this->assertArrayHasKeys($contractorData, [
            'data' => 'array'
        ]);

        $data = $contractorData['data'];

        foreach ($data as $dataArray) {
            $this->assertArrayHasKeys($dataArray, [
                'contactid'     => 'int',
                'projectnaam'   => 'string',
                'straat'        => 'string',
                'nr'            => 'string',
                'bus'           => 'string',
                'postcode'      => 'string',
                'gemeente'      => 'string',
                'klantenstatus' => 'string',
                'werkkrachten'  => 'string'
            ]);
        }
    }
}
