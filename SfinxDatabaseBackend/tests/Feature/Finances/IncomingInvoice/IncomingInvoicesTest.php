<?php

namespace Tests\Feature\Finances\IncomingInvoice;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
use Faker\Generator as Faker;
class IncomingInvoicesTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;


    public function test_get_invoices(){
        echo "\n", 'IncomingInvoicesTest::test_get_invoices()', "\n";
        $authResponse = $this->loginByConfig();

        try{
            $dataResponse = $this->json('GET', '/api/v1/finance/incoming-invoice', [], [
                'Authorization' => 'Bearer ' . $authResponse['access_token']
            ]);
        }catch (\Exception $e){
            var_dump($e->getMessage());
        }

        var_dump('-----------');
        var_dump($dataResponse->content());
//        $dataResponse->assertStatus(200);

    }

    public function test_store_invoices(){
        echo "\n", 'IncomingInvoicesTest::test_store_invoices()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/finance/incoming-invoice', [
            'contact_id' => random_int(1, 7),
            'serial_number' => 22556,
            'comment' => '121',
            'invoice_number' => 123,
            'payment_term' => 60 * 60 * 24 * (random_int(1, 7)),//random days
            'is_paid' => random_int(1, 7) < 3,
            'financial_discount' => mt_rand() / mt_getrandmax(),
            'discount' => mt_rand() / mt_getrandmax()
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(201);

    }
    public function test_update_invoices(){
        echo "\n", 'IncomingInvoicesTest::test_update_invoices()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/finance/incoming-invoice/4',  [
            'contact_id' => random_int(1, 7),
            'serial_number' => 22556,
            'comment' => '121fd',
            'invoice_number' => 123,
            'payment_term' => 60 * 60 * 24 * (random_int(1, 7)),//random days
            'is_paid' => random_int(1, 7) < 3,
            'financial_discount' => mt_rand() / mt_getrandmax(),
            'discount' => mt_rand() / mt_getrandmax()
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_delete_invoices(){
        echo "\n", 'IncomingInvoicesTest::test_delete_invoices()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/finance/incoming-invoice/4',  [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }


}
