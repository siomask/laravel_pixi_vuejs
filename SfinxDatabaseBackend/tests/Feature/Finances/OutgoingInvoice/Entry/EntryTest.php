<?php

namespace Tests\Feature\Finances\OutgoingInvoice;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
use Faker\Generator as Faker;
class EntryTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;


    public function test_get_invoice_entries(){
        echo "\n", 'EntryTest::test_get_invoice_entries()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/finance/outgoing-invoice/1/entry', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }

    public function test_store_invoice_entry(){
        echo "\n", 'EntryTest::test_store_invoice_entry()', "\n";

        $authResponse = $this->loginByConfig();
        $faker =  \Faker\Factory::create();
        $dataResponse = $this->json('POST', '/api/v1/finance/outgoing-invoice/1/entry', [
            'name' => $faker->name,
            'amount' => mt_rand() / mt_getrandmax(),
            'vat_percentage' => random_int(1, 100) / 100
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(201);

    }
    public function test_update_invoice_entry(){
        echo "\n", 'EntryTest::test_update_invoice_entry()', "\n";

        $authResponse = $this->loginByConfig();
        $faker =  \Faker\Factory::create();
        $dataResponse = $this->json('PUT', '/api/v1/finance/outgoing-invoice/1/entry/4',  [
            'name' => $faker->name,
            'amount' => mt_rand() / mt_getrandmax(),
            'vat_percentage' => random_int(1, 100) / 100
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_delete_invoice_entry(){
        echo "\n", 'EntryTest::test_delete_invoice_entry()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/finance/outgoing-invoice/1/entry/4bit',  [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }


}
