<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ConstructionSiteTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_move_all_collaborators()
    {
        echo "\n", 'ConstructionSite::test_move_all_collaborators()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/construction-site/1/move', [
            'daysToMove'=>2
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);


    }
    public function test_get_sites()
    {
        echo "\n", 'ConstructionSite::test_get_sites()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/construction-site', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'selected' => 'array',
            'allSites' => 'array'
        ]);
    }
    public function test_delete_site()
    {
        echo "\n", 'ConstructionSite::test_delete_site()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/planner/construction-site/1', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_update_site()
    {
        echo "\n", 'ConstructionSite::test_delete_site()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/planner/construction-site/1', [
              "estimators"=>[],
            "projectLeaders"=>[
                ["user_id"=>2,"percentage"=>"50.00"],
                ["user_id"=>3,"percentage"=>"50.00"]
            ],
            "presented_price"=>13
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
}
