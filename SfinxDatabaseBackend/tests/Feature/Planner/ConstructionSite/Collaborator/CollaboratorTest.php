<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class CollaboratorTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_move_all_collaborators()
    {
        echo "\n", 'CollaboratorTest::test_move_all_collaborators()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/construction-site/1/collaborator/2/move', [
            'daysToMove'=>-2
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);

    }
    public function test_update_site_collaborator()
    {
        echo "\n", 'CollaboratorTest::test_update_site_collaborator()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/planner/construction-site/1/collaborator/33', [
            'start'=>'2018-06-02',
            'end'=>'2018-06-02',
            'request_date'=>'2018-06-02',
            'is_contractor_type_assigned'=>0,
            'is_contractor_type_ordered'=>1,
            'comment'=>'fghgh',
            'contractor_type_id'=>'3',
            'contact_id'=>'2',
            'field_will_be_ignored'=>'fghgh',
//            'is_ordered'=>1,/
//            'is_assigned'=>1,
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);

    }
    public function test_show_start_end_only()
    {
        echo "\n", 'CollaboratorTest::test_update_site_collaborator()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/construction-site/1/collaborator/1', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'start_date' => 'date'
        ]);

    }
    public function test_fetch_all()
    {
        echo "\n", 'CollaboratorTest::test_fetch_all()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/construction-site/1/collaborator', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'date'
        ]);

    }
    public function test_create_new()
    {
        echo "\n", 'CollaboratorTest::test_create_new()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/construction-site/1/collaborator', [
            'contractor_type_id'=>1,
            'contact_id'=>1
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(201);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'date'
        ]);

    }
    public function test_delete()
    {
        echo "\n", 'CollaboratorTest::test_delete()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/planner/construction-site/1/collaborator/29', [ ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);



    }
}
