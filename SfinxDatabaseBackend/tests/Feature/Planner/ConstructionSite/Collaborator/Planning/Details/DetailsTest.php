<?php

namespace Tests\Feature\Planner\ConstructionSite\Collaborator\Planning\Details;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class DetailsTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;


    public function test_get_active_teams()
    {
        $this->_URL = '/api/v1/planner/details';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_get_active_teams()', "\n";

        $response = $this->json('GET', $this->_URL, [], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

//        $data = $response->json();

//        $this->assertArrayHasKeys($data, [
//            'id' => 'number'
//        ]);
    }

    public function test_get_details_by_planned_day()
    {
//        http://localhost:8000/api/v1/planner/construction-site/1/collaborator/31/planning/6/details?start=6/7/2018&end=6/7/2018
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/31/planning/6/details?start=6/7/2018&end=6/7/2018';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_get_details_by_planned_day()', "\n";

        $response = $this->json('GET', $this->_URL, [], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        foreach ($response->json()['data'] as $d) {
            foreach ($d['teamMembers'] as $member) {
                foreach ($member['plannings'] as $p) {
                    var_dump("----++++", $p);
                }
            }
        }
//        var_dump($response->content());
        $response->assertStatus(200);

    }

    public function test_store_details_by_planned_day()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/1/details';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_store_details_by_planned_day()', "\n";

        $response = $this->json('POST', $this->_URL, [
            "day" => "5/24/2018",
            "teamMemberId" => "3",
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

    }

    public function test_delete_details()
    {
        $this->_URL = '/api/v1/planner/construction-site/2/collaborator/6/planning/1/details/7?teamId=3';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_store_details_by_planned_day()', "\n";

        $response = $this->json('DELETE', $this->_URL, [], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

    }

}
