<?php

namespace Tests\Feature\Planner\ConstructionSite\Collaborator\Planning;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class PlanningTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;



    public function test_create_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_create_planning_data()', "\n";

        $response = $this->json('POST', $this->_URL, [
            'start' => '2018-06-01',
            'end' => '2018-06-05',
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'id' => 'number'
        ]);
    }
    public function test_create_planning_data_in_weekend()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_create_planning_data()', "\n";

        $response = $this->json('POST', $this->_URL, [
            'start' => '2018-06-09',
            'end' => '2018-06-09',//can planed only one day
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

//        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'id' => 'number'
        ]);
    }
    public function test_update_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/1';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_update_planning_data()', "\n";

        $response = $this->json('PUT', $this->_URL, [
            'start' => '2018-06-04',
            'end' => '2018-06-05',
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

//        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'state' => 'number'
        ]);
    }

    public function test_delete_from_the_end_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/3?date=2018-06-04';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_delete_from_the_end_planning_data()', "\n";

        $response = $this->json('DELETE', $this->_URL, [], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'type' => 'number'
        ]);
        $this->assertEquals(3, $data['type']);
    }

    public function test_delete_from_the_start_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/3?date=2018-06-01';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_delete_from_the_start_planning_data()', "\n";

        $response = $this->json('DELETE', $this->_URL, [
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'type' => 'number'
        ]);
        $this->assertEquals(2, $data['type']);
    }

    public function test_delete_all_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/3?date=2018-06-04&dropall=true';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_update_planning_data()', "\n";

        $response = $this->json('DELETE', $this->_URL, [
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'type' => 'number'
        ]);
        $this->assertEquals(1, $data['type']);
    }

    public function test_split_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/3?date=2018-06-04&isSplit=true';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_split_planning_data()', "\n";

        $response = $this->json('DELETE', $this->_URL, [
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->json());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'type' => 'number'
        ]);
        $this->assertEquals(4, $data['type']);
    }

    public function test_move_planning_data()
    {
        $this->_URL = '/api/v1/planner/construction-site/1/collaborator/2/planning/6/move';
        $this->authResponse = $this->loginByConfig();


        echo "\n", 'PlanningTest::test_move_planning_data()', "\n";

        $response = $this->json('POST', $this->_URL, [
            'daysToMove'=>2
        ], [
            'Authorization' => 'Bearer ' . $this->authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'state' => 'number'
        ]);
        $this->assertEquals(2, $data['state']);
    }

}
