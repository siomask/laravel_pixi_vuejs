<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class ExternalTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_get_site_externals()
    {
        echo "\n", 'ExternalTest::test_get_site_externals()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/construction-site/1/external', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }

    public function test_store_site_external()
    {
        echo "\n", 'ExternalTest::test_store_site_external()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/construction-site/1/external', [
            'comment' => "sdf",
            'contact_id' => 1,
            'ordered_by' => 2,
            'visible' => false
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(201);

    }

    public function test_update_site_external()
    {
        echo "\n", 'ExternalTest::test_update_site_external()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/planner/construction-site/1/external/4', [
            'comment' => "TEst",
            'contact_id' => 1,
            'ordered_by' => 2,
            'visible' => true
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }

    public function test_delete_site_external()
    {
        echo "\n", 'ExternalTest::test_delete_site_external()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/planner/construction-site/1/external/4', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }

}
