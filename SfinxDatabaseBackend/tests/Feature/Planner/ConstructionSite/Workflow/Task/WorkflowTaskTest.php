<?php

namespace Tests\Feature\Planner\ConstructionSite\Workflow\Task;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class WorkflowTaskTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_get_site_workflow_list()
    {
        echo "\n", 'WorkflowTaskTest::test_get_site_workflow_list()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/construction-site/1/workflow-task', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        //will return list of task with thir pivots in field pivot
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }

    public function test_store_site_workflow_task()
    {
        echo "\n", 'WorkflowTaskTest::test_store_site_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/construction-site/1/workflow-task', [
            'workflow_task_id'=>3,//----id of task
            'start'=>'2018-02-02' ,
            'end'=>'2018-02-02'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_update_site_workflow_task()
    {
        echo "\n", 'WorkflowTaskTest::test_store_site_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/planner/construction-site/1/workflow-task/13', [
            'task_status'=>'2' ,
            'start'=>'2018-02-02' ,
            'end'=>'2018-02-02'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_delete_site_workflow_task()
    {
        echo "\n", 'WorkflowTaskTest::test_delete_site_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/planner/construction-site/1/workflow-task/12', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
}
