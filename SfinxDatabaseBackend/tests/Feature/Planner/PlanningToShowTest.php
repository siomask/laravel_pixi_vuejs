<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class PlanningToShowTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetPlanners()
    {
        echo "\n", 'PlanningToShowTest::testGetPlanners()', "\n";

        $authResponse = $this->loginByConfig();

        $plannerResponse = $this->json('GET','/api/planner/planning_to_show', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $plannerResponse->assertStatus(200);


        $this->assertJson($plannerResponse->getContent(), 'the planner response data is not json');

        $plannerData = $plannerResponse->json();

        $this->assertArrayHasKeys($plannerData, [
            'data' => 'array'
        ]);

        $data = $plannerData['data'];

        foreach ($data as $dataArray) {
            $this->assertArrayHasKeys($dataArray, [
                'planningtonenid' => 'int',
                'site_name'       => 'string'
            ]);
        }
    }
}
