<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class PlannerTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetPlanners()
    {
        echo "\n", 'PlanningToShowTest::testGetPlanners()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/planner', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        $response->assertStatus(200);

        $this->assertJson($response->getContent(), 'the response data is not json');

        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

        $dataItems = $data['data'];

        foreach ($dataItems as $dataItem) {
            $this->assertIsArray($dataItem);
            $this->assertArrayHasKeys($dataItem, [
                'site' => 'int',
                'info' => 'array',
                'branches' => 'array'
            ]);

            $info = $dataItem['info'];
            $this->assertArrayHasKeys($info, ['title' => 'string']);

            $branches = $dataItem['branches'];
            foreach ($branches as $branch) {
                $this->assertIsArray($branch);
                $this->assertArrayHasKeys($branch, [
                    'branch'  => 'int',
                    'info'    => 'array',
                    'contact' => 'array',
                    'data'    => 'array'
                ]);

                $info = $branch['info'];
                $this->assertIsArray($info);
                $this->assertArrayHasKeys($info, ['abbreviation' => 'string', 'full' => 'string']);

                $contact = $branch['contact'];
                $this->assertIsArray($contact);
                $this->assertArrayHasKeys($contact, ['contractor' => 'int', 'name' => 'string']);
            }
        }
    }

    public function test_get_planners()
    {
        echo "\n", 'PlanningToShowTest::test_get_planners()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/planner', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($response->getContent());
//        var_dump("---------",$response->json()['cellsId']);
//        foreach ($response->json()['constructionSites'] as $sites){
//            var_dump("---------",$sites['info']);
//        }
        $response->assertStatus(200);





    }

    public function test_get_planner_details()
    {
        echo "\n", 'PlannerTest::test_get_planner_details()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/planner/details?start=2018-02-02&end=2018-02-02', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($response->getContent());
        $response->assertStatus(200);





    }
}
