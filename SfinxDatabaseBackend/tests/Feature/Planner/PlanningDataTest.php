<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class PlanningDataTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testStorePlanningData()
    {
        echo "\n", 'PlanningDataTest::testStorePlanningData()', "\n";

        $authResponse = $this->loginByConfig();

        $data = [
            'branch' => 6931,
            'end'    => 1518048000,
            'start'  => 1518048000,
        ];

        $this->assertIsArray($data);
        $this->assertArrayHasKeys($data, [
            'branch' => 'int',
            'end'    => 'int',
            'start'  => 'int',
        ]);

        $response = $this->json('POST', '/api/planner/data', $data, [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $response->assertStatus(200);

        $this->assertJson($response->getContent(), 'the response is not json');

        $data = $response->json();

        $this->assertArrayHasKeys($data, [
            'werfdetailatid'           => 'int',
            'begin'                    => 'int',
            'einde'                    => 'int',
            'opmerkingvoorklant'       => 'string',
            'opmerkingvooraannemer'    => 'string',
            'werfdetailplanningdataid' => 'int',
        ]);
    }

    public function testRemovePlanningData()
    {
        echo "\n", 'PlanningToShowTest::testRemovePlanningData()', "\n";

        // auth
        $authResponse = $this->loginByConfig();

        // delete by id
        $data = [
            'id' => 123,
        ];

        $response = $this->json('POST', '/api/planner/data/remove', $data, [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $this->assertTrue($response->isNotFound() || $response->isOk());
        if ($response->isOk()) {
            $this->assertJson($response->getContent());
            $data = $response->json();
            $this->assertIsArray($data);
            $this->assertArrayHasKeys($data, ['msg' => 'string', 'type' => 'int']);
        }

        // delete by branch and date (dropall is optional)

        $data = [
            'branch'  => 123,
            'date'    => 123123123,
            'dropall' => false,
        ];

        $response = $this->json('POST', '/api/planner/data/remove', $data, [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $this->assertTrue($response->isNotFound() || $response->isOk());
        if ($response->isOk()) {
            $this->assertJson($response->getContent());
            $data = $response->json();
            $this->assertIsArray($data);
            $this->assertArrayHasKeys($data, ['msg' => 'string', 'type' => 'int']);
        }

    }
}
