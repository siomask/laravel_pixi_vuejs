<?php

namespace Tests\Feature\Planner;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class DataRangeTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function testGetData()
    {
        echo "\n", 'DataRangeTest::testGetData()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/planner/daterange', [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);

        $dataResponse->assertStatus(200);

        $this->assertJson($dataResponse->getContent(), 'the date range response data is not json');

        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);

        $data = $jsonData['data'];

        $this->assertArrayHasKeys($data, [
            'id'    => 'int',
            'start' => 'int',
            'end'   => 'int'
        ]);
    }

    public function testStartDateRange() {
        echo "\n", 'DataRangeTest::testStartDateRange()', "\n";

        $authResponse = $this->loginByConfig();

        $siteId = '1388';

        $response = $this->json('POST', "/api/planner/daterange/start/$siteId", [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);
        $response->assertStatus(200);
        $this->assertJson($response->getContent());
        $data = $response->json();
        $this->assertResponse($data);
    }

    public function testEndDateRange() {
        echo "\n", 'DataRangeTest::testEndDateRange()', "\n";

        $authResponse = $this->loginByConfig();

        $siteId = '1388';

        $response = $this->json('POST', "/api/planner/daterange/end/$siteId", [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);
        $response->assertStatus(200);
        $this->assertJson($response->getContent());
        $data = $response->json();
        $this->assertResponse($data);
    }

    public function testLaterThreeMonthsDateRange() {
        echo "\n", 'DataRangeTest::testLaterThreeMonthsDateRange()', "\n";

        $authResponse = $this->loginByConfig();

        $siteId = '1388';

        $response = $this->json('POST', "/api/planner/daterange/later3months/$siteId", [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);
        $response->assertStatus(200);
        $this->assertJson($response->getContent());
        $data = $response->json();
        $this->assertResponse($data);
    }

    public function testFromToDateRange() {
        echo "\n", 'DataRangeTest::testEndDateRange()', "\n";

        $authResponse = $this->loginByConfig();

        $siteId = '1388';
        $start = 123123123;
        $end = 234234234;

        $response = $this->json('POST', "/api/planner/daterange/$start/$end/$siteId", [], [
            'Authorization' => 'Bearer ' . $authResponse['token']
        ]);
        $response->assertStatus(200);
        $this->assertJson($response->getContent());
        $data = $response->json();
        $this->assertResponse($data);
    }

    private function assertResponse($data) {
        $dataItems = $data;

        foreach ($dataItems as $dataKey => $dataItem) {
            $this->assertIsArray($dataItem);
            $this->assertArrayHasKeys($dataItem, [
                'site' => 'int',
                'info' => 'array',
                'branches' => 'array'
            ]);

            $info = $dataItem['info'];
            $this->assertArrayHasKeys($info, ['title' => 'string']);

            $branches = $dataItem['branches'];
            foreach ($branches as $branch) {
                $this->assertIsArray($branch);
                $this->assertArrayHasKeys($branch, [
                    'branch'  => 'int',
                    'info'    => 'array',
                    'contact' => 'array',
                    'data'    => 'array'
                ]);

                $info = $branch['info'];
                $this->assertIsArray($info);
                $this->assertArrayHasKeys($info, ['abbreviation' => 'string', 'full' => 'string']);

                $contact = $branch['contact'];
                $this->assertIsArray($contact);
                $this->assertArrayHasKeys($contact, ['contractor' => 'int', 'name' => 'string']);
            }
        }
    }




    public function test_get_date_range(){
        echo "\n", 'DataRangeTest::test_get_date_range()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/planner/daterange', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        $dataResponse->assertStatus(200);
        var_dump($dataResponse->content());
    }

    public function test_store_date_range(){
        echo "\n", 'DataRangeTest::test_get_date_range()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/daterange', [
            'start'=>'2018-05-19',
            'end'=>'2018-06-10'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
    public function test_update_date_range(){
        echo "\n", 'DataRangeTest::test_get_date_range()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/planner/daterange/1', [
            'start'=>'2018-05-19',
            'end'=>'2018-06-11'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }

    public function test_start_date_range(){
        echo "\n", 'DataRangeTest::test_get_date_range()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/planner/daterange', [
            'action'=>'EARLIEST'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);
        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }

}
