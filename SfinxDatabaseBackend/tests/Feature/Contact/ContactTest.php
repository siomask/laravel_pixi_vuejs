<?php

namespace Tests\Feature\Contact;

use Tests\TestCase;
use Tests\Utils\TestHelperTrait;
use Tests\Utils\AuthHelperTrait;

class ContactTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_fetch_contacts()
    {
        echo "\n", "ContactTest::test_fetch_contacts()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact?type=2,1&name=gh', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

//        $this->assertJson($response->getContent(), 'the contractor response data is not json');
//
//        $contractorData = $response->json();

    }
    public function test_fetch_all_contacts_types()
    {
        echo "\n", "ContactTest::test_fetch_all_contacts_types()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contact/types', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

//        $this->assertJson($response->getContent(), 'the contractor response data is not json');
//
//        $contractorData = $response->json();

    }

    public function test_store_contact()
    {
        echo "\n", "ContactTest::test_store_contact()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/contact', [
            'type_id'=>1,
            'first_name'=>"Test",
            'last_name'=>"Test 3",
            'street'=>"Test street",
            'postcode'=>"Test postcode",
            'number_house'=>"Test house",
            'custom_field_ignored'=>"Test house"
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);

    }
    public function test_update_contact()
    {
        echo "\n", "ContactTest::test_update_contact()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/contact/20', [
            'type_id'=>2,
            'first_name'=>"sa",
            'last_name'=>"Test 3s",
            'street'=>"Test street",
            'postcode'=>"Test postcode",
            'number_house'=>"Test house",
            'custom_field_ignored'=>"Test house"
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

    }
    public function test_remove_contact()
    {
        echo "\n", "ContactTest::test_remove_contact()", "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/contact/19', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);

    }
}
