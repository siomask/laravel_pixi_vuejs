<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class RightsTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_fetch_user_group_rights()
    {
        echo "\n", 'RightsTest::test_fetch_user_group_rights()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/user-group/1/right', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_store_user_group_right()
    {
        echo "\n", 'RightsTest::test_store_user_group_right()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/user-group/1/right', [
            'id'=>'5'//right id
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }
    public function test_update_user_group()
    {
        echo "\n", 'RightsTest::test_update_user_group()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/user-group/1/right/3', [
            'rights_id'=>'4'//new right id
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }
    public function test_delete_user_group()
    {
        echo "\n", 'GroupsTest::test_delete_user_group()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/user-group/1/right/5', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }

}
