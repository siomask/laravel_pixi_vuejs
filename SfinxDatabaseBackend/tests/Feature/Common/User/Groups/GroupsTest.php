<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class GroupsTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_fetch_user_group()
    {
        echo "\n", 'UserTest::test_fetch_users()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/user-group', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        $response->assertStatus(200);
        $data = $response->json();
        var_dump($response->content());

        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_store_user_group()
    {
        echo "\n", 'GroupsTest::test_store_user_group()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/user-group', [
            'name'=>'Test',
            'description'=>'Test',
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }
    public function test_update_user_group()
    {
        echo "\n", 'GroupsTest::test_update_user_group()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/user-group/16', [
            'name'=>'Test',
            'description'=>'Test',
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }
    public function test_delete_user_group()
    {
        echo "\n", 'GroupsTest::test_delete_user_group()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/user-group/16', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }

}
