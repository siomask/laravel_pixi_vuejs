<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class ContractorTypeTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_store_contractor_type()
    {
        echo "\n", 'ContractorTypeTest::test_store_contractor_type()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/contractor-type', [
            'abbreviation'=>'Test',
            'name'=>'Test',
//            'sequence'=>1//by default count of items
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }

    public function test_fetch_contractor_type()
    {
        echo "\n", 'ContractorTypeTest::test_fetch_contractor_type()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/contractor-type', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_contractor_type()
    {
        echo "\n", 'ContractorTypeTest::test_update_contractor_type()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/contractor-type/41', [
            'abbreviation'=>'Testuuui',
            'name'=>'Test',
            'sequence'=>22
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_contractor_type()
    {
        echo "\n", 'ContractorTypeTest::update_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/contractor-type/41', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
