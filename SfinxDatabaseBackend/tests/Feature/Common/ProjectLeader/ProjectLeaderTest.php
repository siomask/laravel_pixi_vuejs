<?php

namespace Tests\Feature\Common\ProjectLeader;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class ProjectLeaderTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;


    public function test_delete_proj_leader()
    {
        echo "\n", 'ProjectLeaderTest::test_delete_proj_leader()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','api/v1/user/3/project-leader/-1', [
            'sites'=>[111,1112]
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }

    public function test_ex()
    {
        echo "\n", 'ProjectLeaderTest::test_delete_proj_leader()', "\n";




        var_dump(explode(",","2,45"));

    }

}
