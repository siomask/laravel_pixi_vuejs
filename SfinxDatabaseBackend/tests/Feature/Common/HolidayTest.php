<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class HolidayTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_fetch_all()
    {
        echo "\n", 'HolidayTest::test_fetch_all()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/holiday', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);
    }

    public function test_create_holiday()
    {
        echo "\n", 'HolidayTest::test_create_holiday()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/holiday', [
            'name'=>'12',
            'start'=>'2018-05-19',
            'end'=>'2018-05-20'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);
    }
    public function test_update_holiday()
    {
        echo "\n", 'HolidayTest::test_update_holiday()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/holiday/3', [
            'name'=>'1sdf2',
            'start'=>'2018-05-19',
            'end'=>'2018-05-23'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);
    }
    public function test_delete_holiday()
    {
        echo "\n", 'HolidayTest::test_delete_holiday()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/holiday/3', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);
    }
}
