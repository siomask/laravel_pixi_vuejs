<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class UserTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_project_leader()
    {
        echo "\n", 'UserTest::test_store_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/user/project-leader', [
            'id'=>3
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }
    public function test_store_user()
    {
        echo "\n", 'UserTest::test_store_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/user', [
            'first_name'=>'Test',
            'name'=>'Test',
            'username'=>'Test',
            'password'=>'Test',
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }
    public function test_store_user_without_required_data()
    {
        echo "\n", 'UserTest::test_store_user_without_required_data()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/user', [
            'first_name'=>'Test',
//            'username'=>'admin',
//            'password'=>'admin',
            'name'=>'Test'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(400);


    }

    public function test_fetch_users()
    {
        echo "\n", 'UserTest::test_fetch_users()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/user', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_user()
    {
        echo "\n", 'UserTest::update_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/user/2', [
            'first_name'=>'TestUpdated',
            'username'=>'admin1',
//            'password'=>'test1',
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_user()
    {
        echo "\n", 'UserTest::update_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/user/2', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
