<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;

class AccountTest extends TestCase
{
    private $host;
    private $user;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->host = 'sfinxdatabase.local';
        $this->user = new \StdClass();
        $this->user->naam = 'admin';
        $this->user->paswoord = 'admin';
    }

    private function login($headers): TestResponse
    {
        $response = $this->json('GET','/api/account',
            [], array_merge([
                'Accept' => 'application/json, text/plain, */*',
                'Content-Type' => 'application/json',
            ], $headers));

        return $response;
    }

    private function checkAuthData($authData) {
        $this->assertInstanceOf('stdClass', $authData);
        $this->assertAttributeContains('200','status', $authData);
        $this->assertAttributeContains('Authorized','message', $authData);
        $this->assertAttributeNotEmpty('token', $authData);
    }

    public function testLogin()
    {
        echo "\n", "AccountTest::testLogin()", "\n";
        $authResponse = $this->login([
            'username' => $this->user->naam,
            'password' => $this->user->paswoord,
            'host'     => $this->host
        ]);
        $authResponse->assertStatus(200);
        $authData = json_decode($authResponse->getContent());
        $this->checkAuthData($authData);
    }

    public function testRefreshToken()
    {
        echo "\n", "AccountTest::testRefreshToken()", "\n";
        $authResponse = $this->login([
            'username' => $this->user->naam,
            'password' => $this->user->paswoord,
        ]);
        $authResponse->assertStatus(200);
        $authData = json_decode($authResponse->getContent());
        $this->checkAuthData($authData);
        $refreshTokenResponse = $this->login(['Authorization' => "Bearer {$authData->token}"]);
        $refreshTokenResponse->assertStatus(200);
        $refreshTokenData = json_decode($authResponse->getContent());
        $this->checkAuthData($refreshTokenData);
    }

    public function testUserData()
    {
        echo "\n", "AccountTest::testUserData()", "\n";
        $authResponse = $this->login([
            'username' => $this->user->naam,
            'password' => $this->user->paswoord,
        ]);
        $authData = json_decode($authResponse->getContent());
        $this->checkAuthData($authData);
        $userResponse = $this->json('GET', '/api/account/user', [], [
            'Authorization' => "Bearer {$authData->token}",
        ]);
        $userResponse->assertStatus(200);
        $userData = $userResponse->original;
        $this->assertCount(2, $userData);
        $this->assertArrayHasKey('rights', $userData);
        $this->assertArrayHasKey('profile', $userData);
        $this->assertCount(2, $userData);
        $this->assertArrayHasKey('username', $userData['profile']);
        $this->assertArrayHasKey('accountType', $userData['profile']);
    }
}
