<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Illuminate\Foundation\Testing\TestResponse;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;
class RightsTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_create_right()
    {
        echo "\n", 'RightsTest::test_create_right()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST','/api/v1/rights', [
            'name'=>'Tes',
            'parent_right_id'=>1,
            'description'=>"bla bla"
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }
    public function test_update_right()
    {
        echo "\n", 'RightsTest::test_update_right()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/rights/16', [
            'name'=>'T12es',
            'parent_right_id'=>1,
            'description'=>"bla bla"
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }
    public function test_delete_right()
    {
        echo "\n", 'RightsTest::test_update_right()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/rights/15', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(200);


    }

    public function test_fetch_rights()
    {
        echo "\n", 'RightsTest::test_fetch_rights()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET','/api/v1/rights', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        $response->assertStatus(200);
        $data = $response->json();

        var_dump($response->content());
        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_user()
    {
        echo "\n", 'UserTest::update_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT','/api/v1/user/2', [
            'first_name'=>'TestUpdated',
            'username'=>'admin1',
//            'password'=>'test1',
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_user()
    {
        echo "\n", 'UserTest::update_user()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE','/api/v1/user/2', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
