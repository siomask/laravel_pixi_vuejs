<?php

namespace Tests\Feature\Common;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class MemberTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_store_member()
    {
        echo "\n", 'MemberTest::test_store_member()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('POST', '/api/v1/member', [
            'first_name' => 'Test',
            'last_name' => 'Test'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($response->content());
        $response->assertStatus(201);


    }


    public function test_fetch_members()
    {
        echo "\n", 'MemberTest::test_fetch_members()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET', '/api/v1/member', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_update_member()
    {
        echo "\n", 'MemberTest::test_update_member()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('PUT', '/api/v1/member/5', [
            'first_name' => 'TestUpdatdsed'
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);
        $data = $response->json();


        $this->assertArrayHasKeys($data, [
            'data' => 'array'
        ]);

    }

    public function test_delete_member()
    {
        echo "\n", 'MemberTest::test_delete_member()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('DELETE', '/api/v1/member/5', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }


    public function test_get_planning()
    {
        echo "\n", 'MemberTest::test_get_planning()', "\n";

        $authResponse = $this->loginByConfig();
        $response = $this->json('GET', '/api/v1/member/1/plannings?month=2018-08', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);


        var_dump($response->content());
        $response->assertStatus(200);

    }
}
