<?php

namespace Tests\Feature\Workflow\Task;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class TaskTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_get_workflow_tasks()
    {
        echo "\n", 'TaskTest::test_get_workflow_tasks()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/workflow/task', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }
    public function test_create_workflow_task()
    {
        echo "\n", 'TaskTest::test_create_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/workflow/task', [
            'name'=>"test122",
            'parent_workflow_task_id'=>3
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(201);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }

    public function test_update_workflow_task()
    {
        echo "\n", 'TaskTest::test_create_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('PUT', '/api/v1/workflow/task/11', [//7 pivot id
//            'description'=>"TEST update",
            'name'=>"r",
            'parent_workflow_task_id'=>1,//null will unmark as subtask
//            'sequence'=>1
        ], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }
    public function test_delete_site()
    {
        echo "\n", 'ConstructionSite::test_delete_site()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/workflow/task/2', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
}
