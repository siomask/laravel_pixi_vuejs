<?php

namespace Tests\Feature\Workflow\Task\FunctionGroup;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class FunctionGroupTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_add_function_group_to_workflow_task()
    {
        echo "\n", 'FunctionGroupTest::test_add_function_group_to_workflow_task()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('POST', '/api/v1/workflow/task/1/function-group', ['id'=>"3"], [//request body id == founctionGroup.id
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'data' => 'array'
        ]);
    }

    public function test_delete_function_group_from_workflow_task()
    {
        echo "\n", 'ConstructionSite::test_delete_site()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('DELETE', '/api/v1/workflow/task/1/function-group/3', [], [//3 == founctionGroup.id
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->content());
        $dataResponse->assertStatus(200);

    }
}
