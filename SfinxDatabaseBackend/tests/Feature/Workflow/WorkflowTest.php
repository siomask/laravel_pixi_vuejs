<?php

namespace Tests\Feature\Workflow\Task;

use Tests\TestCase;
use Tests\Utils\AuthHelperTrait;
use Tests\Utils\TestHelperTrait;

class TaskTest extends TestCase
{
    use TestHelperTrait, AuthHelperTrait;

    public function test_get_workflow()
    {
        echo "\n", 'TaskTest::test_get_workflow_tasks()', "\n";

        $authResponse = $this->loginByConfig();

        $dataResponse = $this->json('GET', '/api/v1/workflow', [], [
            'Authorization' => 'Bearer ' . $authResponse['access_token']
        ]);

        var_dump($dataResponse->json()['construction_sites']);
        $dataResponse->assertStatus(200);


        $jsonData = $dataResponse->json();

        $this->assertArrayHasKeys($jsonData, [
            'tasks' => 'array',
            'function_groups' => 'array'
        ]);
    }

}
