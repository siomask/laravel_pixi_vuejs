docker stop apidev
docker rm apidev
docker build -t sfinx/apidev git@gitlab.com:benvh/SfinxDatabaseBackend.git#development
docker run -d -h apidev.sfinx.be -p 85:80 --restart=always --env-file ./.env --name apidev sfinx/apidev