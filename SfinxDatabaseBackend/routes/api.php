<?php

use Illuminate\Http\Request;


Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace' => 'Common'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});

Route::group(['prefix' => 'contact', 'namespace' => 'Contact'], function () {
    Route::resource('clients', 'ClientsController', ['only' => ['index']]);
    Route::resource('contractors', 'ContractorsController', ['only' => ['index']]);
    Route::resource('suppliers', 'SuppliersController', ['only' => ['index']]);
    Route::resource('architects', 'ArchitectsController', ['only' => ['index']]);
    Route::resource('prevention_officers', 'PreventionOfficersController', ['only' => ['index']]);
    Route::resource('epb_reporters', 'EPBReportersController', ['only' => ['index']]);
    Route::resource('stability_engineers', 'StabilityEngineersController', ['only' => ['index']]);
    Route::resource('construction_officers', 'ConstructionOfficersController', ['only' => ['index']]);
});


Route::group(['middleware' => 'auth', 'prefix' => 'v1', 'namespace' => 'v1'], function () {


    Route::resource('holiday', 'Common\HolidayController');

    Route::resource('user', 'Common\User\UserController');
    Route::resource('construction-site-status', 'Common\ConstructionSiteStatusController');
    Route::post('user/project-leader', 'Common\User\UserController@projectLeader');
    Route::resource('contractor-type', 'Common\ContractorTypeController');
    Route::resource('member', 'Common\MemberController');
    Route::get('member/{id}/plannings', 'Common\MemberController@allPlanning');
    Route::resource('workflow', 'Workflow\WorkflowController', ['only' => ['index']]);

    Route::group([
        'prefix' => 'workflow',
        'namespace' => 'Workflow'
    ], function () {
        Route::resource('task', 'Task\TaskController');
        Route::resource('function-group', 'FunctionGroup\FunctionGroupController');

        Route::group([
            'prefix' => 'task/{taskId}',
            'namespace' => 'Task',
            'middleware' => ['workflowTaskCheck']
        ], function () {
            Route::resource('function-group', 'FunctionGroup\FunctionGroupController');
        });


        Route::resource('construction-site/{siteId}/workflow-task', 'ConstructionSite\Task\TaskController')
            ->middleware('workflowConstructionSiteCheck');


    });

    Route::group([
        'prefix' => '',
        'namespace' => 'Common'
    ], function () {
        Route::resource('construction-site-setting-type', 'ConstructionSiteSettingTypeController');
        Route::resource('user-group', 'User\Groups\GroupsController');
        Route::resource('construction-site', 'User\ConstructionSite\ConstructionSiteController');

        Route::post('user/construction-site/reset', 'User\ConstructionSite\ConstructionSiteController@reset');
        Route::resource('user', 'User\UserController');
        Route::resource('rights', 'Rights\RightsController');
        Route::post('user/settings', 'User\UserController@settings');


        Route::group([
            'prefix' => 'user-group/{userGroupId}/',
            'namespace' => 'User\Groups',
            'middleware' => ['userGroupCheck']
        ], function () {
            Route::resource('right', 'Right\RightController');
        });

        Route::group([
            'prefix' => 'user/{userId}/',
            'namespace' => 'User',
        ], function () {
            Route::resource('project-leader', 'ProjectLeader\ProjectLeaderController');

        });

        Route::group([
            'prefix' => 'user',
            'namespace' => 'User',
        ], function () {
            Route::group([
                'prefix' => 'contact/{contactId}',
                'namespace' => 'Contact',
                'middleware' => ['contactCheck']
            ], function () {
                Route::resource('construction-site', 'ConstructionSite\ConstructionSiteController');
                Route::resource('collaborator', 'Collaborator\CollaboratorController');
            });
        });
    });

    Route::group([
        'prefix' => '',
        'namespace' => 'Contact'
    ], function () {
//        Route::resource('contact', 'ContactController');
        Route::resource('client', 'Client\ClientController');
        Route::group([
            'prefix' => 'client/{clientId}/',
            'namespace' => 'Client',
            'middleware' => ['contactClientCheck']
        ], function () {

            Route::resource('construction-site', 'ConstructionSite\ConstructionSiteController');

            Route::group([
                'prefix' => 'construction-site/{constructionSiteId}/',
                'namespace' => 'ConstructionSite',
                'middleware' => ['contactClientConstructionSiteCheck']
            ], function () {
                Route::resource('projectleader', 'ProjectLead\ProjectLeaderController');
                Route::resource('estimator', 'Estimator\EstimatorController');
                Route::resource('external', 'External\ExternalController');
                Route::resource('collaborator', 'Collaborator\CollaboratorController');

            });
        });
        Route::group([
            'prefix' => 'contact/{contactId}/',
            'namespace' => 'Contact',
            'middleware' => ['contactCheck']
        ], function () {
            Route::resource('collaborator', 'Collaborator\CollaboratorController');
        });
    });
    Route::group([
        'prefix' => 'finance',
        'namespace' => 'Finances'
    ], function () {//invoiceCheck
        Route::resource('type', 'TypesController');
        Route::resource('incoming-invoice', 'IncomingInvoice\IncomingInvoicesController');
        Route::resource('outgoing-invoice', 'OutgoingInvoice\OutgoingInvoicesController');
        Route::resource('summary', 'SummaryController');
        Route::resource('accounts', 'InvoiceAccountsController');
        Route::group([
            'prefix' => 'incoming-invoice/{invoiceId}/',
            'namespace' => 'IncomingInvoice',
            'middleware' => ['invoiceCheck']
        ], function () {
            Route::resource('entry', 'Entry\EntryController');
            Route::resource('detail', 'DetailsController');
        });

        Route::group([
            'prefix' => 'outgoing-invoice/{invoiceId}/',
            'namespace' => 'OutgoingInvoice',
            'middleware' => ['outgoingInvoiceCheck']
        ], function () {
            Route::resource('entry', 'Entry\EntryController');
            Route::resource('payment', 'Payment\PaymentController');
        });
    });


    Route::group([
        'prefix' => 'contact',
        'namespace' => 'Contact'
    ], function () {
        Route::resource('types', 'Type\TypeController');

        Route::group([
            'prefix' => '{contactId}/',
            'middleware' => ['contactCheck']
        ], function () {
            Route::resource('team', 'Team\TeamController');
            Route::resource('collaborator', 'Collaborator\CollaboratorController');
            Route::group([
                'prefix' => 'team/{teamId}/',
                'namespace' => 'Team',
                'middleware' => ['contactTeamCheck']
            ], function () {
                Route::resource('period', 'TeamPeriod\TeamPeriodController');

                Route::group([
                    'prefix' => 'period/{periodId}/',
                    'namespace' => 'TeamPeriod',
                    'middleware' => ['contactTeamPeriodCheck']
                ], function () {
                    Route::resource('member', 'Member\MemberController');
                });
            });
        });
    });
    Route::resource('contact', 'Contact\ContactController');
    Route::group([
        'prefix' => 'planner',
        'namespace' => 'Planner'
    ], function () {
        Route::resource('', 'PlannerController');
        Route::resource('daterange', 'DateRangeController');
        Route::get('/details', 'ConstructionSite\Collaborator\Planning\Details\DetailsController@activeTeams');

        Route::resource('construction-site', 'ConstructionSite\ConstructionSiteController');
        Route::resource('collaborator', 'Collaborator\CollaboratorController');
        Route::group([
            'prefix' => 'construction-site/{constructionSiteId}/',
            'namespace' => 'ConstructionSite',
            'middleware' => ['plannerConstructionSiteCheck']
        ], function () {
            Route::resource('projectleader', 'ProjectLead\ProjectLeaderController');
            Route::resource('estimator', 'Estimator\EstimatorController');
            Route::resource('external', 'External\ExternalController');
            Route::resource('collaborator', 'Collaborator\CollaboratorController');
            Route::post('move', 'ConstructionSiteController@move');
//            Route::resource('workflow-task', 'Workflow\Task\TaskController');

            Route::group([
                'prefix' => 'collaborator/{collaboratorId}/',
                'namespace' => 'Collaborator',
                'middleware' => ['plannerConstructionSiteCollaboratorCheck']
            ], function () {
                Route::resource('planning', 'Planning\PlanningController');
                Route::post('move', 'CollaboratorController@move');

                Route::group([
                    'prefix' => 'planning/{planningId}/',
                    'namespace' => 'Planning',
                    'middleware' => ['planningDetailsCheck']
                ], function () {
                    Route::resource('details', 'Details\DetailsController');
                    Route::post('details/message', 'Details\DetailsController@message');
                    Route::post('details/pdf', 'Details\DetailsController@pdf');
                });
            });

        });

    });





    Route::resource('self', 'Common\User\Self\UserSelfController');
});

