<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \Barryvdh\Cors\HandleCors::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:300,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'clientCheck' => \App\Http\Middleware\ClientLoad::class,
        'workflowConstructionSiteCheck' => \App\Http\Middleware\Workflow\ConstructionSiteCheck\ConstructionSiteCheck::class,
        'plannerConstructionSiteCheck' => \App\Http\Middleware\Planner\ConstructionSiteCheck\ConstructionSiteCheck::class,
        'plannerConstructionSiteCollaboratorCheck' => \App\Http\Middleware\Planner\ConstructionSiteCheck\CollaboratorCheck\CollaboratorCheck::class,
        'planningDetailsCheck' => \App\Http\Middleware\Planner\ConstructionSiteCheck\CollaboratorCheck\PlanningCheck\PlanningDetailsCheck::class,
        'contactCheck' => \App\Http\Middleware\Contact\ContactCheck::class,
        'contactTeamCheck' => \App\Http\Middleware\Contact\Team\TeamCheck::class,
        'contactTeamPeriodCheck' => \App\Http\Middleware\Contact\Team\TeamPeriod\TeamPeriodCheck::class,
        'contactClientCheck' => \App\Http\Middleware\Contact\Client\ClientCheck::class,
        'contactClientConstructionSiteCheck' => \App\Http\Middleware\Contact\Client\ConstructionSite\ConstructionSiteCheck::class,
        'userGroupCheck' => \App\Http\Middleware\Common\User\Group\GroupCheck::class,
        'invoiceCheck' => \App\Http\Middleware\Finances\IncomingInvoice\IncomingInvoiceCheck::class,
        'outgoingInvoiceCheck' => \App\Http\Middleware\Finances\OutgoingInvoice\OutgoingInvoiceCheck::class,
        'workflowTaskCheck' => \App\Http\Middleware\Workflow\Task\TaskCheck::class,
        'AddUserRights' => \App\Http\Middleware\AddUserRights::class,
    ];
}
