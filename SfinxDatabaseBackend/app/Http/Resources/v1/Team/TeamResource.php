<?php

namespace App\Http\Resources\v1\Team;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TeamResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "abbreviation"=>empty($this->abbreviation)?'':$this->abbreviation,
            "name"=>empty($this->name)?'':$this->name,
            "periods"=>TeamPeriodResource::collection($this->period),
            'teamMembers' => isset($this->teamMembers)?TeamMemberPlannedResource::collection($this->teamMembers):[],
        ];
    }
}
