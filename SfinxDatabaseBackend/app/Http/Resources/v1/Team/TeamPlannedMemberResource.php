<?php

namespace App\Http\Resources\v1\Team;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TeamPlannedMemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "collaborator_planning_entry_id"=>isset($this->pivotPlanning) ?($this->pivotPlanning->collaborator_planning_entry_id):null ,
            "team_period_team_member_id"=>$this->team_period_team_member_id,
            "date"=>$this->date,
            "sequence"=>$this->sequence
        ];
    }
}
