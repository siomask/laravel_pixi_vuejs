<?php

namespace App\Http\Resources\v1\Team;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TeamPeriodResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "start"=>$this->start,
            "members"=>TeamPeriodMemberResource::collection($this->members),
            "end"=>$this->end
        ];
    }
}
