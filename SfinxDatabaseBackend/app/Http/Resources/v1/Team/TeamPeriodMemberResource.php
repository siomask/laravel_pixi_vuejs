<?php

namespace App\Http\Resources\v1\Team;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TeamPeriodMemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $res = [
            "id" => $this->id,
            "first_name" => $this->first_name,
            "last_name" => $this->last_name,
            "name" => $this->first_name . " " . $this->last_name,
        ];
        if (isset($this->member)) {
            $res["id"] = $this->member->id;
            $res["first_name"] = $this->member->first_name;
            $res["last_name"] = $this->member->last_name;
        }

        if($this->pivot){
            $res['member_price'] = $this->pivot->member_price;
        }
        return $res;
    }
}
