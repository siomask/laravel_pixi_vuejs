<?php

namespace App\Http\Resources\v1\Common;

use Illuminate\Http\Resources\Json\Resource;

class RightsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'name' => $this->name,
            'description' => $this->description,
            'sequence' => $this->sequence,
            'parent' => isset($this->parentRight)?new RightsResource($this->parentRight):null,
            'id' => $this->id
        ];
    }



}
