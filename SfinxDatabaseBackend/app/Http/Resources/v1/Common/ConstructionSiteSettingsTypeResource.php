<?php

namespace App\Http\Resources\v1\Common;

use Illuminate\Http\Resources\Json\Resource;

class ConstructionSiteSettingsTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'name' => $this->name,
            'id' => $this->id
        ];
    }



}
