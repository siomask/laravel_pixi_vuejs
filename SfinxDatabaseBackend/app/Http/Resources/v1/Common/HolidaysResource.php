<?php

namespace App\Http\Resources\v1\Common;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Team\TeamPeriodMemberResource;

class HolidaysResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'member' => isset($this->member)?new TeamPeriodMemberResource($this->member):null,
            'start' => $this->start,
            'name' => $this->name,
            'end' => $this->end,
            'id' => $this->id,
        ];
    }



}
