<?php

namespace App\Http\Resources\v1\Workflow;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Planner\ConstructionSiteStatusResource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSiteWorkflowTaskResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
//            'start' => $this->start,
//            'end' => $this->end,
//            'task_status' => $this->task_status,
            'contact' => new ContactResource($this->contact),
            'clientId' => $this->contact->id,
            'name' => $this->contact->first_name." ".$this->last_name,
            'status' => new ConstructionSiteStatusResource(  $this->status),
            'sequence' => $this->sequence,
            'tasks' =>     \App\Http\Resources\v1\Workflow\Task\ConstructionSiteTaskResource::collection($this->workflowTasks)
        ];
    }
}
