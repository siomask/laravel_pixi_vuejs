<?php

namespace App\Http\Resources\v1\Workflow\Task;

use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TaskResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
//            'workflowFunctionGroups' => isset($this->workflowFunctionGroups)?FunctionGroupResource::collection($this->workflowFunctionGroups):[],
            'parent_id' => $this->parent_workflow_task_id,
            'parent_workflow_task_id' => $this->parent_workflow_task_id,
            'pivot' => $this->pivot,
            'sequence' => $this->sequence
        ];
    }

    static function parseItemsWithParent($items)
    {
        $parents = array();
        foreach ($items as $task) {
            $n_task = (object)[
                'id' => $task->id,
                'name' => $task->name,
                'description' => $task->description,
                'parent_id' => $task->parent_workflow_task_id,
                'parent_workflow_task_id' => $task->parent_workflow_task_id,
                'children' => [],
                'pivot' => $task->pivot,

                'sequence' => $task->sequence
            ];
            $parents[$task->id] = $n_task;
            if (isset($task->parent_workflow_task_id)) {
                $parents[$task->parent_workflow_task_id]->children[] = $n_task;
            }

        }
        return  self::sortBySequence(array_filter($parents, function ($e) {
            $e->is_parent = empty($e->parent_workflow_task_id) || count($e->children) > 0;
            return ($e->parent_id == null);
        }));
    }

    static function sortBySequence($list)
    {
        usort($list, function ($a, $b) {
            return $a->sequence > $b->sequence ? 1 : -1;
        });
        foreach ($list as $item){
            $item->children =self::sortBySequence($item->children);
        }
        return $list;
    }

}
