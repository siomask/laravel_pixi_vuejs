<?php

namespace App\Http\Resources\v1\Workflow\Task;

use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSiteTaskResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->pivot ? [
            'id' => $this->pivot->id,
            'description' => $this->pivot->description,
            'task_id' => $this->id,
            'site_id' => $this->pivot->construction_site_id,
            'start' => $this->pivot->start,
            'end' => $this->pivot->end,
            'task_status' => $this->pivot->task_status
        ] : [
            'id' => $this->id,
            'description' => $this->description,
            'task_id' => $this->workflow_task_id,
            'site_id' => $this->construction_site_id,
            'start' => $this->start,
            'end' => $this->end,
            'task_status' => $this->task_status
        ];
    }


}
