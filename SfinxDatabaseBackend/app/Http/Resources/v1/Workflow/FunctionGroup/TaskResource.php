<?php

namespace App\Http\Resources\v1\Workflow\FunctionGroup;

use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class TaskResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'function_group_id' => $this->pivot->workflow_function_group_id,
            'task_id' => $this->pivot->workflow_task_id
        ];
    }

}
