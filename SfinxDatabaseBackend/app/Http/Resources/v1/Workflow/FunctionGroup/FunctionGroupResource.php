<?php

namespace App\Http\Resources\v1\Workflow\FunctionGroup;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class FunctionGroupResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'pivot' => $this->pivot,
            'tasks' => \App\Http\Resources\v1\Workflow\FunctionGroup\TaskResource::collection($this->workflowTasks),
            'sequence' => $this->sequence
        ];
    }


}
