<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Models\Team\Team;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ContactConstructionSitesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return ConstructionSitesResource::wrap($this);
    }
}
