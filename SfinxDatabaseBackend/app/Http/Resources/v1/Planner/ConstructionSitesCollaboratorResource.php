<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Planner\ConstructionSitesCollaboratorPlanningResource;
use App\Models\Team\Team;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSitesCollaboratorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        $res = [
//            'contact' => [
//                'name' => isset($this->contact) ? $this->contact->first_name . " " . $this->contact->last_name : 'No collaborator',
//                'type' => isset($this->contact) ? $this->contact->type_id : -1,
//                'id' => isset($this->contact) ? $this->contact->id : -1
//            ],
            'contact' =>  isset($this->contact)?new ContactResource($this->contact):['name'=>'No collaborator',],
            'teams' =>  isset($this->contact) && isset($this->contact->teams) ? Team::parseActiveTeamMembers($this->contact->teams) : [],
//            'teammm' =>isset($this->contact) && isset($this->contact->teams) ?$this->contact->teams:[],
            'branch' => $this->id,
            'name' => isset($this->contact) ? $this->contact->first_name . " " . $this->contact->last_name : 'No collaborator',
            'abbreviation' => isset($this->info) ? $this->info->abbreviation : '',
            'id' => $this->id,
            'is_contractor_type_assigned' => $this->is_contractor_type_assigned,
            'is_contractor_type_ordered' => $this->is_contractor_type_ordered,
            'is_assigned' => $this->is_assigned,
            'is_ordered' => $this->is_ordered,
            'info' => new ConstructionTypeResource($this->info),
            'data' => ConstructionSitesCollaboratorPlanningResource::collection($this->data),
            'site' => $this->construction_site_id,
            'comment' => $this->comment,
            'request_date' => $this->request_date,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
        if ($this->is_contractor_type_assigned == 1
            || $this->is_contractor_type_ordered == 1
            || $this->is_ordered == 1 ||
            $this->is_assigned == 1) {
            $res['start_date'] = $this->start;
            $res['end_date'] = $this->end;
        }
        return $res;
    }
}
