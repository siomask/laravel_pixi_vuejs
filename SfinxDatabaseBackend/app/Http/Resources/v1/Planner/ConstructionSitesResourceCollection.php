<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\Team\Team;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Client\ProjLeaderResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSitesResourceCollection extends Resource
{
    private $items;

    public function __construct($resource)
    {
        $this->pagination = [
            'next_page_url' => $resource->nextPageUrl(),
        ];

        $this->items = $resource = $resource->getCollection();

        parent::__construct($resource);
    }


    public function toArray($request)
    {
        $self = $this;
        return [
            'data' => ($this->items->transform(function ($collection) use ($self) {
                return ConstructionSitesResource::wrap($collection);
            })),
            'next_page_url' => $this->pagination['next_page_url']
        ];
    }

}
