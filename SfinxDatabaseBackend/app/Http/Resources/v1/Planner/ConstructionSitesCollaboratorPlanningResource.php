<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Planner\ConstructionSite\Collaborator\Planning\Details\MessagesResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSitesCollaboratorPlanningResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'start' =>$this->start,
            'end' =>$this->end,
            'description' =>$this->description,
            'description_to_do' =>$this->description_to_do,
            'messages' =>MessagesResource::collection($this->messages),
//            'planningMembers' =>$this->planningMembers,
        ];
    }
}
