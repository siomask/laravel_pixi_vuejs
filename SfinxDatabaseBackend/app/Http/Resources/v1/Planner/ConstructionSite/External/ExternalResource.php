<?php

namespace App\Http\Resources\v1\Planner\ConstructionSite\External;

use Illuminate\Http\Resources\Json\Resource;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Models\Team\Team;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ExternalResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'ordered_by'=>$this->ordered_by,
            'comment'=>$this->comment,
            'contact'=>isset($this->contact)?new ContactResource($this->contact):null,
            'contactName'=>isset($this->contact)?$this->contact->first_name." ".$this->contact->last_name:null,
            'visible'=>$this->visible
        ];
    }
}
