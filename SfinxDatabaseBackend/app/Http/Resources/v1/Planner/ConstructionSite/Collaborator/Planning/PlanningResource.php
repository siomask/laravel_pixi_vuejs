<?php

namespace App\Http\Resources\v1\Planner\ConstructionSite\Collaborator\Planning;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Planner\ConstructionSite\Collaborator\Planning\Details\MessagesResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class PlanningResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'start' =>$this->start,
            'end' =>$this->end,
            'description_to_do' =>$this->description_to_do,
            'description' =>$this->description,
            'messages' =>MessagesResource::collection($this->messages)
        ];
    }
}
