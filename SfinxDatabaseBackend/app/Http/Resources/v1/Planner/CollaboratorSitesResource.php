<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Models\Team\Team;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class CollaboratorSitesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $res = self::wrap($this);
        $res['branches']= isset($this->branches)?ConstructionSitesCollaboratorResource::collection($this->branches):[];
        return $res;
    }

    static function wrap($item){
        return    [
            'info' =>[
                'title'=>$item->_contact->first_name
            ],
            'contact'=>new ContactResource($item->_contact),
            'id'=>$item->id,
            'type'=>$item->_contact->type_id,

            'site' => $item->id,
        ];
    }
}
