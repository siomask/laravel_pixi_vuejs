<?php

namespace App\Http\Resources\v1\Planner;
use App\Http\Resources\v1\User\UserResource;
use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ProjectLeaderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user)
        ];
    }
}
