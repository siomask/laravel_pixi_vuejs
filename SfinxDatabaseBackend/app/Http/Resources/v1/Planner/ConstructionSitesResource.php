<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\Team\Team;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Client\ProjLeaderResource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSitesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return self::wrap($this);
    }

    static function wrap($item){
        $contact =  isset($item->contact)?new ContactResource($item->contact):(object)[];
        return [
            'info' =>$contact,
            'name' =>  ContactResource::wrap($item->contact)['name'],
            'sequence' =>  ($item->sequence),
            'status' => new ConstructionSiteStatusResource($item->status),
            'externals'=> ($item->externals),
            'contact'=>$contact,
            'projectLeaders'=> ProjLeaderResource::collection($item->projectLeads),
            'projectLeader'=>self::reduceItems($item->projectLeads),
            'estimator'=>self::reduceItems($item->estimators) ,
            'estimators'=> ProjLeaderResource::collection($item->estimators),
            'id'=>$item->id,
            'site' => $item->id,
            'pivot'=>$item->pivot,
            'presented_price'=>$item->presented_price,
            'type'=>isset($item->contact)?$item->contact->type_id:null,
//            'teams'=>isset($this->contact->teams)?Team::parseActiveTeamMembers($this->contact->teams):[],
            'branches' => isset($item->collaborators)?ConstructionSitesCollaboratorResource::collection($item->collaborators):[]
        ];
    }
    static function reduceItems($items){
        $res='';
        foreach ($items as $item){
            $res.=(strlen($res)>0?", ":'')."(".$item->percentage."%)".$item->user->name." ".$item->user->first_name;
        }
        return $res;
    }

}
