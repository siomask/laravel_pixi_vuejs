<?php

namespace App\Http\Resources\v1\Planner;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactTypeResource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionSiteStatusResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> ($this->name),
            'contactType'=> new ContactTypeResource($this->contactType),
            'id'=>$this->id,
        ];
    }

}
