<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\Resource;

class MainProjectLeaderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $result = [];
        foreach ($this as $beheer) {
            foreach ($beheer as $v => $_id) {
                if (isset($_id->projectlLeaders)) {
                    foreach ($_id->projectlLeaders as $projectl_leaders_id) {
                        if(isset($projectl_leaders_id->leader))$result[] = [
                            'price' => $_id->bedragvoorgelegdaanklant,
                            'procent' => $projectl_leaders_id->procent,
                            'projectleiderid' => $projectl_leaders_id->leader->projectleiderid,
                            'voornaam' => $projectl_leaders_id->leader->voornaam,
                            'naam' => $projectl_leaders_id->leader->naam
                        ];
                    }
                }
            }
        }
        return $result;

    }
}
