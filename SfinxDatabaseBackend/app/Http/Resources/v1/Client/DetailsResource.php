<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\Resource;

class DetailsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        $_data = $this->parseWerfBeheer($this->beheer);
        return [
            'client_name' => $this->projectnaam,
//            'full_connection_for_leaders' => $this->beheer,
            'project_leaders' => $_data['leaders'],
            'externe_partijen' => $_data['externe_partijen'],
            'offeres' => $_data['offeres'],
            'orders' => [],
            'price' => isset($this->beheer)?$this->beheer[0]->bedragvoorgelegdaanklant:-1,
            'type' => new TypesResource($this->typeClient)
        ];
    }

    private function parseWerfBeheer($list)
    {
        $result = [
            'leaders' => [],
            'externe_partijen' => [],
            'offeres' => [],
        ];
        if (!isset($list)) return $result;

        foreach ($list as $beheer) {

            //leaders
            if (isset($beheer->projectlLeaders)) {
                foreach ($beheer->projectlLeaders as $projectl_leaders_id) {
                    if (isset($projectl_leaders_id->leader)) $result['leaders'][] = [
                        'price' => $beheer->bedragvoorgelegdaanklant,
                        'procent' => $projectl_leaders_id->procent,
                        'projectleiderid' => $projectl_leaders_id->leader->projectleiderid,
                        'voornaam' => $projectl_leaders_id->leader->voornaam,
                        'naam' => $projectl_leaders_id->leader->naam
                    ];
                }
            }

            if (isset($beheer->werfDetails)) $result['externe_partijen'][] = $beheer->werfDetails;


            if (isset($beheer->werfDetailat)) {
                foreach ($beheer->werfDetailat as $werfDetailats) {
                    $result['offeres'][] = $werfDetailats->werfDetailandere;
                }
            }
        }
        return $result;

    }


}
