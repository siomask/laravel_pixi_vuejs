<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\User\UserResource;
class ProjLeaderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'percentage' =>  ($this->percentage)
        ];
    }
}
