<?php

namespace App\Http\Resources\v1\Client;

use Illuminate\Http\Resources\Json\Resource;

class TypesResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->typeid,
            'name' => $this->naam 
        ];
    }
}
