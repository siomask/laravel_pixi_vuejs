<?php

namespace App\Http\Resources\v1\Finances;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;

class TypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
