<?php

namespace App\Http\Resources\v1\Finances;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;

class InvoiceAccountsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
            'amount' => $this->amount,
            'sequence' => $this->sequence,
            'parent_invoice_account_id' => $this->parent_invoice_account_id,
            'parent_id' => $this->parent_invoice_account_id
        ];
    }

    static function parseItemsWithParent($items)
    {
        $parents = array();
        foreach ($items as $task) {
            $n_task = (object)[
                'id' => $task->id,
                'amount' => $task->amount,
                'description' => $task->description,
                'parent_id' => $task->parent_invoice_account_id,
                'parent_invoice_account_id' => $task->parent_invoice_account_id,
                'children' => [],
                'pivot' => $task->pivot,

                'sequence' => $task->sequence
            ];
            $parents[$task->id] = $n_task;
            if (isset($task->parent_invoice_account_id)) {
                $parents[$task->parent_invoice_account_id]->children[] = $n_task;
            }

        }
        return  self::sortBySequence(array_filter($parents, function ($e) {
            $e->is_parent = empty($e->parent_invoice_account_id) || count($e->children) > 0;
            return ($e->parent_invoice_account_id == null);
        }));
    }
    static function sortBySequence($list)
    {
        usort($list, function ($a, $b) {
            return $a->sequence > $b->sequence ? 1 : -1;
        });
        foreach ($list as $item){
            $item->children =self::sortBySequence($item->children);
        }
        return $list;
    }
}
