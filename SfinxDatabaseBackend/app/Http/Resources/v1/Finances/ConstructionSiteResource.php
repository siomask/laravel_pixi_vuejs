<?php

namespace App\Http\Resources\v1\Finances;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\Finances\OutgoingInvoice\OutgoingInvoice;
use App\Models\Finances\IncomingInvoice\IncomingInvoice;
use App\Http\Resources\v1\Contact\ContactResource;

class ConstructionSiteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
       return self::wrap($this);
    }

    static function wrap($item){
        $_contact = isset($item->contact) ? new ContactResource($item->contact) : (object)[];
        return [
            'sequence' => ($item->sequence),
            'bk_wn' => (self::bk_wn($item)) ,
            'total_amount_incoming_invoices' => (self::totalAmountIncomingInvoices($item)),
            'date_last_incoming_invoice' => (self::lastDateIncomingInvoices($item)),
            'date_last_outgoing_invoice' => (self::lastDateOutgoingInvoices($item)),
            'total_amount_outgoing_invoices' => (self::totalAmountOutGoingInvoices($item)),
            'date_financial_details' => ($item->date_financial_details),
            'total_amount_orders' => ($item->total_amount_orders),
            'total_amount_processed' => ($item->total_amount_processed),
            'contact' =>$_contact,
            'name' =>  ContactResource::wrap($item->contact)['name'],
            'id' => $item->id,
            'pivot' => $item->pivot,
            'presented_price' => $item->presented_price,
            'type' => ContactResource::wrap($item->contact)['id'],
        ];
    }
    static function totalAmountOutGoingInvoices($item)
    {
        return OutgoingInvoice::totalInvoices($item->contact);
    }

    static function lastDateOutgoingInvoices($item)
    {
        $invoice = OutgoingInvoice::lastByDate($item->contact);
        if (!empty($invoice)) return $invoice->date;
        return null;
    }

    static function totalAmountIncomingInvoices($item)
    {
        return IncomingInvoice::totalInvoices($item->contact);
    }

    static function lastDateIncomingInvoices($item)
    {
        $invoice = IncomingInvoice::lastByDate($item->contact);
        if (!empty($invoice)) return $invoice->date;
        return null;
    }

    static function bk_wn($item)
    {
        $total = 0;
        $siteId = $item->id;
        $resp = [];

        if(!empty($item->collaborators)){
            foreach ($item->collaborators as $collaborator) {
                if (isset($collaborator->contact) && $collaborator->data->count() > 0) {

                    foreach ($collaborator->data as $dataPlanning) {
                        if ($collaborator->id == $dataPlanning->construction_site_collaborator_id) {
                            foreach ($collaborator->contact->teams as $team) {

                                foreach ($team->period as $period) {
                                    foreach ($period->periodMembers as $periodMember) {
                                        foreach ($periodMember->plannings as $planning) {

                                            if(is_array($planning))$planning=(object)$planning;
                                            if (isset($planning->pivotPlannings) && $planning->pivotPlannings->collaborator_planning_entry_id == $dataPlanning->id) {
                                                if (empty($resp[$siteId])){
                                                    $resp[$siteId] = (object)[
                                                        'id' => $siteId,
                                                        'price_per_hour' => $periodMember->member_price,
                                                        'plannings' => []
                                                    ];
                                                }
                                                $planMember = clone $planning;
                                                $resp[$siteId]->plannings[$planning->id] = $planMember;//can catch duplicate
                                            }

                                        }
                                    }
                                }
                            }

                        }
                    }
                }
            }
        }



        foreach ($resp as $site){
            $site->countOfPlanning = 0;
            foreach ($site->plannings as $plannings){
                $site->countOfPlanning++;
            }

        }
        foreach ($resp as $site){
            foreach ($site->plannings as $plannings){
                $working_hour = floatval(empty($plannings->hours)?8/$site->countOfPlanning:$plannings->hours);//emty will include 0
                $total += $working_hour*$site->price_per_hour;
            }
        }
        return $total;
    }
}
