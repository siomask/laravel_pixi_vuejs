<?php

namespace App\Http\Resources\v1\Finances\IncomingInvoice;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\Entry\EntryResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class IncomingInvoiceResourceCollection extends ResourceCollection
{

    private $items;

    public function __construct($resource)
    {
        $this->pagination = [
//            'total' => $resource->total(),
//            'count' => $resource->count(),
//            'per_page' => $resource->perPage(),
            'next_page_url' => $resource->nextPageUrl(),
//            'current_page' => $resource->currentPage(),
//            'total_pages' => $resource->lastPage()
        ];

        $this->items = $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'data' => ($this->items->transform(function ($collection) {
                return IncomingInvoiceResource::wrap($collection);
            })),
            'next_page_url' => $this->pagination['next_page_url']
        ];
    }

}
