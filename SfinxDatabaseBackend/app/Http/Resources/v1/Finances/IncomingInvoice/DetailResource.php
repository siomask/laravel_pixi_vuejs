<?php

namespace App\Http\Resources\v1\Finances\IncomingInvoice;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\Entry\EntryResource;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
class DetailResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'amount' => $this->amount,
            'incoming_invoices_id' => $this->incoming_invoices_id,
            'construction_site_id' => $this->construction_site_id,
            'contractor_type_id' => $this->contractor_type_id
        ];
    }

}
