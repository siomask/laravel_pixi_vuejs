<?php

namespace App\Http\Resources\v1\Finances\IncomingInvoice;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\Entry\EntryResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class IncomingInvoiceResource extends Resource
{
    public function toArray($request)
    {
        return self::wrap($this);
    }


    static function wrap($collection){
        $contact = isset($collection->contact)?new ContactResource($collection->contact):(object)[];
        return [
            'id' => $collection->id,
            'serial_number' => $collection->serial_number,
            'payment_term' => $collection->payment_term,
            'start' => $collection->date,
            'end' => \Carbon\Carbon::parse($collection->date)->addSeconds($collection->payment_term)->toDateTimeString(),
            'sender' => $contact,
            'entries' => EntryResource::collection($collection->entries),
            'invoice_number' => $collection->invoice_number,
            'total_entry_amount' => self::totalEntryAmount($collection->entries),
            'total_entry_amount_vat' => self::totalEntryAmount($collection->entries, true),
            'btw' => self::btw($collection->entries),
            'is_paid' => $collection->is_paid,
            'financial_discount' => $collection->financial_discount,
            'discount' => $collection->discount,
            'comment' => $collection->comment
        ];
    }

    static function totalEntryAmount($items, $withVat = false)
    {
        $res = 0;
        foreach ($items as $item) {
            $res += $item->amount;
            if ($withVat) $res += $item->vat_percentage;
        }
        return round($res, 2);
    }

    static function btw($items)
    {
        $res = '';
        $vats = [];
        foreach ($items as $item) {
            $res .= (strlen($res) > 0 ? ', ' : '') . $item->vat_percentage;
            if (array_search($item->vat_percentage, $vats) == false) $vats[] = $item->vat_percentage;
        }
        if (count($vats) == 1) $res = $vats[0];
        return $res;
    }
}
