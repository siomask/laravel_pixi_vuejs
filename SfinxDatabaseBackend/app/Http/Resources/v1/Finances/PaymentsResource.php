<?php

namespace App\Http\Resources\v1\Finances;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;

class PaymentsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'amount' => $this->amount
        ];
    }
}
