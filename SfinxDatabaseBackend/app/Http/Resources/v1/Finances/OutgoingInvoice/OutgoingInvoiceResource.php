<?php

namespace App\Http\Resources\v1\Finances\OutgoingInvoice;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Finances\PaymentsResource;
use App\Http\Resources\v1\Finances\TypeResource;
use App\Http\Resources\v1\Finances\OutgoingInvoice\Entry\EntryResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\IncomingInvoiceResource;

class OutgoingInvoiceResource extends IncomingInvoiceResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  self::wrap($this);
    }
    static function wrap($collection){
        $contact = isset($collection->contact)?new ContactResource($collection->contact):(object)[];
        return [
            'id' => $collection->id,
            'start' => $collection->date,
            'end' => \Carbon\Carbon::parse($collection->date)->addSeconds($collection->payment_term)->toDateTimeString(),
            'sender' => $contact,
            'payments' => PaymentsResource::collection($collection->payments),
            'type' => new TypeResource($collection->invoiceType),
            'invoice_number' =>  $collection->invoice_number ,
            'payment_term' =>  $collection->payment_term ,
            'comment' =>  $collection->comment ,
            'entries' => EntryResource::collection($collection->entries),
            'total_entry_amount' => self::totalEntryAmount($collection->entries),
            'total_entry_amount_vat' => self::totalEntryAmount($collection->entries, true),
            'btw' => self::btw($collection->entries),
        ];
    }

}
