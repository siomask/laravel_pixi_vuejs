<?php

namespace App\Http\Resources\v1\Finances\OutgoingInvoice;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\Entry\EntryResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\v1\Finances\PaymentsResource;
use App\Http\Resources\v1\Finances\TypeResource;

class OutgoingInvoiceResourceCollection extends ResourceCollection
{

    private $items;
    public function __construct($resource)
    {
        $this->pagination = [
            'next_page_url' => $resource->nextPageUrl(),
        ];

        $this->items=$resource = $resource->getCollection();

        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'data' => ( $this->items->transform(function ($collection) {return OutgoingInvoiceResource::wrap($collection); }) ),
            'next_page_url' => $this->pagination['next_page_url']
        ];
    }

}
