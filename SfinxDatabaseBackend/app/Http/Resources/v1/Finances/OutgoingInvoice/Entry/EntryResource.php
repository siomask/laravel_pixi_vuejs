<?php

namespace App\Http\Resources\v1\Finances\OutgoingInvoice\Entry;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\ContactResource;

class EntryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'entry_name' => $this->name,
            'amount' => $this->amount,
            'vat_percentage' => $this->vat_percentage,
        ];
    }
}
