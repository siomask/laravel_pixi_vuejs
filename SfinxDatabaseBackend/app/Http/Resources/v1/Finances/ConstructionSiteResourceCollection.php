<?php

namespace App\Http\Resources\v1\Finances;

use Illuminate\Http\Resources\Json\Resource;
use App\Models\Finances\OutgoingInvoice\OutgoingInvoice;
use App\Models\Finances\IncomingInvoice\IncomingInvoice;
use App\Http\Resources\v1\Contact\ContactResource;

class ConstructionSiteResourceCollection extends Resource
{
    private $items;

    public function __construct($resource)
    {
        $this->pagination = [
            'next_page_url' => $resource->nextPageUrl(),
        ];

        $this->items = $resource = $resource->getCollection();

        parent::__construct($resource);
    }


    public function toArray($request)
    {
        $self = $this;
        return [
            'data' => ($this->items->transform(function ($collection)use($self) {
                return ConstructionSiteResource::wrap($collection);
            })),
            'next_page_url' => $this->pagination['next_page_url']
        ];
    }


}
