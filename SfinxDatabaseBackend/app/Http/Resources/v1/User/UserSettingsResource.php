<?php

namespace App\Http\Resources\v1\User;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Common\RightsResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class UserSettingsResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'planner_date_start' => $this->planner_date_start,
            'planner_date_end' => $this->planner_date_end,
            'planner_client_view' => (boolean)$this->planner_client_view,
            'show_workflow_func_group' => (boolean)$this->show_workflow_func_group,
            'show_workflow_description' => (boolean)$this->show_workflow_description,
            'id' => $this->id
        ];
    }
}
