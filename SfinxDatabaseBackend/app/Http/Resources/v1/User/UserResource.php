<?php

namespace App\Http\Resources\v1\User;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\User\UserGroupResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class UserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'sites' => $this->_sites,
            'name' => $this->name,
            'fullName' => $this->name." ".$this->first_name,
            'first_name' => $this->first_name,
            'username' => $this->username,
            'shouldRefreshPsw' => $this->password =='$2y$10$avvymbWUKIXWThs8.VSMNu0WuGavpCBggEVoqFKabsZAfweK.Dfqq',
            'group' => new UserGroupResource($this->group),
            'settings' => !empty($this->settings)?new UserSettingsResource($this->settings):null,
            'id' => $this->id
        ];
    }
}
