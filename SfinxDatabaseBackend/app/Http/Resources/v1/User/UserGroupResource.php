<?php

namespace App\Http\Resources\v1\User;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Common\RightsResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class UserGroupResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'sequence' => $this->sequence,
            'rights' => !empty($this->rights)?RightsResource::collection($this->rights):null,
            'id' => $this->id
        ];
    }
}
