<?php

namespace App\Http\Resources\v1\Contact;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Contact\Company\CompanyResource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ContactResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return self::wrap($this);
    }
    static  function wrap($item){
        if(empty($item) || !isset($item->id))return [];
        $s = trim($item->first_name);
        $l = trim($item->last_name);
        if(empty($s))$s='';
        if(empty($l))$l='';
        $types = ContactTypeResource::collection($item->types);
        $_type = '';
        foreach ($item->types as $type){
            $_type .=(strlen($_type)>0?",":"").$type->type;
        }
        return [
            'id' =>$item->id,
            'name' =>($s || $l)?$s." ".$l:'Contact '.$item->id,
            'title' =>($s || $l)?$s." ".$l:'Contact '.$item->id,
            'first_name' =>$item->first_name,
            'typeName' =>$_type,
            'last_name' =>$item->last_name,
            'address' =>$item->address,
//            'type' =>$item->type_id,
            'contact_types' =>  $types ,
            'contact_types_indexes' =>  $types->map(function($el){return $el->id;}) ,
            'companies' =>  CompanyResource::collection($item->companyContact),
            'type_id'=> isset($item->type)?$item->type->id:null,
            'street'=>$item->street,
            'postcode'=>$item->postcode,
            'number_house'=>$item->number_house
        ];
    }
}
