<?php

namespace App\Http\Resources\v1\Contact\Client;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ClientResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' =>$this->first_name." ".$this->last_name,
            'first_name' =>$this->first_name,
            'last_name' =>$this->last_name,
            'constructionSites' =>ConstructionSitesResource::collection($this->constructionSites)
        ];
    }
}
