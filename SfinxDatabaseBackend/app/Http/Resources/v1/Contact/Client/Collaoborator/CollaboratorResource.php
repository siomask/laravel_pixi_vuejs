<?php

namespace App\Http\Resources\v1\Contact\Client\Collaborator;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class CollaboratorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' =>isset($this->contact)?$this->contact->first_name." ".$this->contact->last_name:'No Collaborator',
            'abbreviation' =>$this->info->abbreviation
        ];
    }
}
