<?php

namespace App\Http\Resources\v1\Contact\Company;

use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class CompanyActivityResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return self::wrap($this);
    }
    static  function wrap($item){
        if(empty($item))return [];
        return [
            'id' =>$item->id,
            'description' =>$item->description,
            'name' =>$item->name
        ];
    }
}
