<?php

namespace App\Http\Resources\v1\Contact\Company;

use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class CompanyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return self::wrap($this);
    }
    static  function wrap($item){
        if(empty($item))return [];
        return [
            'id' =>$item->id,
            'website' =>$item->website,
            'price' =>$item->price,
            'technical_expertise' =>$item->technical_expertise,
            'aesthetic_quality' =>$item->aesthetic_quality,
            'quality' =>$item->quality,
            'vat_number' =>$item->vat_number,
            'company_activity' =>CompanyActivityResource::collection($item->activity),
            'company_type' => new CompanyTypeResource($item->type),
            'name' =>$item->name,
        ];
    }
}
