<?php

namespace App\Http\Resources\v1\Contact;

use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ContactTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' =>$this->type
        ];
    }
}
