<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class BranchInfoResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'abbreviation' => $this->afkorting,
            'full' => $this->voluit,
        ];
    }
}
