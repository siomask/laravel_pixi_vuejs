<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class PlanningToShowResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $name = isset($this->client) ? ($this->client->type == 3) ? $this->client->projectnaam : $this->client->naam : '';
        return [
            'projectLeader' => isset($this->client) && count($this->client->beheer) > 0  ? new MainProjectLeaderResource($this->client->beheer[0])  : null,
//            'projectLeader' => isset($this->client) && count($this->client->beheer) > 0&& isset($this->client->beheer[0]->projectlLeaders) && count($this->client->beheer[0]->projectlLeaders) > 0 ?new MainProjectLeaderResource($this->client->beheer[0]) : null,
            'type' => isset($this->client) ? $this->client->type : null,
            'contactid' => isset($this->client) ? $this->client->contactid : null,
            'planningtonenid' => $this->planningtonenid,
            'site_name' => $name,
            'naam' => $name
        ];
    }
}
