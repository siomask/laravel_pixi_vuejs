<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class ContractorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'contractor' => $this->contactid,
            'name' => $this->naam,
        ];
    }
}
