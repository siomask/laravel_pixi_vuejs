<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class BranchDataResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->werfdetailplanningdataid,
            'start' => $this->begin,
            'end' => $this->einde,
        ];
    }
}
