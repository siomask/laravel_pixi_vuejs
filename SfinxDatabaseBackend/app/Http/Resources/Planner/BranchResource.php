<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class BranchResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'branch' => $this->werfdetailatid,
            'info' => new BranchInfoResource($this->info),
            'contact' => new ContractorResource($this->contact),
            'data' => BranchDataResource::collection($this->data)
        ];
    }
}
