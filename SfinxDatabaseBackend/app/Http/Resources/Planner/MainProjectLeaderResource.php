<?php

namespace App\Http\Resources\Planner;

use Illuminate\Http\Resources\Json\Resource;

class MainProjectLeaderResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        foreach ($this as $v=>$_id){
           if(isset($_id->projectlLeaders)) {
               foreach ($_id->projectlLeaders as $projectl_leaders_id){
                   return [
                       'projectleiderid'=> $projectl_leaders_id->leader->projectleiderid,
                       'voornaam'=> $projectl_leaders_id->leader->voornaam,
                       'naam'=> $projectl_leaders_id->leader->naam
                   ];
               }
           }
        }
        return null;

      /*  return [
            'projectleiderid'=> $this->projectleiderid,
            'voornaam'=> $this->voornaam,
            'naam'=> $this->naam
        ];*/
    }
}
