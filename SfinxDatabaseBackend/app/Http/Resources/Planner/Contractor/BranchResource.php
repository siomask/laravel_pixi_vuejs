<?php

namespace App\Http\Resources\Planner\Contractor;

use Illuminate\Http\Resources\Json\Resource;

class BranchResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'branch' => $this->werfdetailatid,
            'info' => isset($this->info)?new BranchInfoResource($this->info):null,
            'contact' => [
                'contractor' => $this->contact->contactid,
                'name' => $this->site->contact->projectnaam
            ],
            'data' => isset($this->data)?BranchDataResource::collection($this->data):null
        ];
    }
}
