<?php

namespace App\Http\Resources\Planner\Contractor;

use App\Http\Resources\Planner\Contractor\BranchResource;
use Illuminate\Http\Resources\Json\Resource;

class ContractorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'site' => $this->contractor->contactid,
            'info' => [
                'title' => $this->contractor->naam,
            ],
            'type' => $this->contractor->type,
            'branches' => BranchResource::collection($this->contractor->branches)
        ];
    }
}
