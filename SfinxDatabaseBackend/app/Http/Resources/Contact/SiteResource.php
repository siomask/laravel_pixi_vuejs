<?php

namespace App\Http\Resources\Contact;

use Illuminate\Http\Resources\Json\Resource;

class SiteResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->werfbeheerid,
            'contactid' => $this->contactid,
            'projectLead' => ProjectleadResource::collection($this->projectLeads),
        ];
    }
}
