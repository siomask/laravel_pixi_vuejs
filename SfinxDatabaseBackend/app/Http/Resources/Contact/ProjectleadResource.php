<?php

namespace App\Http\Resources\Contact;

use Illuminate\Http\Resources\Json\Resource;

class ProjectleadResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->projectleiderid,
            'firstName' => $this->voornaam,
            'lastName' => $this->naam,
            'type' => $this->type
        ];
    }
}
