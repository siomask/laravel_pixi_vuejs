<?php

namespace App\Http\Resources\contact;

use Illuminate\Http\Resources\Json\Resource;

class EPBReporterResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->contactid,
            'street' => $this->straat,
            'nr' => $this->nr,
            'bus' => $this->bus,
            'postalcode' => $this->postcode,
            'municipality' => $this->gemeente,
            'type' => $this->klantenstatus,
            'projectnr' => $this->werkkrachten,
            'projectnaam' => $this->projectnaam
        ];
    }
}
