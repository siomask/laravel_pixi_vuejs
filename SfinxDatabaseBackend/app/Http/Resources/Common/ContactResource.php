<?php

namespace App\Http\Resources\Common\Contact;

use Illuminate\Http\Resources\Json\Resource;
/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ContactResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' =>$this->first_name." ".$this->last_name
        ];
    }
}
