<?php

namespace App\Http\Resources\Common;

use Illuminate\Http\Resources\Json\Resource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class ConstructionTypeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sequence' => $this->sequence,
            'name' => $this->name,
            'full' => $this->name,
            'abbreviation' => $this->abbreviation,
            'pivot' => $this->pivot,
        ];
    }
}
