<?php

namespace App\Http\Resources\Common;

use App\Models\Common\AbsenceType;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\v1\Common\HolidaysResource;

/**
 * @property mixed planner_date_start
 * @property mixed planner_date_end
 */
class MemberResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $holidays=[];
        $sickdays=[];
        if(isset($this->absences)){
            $abs_types = AbsenceType::types();
            foreach ($this->absences as $item){
                if($abs_types[0]->id == $item->absence_type_id){
                    $sickdays[]=$item;
                }else if($abs_types[1]->id == $item->absence_type_id){
                    $holidays[]=$item;
                }
            }
        }
        $sickdays = collect($sickdays);
        $holidays = collect($holidays);


        return [
            'id' => $this->id,
            'name' => $this->first_name . " " . $this->last_name,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            '_a' => $this->absences,
            '_absences' => $this->_absences($sickdays),
            'absences' => MemberAbsencesResource::collection($sickdays),
            '_holidays' => $this->_absences($holidays),
            'holidays' => MemberAbsencesResource::collection($holidays)
        ];
    }

    private function _absences($list)
    {
        $res = '';
        foreach ($list as $item) {
            $res .= '<p class="my-0"><b>Start:</b>' . $item->start . ',<b>End:</b>' . $item->end . '</p>';
        }
        return $res;
    }
}
