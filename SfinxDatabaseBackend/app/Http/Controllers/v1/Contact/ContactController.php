<?php

namespace App\Http\Controllers\v1\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Contact\Contact;
use App\Http\Resources\v1\Contact\ContactResourceCollection;
use App\Http\Resources\v1\Contact\ContactResource;
use Validator;

class ContactController extends Controller
{

    public function index(Request $request)
    {
        $contactType = $_contactType = $request->query('type');
        $contactName = $request->query('name');
        $contactId = $request->query('id');
        $with = [
            'sites.projectLeads.user',
            'sites.estimators.user',
            'types'
        ];
        $filters = [];
        if (!empty($contactType)) {
            $contactType = explode(",", $contactType);
            $filters['type'] = $contactType;
        }
        if (!empty($contactName)) {
            $filters['name'] = $contactName;
        }
        if (!empty($contactName)) {
            $filters['id'] = $contactId;
        }

        if ($_contactType == 'all') {
            return response()->json(['data' => ContactResource::collection(Contact::with($with)->get())]);
        } else if (!empty($contactName) || !empty($contactType) || !empty($contactId)) {
            $_d = Contact::with($with);
            if (!empty($contactType)) {
                $_d = $_d->whereHas('types', function ($q) use ($contactType) {
                    $q->whereIn('contact_contact_type.contact_type_id', $contactType);//TODO: need to find better solution
                });
            }
            if (!empty($contactName)) {
                $like = "%" . $contactName . "%";
                $_d = $_d->where('first_name', 'LIKE', $like)
                    ->orWhere('last_name', 'LIKE', $like)
                    ->orWhere('address', 'LIKE', $like);
            }
            if (!empty($contactId)) {
                $_d = $_d->where('id', '=', $contactId);
            }
            $_d = $_d->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
        } else {
            $_d = Contact::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
        }

        foreach ($_d as $item) {
            foreach ($item->sites as $site) {
                $item->projectLead = '';
                $item->estimator = '';
                if (isset($site->projectLeads)) foreach ($site->projectLeads as $lead) {
                    if ($item->projectLead) break;
                    $item->projectLead = $lead->user->first_name . " " . $lead->user->name;
                }
                if (isset($site->estimators)) foreach ($site->estimators as $lead) {
                    if ($item->estimator) break;
                    $item->estimator = $lead->user->first_name . " " . $lead->user->name;
                }
                if ($item->projectLead && $item->estimator) break;
            }
        }
        return new ContactResourceCollection($_d);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'types.*.id' => 'required_with:types.*.id'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {

            if(empty($request->types)) return response()->json(['message' =>'contact type is required'], 400);
            $contact = Contact::create($request->all());
            $contact->types()->attach(array_map(function($a){return $a['id'];}, $request->types));
            return new ContactResource($contact);
        }

    }


    public function update(Request $request, $id)
    {
        $updateTypes = false;
        foreach ($request->all() as $key => $value) {
            if ($key == 'first_name') {
                $validate['first_name'] = 'required|max:255';
            } else if ($key == 'types') {
                $validate['types.*.id'] = 'required_with:types.*.id,';
                $updateTypes = true;
            }
        }
        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $contact = $this->contactItem($request, $id);
            if ($contact instanceof Contact) {
                $contact->update($request->all());
                if($updateTypes){
                    $contact->types()->detach();
                    $contact->types()->attach(array_map(function($a){return $a['id'];}, $request->types));
                }
                $contact = new ContactResource($contact);
            }
        }

        return $contact;
    }

    public function destroy(Request $request, $id)
    {
        $contact = $this->contactItem($request, $id);
        if ($contact instanceof Contact) {
            return $contact->delete() ? 1 : 0;
        }
        return $contact;
    }


    private function contactItem(Request $request, $id)
    {
        try {
            return Contact::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No contact found!!!', 'error' => $e->getMessage()], 400);
        }
    }
}