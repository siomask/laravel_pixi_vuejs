<?php

namespace App\Http\Controllers\v1\Contact\Type;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Contact\ContactType;

class TypeController extends Controller
{

    public function index(Request $request)
    {
        if($request->query('type')== 'main'){
            return  (ContactType::where('contact_type_id',null)->get());
        }else{
            return  (ContactType::all());
        }

    }


}