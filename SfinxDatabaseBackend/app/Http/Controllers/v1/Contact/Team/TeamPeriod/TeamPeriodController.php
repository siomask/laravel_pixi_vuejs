<?php

namespace App\Http\Controllers\v1\Contact\Team\TeamPeriod;

use App\Models\Team\Team;
use App\Http\Resources\v1\Team\TeamPeriodResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TeamPeriodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TeamPeriodResource::collection($this->team($request)->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'start' => 'required|date|date_format:Y-m-d|before_or_equal:end',
            'end' => 'required|date|date_format:Y-m-d|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            return new TeamPeriodResource($this->team($request)->create([
                'start' => $request->start,
                'end' => $request->end
            ]));
        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contactId,$teamId, $id)
    {
        $validator = Validator::make($request->all(), [
            'start' => 'required|date|date_format:Y-m-d|before_or_equal:end',
            'end' => 'required|date|date_format:Y-m-d|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            try {
                $teamPeriod = $this->team($request)->findOrFail($id);
            } catch (\Exception $e) {
                return response()->json(['message' => 'No Team Period found!!!', 'error' => $e->getMessage()], 400);
            }

            $teamPeriod->update([
                'start' => $request->start,
                'end' => $request->end
            ]);
            return new TeamPeriodResource($teamPeriod);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $contactId,$teamId,$id)
    {
        try {
            $teamPeriod = $this->team($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Team Period found!!!', 'error' => $e->getMessage()], 400);
        }
        return $teamPeriod->delete() ? 1 : 0;
    }

    private function team(Request $request)
    {
        return $request->attributes->get('_contact_team')->period();
    }

}
