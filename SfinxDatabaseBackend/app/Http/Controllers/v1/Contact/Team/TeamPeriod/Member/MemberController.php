<?php

namespace App\Http\Controllers\v1\Contact\Team\TeamPeriod\Member;

use App\Models\Common\Member;
use App\Http\Resources\v1\Team\TeamPeriodMemberResource;
use App\Http\Resources\Common\MemberResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TeamPeriodMemberResource::collection($this->teamPeriodMembers($request)->with('member')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $periodMembers =  $this->teamPeriodMembers($request)->firstOrCreate([
                'team_member_id' => $request->id
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Member found!!!'], 400);
        }
        return $periodMembers;


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $contactId, $teamId, $teamPeriodId, $memberid)
    {
        try {
            $periodMembers = $this->teamPeriodMembers($request)->where('team_member_id',$memberid);
            if($periodMembers->count()==0)throw new \Exception("Not Found Anything");
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Member in Team Period found!!!', 'error' => $e->getMessage()], 400);
        }
        return $periodMembers->delete() ? 1 : 0;
    }

    private function teamPeriodMembers(Request $request)
    {
        return $request->attributes->get('_contact_team_period')->periodMembers();
    }

}
