<?php

namespace App\Http\Controllers\v1\Contact\Team;

use App\Http\Resources\v1\Team\TeamResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return TeamResource::collection($this->teams($request)->get());
    }


    private function teams(Request $request)
    {
        return $request->attributes->get('_contact')->teams()->with('period.members');
    }

    private function team(Request $request, $id)
    {
        return $this->teams($request)->findOrFail($id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $contactId)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'bail|required|max:255',
            'abbreviation' => 'bail|required|max:255|unique:teams,abbreviation'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $team = $this->teams($request)->create($request->all());
            $this->updatePeriods($request, $team);
            return new TeamResource($this->team($request, $team->id));
        }


    }

    private function updatePeriods(Request $request, $team)
    {
        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'tempPeriods':
                    {
                        $value = (array)$value;
                        $updates = array_filter($value, function ($k, $el) {
                            return !empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);


                        $newItems = array_filter($value, function ($k, $el) {
                            return empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);

                        $team->period()->whereNotIn('id', array_map(function ($el) {
                            return $el['id'];
                        }, $updates))->delete();


                        foreach ($updates as $item) {
                            $period = $team->period()->findOrFail($item['id']);
                            $period->update($item);
                            $this->updatePeriodMembers($period, $item);
                        }


                        foreach ($newItems as $item) {
                            $this->updatePeriodMembers($team->period()->create($item), $item);
                        }
                        break;
                    }
            }
        }
    }

    private function updatePeriodMembers($period, $item)
    {

        if (isset($item['membersId'])) {
            $membersUpdates = $item['membersId'];
            $updates = array_map(function ($el) {
                return $el['id'];
            }, $membersUpdates);


            $period->members()->whereNotIn('id', $updates)->detach();

            //remove missed
            foreach ($period->members()->get() as $memberPivot) {
                $shouldRemove = true;
                foreach ($updates as $key => $memberId) {
                    if ($memberId == $memberPivot->team_member_id) {
                        $shouldRemove = $key;
                        break;
                    }
                }
                if ($shouldRemove && is_bool($shouldRemove)) {
                    $memberPivot->detach();
                } else {
                    array_splice($updates, $shouldRemove, 1);
                    array_splice($membersUpdates, $shouldRemove, 1);
                }
            }

            //add new missed
            foreach ($updates as $key => $memberId) {
                $period->members()->attach($memberId, [
                    'member_price' => $membersUpdates[$key]['member_price']
                ]);
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $contactId, $id)
    {
        try {
            $team = $this->team($request, $id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Team found!!!', 'error' => $e->getMessage()], 400);
        }

        if (isset($request['abbreviation'])) {
            $validator = Validator::make($request->all(), [
                'abbreviation' => 'bail|required|max:255|unique:teams,abbreviation,' . $id
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }
        }


        $team->update($request->all());
        $this->updatePeriods($request, $team);
        return new TeamResource($this->team($request, $team->id));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $contactId, $id)
    {
        try {
            $team = $this->team($request, $id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Team found!!!', 'error' => $e->getMessage()], 400);
        }
        return $team->delete() ? 1 : 0;
    }
}
