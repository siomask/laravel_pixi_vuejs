<?php

namespace App\Http\Controllers\v1\Contact\Client\ConstructionSite\Collaborator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Planner\ConstructionSitesCollaboratorResource;
use Validator;

class CollaboratorController extends Controller
{



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return ConstructionSitesCollaboratorResource::collection($request->attributes->get('_construction_site')->collaborators()->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $collaborator = ['contractor_type_id' => $request->contractor_type_id];
        if (isset($request->contact_id)) $collaborator['contact_id'] = $request->contact_id;
        return new ConstructionSitesCollaboratorResource ($request->attributes->get('_construction_site')->collaborators()->create($collaborator));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $clientId, $siteId,$collaboratorID)
    {
        try {
            $collaborator = $this->collaborator($request, $collaboratorID);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Collaborator found!!!'], 400);
        }
        if (isset($request['start']) && !empty($request['start'])
            && isset($request['end']) && !empty($request['end'])) {
            $validator = Validator::make($request->all(), [
                'start' => 'date|before_or_equal:end',
                'end' => 'date|after_or_equal:start'
            ]);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }
        }
        if (isset($request['contact_id']) && $request['contact_id'] < 0) {
            $request['contact_id'] = null;
        }
        try {
            $collaborator->update($request->all());
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something bad on update!!!', 'errors' => $e->getMessage()], 400);
        }

        return new ConstructionSitesCollaboratorResource ($collaborator);

    }

    public function destroy(Request $request, $clientId, $siteId,$collaboratorID)
    {
        try {
            $collaborator = $this->collaborator($request, $collaboratorID);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Collaborator found!!!'], 400);
        }
        return $collaborator->delete() ? 1 : 0;
    }

    public function show(Request $request, $clientId, $collaboratorID)
    {
        try {
            $collaborator = $this->collaborator($request, $collaboratorID);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Collaborator found!!!'], 400);
        }
        return new ConstructionSitesCollaboratorResource ($collaborator);
    }

    private function collaborator(Request $request, $collaboratorID)
    {
        return ($request->attributes->get('_construction_site')->collaborators()->findOrFail($collaboratorID));
    }
}