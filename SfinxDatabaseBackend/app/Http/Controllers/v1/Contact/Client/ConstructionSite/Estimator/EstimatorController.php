<?php

namespace App\Http\Controllers\v1\Contact\Client\ConstructionSite\Estimator;

use App\Models\Common\ProjectLead;
use App\Models\Common\AbstractUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\ProjLeaderResource;

class EstimatorController extends Controller
{


    public function store(Request $request)
    {
        return new ProjLeaderResource($this->estimators($request)->create($request->all()));

    }

    public function update(Request $request,$clientId, $siteid, $id)
    {
        $ext = $this->estimator($request, $id);
        if ($ext instanceof AbstractUser) {
            try {
                $ext->update($request->all());
            } catch (\Exception $e) {
                return response()->json(['message' => 'Something bad on update estimator', 'data' => $e->getMessage()], 400);
            }
            return new ProjLeaderResource($ext);
        } else {
            return $ext;
        }
    }

    public function destroy(Request $request,$clientId, $siteid, $id)
    {
        $ext = $this->estimator($request, $id);
        return $ext instanceof AbstractUser ? ($ext->delete() ? 1 : 0) :   ($ext);
    }

    private function estimators(Request $request)
    {
        return $request->attributes->get('_construction_site')->estimators();

    }

    private function estimator(Request $request, $id)
    {
        try {
            $ext = $this->estimators($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No estimator found!!!'], 400);
        }
        return $ext;
    }


}