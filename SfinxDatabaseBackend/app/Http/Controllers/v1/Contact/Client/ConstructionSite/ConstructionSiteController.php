<?php

namespace App\Http\Controllers\v1\Contact\Client\ConstructionSite;

use App\Models\Contact\Contact;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use App\Http\Resources\v1\Contact\ContactResource;


class ConstructionSiteController extends Controller
{

    private function sites(Request $request)
    {
//        return $request->attributes->get('_client')->constructionSites();
    }

    private function site(Request $request, $id)
    {
//        return $this->sites($request)->findOrFail($id);
        return ConstructionSite::findOrFail($id);
    }


    public function update(Request $request, $clientId, $id)
    {
        try {
            $site = $this->site($request, $id);
            $updates = [];
            foreach ($request->all() as $key => $value) {
                switch ($key) {
                    case 'new_projectLeaders':
                        {
                            $value = (array)$value;
                            $updates = array_filter($value, function ($k, $el) {
                                return !empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $newItems = array_filter($value, function ($k, $el) {
                                return empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);

                            $site->projectLeads()->whereNotIn('id', array_map(function ($el) {
                                return $el['id'];
                            }, $updates))->delete();
                            foreach ($updates as $item) {
                                $site->projectLeads()->findOrFail($item['id'])->update($item);
                            }


                            foreach ($newItems as $item) {
                                $site->projectLeads()->create($item);
                            }
                            break;
                        }
                    case 'new_estimators':
                        {
                            $value = (array)$value;
                            $updates = array_filter($value, function ($k, $el) {
                                return !empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $newItems = array_filter($value, function ($k, $el) {
                                return empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $site->estimators()->whereNotIn('id', array_map(function ($el) {
                                return $el['id'];
                            }, $updates))->delete();
                            foreach ($updates as $item) {
                                $site->estimators()->findOrFail($item['id'])->update($item);
                            }
                            foreach ($newItems as $item) {
                                $site->estimators()->create($item);
                            }
                            break;
                        }
                    case 'status_id':
                        {
                            $request['construction_site_statuses_id'] = $value;
                            break;
                        }
                    case 'presented_price':
                        {
                            $updates[] = ['presented_price' => $value];
                            break;
                        }
                    case 'planner_show_empty_rows':
                        {
                            auth()->user()
                                ->settings
                                ->allConstructionSites()->updateExistingPivot($id, [
                                    $key => $value
                                ]);
                            break;
                        }
                    case 'sequence':
                        {
                            $updates[] = ['sequence' => $value];
                            break;
                        }
                }
            }
            $site->update($request->all());

        } catch (\Exception $e) {
            return response()->json(['message' => 'Bad update site!!!', "data" => $e->getMessage()], 400);
        }


        return new ConstructionSitesResource($site);
    }

    public function destroy(Request $request, $clientId, $id)
    {
        try {
            $site = $this->site($request, $id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Site found!!!'], 400);
        }
        return auth()->user()->settings->allConstructionSites()->wherePivot(
            'construction_site_user_setting_type_id', '=', 1
        )->wherePivot(
            'construction_site_id','=',$site->id
        )->detach() ? 1 : 0;
    }

    public function store(Request $request)
    {
        try {
            $site = ConstructionSite::withTrashed()->where("contact_id", $request->id);
            $site->restore();
            $site = $site->first();
            if (empty($site)) {
                $site = ConstructionSite::create([
                    "contact_id" => $request->id
                ]);

            }

        } catch (\Exception $e) {
//            return response()->json([$e->getMessage()], 400);
            $site = ConstructionSite::create([
                "contact_id" => $request->id
            ]);
        } finally {
           /* $construction_type = 1;// $request->construction_type;
            if (auth()->user()->settings->allConstructionSites()->where([
                    ['construction_site_user_setting_type_id', '=', $construction_type],
                    ['construction_site_id', '=', $site->id]
                ])->count() == 0) {
                auth()->user()->settings->allConstructionSites()->attach(
                    $site->id,
                    [
                        'construction_site_user_setting_type_id' => $construction_type
                    ]
                );
            }*/
        }
        return new ConstructionSitesResource($site);

    }

}