<?php

namespace App\Http\Controllers\v1\Contact\Client\ConstructionSite\ProjectLead;

use App\Models\Common\ProjectLead;
use App\Models\Common\AbstractUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Client\ProjLeaderResource;


class ProjectLeaderController extends Controller
{


    public function store(Request $request)
    {
        return new ProjLeaderResource($this->leaders($request)->create($request->all()));

    }

    public function update(Request $request, $clientId,$siteid, $id)
    {
        $ext = $this->leader($request, $id);
        if ($ext instanceof AbstractUser) {
            try {
                $ext->update($request->all());
            } catch (\Exception $e) {
                return response()->json(['message' => 'Something bad on update project leader', 'data' => $e->getMessage()], 400);
            }
            return new ProjLeaderResource($ext);
        } else {
            return $ext;
        }
    }

    public function destroy(Request $request, $clientId,$siteid, $id)
    {
        $ext = $this->leader($request, $id);
        return $ext instanceof AbstractUser ? ($ext->delete() ? 1 : 0) : $ext;
    }

    private function leaders(Request $request)
    {
        return $request->attributes->get('_construction_site')->projectLeads();

    }

    private function leader(Request $request, $id)
    {
        try {
            $ext = $this->leaders($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Leader found!!!'], 400);
        }
        return $ext;
    }

}