<?php

namespace App\Http\Controllers\v1\Contact\Client;

use App\Models\Contact\Client;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Contact\Client\ClientResource;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return/* ClientResource::collection*/
            ($this->client()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return ClientResource
     */
    public function store(Request $request)
    {
        return new ClientResource(Client::create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function show($id)
    {
        try {
            $res = new ClientResource(
                $this->client()->with([
                    'constructionSites',
                    'constructionSites.status',
                    'constructionSites.collaborators',
                    'constructionSites.collaborators.info',
                    'constructionSites.collaborators.contact',
                    'constructionSites.projectLeads.user',
                    'constructionSites.estimators.user'
                ])->findOrFail($id)
            );
        } catch (\Exception $e) {
            return response()->json(['message' => 'No client found!!!', 'error' => $e->getMessage()], 400);
        }
        return $res;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $client = $this->client()->findOrFail($id);
            $client->update($request->all());
            $client = new ClientResource($client);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No client found!!!', 'error' => $e->getMessage()], 400);
        }
        return $client;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $client = $this->client()->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No client found!!!', 'error' => $e->getMessage()], 400);
        }
        return $client->delete() ? 1 : 0;
    }


    private function client()
    {
        $klientId = Contact::$CLIENT_TYPE;
        return Contact::with('types')->whereHas('types', function ($q) use ($klientId) {
            return $q->where([['contact_contact_type.contact_type_id', '=', $klientId]]);
        });
    }
}
