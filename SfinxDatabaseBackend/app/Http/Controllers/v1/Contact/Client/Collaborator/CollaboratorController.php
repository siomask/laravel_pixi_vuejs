<?php

namespace App\Http\Controllers\v1\Contact\Client\Collaborator;

use App\Models\Contact\Client;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Contact\Client\ClientResource;
use Validator;
use App\Http\Resources\v1\Contact\Client\Collaborator\CollaboratorResource;

class CollaboratorController extends Controller
{
    /*public function index($clientId)
    {
        return CollaboratorResource::collection(Client::findOrFail($clientId)->collaborators());
    }

    public function store(Request $request,$clientId)
    {
        $validator = Validator::make($request->all(), [
            'collaborator' => 'number',
            'contractorType' => 'required|number'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            try {
                $collaborators =  Client::findOrFail($clientId)->collaborators();
            } catch (\Exception $e) {
                return response()->json(['message' => 'No Collaborator found!!!', 'error' => $e->getMessage()], 400);
            }
            return new CollaboratorResource($collaborators->create($request->all()));
        }

    }

    public function show($id)
    {
        try {
            $client =  new ClientResource(
                $this->client()->with(['constructionSites', 'constructionSites.projectLeads'])->findOrFail($id)
            );
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Collaborator found!!!', 'error' => $e->getMessage()], 400);
        }
        return $client;
    }

    public function update(Request $request, $id)
    {
        $client = $this->client()->findOrFail($id);
        $client->update($request->all());
        return new ClientResource($client);
    }

    public function destroy($id)
    {
        return $this->client()->findOrFail($id)->delete()?1:0;
    }

    private function client(){
        return Client::where('type_id',Contact::$CLIENT_TYPE);
    }*/

    public function store(Request $request)
    {
        $sites = auth()->user()
            ->settings
            ->collaborators();
        $site =null;//$sites->withTrashed()->where("contact_id", $request->id)->first();
        if (empty($site)) {
            $site = $sites->create([
                "contact_id" => $request->id
            ]);
        }else{
            $site->restore();
        }
        return $site;

    }

    public function destroy(Request $request, $id)
    {
        return auth()->user()
            ->settings
            ->collaborators()
            ->where("contact_id", $id)->delete() ? 1 : 0;
    }
}
