<?php

namespace App\Http\Controllers\v1\Finances;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\TypeResource;
use App\Models\Finances\InvoiceType;

class TypesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index()
    {
        return TypeResource::collection(InvoiceType::all());
    }


}