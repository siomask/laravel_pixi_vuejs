<?php

namespace App\Http\Controllers\v1\Finances\IncomingInvoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\IncomingInvoice\IncomingInvoiceResource;
use App\Http\Resources\v1\Finances\IncomingInvoice\IncomingInvoiceResourceCollection;
use App\Models\Finances\IncomingInvoice\IncomingInvoice;

class IncomingInvoicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        $_name = $request->query('name');
        $contact_id = $request->query('contact_id');
        $with = ['contact'];
        $filters = [];
        if (!empty($_name)) {
            $filters['name'] = $_name;
        }
        if (!empty($contact_id)) {
            $filters['contact_id'] = $contact_id;
        }

        if (!empty($_name) || !empty($contact_id)) {
            $_result = IncomingInvoice::with($with);
            if (!empty($_name)) {
                $like = "%" . $_name . "%";
                $_result = $_result->where('invoice_number', 'LIKE', $like)
                    ->orWhere('serial_number', 'LIKE', $like)
                    ->orWhere('comment', 'LIKE', $like);
            }
            if (!empty($contact_id)) {
                $_result = $_result->where([['contact_id', '=', $contact_id]]);
            }
            $_result = $_result->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
        } else {
            $_result = IncomingInvoice::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
        }
        return new IncomingInvoiceResourceCollection($_result);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $invoice = IncomingInvoice::create($request->all());
            $invoice = new IncomingInvoiceResource($invoice);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create invoice', 'data' => $e->getMessage()], 400);
        }
        return $invoice;
    }

    public function update(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof IncomingInvoice) {
            try {
                $invoice->update($request->all());
                $invoice = new IncomingInvoiceResource($invoice);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update invoice', 'data' => $e->getMessage()], 400);
            }
        }

        return $invoice;
    }

    public function destroy(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof IncomingInvoice) {
            $invoice = $invoice->delete() ? 1 : 0;
        }
        return $invoice;
    }

    private function invoice($id)
    {
        try {
            $invoice = IncomingInvoice::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No invoice found'], 400);
        }
        return $invoice;
    }


}