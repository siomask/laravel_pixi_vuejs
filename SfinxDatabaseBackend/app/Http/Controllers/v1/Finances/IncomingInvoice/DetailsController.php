<?php

namespace App\Http\Controllers\v1\Finances\IncomingInvoice;

use Illuminate\Http\Request;
use App\Models\Finances\IncomingInvoice\ConstructionSiteIncomingInvoice;
use App\Http\Resources\v1\Finances\IncomingInvoice\DetailResource;
use App\Http\Controllers\Controller;

class DetailsController extends Controller
{

    private function sites(Request $request)
    {
        return $request->attributes->get('_invoice')->sites()->withPivot([
            'contractor_type_id',
            'amount'
        ]);
    }


    public function index(Request $request)
    {
        return DetailResource::collection(ConstructionSiteIncomingInvoice::where(
            [
                ['incoming_invoice_id', '=', $request->attributes->get('_invoice')->id]
            ]
        )->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
//            $this->sites($request)->attach($request->construction_site_id, [
//                'contractor_type_id' => $request->contractor_type_id,
//                'amount' => $request->amount
//            ]);
//            $item = ConstructionSiteIncomingInvoice::orderBy('id', 'DESC')->first();
            $item = ConstructionSiteIncomingInvoice::create([
                'incoming_invoice_id' => $request->attributes->get('_invoice')->id,
                'construction_site_id' => $request->construction_site_id,
                'contractor_type_id' => $request->contractor_type_id,
                'amount' => $request->amount
            ]);
            return new DetailResource($item );
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t add site to invoice', 'data' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, $invoiceId, $id)
    {
        try {
            $detail = ConstructionSiteIncomingInvoice::findOrFail($id);
            $detail->update([
                'construction_site_id' => $request->construction_site_id,
                'contractor_type_id' => $request->contractor_type_id,
                'amount' => $request->amount
            ]);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t update site of invoice', 'data' => $e->getMessage()], 400);
        }
        return new DetailResource($detail);


    }

    public function destroy(Request $request, $invoiceId, $id)
    {
        try {
            return ConstructionSiteIncomingInvoice::findOrFail($id)->delete() ? 1 : 0;
        } catch (\Exception $e) {
            return response()->json(['message' => 'No relation found', 'data' => $e->getMessage()], 400);
        }
    }


}