<?php

namespace App\Http\Controllers\v1\Finances;

use App\Models\Planner\ConstructionSite\ConstructionSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\ConstructionSiteResource;
use App\Http\Resources\v1\Finances\ConstructionSiteResourceCollection;

//use App\Models\Finances\InvoiceTypes;

class SummaryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        $_name = $request->query('name');
        $_siteId = $request->query('siteId');
        $with = [
            'collaborators.data',
            'collaborators.contact.teams.period.periodMembers.plannings.pivotPlannings'
        ];
        $filters = [];
        if (!empty($_name)) {
            $filters['name'] = $_name;
        }
        if (!empty($_siteId)) {
            $filters['siteId'] = $_siteId;
        }

        if (!empty($_name) || !empty($_siteId)) {
            $_result = ConstructionSite::with($with);
            if (!empty($_siteId)) {
                $_result = $_result->where([['id', '=', $_siteId]]);
            }
            if (!empty($_name)) {
                $like = "%" . $_name . "%";
//                $_result = $_result->where('contact_id', 'LIKE', $like);

                $with['contact'] = function ($q) use ($like) {
                    return $q->where('first_name', 'LIKE', $like)
                        ->orWhere('first_name', 'LIKE', $like);
                };
            }
            $_result = $_result->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
        } else {
            $_result = ConstructionSite::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
        }
        return new ConstructionSiteResourceCollection($_result);
    }

    public function update(Request $request, $id)
    {
        try {
            $site = ConstructionSite::findOrFail($id);
            $updates = [];
            foreach ($request->all() as $key => $value) {
                switch ($key) {
                    case 'total_amount_orders':
                    case 'total_amount_processed':
                    case 'date_financial_details':
                        {
                            $updates[$key] = $value;
                            break;
                        }
                }
            }
            $site->update($updates);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Bad update site!!!', "data" => $e->getMessage()], 400);
        }


        return new ConstructionSiteResource($site);
    }

}