<?php

namespace App\Http\Controllers\v1\Finances;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\InvoiceAccountsResource;
use App\Models\Finances\InvoiceAccount;
use Validator;
class InvoiceAccountsController extends Controller
{

    public function index(Request $request)
    {
        return response()->json([
            'data' => InvoiceAccountsResource::parseItemsWithParent(InvoiceAccount::all())
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $invoice = InvoiceAccount::create($request->all());
            return new InvoiceAccountsResource($invoice);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create invoice account', 'data' => $e->getMessage()], 400);
        }
    }

    public function update(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof InvoiceAccount) {
            try {
                $updates = [];
                $validate = [];

                foreach ($request->all() as $key => $value) {
                    switch ($key) {
                        case 'description':
                            {
                                $validate['description'] = 'max:255';
                                $updates['description'] = ($request->description);
                                break;
                            }
                        case 'parent_id':
                            {
                                $updates['parent_invoice_account_id'] = ($request->parent_id);
                                break;
                            }
                        case 'sequence':
                            {
                                $validate['sequence'] = 'integer';
                                $updates['sequence'] = ($request->sequence);
                                break;
                            }
                    }
                }

                $validator = Validator::make($request->all(), $validate);
                if ($validator->fails()) {
                    return response()->json(['errors' => $validator->errors()], 400);
                } else {
                    if (count($updates) > 0) {
                        if (isset($request->sequence)) {
                            if($invoice->parent_invoice_account_id !=$updates['parent_invoice_account_id']){
                                InvoiceAccount::where([
                                    ['parent_invoice_account_id', $invoice['parent_invoice_account_id']],
                                    ['sequence', '>=', $invoice['sequence']]
                                ])->decrement('sequence');
                                InvoiceAccount::where([
                                    ['parent_invoice_account_id', $updates['parent_invoice_account_id']],
                                    ['sequence', '>=', $updates['sequence']]
                                ])->increment('sequence');
                            }else{
                                if ($invoice['sequence'] != $updates['sequence']) {
                                    if ($invoice['sequence'] < $updates['sequence']) {
                                        InvoiceAccount::where([
                                            ['parent_invoice_account_id', $invoice['parent_invoice_account_id']],
                                            ['sequence', '>', $invoice['sequence']],
                                            ['sequence', '<=', $updates['sequence']]
                                        ])->decrement('sequence');
                                    } else {
                                        InvoiceAccount::where([
                                            ['parent_invoice_account_id', $invoice['parent_invoice_account_id']],
                                            ['sequence', '<', $invoice['sequence']],
                                            ['sequence', '>=', $updates['sequence']]
                                        ])->increment('sequence');
                                    }
                                }
                            }

                        }
                        $invoice->update($updates);
                    }
                    $invoice = new InvoiceAccountsResource($invoice);
                }

            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update invoice', 'data' => $e->getMessage()], 400);
            }
        }

        return $invoice;
    }

    public function destroy(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof InvoiceAccount) {
            $invoice = $invoice->delete() ? 1 : 0;
        }
        return $invoice;
    }

    private function invoice($id)
    {
        try {
            $invoice = InvoiceAccount::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No invoice account found'], 400);
        }
        return $invoice;
    }


}