<?php

namespace App\Http\Controllers\v1\Finances\OutgoingInvoice\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\PaymentsResource;
use App\Models\Finances\Payment;

class PaymentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        return PaymentsResource::collection($this->payments($request)->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $entry = $this->payments($request)->create($request->all());
            $entry = new PaymentsResource($entry);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create outgoing invoice payment', 'data' => $e->getMessage()], 400);
        }
        return $entry;
    }

    public function update(Request $request, $invoiceId, $id)
    {
        $entry = $this->payment($request, $id);
        if ($entry instanceof Payment) {
            try {
                $entry->update($request->all());
                $entry = new PaymentsResource($entry);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update outgoing invoice payment', 'data' => $e->getMessage()], 400);
            }
        }

        return $entry;
    }

    public function destroy(Request $request, $invoiceId, $id)
    {
        $entry = $this->payment($request, $id);
        if ($entry instanceof Payment) {
            $entry = $entry->delete() ? 1 : 0;
        }
        return $entry;
    }

    private function payments(Request $request)
    {
        return $request->attributes->get('_outgoing_invoice')->payments();
    }

    private function payment(Request $request, $id)
    {
        try {
            $entry = $this->payments($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No outgoing invoice payment found'], 400);
        }
        return $entry;
    }


}