<?php

namespace App\Http\Controllers\v1\Finances\OutgoingInvoice;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\OutgoingInvoice\OutgoingInvoiceResource;
use App\Http\Resources\v1\Finances\OutgoingInvoice\OutgoingInvoiceResourceCollection;
use App\Models\Finances\OutgoingInvoice\OutgoingInvoice;

class OutgoingInvoicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        $_name = $request->query('name');
        $with = ['contact'];
        $filters = [];
        if (!empty($_name)) {
            $filters['name'] = $_name;
        }

        if (!empty($_name)) {
            $_result = OutgoingInvoice::with($with);
            if (!empty($_name)) {
                $like = "%" . $_name . "%";
                $_result = $_result->where('invoice_number', 'LIKE', $like)
                    ->orWhere('date', 'LIKE', $like)
                    ->orWhere('comment', 'LIKE', $like);
            }
            $_result = $_result->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
        } else {
            $_result = OutgoingInvoice::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
        }
        return new OutgoingInvoiceResourceCollection($_result);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $invoice = OutgoingInvoice::create($request->all());
            $invoice = new OutgoingInvoiceResource($invoice);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create outgoing invoice', 'data' => $e->getMessage()], 400);
        }
        return $invoice;
    }

    public function update(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof OutgoingInvoice) {
            try {
                $invoice->update($request->all());
                $invoice = new OutgoingInvoiceResource($invoice);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update outgoing invoice', 'data' => $e->getMessage()], 400);
            }
        }

        return $invoice;
    }

    public function destroy(Request $request, $id)
    {
        $invoice = $this->invoice($id);
        if ($invoice instanceof OutgoingInvoice) {
            $invoice = $invoice->delete() ? 1 : 0;
        }
        return $invoice;
    }

    private function invoice($id)
    {
        try {
            $invoice = OutgoingInvoice::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No outgoing invoice found'], 400);
        }
        return $invoice;
    }


}