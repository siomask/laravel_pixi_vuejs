<?php

namespace App\Http\Controllers\v1\Finances\OutgoingInvoice\Entry;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Finances\IncomingInvoice\Entry\EntryResource;
use App\Models\Finances\OutgoingInvoice\Entry\OutgoingInvoiceEntry;

class EntryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        return EntryResource::collection($this->entries($request)->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $entry = $this->entries($request)->create($request->all());
            $entry = new EntryResource($entry);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create outgoing invoice entry', 'data' => $e->getMessage()], 400);
        }
        return $entry;
    }

    public function update(Request $request, $invoiceId, $id)
    {
        $entry = $this->entry($request, $id);
        if ($entry instanceof OutgoingInvoiceEntry) {
            try {
                $entry->update($request->all());
                $entry = new EntryResource($entry);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update outgoing invoice entry', 'data' => $e->getMessage()], 400);
            }
        }

        return $entry;
    }

    public function destroy(Request $request, $invoiceId, $id)
    {
        $entry = $this->entry($request, $id);
        if ($entry instanceof OutgoingInvoiceEntry) {
            $entry = $entry->delete() ? 1 : 0;
        }
        return $entry;
    }

    private function entries(Request $request)
    {
        return $request->attributes->get('_outgoing_invoice')->entries();
    }

    private function entry(Request $request, $id)
    {
        try {
            $entry = $this->entries($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No outgoing invoice entry found'], 400);
        }
        return $entry;
    }


}