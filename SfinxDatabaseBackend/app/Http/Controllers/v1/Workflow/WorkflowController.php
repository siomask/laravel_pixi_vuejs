<?php

namespace App\Http\Controllers\v1\Workflow;

use App\Http\Controllers\Controller;
use App\Models\Common\ProjectLead;
use App\Models\Contact\Contact;
use App\Models\Contact\ContactType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Resources\v1\Workflow\Task\TaskResource;
use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use App\Models\Workflow\Task\WorkflowTask;
use App\Models\Workflow\WorkflowFunctionGroup;
use App\Http\Resources\v1\Workflow\ConstructionSiteWorkflowTaskResource;
use Validator;
use App\Http\Resources\v1\Contact\ContactResource;
class WorkflowController extends Controller
{

    public function index(Request $request)
    {
        $constractionSits = [];
        $conts=[];
        $constructionSites = auth()->user()->settings->workflowConstructionSites()->with('workflowTasks')->get();
            foreach ($constructionSites as $site) {
                $constractionSits[] = $site->id;
                if (isset($site['contact'])) $conts[] = $site['contact']->id;
            }

//        $allSites = ContactResource::collection(Contact::whereNotIn('id', $conts)
//            ->whereIn('type_id', ContactType::$TYPES)->get());

        return response([
            'projectLeaders'=>ProjectLead::leaders($constractionSits),
            'tasks'=>TaskResource::parseItemsWithParent(WorkflowTask::all()),
            'function_groups'=>FunctionGroupResource::collection(WorkflowFunctionGroup::with('workflowTasks')->get()),
            'selected_construction_sites'=> ConstructionSiteWorkflowTaskResource::collection($constructionSites),
//            'all_construction_sites'=> $allSites
        ]);

    }



}