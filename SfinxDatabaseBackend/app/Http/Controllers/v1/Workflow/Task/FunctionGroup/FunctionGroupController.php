<?php

namespace App\Http\Controllers\v1\Workflow\Task\FunctionGroup;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use App\Models\Workflow\Task\WorkflowTask;
use Validator;

class FunctionGroupController extends Controller
{


    public function store(Request $request)
    {
        if($this->task($request)->where('workflow_function_group_id',$request->id)->count()>0){
            return new FunctionGroupResource($this->task($request)->where('workflow_function_group_id',$request->id)->first());
        }else{
            $this->task($request)->attach($request->id,[]);
            return new FunctionGroupResource($this->task($request)->where("workflow_function_groups.id", "=", $request->id)
                ->orderBy('id', 'desc')->first());
        }



    }

    private function task($request){
        return $request->attributes->get('_workflow_task')->workflowFunctionGroups();
    }

    public function destroy(Request $request, $taskId, $funcGroupId)
    {
        return $this->task($request)->detach($funcGroupId)? 1 : 0;
    }


}