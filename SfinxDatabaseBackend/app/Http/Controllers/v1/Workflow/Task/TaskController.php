<?php

namespace App\Http\Controllers\v1\Workflow\Task;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Resources\v1\Workflow\Task\TaskResource;
use App\Models\Workflow\Task\WorkflowTask;
use Mockery\Exception;
use Validator;

class TaskController extends Controller
{

    public function index(Request $request)
    {

        return TaskResource::parseItemsWithParent(WorkflowTask::all());

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:45|required',
            'description' => 'max:191'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {

            $createItem=[];
            foreach ($request->all() as $key => $value) {
                switch ($key) {
                    case 'parent_id':
                        {
                            $createItem['parent_workflow_task_id'] = $value;
                            break;
                        }
                        default:{
                            $createItem[$key]=$value;
                        }
                }
            }
            $task = WorkflowTask::create($createItem);
            if(isset($request->functionGrouopsid))$this->applyFunctionGroup($request->functionGrouopsid,$task);
            return new TaskResource($task);
        }

    }

    private function applyFunctionGroup($functionGrouopsid,$task){
        $allGroupsFromTask=$task->workflowFunctionGroups()->get()->toArray();
        foreach ($functionGrouopsid as $k){
            $shouldAdd =-1;
            foreach ($allGroupsFromTask as $key=>$item){
                if($item['pivot']['workflow_function_group_id'] ==$k){
                    $shouldAdd = $key;
                    break;
                }
            }
            if($shouldAdd>-1){
                array_splice($allGroupsFromTask, $shouldAdd, 1);

            }else{
                $task->workflowFunctionGroups()->attach($k);
            }


        }
        foreach ($allGroupsFromTask as  $item){
            $task->workflowFunctionGroups()->detach($item['pivot']['workflow_function_group_id']);
        }
    }
    public function update(Request $request, $id)
    {
        $updates = [];
        $validate = [];

        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'name':
                    {
                        $validate['name'] = 'bail|max:45';
                        $updates['name'] = $request->name;
                        break;
                    }
                case 'description':
                    {
                        $validate['description'] = 'max:255';
                        $updates['description'] = ($request->description);
                        break;
                    }
                case 'parent_id':
                    {
                        $updates['parent_workflow_task_id'] = ($request->parent_id);
                        break;
                    }
                case 'sequence':
                    {
                        $validate['sequence'] = 'integer';
                        $updates['sequence'] = ($request->sequence);
                        break;
                    }
            }
        }


        try {
            $task = WorkflowTask::findOrFail($id);

//               return  response()->json(['message' => 'No Workflow Task found!!!','data'=>$task->workflowFunctionGroups()->get()->toArray()], 400);
            if(isset($request->functionGrouopsid))$this->applyFunctionGroup($request->functionGrouopsid,$task);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Workflow Task found!!!','data'=>$e->getMessage()], 400);
        }
        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            if (count($updates) > 0) {
                if (isset($request->sequence)) {
                    if($task->parent_workflow_task_id !=$updates['parent_workflow_task_id']){
                        WorkflowTask::where([
                            ['parent_workflow_task_id', $task['parent_workflow_task_id']],
                            ['sequence', '>=', $task['sequence']]
                        ])->decrement('sequence');
                        WorkflowTask::where([
                            ['parent_workflow_task_id', $updates['parent_workflow_task_id']],
                            ['sequence', '>=', $updates['sequence']]
                        ])->increment('sequence');
                    }else{
                        if ($task['sequence'] != $updates['sequence']) {
                            if ($task['sequence'] < $updates['sequence']) {
                                WorkflowTask::where([
                                    ['parent_workflow_task_id', $task['parent_workflow_task_id']],
                                    ['sequence', '>', $task['sequence']],
                                    ['sequence', '<=', $updates['sequence']]
                                ])->decrement('sequence');
                            } else {
                                WorkflowTask::where([
                                    ['parent_workflow_task_id', $task['parent_workflow_task_id']],
                                    ['sequence', '<', $task['sequence']],
                                    ['sequence', '>=', $updates['sequence']]
                                ])->increment('sequence');
                            }
                        }
                    }

                }
                $task->update($updates);
            }
            return new TaskResource($task);
        }

    }

    public function destroy(Request $request, $id)
    {
        try {
            $task = WorkflowTask::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Workflow Task found!!!'], 400);
        }
        return $task->delete() ? 1 : 0;
    }


}