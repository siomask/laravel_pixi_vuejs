<?php

namespace App\Http\Controllers\v1\Workflow\ConstructionSite\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Workflow\Task\ConstructionSiteTaskResource;
use App\Http\Resources\v1\Workflow\Task\TaskResource;
use App\Models\Workflow\Task\WorkflowTask;
use App\Models\Workflow\ConstructionSiteWorkflowTask;
use Carbon\Carbon;
use Validator;

class TaskController extends Controller
{

    public function index(Request $request)
    {

//        return TaskResource::collection($this->siteWorkFlowTasks($request)->withPivot(['start', 'end', 'task_status', 'id'])->paginate(15));
        return ConstructionSiteWorkflowTask::paginate(15);

    }

    private function siteWorkFlowTasks(Request $request)
    {
        return $request->attributes->get('_workflow_construction_site')->workflowTasks();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'task_id' => 'required|integer',
//            'start' => 'required|date|before_or_equal:end',
//            'end' => 'required|date|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $this->checkStatus($request);
            $this->siteWorkFlowTasks($request)->attach($request->task_id, self::availableEdit($request));
//            var_dump($this->siteWorkFlowTasks($request)->where("workflow_tasks.id", "=", $request->workflow_task_id)->withPivot(['start', 'end', 'task_status', 'id'])->orderBy('id', 'asc')->first());
//            return new TaskResource($this->siteWorkFlowTasks($request)->where("workflow_tasks.id", "=", $request->workflow_task_id)
//                ->withPivot(['start', 'end', 'task_status', 'id'])->orderBy('id', 'desc')->first());
            return new ConstructionSiteTaskResource(ConstructionSiteWorkflowTask::orderBy('id', 'desc')->first());
        }
    }

    private function checkStatus($request)
    {
        if ($request->task_status == 2) {
            $request->start = Carbon::now()->toDateTimeString();
            if ($request->end < $request->start) $request->end = $request->start;
        } else if ($request->task_status == 3 || $request->task_status == 4) {
            $request->end = Carbon::now()->toDateTimeString();
            if ($request->end < $request->start) $request->start = $request->end;
        }

    }

    static function availableEdit($request){
        $res = [];
        foreach ($request->all() as $key=>$value){
            switch ($key){
                case 'task_id':{
                    $res['workflow_task_id']=$value;
                    break;
                }
                case 'end':
                case 'start':
                case 'description':
                case 'task_status':
                case 'task_status':{
                    $res[$key]=$value;
                    break;
                }
            }
        }
        return $res;
    }
    public function update(Request $request, $siteId, $id)
    {
        try {
            $site = ConstructionSiteWorkflowTask::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Workflow Construction Site  Task found!!!'], 400);
        }
//        $this->siteWorkFlowTasks($request)->updateExistingPivot($id, $updates);
        $this->checkStatus($request);
        $site->update(self::availableEdit($request));
//        return new TaskResource($this->siteWorkFlowTasks($request)->withPivot(['start', 'end', 'task_status', 'id'])->findOrFail($id) );
        return new ConstructionSiteTaskResource($site);
    }

    public function destroy(Request $request, $siteId, $id)
    {
        try {
            $site = ConstructionSiteWorkflowTask::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Workflow Construction Site Task found!!!'], 400);
        }
        return $site->delete() ? 1 : 0;
    }
}