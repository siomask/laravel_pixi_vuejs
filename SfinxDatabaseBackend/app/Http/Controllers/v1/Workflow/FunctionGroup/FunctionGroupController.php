<?php

namespace App\Http\Controllers\v1\Workflow\FunctionGroup;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Workflow\FunctionGroup\FunctionGroupResource;
use App\Models\Workflow\WorkflowFunctionGroup;
use Validator;
use Illuminate\Http\Request;

class FunctionGroupController extends Controller
{

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'max:45|required',
            'description' => 'max:191'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $item = WorkflowFunctionGroup::create($request->all());
            return new FunctionGroupResource($item);
        }

    }

    public function update(Request $request, $id)
    {
        $updates = [];
        $validate = [];

        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'name':
                    {
                        $validate[$key] = 'bail|max:45';
                        $updates[$key] = $value;
                        break;
                    }
                case 'description':
                    {
                        $validate[$key] = 'max:191';
                        $updates[$key] = $value;
                        break;
                    }
                case 'sequence':
                    {
                        $validate[$key] = 'integer';
                        $updates[$key] = $value;
                        break;
                    }
            }
        }

        $item = $this->group($id);
        if ($item instanceof WorkflowFunctionGroup) {
            $oldSequence = $item->sequence;
            $validator = Validator::make($request->all(), $validate);
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            } else {
                if (count($updates) > 0) {

                    $item->update($updates);
                    if ($oldSequence > $item->sequence) {//move up
                        WorkflowFunctionGroup::where([
                            ['sequence', '<=', $oldSequence],
                            ['sequence', '>=', $item->sequence],
                        ])->whereNotIn('id', [$id])->increment('sequence');
                    } else if ($oldSequence < $item->sequence) {//move down
                        WorkflowFunctionGroup::where([
                            ['sequence', '>=', $oldSequence],
                            ['sequence', '<=', $item->sequence],
                        ])->whereNotIn('id', [$id])->decrement('sequence');
                    }
                }
                return new FunctionGroupResource($item);
            }
        }
        return $item;
    }

    private function group($id)
    {
        try {
            return WorkflowFunctionGroup::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Workflow Function Group found!!!'], 400);
        }
    }

    public function destroy(Request $request, $id)
    {
        $item = $this->group($id);
        if ($item instanceof WorkflowFunctionGroup) {
            return $item->delete() ? 1 : 0;
        }
        return $item;
    }

}