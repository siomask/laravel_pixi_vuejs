<?php

namespace App\Http\Controllers\v1\Common\Rights;

use App\Models\Common\Right;
use App\Http\Resources\v1\User\UserGroupResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Common\RightsResource;
use Validator;
use App\Models\Common\ProjectLead;

class RightsController extends Controller
{

    public function index()
    {
        return RightsResource::collection(Right::all());
    }

    public function store(Request $request)
    {
        try {
            $right = new RightsResource(Right::create($request->all()));
        } catch (\Exception $e) {
            return response()->json(['message' => 'Can`t create new right', 'errors' => $e->getMessage()], 400);
        }
        return $right;
    }

    public function update(Request $request, $id)
    {
        $right = $this->checkRight($id);
        if ($right instanceof Right) {
            try {
                $right->update($request->all());
                $right = new RightsResource($right);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Can`t update right', 'errors' => $e->getMessage()], 400);
            }

        }
        return $right;
    }

    public function destroy(Request $request, $id)
    {
        $right = $this->checkRight($id);
        if ($right instanceof Right) {
            return $right->delete()?1:0;
        }
        return $right;
    }

    private function checkRight($id)
    {
        try {
            $right = Right::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'no right found', 'errors' => $e->getMessage()], 400);
        }
        return $right;
    }

}
