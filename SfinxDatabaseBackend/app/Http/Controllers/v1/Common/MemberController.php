<?php

namespace App\Http\Controllers\v1\Common;

use App\Models\Common\Member;
use App\Models\Common\AbsenceType;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use App\Models\Team\TeamMember\TeamPeriodTeamMember;
use App\Http\Resources\Common\MemberResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return MemberResource::collection(Member::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $member = Member::create($request->all());
            return new MemberResource($this->updateAbscenes($request, $member));
        }


    }


    private function updateAbscenes(Request $request, $member)
    {
        $types =  AbsenceType::types();
        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'tempAbsences':
                    {
                        $value = (array)$value;
                        $updates = array_filter($value, function ($k, $el) {
                            return !empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);


                        $newItems = array_filter($value, function ($k, $el) {
                            return empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);

                        $member->absences()->whereNotIn('id', array_map(function ($el) {
                            return $el['id'];
                        }, $updates))->where([
                            ['absence_type_id','=',$types[0]->id]
                        ])->delete();


                        foreach ($updates as $item) {
                            $member->absences()->findOrFail($item['id'])->update($item);
                        }


                        foreach ($newItems as $item) {
                            $item['absence_type_id'] = $types[0]->id;//TODO: change it to use id from db
                            $member->absences()->create(
                                [
                                    'absence_type_id' => 1,
                                    'start' => $item['start'],
                                    'end' => $item['end']
                                ]
                            );
                        }
                        break;
                    }
                case 'tempHolidays':
                    {
                        $value = (array)$value;
                        $updates = array_filter($value, function ($k, $el) {
                            return !empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);


                        $newItems = array_filter($value, function ($k, $el) {
                            return empty($el['id']);
                        }, ARRAY_FILTER_USE_BOTH);

                        $member->absences()->whereNotIn('id', array_map(function ($el) {
                            return $el['id'];
                        }, $updates))->where([
                            ['absence_type_id','=',$types[1]->id]
                        ])->delete();


                        foreach ($updates as $item) {
                            $member->absences()->findOrFail($item['id'])->update($item);
                        }


                        foreach ($newItems as $item) {
                            $item['absence_type_id']=$types[1]->id;//TODO: change it to use id from db
                            $member->absences()->create($item);
                        }
                        break;
                    }
            }

        }

        return Member::findOrFail($member->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            $member = Member::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Member found!!!'], 400);
        }
        $member->update($request->all());
        return new MemberResource($this->updateAbscenes($request, $member));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $mm = Member::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Member found!!!'], 400);
        }
        return $mm->delete() ? 1 : 0;
    }


    public function allPlanning(Request $request, $id){

        try{
            return ConstructionSite::plannings($request->query('month'),$id);
        }catch (\Exception $e){
            return response()->json(["message"=>"Bad request","error"=>$e->getMessage()],400);
        }

    }
}
