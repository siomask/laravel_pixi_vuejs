<?php

namespace App\Http\Controllers\v1\Common\User\ProjectLeader;

use App\Models\Common\User;
use App\Http\Resources\v1\User\UserResource;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Common\ProjectLead;
use Illuminate\Support\Facades\DB;

class ProjectLeaderController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $userId)
    {
        $results = [];
        $sites = array_map(function ($e) {
            return ['construction_site_id' => $e['construction_site_id']];
        },
            ProjectLead::where('user_id', $userId)
                ->whereNotIn(
                    'construction_site_id',
                    array_map(function ($e) {
                        return $e['construction_site_id'];
                    }, auth()->user()->settings->settings()->get()->toArray())
                )/*->whereHas('constructionSite')*/
                ->get()->toArray()
        );

        //restore deleted
        $leaders = ProjectLead::with([
            'user',
            'constructionSite' => function ($q) {
                $q->withTrashed()->with([
                    'contact']);
            }
        ])->where('user_id', $userId)->get();

        foreach ($leaders as $item) {
//            DB::table('construction_sites')->where('id', $item->construction_site_id)->update(['deleted_at' => null]);
            $results[] = $item->construction_site_id;
            if (auth()->user()
                    ->settings->settings()->where([
                        ['construction_site_id', $item->construction_site_id],
                        ['construction_site_user_setting_type_id', 2]//:TODO workflow
                    ])->count() == 0) {
                $results[] = auth()->user()
                    ->settings->settings()->create(
                        [
                            'construction_site_id' => $item->construction_site_id,
                            'construction_site_user_setting_type_id' => 2
                        ]
                    );
            }
            if (auth()->user()
                    ->settings->settings()->where([
                        ['construction_site_id', $item->construction_site_id],
                        ['construction_site_user_setting_type_id', 1]
                    ])->count() == 0) {
                $results[] = auth()->user()
                    ->settings->settings()->create(
                        [
                            'construction_site_id' => $item->construction_site_id,
                            'construction_site_user_setting_type_id' => 1
                        ]
                    );
            }
        }

        //create new
//        foreach ($sites as $site) {
//            $results[] = auth()->user()
//                ->settings->settings()->create($site);
//        }
        return ProjectLead::mapLeaders($leaders);

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return auth()->user()
            ->settings->settings()->whereIn('construction_site_id', explode(",", $request->query('sites')))->forceDelete() ? 1 : 0;
    }


}
