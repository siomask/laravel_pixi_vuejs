<?php

namespace App\Http\Controllers\v1\Common\User\Contact\ConstructionSite;

use App\Models\Contact\Contact;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ContactConstructionSitesResource;
use App\Http\Resources\v1\Contact\ContactResource;
use Illuminate\Support\Facades\DB;

class ConstructionSiteController extends Controller
{

    private function site(Request $request, $id)
    {
        return ConstructionSite::findOrFail($id);
    }
    public function destroy(Request $request, $contactId, $id)
    {
        try {
            $site = $this->site($request, $id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Site found!!!'], 400);
        }
        return auth()->user()->settings->allConstructionSites()
//            ->wherePivot('construction_site_user_setting_type_id', '=', $request->siteType['id'])
            ->wherePivot(
            'construction_site_id','=',$site->id
        )->detach() ? 1 : 0;
    }

    public function store(Request $request)
    {
        try {
            $site = ConstructionSite::withTrashed()->where("contact_id", $request->id);
            $site->restore();
            $site = $site->first();
            if (empty($site)) {
                $site = ConstructionSite::create([
                    "contact_id" => $request->id
                ]);
            }

        } catch (\Exception $e) {
            $site = ConstructionSite::create([
                "contact_id" => $request->id
            ]);
        } finally {
            $construction_type = $request->siteType['id'];
            if (auth()->user()->settings->allConstructionSites()->where([
                    ['construction_site_user_setting_type_id', '=', $construction_type],
                    ['construction_site_id', '=', $site->id]
                ])->count() == 0) {
                auth()->user()->settings->allConstructionSites()->attach(
                    $site->id,
                    [
                        'construction_site_user_setting_type_id' => $construction_type
                    ]
                );
            }
        }
        return  new ContactConstructionSitesResource($site);

    }
}