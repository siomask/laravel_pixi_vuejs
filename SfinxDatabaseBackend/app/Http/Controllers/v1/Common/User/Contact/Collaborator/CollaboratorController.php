<?php

namespace App\Http\Controllers\v1\Common\User\Contact\Collaborator;

use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Contact\Client\ClientResource;
use Validator;
use App\Http\Resources\v1\Contact\Client\Collaborator\CollaboratorResource;

class CollaboratorController extends Controller
{


    public function store(Request $request)
    {
        $collaborators = auth()->user()
            ->settings
            ->selectedCollaborators()->wherePivot(
                'contact_id','=',$request->id
            )->get();
        if ($collaborators->count() == 0) {
            auth()->user()
                ->settings
                ->selectedCollaborators()->attach($request->id);
        }
        return response()->json(['message'=>'Collaborattors of contact was added']);

    }

    public function destroy($id)
    {
        return auth()->user()
            ->settings
            ->selectedCollaborators()->wherePivot(
                'contact_id','=',$id
            )->detach() ? 1 : 0;
    }
}
