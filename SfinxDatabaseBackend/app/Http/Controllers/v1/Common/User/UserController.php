<?php

namespace App\Http\Controllers\v1\Common\User;

use App\Models\Common\User;
use App\Http\Resources\v1\User\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Common\ProjectLead;
use App\Http\Resources\v1\Planner\DateRangeResource;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UserResource::collection(User::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'max:255',
            'name' => 'max:255',
            'username' => 'bail|required|unique:users|max:255',
            'password' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $request->password = bcrypt($request->password);
            return new UserResource(User::create($request->all()));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = [];
        $validate = [];

        foreach ($request->all() as $key => $value) {
            if ($key == 'username') {
                $validate['username'] = 'bail|max:255|unique:users,username,' . $id;
            } else if ($key == 'password') {
                $validate['password'] = 'max:255';
                $request['password'] = bcrypt($request->password);
            }
        }

        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            try {
                $user = User::findOrFail($id);
            } catch (\Exception $e) {
                return response()->json(['message' => 'No User found!!!', 'data' => $e->getMessage()], 400);
            }

            $user->update($request->all());
            return new UserResource($user);
        }


    }

    public function settings(Request $request)
    {

        $settings = auth()->user()->settings()->first();
        $settings->update($request->all());
        return new DateRangeResource($settings);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No User found!!!'], 400);
        }
        return $user->delete() ? 1 : 0;
    }

}
