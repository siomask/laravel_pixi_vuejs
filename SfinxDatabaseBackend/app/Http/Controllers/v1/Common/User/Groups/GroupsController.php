<?php

namespace App\Http\Controllers\v1\Common\User\Groups;

use App\Models\User\UserGroup;
use App\Http\Resources\v1\User\UserGroupResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Common\ProjectLead;

class GroupsController extends Controller
{

    public function index()
    {
        return UserGroupResource::collection(UserGroup::all());
    }

    public function store(Request $request)
    {
        try {
            $group = new UserGroupResource(UserGroup::create($request->all()));
        } catch (\Exception $e) {
            return response()->json(['message' => 'Couldn`t create user group', 'data' => $e->getMessage()], 400);
        }
        return $group;
    }

    public function update(Request $request, $id)
    {
        $group = $this->group($id);
        if ($group instanceof UserGroup) {
            try {
                $group->update($request->all());
                $group = new UserGroupResource($group);
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update user group found'], 400);
            }
        }
        return $group;
    }

    public function destroy(Request $request, $id)
    {
        $group = $this->group($id);
        if ($group instanceof UserGroup) {
            try {
                $group = $group->delete() ? 1 : 0;
            } catch (\Exception $e) {
                return response()->json(['message' => 'Couldn`t update user group found'], 400);
            }
        }
        return $group;
    }

    private function group($id)
    {
        try {
            $group = UserGroup::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No user group found'], 400);
        }
        return $group;
    }

}
