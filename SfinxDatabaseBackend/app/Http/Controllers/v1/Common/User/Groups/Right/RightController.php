<?php

namespace App\Http\Controllers\v1\Common\User\Groups\Right;

use App\Models\Common\Right;
use App\Http\Resources\v1\User\UserGroupResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Common\RightsResource;
use Validator;
use App\Models\Common\ProjectLead;

class RightController extends Controller
{

    public function index(Request $request)
    {
        return RightsResource::collection($this->rights($request)->get());
    }

    public function store(Request $request)
    {
        try {
            $right = new RightsResource(Right::findOrFail($request->id));
            $this->rights($request)->attach($request->id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Can`t add right', 'errors' => $e->getMessage()], 400);
        }
        return $right;
    }


    public function destroy(Request $request, $groupId, $id)
    {
        return $this->rights($request)->detach($id) ? 1 : 0;
    }

    private function rights(Request $request)
    {
        return $request->attributes->get('_userGroup')->rights();
    }

}
