<?php

namespace App\Http\Controllers\v1\Common\User\Self;

use App\Models\Common\User;
use App\Http\Resources\v1\User\UserResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Models\Common\ProjectLead;
use App\Http\Resources\v1\Planner\DateRangeResource;

class UserSelfController extends Controller
{

    public function update(Request $request, $id)
    {
        $validate = [];

        foreach ($request->all() as $key => $value) {
            if ($key == 'username') {
                $validate['username'] = 'bail|max:255|unique:users,username,' . $id;
            } else if ($key == 'password') {
                $validate['password'] = 'max:255';
                $request['password'] = bcrypt($request->password);
            }
        }

        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            try {
                $user = auth()->user();
            } catch (\Exception $e) {
                return response()->json(['message' => 'No User found!!!', 'data' => $e->getMessage()], 400);
            }

            $user->update($request->all());
            return new UserResource($user);
        }


    }



}
