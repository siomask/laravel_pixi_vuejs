<?php

namespace App\Http\Controllers\v1\Common\User\ConstructionSite;

use App\Models\Contact\Contact;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSitesResourceCollection;
use App\Http\Resources\v1\Contact\ContactResource;
use Illuminate\Support\Facades\DB;

class ConstructionSiteController extends Controller
{

    public function index(Request $request){
//        return ConstructionSitesResource::collection(ConstructionSite::all());
            $_name = $request->query('name');
            $_siteId = $request->query('siteId');
            $with = [
                'collaborators'
            ];
            $filters = [];
            if (!empty($_name)) {
                $filters['name'] = $_name;
            }
            if (!empty($_siteId)) {
                $filters['siteId'] = $_siteId;
            }

            if (!empty($_name) || !empty($_siteId)) {
                $_result = ConstructionSite::with($with);
                if (!empty($_siteId)) {
                    $_result = $_result->where([['id', '=', $_siteId]]);
                }
                if (!empty($_name)) {
                    $like = "%" . $_name . "%";
                    $with['contact'] = function ($q) use ($like) {
                        return $q->where('first_name', 'LIKE', $like)
                            ->orWhere('first_name', 'LIKE', $like);
                    };
                }
                $_result = $_result->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
            } else {
                $_result = ConstructionSite::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
            }
            return new ConstructionSitesResourceCollection($_result);
//        return ConstructionSite::paginate(Controller::$MAX_PAGINATE_ITEMS);
    }
    public function reset()
    {
        $user = auth()->user();
        $restore_sites = [];
        $_restore_sites = [];
        $sites = [];
        foreach ($user->projLeaders()->get() as $item) {
            $sites[] = $item->construction_site_id;
        }
        foreach ($user->estimators()->get() as $item) {
            $sites[] = $item->construction_site_id;
        }

        $userSites = [];
        foreach ($user->settings->workflowConstructionSites()->withTrashed()->get() as $item) {
            if (array_search($item->id, $sites) === false) {
                $user->settings->workflowConstructionSites()
                    ->wherePivot(
                        'construction_site_id','=',$item->id
                    )->detach() ? 1 : 0;
//                $item->delete();//remove which don`t belong to current user
            } else {

                DB::table('construction_sites')->where('id', $item->id)->update(['deleted_at' => null]);
            }
            $userSites[] = $item->id;
        }


        //add missed
        foreach ($sites as $site) {
            if (array_search($site, $userSites) === false) {
                $userSites[] = $site;
                $user->settings->workflowConstructionSites()->attach(
                    $site,
                    [
//                        'construction_site_id' => $site,
                        'construction_site_user_setting_type_id' => 2//only for workflow //:TODO
                    ]
                );
            }

        }


//        return response()->json([$user->settings->workflowConstructionSites()->withTrashed()->get()], 400);
        return response()->json(['message' => "user sites was updated"], 200);

    }

}