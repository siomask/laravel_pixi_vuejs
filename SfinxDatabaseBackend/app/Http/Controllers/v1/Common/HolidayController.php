<?php

namespace App\Http\Controllers\v1\Common;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Models\Common\ContractorType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Resources\v1\Common\HolidaysResource;
use App\Models\Common\Holiday;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        return HolidaysResource::collection(Holiday::all());
        $_name = $request->query('name');
        $with = [];
        $filters = [];
        if (!empty($_name)) {
            $filters['name'] = $_name;
        }

        if (!empty($_name)  ) {
            $_result= Holiday::with($with);
            if (!empty($_name)) {
                $like = "%" . $_name . "%";
                $_result = $_result->where('name', 'LIKE', $like)
                    ->orWhere('end', 'LIKE', $like)
                    ->orWhere('start', 'LIKE', $like);
            }
            $_result = $_result->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS)->appends($filters);
        } else {
            $_result = Holiday::with($with)->orderBy('id', 'desc')->paginate(Controller::$MAX_PAGINATE_ITEMS);
        }

        return  $_result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'start' => 'required|date|before_or_equal:end',
            'end' => 'required|date|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            return new HolidaysResource(Holiday::create($request->all()));
        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'start' => 'required|date|before_or_equal:end',
            'end' => 'required|date|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            try {
                $h = Holiday::findOrFail($id);
                $h->update($request->all());
            } catch (\Exception $e) {
                return response()->json(['message' => 'No Holiday found!!!'], 400);
            }

            return new HolidaysResource($h);
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $h = Holiday::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Holiday found!!!'], 400);
        }
        return $h->delete() ? 1 : 0;
    }
}
