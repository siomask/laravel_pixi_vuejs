<?php

namespace App\Http\Controllers\v1\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Common\ConstructionSiteSettingsTypeResource;
use App\Models\Common\ConstructionSiteUserSettingType;

class ConstructionSiteSettingTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConstructionSiteSettingsTypeResource::collection(ConstructionSiteUserSettingType::all());
    }


}
