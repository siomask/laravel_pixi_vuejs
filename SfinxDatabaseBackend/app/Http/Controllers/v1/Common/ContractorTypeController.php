<?php

namespace App\Http\Controllers\v1\Common;

use App\Http\Resources\Common\ConstructionTypeResource;
use App\Models\Common\ContractorType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ContractorTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConstructionTypeResource::collection(ContractorType::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'abbreviation' => 'bail|required|max:255',
            'name' => 'bail|required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            return new ConstructionTypeResource(ContractorType::create($request->all()));
        }


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updates = [];
        $validate=[];

        if (isset($request->abbreviation)) {
            $validate['abbreviation'] ='max:255';
            $updates['abbreviation'] = $request->abbreviation;
        }
        if (isset($request->name)) {
            $validate['name'] ='max:255';
            $updates['name'] =  ($request->name);
        }
        if (isset($request->sequence)) {
            $updates['sequence'] =  ($request->sequence);
        }


        try {
            $contractorType = ContractorType::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Contractor Type found!!!'], 400);
        }

        $oldSequence = $contractorType->sequence;
        $validator = Validator::make($request->all(), $validate);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            if (count($updates) > 0) {
                $contractorType->update($updates);
                if($oldSequence >$contractorType->sequence){//move up
                     ContractorType::where([
                        ['sequence','<=',$oldSequence],
                        ['sequence','>=',$contractorType->sequence],
                    ])->whereNotIn('id',[$id])->increment('sequence');
                }else if($oldSequence <$contractorType->sequence){//move down
                    ContractorType::where([
                        ['sequence','>=',$oldSequence],
                        ['sequence','<=',$contractorType->sequence],
                    ])->whereNotIn('id',[$id])->decrement('sequence');
                }
            }
            return new ConstructionTypeResource($contractorType);
        }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $contractorType = ContractorType::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Contractor Type found!!!'], 400);
        }
        return $contractorType->delete() ? 1 : 0;
    }
}
