<?php

namespace App\Http\Controllers\v1\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Planner\ConstructionSiteStatusResource;
use App\Models\Planner\ConstructionSite\ConstructionSiteStatus;

class ConstructionSiteStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ConstructionSiteStatusResource::collection(ConstructionSiteStatus::all());
    }


}
