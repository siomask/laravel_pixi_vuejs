<?php

namespace App\Http\Controllers\v1\Planner\ConstructionSite\Collaborator\Planning;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team\TeamMember\TeamMemberPlanningEntry;
use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSite\Collaborator\Planning\PlanningResource;


class PlanningController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (empty($request['end']) || empty($request['start'])) return response(['state' => 1, "msg" => "Required data missed"]);

        /*if (Planning::checkIfInNotPlanning($request['start'], $request['end'],$request->attributes->get('_construction_site_collaborator'))) {
            return ([
                'state' => 1,
                "msg" => "Can`t plane in holiday or out of branch daterange for branch(" . $request['collaboratorId'] . ") from(" . $request['start'] . ") 
                to (" . $request['end'] . ")"]);
        }*/

        $planningData = $this->planning($request)->get()->toArray();

        $dateRange = Planning::getDateRange($planningData, $request, true);
        if (!empty($dateRange)) {

            $createdPlanned = $this->planning($request)->create([
                'description_to_do' => $request['description_to_do'],
                'description' => $request['description'],
                'start' => $dateRange['start']->toDateString(),
                'end' => $dateRange['end']->toDateString()
            ]);
            TeamMemberPlanningEntry::updatePlannings($dateRange, $createdPlanned->id);
            return new PlanningResource($createdPlanned);
        } else {
            return response(['state' => 1, "msg" => "No need To create planned data"]);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $siteId, $colId, $id)
    {
        $res = $this->updateDateRange($request, $id);
        $_planning = $this->planning($request)->findOrFail($id);
        $_planning->update([
            'description_to_do' => $request['description_to_do'],
            'description' => $request['description'],
        ]);
        $res['data']=new PlanningResource($_planning);
        return response($res);
    }

    private function planning($request)
    {
        return $request->attributes->get('_construction_site_collaborator')->data();
    }

    private function updateDateRange($request, $id)
    {
        if (empty($request['end']) || empty($request['start'])) return response()->json(['state' => 1, "msg" => "Required data missed"], 400);
        try {
            $plannedD = $this->planning($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Planning data found!!!'], 400);
        }
//        $startOld = Carbon::parse($plannedD->start);
//        $startNew = Carbon::parse($request['start']);
        $daysUpdateTime = $request['daysUpdateTime'];//($startOld->gt($startNew)?-1:1)*$startOld->diffInDays($startNew);

        return Planning::updateDateRange([
            'ignore' => [],
            'merge' =>  ($request->merge),
            'plannedD' => $plannedD,
            'daysUpdateTime' => $daysUpdateTime,
            'branch' => $request->attributes->get('_construction_site_collaborator'),
            'collaboratorId' => $request->attributes->get('_construction_site_collaborator')->id,
            'start' => Carbon::parse($request['start'])->toDateTimeString(),
            'end' => Carbon::parse($request['end'])->toDateTimeString()
        ], $id);

    }


    public function destroy(Request $request, $siteId, $colId, $id)
    {
        try {
            $data = $this->planning($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Planning data found!!!'], 400);
        }

        $date = Carbon::parse($request->input('date'))->toDateTimeString();
        $isDropAll = !empty($request->input('dropall'));
        $isSplit = !empty($request->input('isSplit'));

        if (
            Carbon::parse($data->start)->gt(Carbon::parse($date)) ||
            Carbon::parse($data->end)->lt(Carbon::parse($date))
        ) return array('msg' => 'incorrect date range', 'type' => 0);

        if ($isSplit) {
            return $this->splitGroup($data, $request);
        } else if ($isDropAll || $data->start == $data->end) {
            $data->delete();
            return array(
                'msg' => 'group elements was removed,start or end',
                'type' => 1
            );
        } else if (Carbon::parse($data->start)->eq(Carbon::parse($date))) {
            $data->start = Carbon::parse($date)->addDays(1);

            return array(
                'msg' => 'group elements was updated,start',
                'type' => 2,
                'v' => TeamMemberPlanningEntry::dropPlanning($id, Carbon::parse($request['date']), $data->start, 1),
                'v1' => $data->save()
            );
        } else if (Carbon::parse($data->end)->eq(Carbon::parse($date))) {
            $data->end = Carbon::parse($date)->subDays(1);

            return array(
                'msg' => 'group elements was updated,end',
                'type' => 3,
                'v' => TeamMemberPlanningEntry::dropPlanning($id, $data->end, Carbon::parse($request['date']), 2),
                'v1' => $data->save()
            );
        } else {//remove in the middle or split
            return $this->splitGroup($data, $request, true);
        }
    }

    private function splitGroup($data, $request, $withRemove = false)
    {

        $date = Carbon::parse($request->input('date'))->toDateTimeString();
        $isSplit = !empty($request->input('isSplit'));

        $data1 = clone $data;
        $data2 = clone $data;
        $res_groups = array();

        unset($data1->id);
        unset($data2->id);

        if ($isSplit) {
            if (Carbon::parse($date)->eq(Carbon::parse($data1->end))) {//split from end
                $data1->end = Carbon::parse($date)->subDays(1)->toDateTimeString();
                if (Carbon::parse($data1->end)->lt(Carbon::parse($data1->start))) $data1->end = $data1->start;

                $data2->start = $date;
                if (Carbon::parse($data2->end)->lt(Carbon::parse($data2->start))) $data2->end = $data2->start;

            } else {//split from begin or split somewhere in the middle
                $data1->end = $date;
                if ($data1->end < $data1->start) $data1->end = $data1->start;

                $data2->start = Carbon::parse($date)->addDays(1)->toDateTimeString();
                if ($data2->end < $data2->start) $data2->end = $data2->start;
            }
        } else {
            $data1->end = Carbon::parse($date)->subDays(1)->toDateTimeString();
            $data2->start = Carbon::parse($date)->addDays(1)->toDateTimeString();
        }

        $res_groups[] = $this->planning($request)->create($data1->toArray());
        $res_groups[] = $this->planning($request)->create($data2->toArray());

        $updates = TeamMemberPlanningEntry::updatePlanningMembers($res_groups, $data, $isSplit ? 0 : $date);
        $data->delete();
        return array(
            'msg' => 'group elements were splited',
            'groups' => PlanningResource::collection(collect($res_groups)),
            'type' => 4,
            'updates' => $updates
        );
    }

    public function move(Request $request, $siteId, $colId, $id)
    {
        if (empty($request->daysToMove)) {
            $res = (["state" => 1, "msg" => "Missed required data or nothing to move"]);
        } else {
            $daysToMove = $request->daysToMove;

            $plannedD = $this->planning($request)->findOrFail($id);

            $res = (["state" => 2,
                "data" => Planning::onMove([
                    'ignore' => [],
                    'plannedD' => $plannedD,
                    'daysUpdateTime' => $daysToMove,
                    'branch' => $request->attributes->get('_construction_site_collaborator'),
                    'collaboratorId' => $request->attributes->get('_construction_site_collaborator')->id,
                    'start' => Carbon::parse($plannedD['start'])->addDays($daysToMove)->toDateTimeString(),
                    'end' => Carbon::parse($plannedD['end'])->addDays($daysToMove)->toDateTimeString()
                ]),
                'id' => $id]);

        }

        return $res;
    }

}