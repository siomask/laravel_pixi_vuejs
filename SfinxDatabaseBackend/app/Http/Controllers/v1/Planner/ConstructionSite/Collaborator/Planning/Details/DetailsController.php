<?php

namespace App\Http\Controllers\v1\Planner\ConstructionSite\Collaborator\Planning\Details;

use App\Models\Team\TeamMember\TeamMemberPlanningEntry;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Team\Team;
use App\Models\Team\CollaboratorTeamPlanningEntries;
use App\Models\Team\TeamPeriod\TeamPeriod;

use Carbon\Carbon;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;
use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use Mockery\Exception;
use App\Http\Resources\v1\Team\TeamPlannedMemberResource;
use Illuminate\Support\Facades\Storage;

class DetailsController extends Controller
{

    public function index(Request $request, $siteId, $branchId, $planningId)
    {

        return $this->activeTeams($request, $planningId);
    }

    public function activeTeams(Request $request, $planningId = -1)
    {
        $from = $request->input('start');
        $to = $request->input('end');

        $planningDAta = $request->attributes->get('_construction_site_collaborator_data');
        return response([
            'data' => Team::parseActiveTeamMembers(
                Team::loadAllPlannedTeamsMembers($planningId, $from, $to, $request->attributes->get('_construction_site_collaborator'))
            ),
            'planningData' => $planningDAta,
            'planningDayData' => isset($planningDAta) ? $planningDAta->messages()->where('date', Carbon::parse($from))->first() : null
        ]);
    }

    public function store(Request $request, $siteId, $branchId, $planningId)
    {

        $pivotTeamPlanMemb = $this->validateDetail($request, $siteId, $branchId, $planningId);
        if (!isset($pivotTeamPlanMemb->id)) return $pivotTeamPlanMemb;

        $_team_period_team_member_id = isset($request->teamPeriodId) ? $request->teamPeriodId : $request->team_period_team_member_id;
        $date = isset($request->day) ? $request->day : $request->date;
        $date = Carbon::parse($date)->toDateTimeString();
        $item = false;/*$pivotTeamPlanMemb->planningMembers()->where([
            'date' => $date,
            'team_period_team_member_id' => ($_team_period_team_member_id)
        ])->first()*/;

        if (!empty($item)) {
            $item->update([
                'sequence' => $request->sequence,
            ]);
            return new TeamPlannedMemberResource($item);
        } else {
            return new TeamPlannedMemberResource($pivotTeamPlanMemb->planningMembers()->create([
                'date' => $date,
                'sequence' => $request->sequence,
                'team_period_team_member_id' => ($_team_period_team_member_id)
            ]));
        }
    }

    public function update(Request $request, $siteId, $branchId, $planningId, $detailId)
    {

        $pivotTeamPlanMemb = $this->validateDetail($request, $siteId, $branchId, $planningId, $detailId);
        if (!isset($pivotTeamPlanMemb->id)) return $pivotTeamPlanMemb;
        try {
            $item = $pivotTeamPlanMemb->planningMembers()->findOrFail($detailId);
            $updates =[];
            foreach ($request->all() as $key => $value) {
                switch ($key) {
                    case 'sequence':
                    case 'hours':
                        {
                            $updates[$key]=$value;
                            break;
                        }
                }
            }
            $item->update($updates);
            return new TeamPlannedMemberResource($item);
        } catch (Exeption $e) {
            return response()->json(['message' => 'No detail found!!!'], 400);
        }

    }

    private function validateDetail(Request $request, $siteId, $branchId, $planningId, $detailId = null)
    {
        $_team_period_team_member_id = isset($request->teamPeriodId) ? $request->teamPeriodId : $request->team_period_team_member_id;
        $_team_id = $request->teamId;
        $date = isset($request->day) ? $request->day : $request->date;

        if (empty($_team_id) || empty($_team_period_team_member_id) || empty($date)) {
            return response()->json(['message' => 'Missed required data'], 400);
        }

        if (Planning::checkIfInNotPlanning($date, $date, $request->attributes->get('_construction_site_collaborator'))) {
            return [
                'state' => 2,
                "msg" => "Can`t plane member in holiday day(" . Carbon::parse($date)->toDateTimeString() . ")"
            ];
        }

        $date = Carbon::parse($date)->toDateTimeString();
        if (TeamPeriod::with('periodMembers.member.absences')
                ->whereHas('periodMembers', function ($q) use ($_team_period_team_member_id) {
                    $q->where('id', $_team_period_team_member_id);
                })->whereHas('periodMembers.member.absences', function ($q) use ($date, $request) {
                    $q->where([
                        ['team_member_id', $request['teamMemberId']],
                        ['start', '<=', $date],
                        ['end', '>=', $date]
                    ]);
                })->count() > 0) return response()->json(['msg' => 'can`t plane in sick day', 'state' => 3]);

        if (isset($request['hard'])) {//from history

        } else {
            $list = Collaborator::find($branchId)->contact()->with([
                'teams.period.periodMembers.plannings'
            ])->whereHas('teams.period.periodMembers.plannings', function ($q) use ($request, $_team_period_team_member_id, $date, $detailId) {
                $q->where([
                    ['date', $date],
                    ['team_period_team_member_id', $_team_period_team_member_id],
                    ['sequence', $request->sequence],
                ])->whereNotIn('id', isset($detailId) ? [$detailId] : []);
            });
            if ($list->count() > 0) return response()->json(['msg' => 'already in use', 'state' => 2]);
        }

        $pivotTeamPlanMemb = CollaboratorTeamPlanningEntries::where([
            'team_id' => $_team_id,
            'collaborator_planning_entry_id' => $request->attributes->get('_construction_site_collaborator_data')->id
        ])->first();
        if (empty($pivotTeamPlanMemb)) {
            $request->attributes->get('_construction_site_collaborator_data')->teams()->attach([
                'team_id' => $_team_id
            ]);
            $pivotTeamPlanMemb = CollaboratorTeamPlanningEntries::orderBy('id', 'desc')->first();
        }
        return $pivotTeamPlanMemb;
    }

    public function message(Request $request, $siteId, $branchId, $planningId)
    {


        $res = [];
        if ($request->date) {
            if (Planning::checkIfInNotPlanning($request->date, $request->date, $request->attributes->get('_construction_site_collaborator'))) {
                return response()->json([
                    "message" => "Can`t plane day message in holiday day"
                ], 400);
            }
            $res = $request->attributes->get('_construction_site_collaborator_data')->messages();
            if ($request->id) {
                try {
                    $res = $res->findOrFail($request->id);
                } catch (\Exception $e) {
                    return response()->json(['message' => 'No Planning message found!!!'], 400);
                }
                $res->update(
                    [
                        'description' => $request->description,
                        'description_to_do' => $request->description_to_do,
                    ]
                );
            } else {
                $res = $res->create([
                    'description' => $request->description,
                    'description_to_do' => $request->description_to_do,
                    'date' => Carbon::parse($request->date)->toDateTimeString()
                ]);
            }

        } else {
            $res = $request->attributes->get('_construction_site_collaborator_data');
            if (Planning::checkIfInNotPlanning($res->start, $res->end, $request->attributes->get('_construction_site_collaborator'))) {
                return response()->json([
                    "message" => "Can`t plane period message in holiday day"
                ], 400);
            }
            $res->update([
                'description' => $request->description,
                'description_to_do' => $request->description_to_do,
            ]);
        }
        return $res;
    }

    public function pdf()
    {
        $headers = [
            'Content-Type' => 'application/pdf',
        ];

        return Storage::download('/v1/planner/test.pdf', 'filename.pdf', $headers);
    }


    public function destroy(Request $request, $siteId, $branchId, $planningId, $id)
    {
        try {
            $teamId = $request->query('teamId');
            if (empty($teamId)) throw new Exception('No Team id provided');
            $res = TeamMemberPlanningEntry::findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Planning member found!!!', 'data' => $e->getMessage()], 400);
        }
        return $res->delete() ? 1 : 0;
    }

}