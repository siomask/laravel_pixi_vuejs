<?php

namespace App\Http\Controllers\v1\Planner\ConstructionSite;

use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use App\Http\Resources\v1\Contact\ContactResource;


class ConstructionSiteController extends Controller
{


    public function move(Request $request)
    {
        if (empty($request->daysToMove)) {
            $res = (["state" => 1, "msg" => "Missed required data or nothing to move"]);
        } else {
            $start = !isset($request->start) ? (new Carbon())->subCenturies(10) : Carbon::parse($request->start);
            $end = !isset($request->end) ? (new Carbon())->addCenturies(10) : Carbon::parse($request->end);
            $daysToMove = $request->daysToMove;

            /* Gather all contractors/internal teams data within the daterange selected by the current user */
            $branches = $request->attributes->get('_construction_site')->collaborators()->get();
            $results = [];
            foreach ($branches as $branch) {
                $results[] = Planning::move($branch, $start, $end, $daysToMove);
            }
            $res = (["state" => 2, "data" => $results]);

        }
        return response($res);
    }

    public function index(Request $request)
    {

        $selectedSites = auth()->user()
            ->settings
            ->selectedConstructionSites()
            ->with('contact')->get();

        $conts = [];
        foreach ($selectedSites as $sel) {
            if (isset($sel->contact)) $conts[] = $sel->contact->id;
        }

        return response([
            'selected' => ConstructionSitesResource::collection($selectedSites),
            'allSites' => ContactResource::collection(Contact::whereNotIn('id', $conts)
                ->whereIn('type_id', [
                    1,//clients
                    3,//contractors
                    20//teams
                ])->get())
        ]);
    }

    public function store(Request $request)
    {
        $sites = auth()->user()
            ->settings
            ->selectedConstructionSites();
        $site = $sites->withTrashed()->where("contact_id", $request->id)->first();
        if (empty($site)) {
            $site = $sites->create([
                "contact_id" => $request->id
            ]);
        } else {
            $site->restore();
        }
        return $site;

    }

    public function update(Request $request, $id)
    {
        try {
            $site = auth()->user()
                ->settings
                ->selectedConstructionSites()->findOrFail($id);
            $updates = [];
            foreach ($request->all() as $key => $value) {
                switch ($key) {
                    case 'projectLeaders':
                        {
                            $value = (array)$value;
                            $updates = array_filter($value, function ($k, $el) {
                                return !empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $newItems = array_filter($value, function ($k, $el) {
                                return empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);

                            $site->projectLeads()->whereNotIn('id', array_map(function ($el) {
                                return $el['id'];
                            }, $updates))->delete();
                            foreach ($updates as $item) {
                                $site->projectLeads()->findOrFail($item['id'])->update($item);
                            }


                            foreach ($newItems as $item) {
                                $site->projectLeads()->create($item);
                            }
                            break;
                        }
                    case 'estimators':
                        {
                            $value = (array)$value;
                            $updates = array_filter($value, function ($k, $el) {
                                return !empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $newItems = array_filter($value, function ($k, $el) {
                                return empty($el['id']);
                            }, ARRAY_FILTER_USE_BOTH);
                            $site->estimators()->whereNotIn('id', array_map(function ($el) {
                                return $el['id'];
                            }, $updates))->delete();
                            foreach ($updates as $item) {
                                $site->estimators()->findOrFail($item['id'])->update($item);
                            }
                            foreach ($newItems as $item) {
                                $site->estimators()->create($item);
                            }
                            break;
                        }
                    case 'status_id':
                        {
                            $updates['construction_site_statuses_id'] = $value;
                            break;
                        }
                    case 'empty_rows':
                        {
                            auth()->user()
                                ->settings
                                ->selectedConstructionSites()->updateExistingPivot($id,[
                                    'planner_show_empty_rows'=>$value
                                ]);
                            break;
                        }
                    case 'presented_price':
                        {
                            $updates['presented_price'] = $request->presented_price;
                            break;
                        }
                    case 'sequence':
                        {
                            $updates['sequence'] = $request->sequence;
                            break;
                        }
                }
            }
            $site->update($updates);

        } catch (\Exception $e) {
            return response()->json(['message' => 'Bad update site!!!', "data" => $e->getMessage()], 400);
        }


        return new ConstructionSitesResource($site);
    }

    private function updateEmtyRows($sitesId){
        foreach ($sitesId as $item){

        }
    }
    public function destroy(Request $request, $id)
    {
        try {
            $site = auth()->user()
                ->settings
                ->selectedConstructionSites()->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No Site found!!!'], 400);
        }
        return $site->delete() ? 1 : 0;
    }

}