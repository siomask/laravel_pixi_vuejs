<?php

namespace App\Http\Controllers\v1\Planner\ConstructionSite\External;

use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use App\Models\Planner\ConstructionSite\External\ConstructionSiteExternal;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSite\External\ExternalResource;


class ExternalController extends Controller
{


    public function index(Request $request)
    {
        return ExternalResource::collection($this->externals($request)->get());
    }

    public function store(Request $request)
    {
        return new ExternalResource($this->externals($request)->create($request->all()));

    }

    public function update(Request $request, $siteId,$id)
    {
        $ext = $this->external($request, $id);
        if ($ext instanceof ConstructionSiteExternal) {
            try{
                $ext->update($request->all());
            }catch (\Exception $e){
                return response()->json(['message' => 'Something bad on update external','data'=>$e->getMessage()], 400);
            }
            return new ExternalResource($ext);
        } else {
            return $ext;
        }
    }

    public function destroy(Request $request, $siteId,$id)
    {
        $ext = $this->external($request, $id);
        return $ext instanceof ConstructionSiteExternal ? ($ext->delete() ? 1 : 0) : $ext;
    }

    private function externals(Request $request)
    {
        return $request->attributes->get('_construction_site')->externals();
    }

    private function external(Request $request, $id)
    {
        try {
            $ext = $this->externals($request)->findOrFail($id);
        } catch (\Exception $e) {
            return response()->json(['message' => 'No External found!!!'], 400);
        }
        return $ext;
    }

}