<?php

namespace App\Http\Controllers\v1\Planner\ConstructionSite\Workflow\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\v1\Workflow\ConstructionSiteWorkflowTaskResource;
use App\Http\Resources\v1\Workflow\Task\TaskResource;
use App\Models\Workflow\Task\WorkflowTask;
use App\Models\Workflow\ConstructionSiteWorkflowTask;
use Carbon\Carbon;
use Validator;

class TaskController extends Controller
{

    public function index(Request $request)
    {

        return TaskResource::collection($this->siteWorkFlowTasks($request)->withPivot(['start', 'end', 'task_status', 'id'])->paginate(15));

    }

    private function siteWorkFlowTasks(Request $request)
    {
        return $request->attributes->get('_construction_site')->workflowTasks();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'workflow_task_id' => 'required|integer',
            'start' => 'required|date|before_or_equal:end',
            'end' => 'required|date|after_or_equal:start'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        } else {
            $this->siteWorkFlowTasks($request)->attach($request->workflow_task_id, $request->all());
            return new TaskResource($this->siteWorkFlowTasks($request)->where("workflow_tasks.id", "=", $request->workflow_task_id)->withPivot(['start', 'end', 'task_status', 'id'])->orderBy('id', 'desc')->first());
        }
    }

    public function update(Request $request,$id)
    {
        $updates = $request->all();
        unset($updates['id']);
        $this->siteWorkFlowTasks($request)->updateExistingPivot($id, $updates);
        return new TaskResource($this->siteWorkFlowTasks($request)->withPivot(['start', 'end', 'task_status', 'id'])->find($id));
    }

    public function destroy(Request $request, $id)
    {
        return $this->siteWorkFlowTasks($request)->findOrFail($id)->delete() ? 1 : 0;
    }
}