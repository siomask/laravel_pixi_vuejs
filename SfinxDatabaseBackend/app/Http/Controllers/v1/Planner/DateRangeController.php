<?php

namespace App\Http\Controllers\v1\Planner;

use App\Http\Resources\v1\Planner\DateRangeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DateRangeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        $dateRange = auth()->user()->settings()->first();
        return new DateRangeResource($dateRange);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $start = \Carbon\Carbon::parse($request['start'])->toDateTimeString();
        $end = \Carbon\Carbon::parse($request['end'])->toDateTimeString();
        $dateRange = auth()->user()->settings()->first();
        if (!empty($dateRange)) {
            $siteId = $request['siteID'];
            $action = strtoupper($request['action']);
            switch ($action) {
                case 'EARLIEST':
                    {
                        return response($this->getPlannerData($request, $action, null, null, $siteId));
                        break;
                    }
                case 'LATEST':
                    {
                        return response($this->getPlannerData($request, $action, null, null, $siteId));
                        break;
                    }
                case 'LATER3MONTHS':
                    {
                        return response($this->getPlannerData($request, $action, null, null, $siteId));
                        break;
                    }
                case 'CUSTOM':
                    {
                        return response($this->getPlannerData($request, $action, $start, $end, $siteId));
                        break;
                    }
                default:
                    {

                        $dateRange->update([
                            'planner_date_start' => $start,
                            'planner_date_end' => $end
                        ]);
                    }
            }

        } else {
            $dateRange = auth()->user()->settings()->create([
                'planner_date_start' => $start,
                'planner_date_end' => $end
            ]);
        }


        return response(new DateRangeResource($dateRange));
    }

    public function update(Request $request, $id)
    {
        $dateRange = auth()->user()->settings()->find($id)
            ->update([
                'planner_date_start' => $request['start'],
                'planner_date_end' => $request['end']
            ]);
        return $dateRange ? 1 : 0;
    }

    public function getPlannerData(Request $request, $type, $from = null, $to = null, $siteId)
    {

        $_dateRange = $this->getDateRange($type, $from, $to, $siteId);

        $updates = [];
        if (!empty($_dateRange['begin'])) $updates['planner_date_start'] =is_string($_dateRange['begin'])?$_dateRange['begin']: $_dateRange['begin']->toDateTimeString();
        if (!empty($_dateRange['end'])) $updates['planner_date_end'] = is_string($_dateRange['end'])?$_dateRange['end']:$_dateRange['end']->toDateTimeString();

        $dateRange = auth()->user()->settings()->first();
        if (count($updates) > 0) $dateRange->update($updates);


        return new DateRangeResource($dateRange);///PlannerController::constructionSites();
    }

    private function getDateRange($type, $from = null, $to = null, $siteId = null)
    {
        if ($type == 'EARLIEST' || $type == 'LATEST') {
            $plannerRange = auth()->user()->settings()->firstOrFail();


            $planningData = auth()->user()
                ->settings
                ->selectedConstructionSites()
                ->with([
                    'collaborators.data',
                ]);

            if (isset($siteId)) {
                $planningData = collect([$planningData->findOrFail($siteId)]);
            } else {
                $planningData = $planningData->get();
            }


            $planningData = iterator_to_array($planningData);

            $branches = array_map(function ($e) {
                return isset($e->collaborators) ? iterator_to_array($e->collaborators) : [];
            }, $planningData);


            $branches = call_user_func_array("array_merge", $branches);
            $date = null;
            if ($type == 'EARLIEST') {
                foreach ($branches as $branch) {
                    foreach ($branch->data as $data) {
                        if (empty($date)) $date = \Carbon\Carbon::parse($data['start']);
                        $date = $date->min(\Carbon\Carbon::parse($data['start']));
                    }
                }
                return [
                    'begin' => $date,
                    'end' => $plannerRange->planner_date_end
                ];
            } else if ($type == 'LATEST') {
                foreach ($branches as $branch) {
                    foreach ($branch->data as $data) {
                        if (empty($date)) $date = \Carbon\Carbon::parse($data['end']);
                        $date = $date->max(\Carbon\Carbon::parse($data['end']));
                    }
                }
                return [
                    'begin' => $plannerRange->planner_date_start,
                    'end' => $date
                ];
            }
        } else if ($type == 'LATER3MONTHS') {
            return [
                'begin' => \Carbon\Carbon::now()->subDays(7),
                'end' => \Carbon\Carbon::now()->addDays(90)
            ];
        } else if ($type == 'CUSTOM') {
            return [
                'begin' => \Carbon\Carbon::parse($from),
                'end' => \Carbon\Carbon::parse($to)
            ];
        }
    }


}