<?php

namespace App\Http\Controllers\v1\Planner\Collaborator;

use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use App\Models\Contact\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use App\Http\Resources\v1\Contact\ContactResource;


class CollaboratorController extends Controller
{




    public function store(Request $request)
    {
        $sites = auth()->user()
            ->settings
            ->collaborators();
        $site =null;//$sites->withTrashed()->where("contact_id", $request->id)->first();
        if (empty($site)) {
            $site = $sites->create([
                "contact_id" => $request->id
            ]);
        }else{
            $site->restore();
        }
        return $site;

    }

    public function destroy(Request $request, $id)
    {
        return auth()->user()
            ->settings
            ->collaborators()
            ->where("contact_id", $id)->delete() ? 1 : 0;
    }

}