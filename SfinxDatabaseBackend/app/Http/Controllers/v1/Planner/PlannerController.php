<?php

namespace App\Http\Controllers\v1\Planner;

use App\Http\Controllers\Controller;

use App\Models\Contact\ContactType;
use App\Models\Team\Team;
use App\Models\Common\Holiday;
use App\Models\Common\ProjectLead;
use App\Http\Resources\v1\Planner\DateRangeResource;
use App\Http\Resources\v1\Planner\ConstructionSitesResource;
use App\Http\Resources\v1\Planner\CollaboratorSitesResource;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Resources\v1\Contact\ContactResource;
use App\Models\Contact\Contact;
use App\Models\Common\User;
use App\Http\Resources\v1\User\UserResource;
use App\Http\Resources\v1\Planner\ProjectLeaderResource;
use App\Http\Resources\Common\ConstructionTypeResource;

class PlannerController extends Controller
{

    public function index(Request $request)
    {
        $settings = auth()->user()->settings()->first();

        if (!empty($request->query('start'))) {
            $settings->planner_date_start = Carbon::parse($request->query('start'));
        } else {
            if (empty($settings->planner_date_start)) {
                $settings->planner_date_start = Carbon::now();
            } else {
                $settings->planner_date_start = Carbon::parse($settings->planner_date_start);
            }
        }
        if (!empty($request->query('end'))) {
            $settings->planner_date_end = Carbon::parse($request->query('end'));
        } else {
            if (empty($settings->planner_date_end)) {
                $settings->planner_date_end = Carbon::now()->addDays(7);
            } else {
                $settings->planner_date_end = Carbon::parse($settings->planner_date_end);
            }
        }

        $_set_date_range = (object)[
            'planner_date_start' => Carbon::parse($settings->planner_date_start)->subDays(10)->toDateTimeString(),
            'planner_date_end' => Carbon::parse($settings->planner_date_end)->addDays(10)->toDateTimeString()
        ];

        $settings->planner_date_start = explode(" ", $settings->planner_date_start->toDateTimeString())[0];
        $settings->planner_date_end = explode(" ", $settings->planner_date_end->toDateTimeString())[0];

        $conts = [];
        $constractionSits = [];
        $result = [];
        $res_sites = [];
        $_users = [];
        $hollidays = [];
        $allSites = [];
        $isDetails = !empty($request->query('details'));

        $sites = PlannerController::constructionSites(true, $isDetails, $_set_date_range);
        foreach ($sites as $site) {
            $constractionSits[] = $site->id;
            if (isset($site->contact)) {
                $_index = array_search($site->contact->id, $conts);
                if ($_index === false) {
                    $conts[] = $site->contact->id;
                    $res_sites[] = $site;
                } else {
                    foreach ($site->collaborators as $c) {
                        $res_sites[$_index]->collaborators->add($c);
                    }

                }
            }
        }
        $result = ConstructionSitesResource::collection(collect($res_sites));
//        $result =  (collect($res_sites));

        if (!$isDetails) {
            $collaborators =  PlannerController::collaborators($constractionSits, $_set_date_range);
            foreach ($collaborators as $site) {
                if (isset($site['contact'])) $conts[] = $site['contact']->id;
            }
            $result = $result->count() ? $result->merge(($collaborators)) : $collaborators;
        }

        if ($isDetails) {
            $_users = ProjectLead::leaders($constractionSits);
//            $allSites = ContactResource::collection(Contact::whereNotIn('id', $conts)
//                ->whereIn('type_id', ContactType::$TYPES)->get());
        } else {
            $hollidays = Holiday::where([
                [
                    'start', '>=', $_set_date_range->planner_date_start
                ],
                [
                    'start', '<=', $_set_date_range->planner_date_end
                ]
            ])->orWhere([
                [
                    'end', '>=', $_set_date_range->planner_date_start
                ],
                [
                    'end', '<=', $_set_date_range->planner_date_end
                ]
            ])->get();
        }


        return response([
            'holidays' => $hollidays,
            'daterange' => new DateRangeResource($settings),
            'constructionSites' => $result,
            'projectLeaders' => $_users,
//            'allSites' => $allSites
        ]);

    }

    static function projLeads($constractionSits)
    {
        $leaders = ProjectLead::with([
            'user',
            'constructionSite' => function ($q) {
                $q->with([
                    'contact'
                ]);
            }
        ])->whereHas('user', function ($q) {
            $q->whereNotIn('id', [auth()->user()->id]);
        })->whereHas('constructionSite', function ($q) use ($constractionSits) {
            $q->whereNotIn('id', $constractionSits);
        })->get();
        return $leaders;
    }

    static function collaborators($constractionSits, $settings)
    {

        //::TODO change quries to get only data for selected period, plannings members and planning days
        $collaborators = auth()->user()
            ->settings->selectedCollaborators()
            ->with([
                'collaborators' => function ($q) use ($constractionSits, $settings) {
                    $q/*->whereHas('data',function($q)use($settings){
                        return $q->where([
                            ['start', '>=', $settings->planner_date_start],
                            ['start', '<=', $settings->planner_date_end]
                        ])->orWhere([
                            ['end', '>=', $settings->planner_date_start],
                            ['end', '<=', $settings->planner_date_end]
                        ]);
                    })*/
                        ->with([
                            'info',
                            'data' => function ($q) use ($settings) {
                                $q->where([
                                    ['start', '>=', $settings->planner_date_start],
                                    ['start', '<=', $settings->planner_date_end]
                                ])->orWhere([
                                    ['end', '>=', $settings->planner_date_start],
                                    ['end', '<=', $settings->planner_date_end]
                                ])->with('messages');
                            },
                            'contact' => function ($q)use($settings) {
                                $q->with([
                                    'teams' => function ($q)use($settings) {
                                        $q->with([
                                            'period' => function ($q) use($settings){
                                                $q->where(
                                                    [
                                                        ['start', '<=', Team::activeTime()],
                                                        ['end', '>=', Team::activeTime()],
                                                    ]
                                                )->with([
                                                        'periodMembers' => function ($q)use($settings) {
                                                            $q->with([
                                                                'member'=>function($q)use($settings){
                                                                    $q->with([
                                                                        'absences'=>function($q)use($settings){
                                                                            return $q->where([
                                                                                ['start', '>=', $settings->planner_date_start],
                                                                                ['start', '<=', $settings->planner_date_end]
                                                                            ])->orWhere([
                                                                                ['end', '>=', $settings->planner_date_start],
                                                                                ['end', '<=', $settings->planner_date_end]
                                                                            ]);
                                                                        }
                                                                    ]);
                                                                },
                                                                'plannings'=>function($q)use($settings){
                                                                    return $q->where([
                                                                        ['date', '>=', $settings->planner_date_start],
                                                                        ['date', '<=', $settings->planner_date_end]
                                                                    ]);
                                                                }
                                                            ]);
                                                        }]
                                                )->whereHas('periodMembers.member');
                                            }

                                        ]);
                                    }
                                ]);
                            },
                            'constructionSites' => function ($q) {
                                $q->with('contact');
                            }
                        ])->whereHas('constructionSites');
                }
            ])->whereHas('collaborators', function ($q) {
                return $q->where('is_assigned', 1)
                    ->orWhere('is_contractor_type_assigned', 1);
            })
            ->get();

        $sites = [];

        foreach ($collaborators as $site) {
            $_contacts = [];

            foreach ($site->collaborators as $ites) {
                if ($ites->is_assigned == 1 || $ites->is_contractor_type_assigned == 1) {
                    $contact = $ites->constructionSites->contact;
                    if(empty($contact))$contact=(object)[];
                    $_C = clone $contact;
                    $_contact = clone $contact;
                    $_contacts[] = $_C;
                    $_C->teams = [];
                    $_C->info = new ConstructionTypeResource($ites->info);
                    $_C->data = $ites->data;
                    $_C->teams = array_search($ites->constructionSites->id, $constractionSits) === false && isset($ites->contact) ? Team::parseActiveTeamMembers($ites->contact->teams) : [];
                    $_C->contact = isset($_contact) ? new ContactResource($_contact) : ['name' => 'Wrong Contact'];
                    $_C->id = $ites->id;
                    $_C->siteId = $ites->constructionSites->id;
                }
            }
            $sites[] = [
                'id' => $site->id,
                'type' => 3,
                'branches' => $_contacts,
                'contact' => new ContactResource($site),
                'info' => [
                    'title' => $site->first_name . " " . $site->last_name,
                ]

            ];
        }
        return $sites;

    }

    static function constructionSites($isNotCollection = false, $noDeep = false, $settings)
    {
        $sites = auth()->user()
            ->settings
            ->selectedConstructionSites();

        if (!$noDeep) {
            $sites->with([
                'contact',
                'collaborators' => function ($q) {
                    $q->where('is_assigned', 1)->orWhere('is_contractor_type_assigned', 1);
                },
                'collaborators.info',
                'collaborators.data' => function ($q) use ($settings) {
                    $q->where([
                        ['start', '>=', $settings->planner_date_start],
                        ['start', '<=', $settings->planner_date_end]
                    ])->orWhere([
                        ['end', '>=', $settings->planner_date_start],
                        ['end', '<=', $settings->planner_date_end]
                    ]);
                },
                'collaborators.contact' => function ($q) use($settings){
                    $q->with([
                        'teams' => function ($q)use($settings) {
                            $q->with([
                                'period' => function ($q)use($settings) {
                                    $q->where(
                                        [
                                            ['start', '<=', Team::activeTime()],
                                            ['end', '>=', Team::activeTime()],
                                        ]
                                    )->with([
                                            'periodMembers' => function ($q)use($settings) {
                                                $q->with([
                                                    'member'=>function($q)use($settings){
                                                        $q->with([
                                                            'absences'=>function($q)use($settings){
                                                                return $q->where([
                                                                    ['start', '>=', $settings->planner_date_start],
                                                                    ['start', '<=', $settings->planner_date_end]
                                                                ])->orWhere([
                                                                    ['end', '>=', $settings->planner_date_start],
                                                                    ['end', '<=', $settings->planner_date_end]
                                                                ]);
                                                            }
                                                        ]);
                                                    },
                                                    'plannings'=>function($q)use($settings){
                                                        return $q->where([
                                                            ['date', '>=', $settings->planner_date_start],
                                                            ['date', '<=', $settings->planner_date_end]
                                                        ]);
                                                    }
                                                ]);
                                            }]
                                    )->whereHas('periodMembers.member');
                                }

                            ]);
                                //->whereHas('period.periodMembers.member');
//                                ->whereHas('period.periodMembers.plannings.pivotPlannings.planningEntry');
                        }
                    ]);
                }
            ]);
        }
        $sites = $sites->get();

        if ($isNotCollection) {
            return $sites;
        } else {
           return ConstructionSitesResource::collection($sites);
        }

    }


}