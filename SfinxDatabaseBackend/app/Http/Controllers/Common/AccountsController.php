<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Models\Common\User;
use App\Models\Common\UserGroup;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AccountsController extends Controller
{

    private function refreshToken($token)
    {
        $privkey = openssl_pkey_get_private(env('PRIVATE_KEY_LOCATION'));
        $jwt = JWT::encode($token, $privkey, 'RS256');
        return $jwt;
    }

    private function createToken($request_server, $request_username, $user)
    {
        $privkey = openssl_pkey_get_private(env('PRIVATE_KEY_LOCATION'));

        $jwt_before_encode = array(
            /* Issuer (iss) - identifies principal that issued the JWT; */
            "iss" => $request_server,
            /* Subject (sub) - identifies the subject of the JWT; */
            /*"sub" => '',*/
            /* Audience (aud) - The "aud" (audience) claim identifies the recipients that the JWT is intended for. Each principal intended to process the JWT MUST identify itself with a value in the audience claim. If the principal processing the claim does not identify itself with a value in the aud claim when this claim is present, then the JWT MUST be rejected. */
            /*"aud" => '',*/
            /* Expiration time (exp) - The "exp" (expiration time) claim identifies the expiration time on or after which the JWT MUST NOT be accepted for processing. */
            "exp" => time() + (60 * 60 * 8),
            /* Not before (nbf) - Similarly, the not-before time claim identifies the time on which the JWT will start to be accepted for processing. */
            /*"nbf" => '',*/
            /* Issued at (iat) - The "iat" (issued at) claim identifies the time at which the JWT was issued. */
            "iat" => time(),
            /* JWT ID (jti) - case sensitive unique identifier of the token even among different issuers. */
            /*"jti" => '',*/
            "username" => $request_username,
            "userid" => $user['gebruikerid'],
            "scope" => [
                "rechten" => $user["rechten"],
                "specialerechten" => $user["specialerechten"],
                "switchview" => $user["switchview"]
            ]
        );

        $jwt = JWT::encode($jwt_before_encode, $privkey, 'RS256');

        return $jwt;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $request_username = $request->header('username');
        $request_password = $request->header('password');
        $request_server = $request->header('host');

        if ($request->header('Authorization')) {
            $pubkey = openssl_pkey_get_public(env('CERTIFICATE_LOCATION'));
            $token = $request->header('Authorization');
            preg_match('/Bearer (.*)/', $token, $matches);

            if (isset($matches[1])) {
                $jwt_before_decode = $matches[1];

                JWT::$leeway = 60;
                try {
                    $jwt_after_decode = JWT::decode($jwt_before_decode, $pubkey, array('RS256'));
                } catch (\Firebase\JWT\ExpiredException $e) {
                    return response('{"status": "401", "message": "Token Expired"}', '401')->header('Content-Type', 'application/json');
                }

                if (time() - 60 < $jwt_after_decode->exp) {
                    $jwt_after_decode->exp = time() + (60 * 60 * 8);
                    $jwt_refreshed = $this->refreshToken($jwt_after_decode);
                    return response('{"status": "200", "message": "Authorized", "token": "' . $jwt_refreshed . '"}', '200')->header('Content-Type', 'application/json');
                } else {
                    return response('{"status": "401", "message": "Not Authorized"}', '401')->header('Content-Type', 'application/json');
                }
            }
        } else {
            if (!$request_username || !$request_password) {
                return response('{"status": "400", "message": "Bad Request"}', '401')->header('Content-Type', 'application/json');
            }

            $user = null;

            try {
                $user = User::where('gebruikersnaam', $request_username)->firstOrFail();
            } catch (\Exception $exception) {
                return response('{"status": "401", "message": "Wrong username/password"}', '401')->header('Content-Type', 'application/json');
            }


            if (Hash::check($request_password, $user["paswoord"])) {
                $jwt = $this->createToken($request_server, $request_username, $user);
                return response('{"status": "200", "message": "Authorized", "token": "' . $jwt . '"}', '200')->header('Content-Type', 'application/json');
            } else {
                return response('{"status": "401", "message": "Not Authorized"}', '401')->header('Content-Type', 'application/json');
            }
        }
        return response();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get user data
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function getUserData(Request $request)
    {
        $username = $request->attributes->get('jwt_after_decode')->username;

        $gebruikersgroep_id = $request->attributes->get('jwt_after_decode')->scope->rechten;
        $gebruikersgroep_name = UserGroup::find($gebruikersgroep_id)->gebruikersgroepennaam;

        $res['rights'] = $request->attributes->get('gebruiker_rechten');
        $res['profile'] = [
            'username' => $username,
            'accountType' => $gebruikersgroep_name
        ];

        return response($res);
    }
}
