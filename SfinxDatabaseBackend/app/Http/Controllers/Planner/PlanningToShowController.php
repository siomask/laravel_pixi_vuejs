<?php

namespace App\Http\Controllers\Planner;

use App\Http\Resources\Planner\PlanningToShowResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\PlanningToShow;
use App\Models\Site\ProjectLeader;
use App\Models\Contact\Client;

class PlanningToShowController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userid = auth()->user()->id;

        $selected = PlanningToShow::with([
                'client',
                'client.beheer.projectlLeaders' => function ($q) {
                    $q->where('functie','=',ProjectLeader\ProjectLeiderPerWerf::$FUNCTIE[0])->with([
                        'leader'
                    ]);
                }
            ])->where('gebruikerid', $userid)
                ->get();

        $conts = [];
        foreach ($selected as $sel){
            if(isset($sel->client))$conts[]=$sel->client->contactid;
        }
        $allExeptSelected =
            Client::select(
                'type',
                'naam',
                'contactid',
                'projectnaam'
            )->whereNotIn('contactid', $conts)
                ->whereIn('type', [
                    1,//clients
                    3,//contractors
                    20//teams
                ])
                ->get();

        return ['allSites'=>$allExeptSelected,'selected'=>PlanningToShowResource::collection($selected)];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userid = auth()->user()->id;

        return PlanningToShow::create([
            'gebruikerid' => $userid,
            'tetonencontactid' => $request['contactid'],

            'tetonencontacttype' => 3,//TODO: hardcode
            'groep' => 3,//TODO: hardcode
            'typeweergave' => '5555'//TODO: hardcode
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return PlanningToShow::destroy($id);
    }
}
