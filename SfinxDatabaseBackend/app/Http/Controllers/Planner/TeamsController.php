<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use App\Models\Site;
use App\Models\Site\Team\Teams;
use App\Models\Site\Werf\WerfDetailTeamsPerWerf;
use App\Models\Site\Werf\WerfDetailLedenPerWerf;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Settings\PlannerRange;


class TeamsController extends Controller
{


    public function index(Request $request)
    {
        return $this->checkAllActiveTeamPlanningMember($request);
    }

    private function checkAllActiveTeamPlanningMember(Request $request)
    {
        $currentTime = $this->currentTime();

        $userid = auth()->user()->id;
        $plannerRange = PlannerRange::where('gebruikerid', $userid)->firstOrFail();


        $activeTeams = Teams::with([
            'teamperiod.teamperiodmember.teammember'
        ])->whereHas('teamperiod', function ($q) use ($currentTime) {
            $q->where(
                [
                    ['teamperiodestart', '<=', $currentTime],
                    ['teamperiodeeinde', '>=', $currentTime],
                ]
            )->whereHas('teamperiodmember.teammember');
        })->with(['plannedTeams' => function ($q) use ($plannerRange) {
            $q->where(
                [
                    ['werfdetailteamsperwerfbegindatum', '>=', $plannerRange->begin],
                    ['werfdetailteamsperwerfeinddatum', '<=', $plannerRange->einde]
                ]
            )->with(['teamPlanedMember' /*=> function ($q) {
                $q->groupBy('werfdetailledenperwerfid')->havingRaw('COUNT(*)>2');
            }*/]);
        }])
            ->get();


        $teams = $activeTeams;
        $DAY_IN_UTC = 60 * 60 * 24;

        $_membersId = array();
        $_membersPlannedId = array();
        $daysFullPlanned = [];
        foreach ($teams as $teamV) {
            $teamPlanns = $teamV->plannedTeams;

            $teamV->_days = [];

            //get all planned infr
            foreach ($teamPlanns as $teamPlannV) {
                foreach ($teamPlannV->teamPlanedMember as $tmpl) {
                    $_id = $tmpl->werfdetailledenperwerfteamlidid;
                    if (!isset($_membersPlannedId[$_id])) {
                        $_membersPlannedId[$_id] = [];
                    }
                    for ($i = $plannerRange->begin; $i < $plannerRange->einde; $i += $DAY_IN_UTC) {
                        if ($tmpl->werfdetailledenperwerfdatumbegin >= $i && $tmpl->werfdetailledenperwerfdatumbegin < $i + $DAY_IN_UTC) {
                            if (!isset($_membersPlannedId[$_id][$i])) $_membersPlannedId[$_id][$i] = [];//planned parts for member
                            $dif = $tmpl->werfdetailledenperwerfdatumeinde - $tmpl->werfdetailledenperwerfdatumbegin;

                            if ($dif == 0 || $dif > $DAY_IN_UTC) {

                            } else {
                                array_push($_membersPlannedId[$_id][$i], [$tmpl->werfdetailledenperwerfdatumbegin, $tmpl->werfdetailledenperwerfdatumeinde, $dif]);
                            }

                            break;
                        }
                    }
                }

            }

            //filter planned day
            $dayPlannedMambers = [];
            $daysParts = 5;
            foreach ($_membersPlannedId as $keyId => $_id) {


                foreach ($_id as $keyDay => $dayP) {
                    if (count($dayP) == 0) {
                        unset($_membersPlannedId[$keyId][$keyDay]);
                    } else {
                        $parts = 0;
                        $planned_paerts=[];
                        foreach ($dayP as $keyDayPart => $dayPart) {
                            if ($dayPart[2] > 0 && $dayPart[2] <= $DAY_IN_UTC / $daysParts) {
                                if(array_search($dayPart[0],$planned_paerts)===false){
                                    $planned_paerts[]=$dayPart[0];
                                    $parts++;
                                }
                            } else if ($dayPart[2] == $DAY_IN_UTC) {
                                $parts = $daysParts;
                                break;
                            }
                        }
                        if ($parts >= $daysParts) {
                            if (!isset($dayPlannedMambers[$keyDay])) $dayPlannedMambers[$keyDay] = [];
                            $dayPlannedMambers[$keyDay][] = $keyId;


                        }
                    }
                }
            }


            //filter days where all member planned
            $_MEMBERS = count($_membersPlannedId);
            $res_day = [];
            foreach ($dayPlannedMambers as $keyDay => $_day) {
//                sort($dayPlannedMambers[$keyDay]);
                if (count($_day) == $_MEMBERS) $res_day[] = $keyDay;
            }

        }


        return response(['data' => $res_day]);
    }


    public function activeTeams(Request $request, $cellId, $from, $to)
    {
        return response(['data' => Teams::parseActiveTeamMembers(Teams::loadAllPlannedTeamsMembers($cellId, $from, $to))]);
    }


    private function currentTime()
    {
        return Teams::currentTime();
    }

    private function parseActiveTeamMembers($teams)
    {
        //TODO: need to find better solution to get members from activeteams
//        $teamIds = [];
        foreach ($teams as $teamV) {
            $teamperiod = $teamV->teamperiod;
            $teamPlanns = $teamV->plannedTeams;

//            $teamIds[] = $teamV->teamid;

            $members = array();
            $membersId = array();
            foreach ($teamperiod as $teamperiodV) {

                $teamperiodmember = $teamperiodV->teamperiodmember;

                foreach ($teamperiodmember as $teamperiodmemberV) {

                    if (!in_array($teamperiodmemberV->teammember->teamlidid, $membersId)) {
                        $member = clone $teamperiodmemberV->teammember;//save only unique teammeber for his team
                        $tlid = $teamperiodmemberV->teammember->teamlidid;
                        $members[] = $member;
                        $membersId[] = $tlid;

                        $planned = [];
                        foreach ($teamPlanns as $teamPlannK => $teamPlannV) {
                            $team_planed_member = $teamPlannV->teamPlanedMember;
                            foreach ($team_planed_member as $team_planed_memberK => $team_planed_memberV) {
                                if ($tlid == $team_planed_memberV->werfdetailledenperwerfteamlidid) {

                                    $planned[] = $team_planed_memberV;

                                    unset($team_planed_member[$team_planed_memberK]);
                                }
                            }
                            if (count($team_planed_member) == 0) unset($teamPlanns[$teamPlannK]);
                        }
                        if (count($planned) > 0) $member->_planned = $planned;

                    }
                }
            }
            $teamV->teamMembers = $members;
            unset($teamV->teamperiod);
            unset($teamV->plannedTeams);
        }
        return $teams;
    }

    private function onSuccess($data = 0)
    {
        return response('{"status": "200", "message": "successs","data":' . $data . '}', '200')->header('Content-Type', 'application/json');
    }

    private function onError($data = 0)
    {
        return response('{"status": "400", "message": "bad data","data":' . $data . '}', '400')->header('Content-Type', 'application/json');
    }

}