<?php

namespace App\Http\Controllers\Planner;

use App\Http\Resources\Planner\DateRangeResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Planner\SiteResource;
use App\Models\Common\UserDateRange;
use App\Models\Site\PlanningToShow;
use App\Models\Site\Site;
use App\Models\Settings\PlannerRange;

class DateRangeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return DateRangeResource
     */
    public function index(Request $request)
    {
        $gebruikerid = auth()->user()->id;
        $dateRange = UserDateRange::where('gebruikerid', '=', $gebruikerid)->firstOrFail();
        return new DateRangeResource($dateRange);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gebruikerid = auth()->user()->id;
        $dateRange = UserDateRange::where('gebruikerid', '=', $gebruikerid)
            ->update([
                'begin' => $request['start'],
                'einde' => $request['end']
            ]);

        return response ($dateRange);
    }

    public function update(Request $request, $id)
    {
        $result = UserDateRange::find($id)
            ->update([
                'begin' => $request['start'],
                'einde' => $request['end']
            ]);
        return $result ? 1 : 0;
    }

    public function getPlannerData(Request $request, $type, $from = null, $to = null, $siteId) {

        $gebruikerid = auth()->user()->id;
        $dateRange = $this->getDateRange($type, $gebruikerid, $from, $to);

        UserDateRange::where('gebruikerid', '=', $gebruikerid)
            ->update([
                'begin' => $dateRange['begin'],
                'einde' => $dateRange['end']
            ]);

        $dateRange = UserDateRange::where('gebruikerid', '=', $gebruikerid)->first();

        $planningToShow = PlanningToShow::where('gebruikerid', $gebruikerid)->get();
        $planningToShow = iterator_to_array($planningToShow);

        $sites = array_map(function($e) use ($dateRange) {
            return isset($e->client)?iterator_to_array($e->client->site()
                ->with([
                    'contact',
                    'branches',
                    'branches.info',
                    'branches.contact',
                    'branches.data' => function ($q) use ($dateRange) {
                        $q->hasData($dateRange->begin, $dateRange->einde);
                    }
                ])
                ->get()):[];
        }, $planningToShow);
        if(count($sites)>1)$sites = call_user_func_array('array_merge', $sites);
        if ($siteId) {
            $sites = array_filter($sites, function($e) use ($siteId) {
                return $e->werfbeheerid == $siteId;
            });
            $sites = array_values($sites);
        }
        $sites = collect($sites);
        $response = SiteResource::collection(
            $sites
        );

        return $response;
    }

    public function getDateRange($type, $gebruikerid, $from = null, $to = null) {

        $timeInterval = 90 * 24 * 3600+7 * 24 * 3600;

        if ($type == 'EARLIEST' || $type == 'LATEST') {
            $userid = auth()->user()->id;
            $plannerRange = PlannerRange::where('gebruikerid', $userid)->firstOrFail();
            $planningData = PlanningToShow::with('client.site.branches.data')
                ->where('gebruikerid', $gebruikerid)
                ->get();
            $planningData = iterator_to_array($planningData);

            $branches = array_map(function($e) {
                return isset($e->client->site)?iterator_to_array($e->client->site->branches):[];
            }, $planningData);
            $branches = call_user_func_array("array_merge", $branches);

            if ($type == 'EARLIEST') {
                $dates = array_map(function($e) {
                    return $e->data->min('begin');
                }, $branches);

                $date = min($dates);
                return [
                    'begin' => $date,
                    'end' => $plannerRange->einde
                ];
            } else if ($type == 'LATEST') {
                $dates = array_map(function($e) {
                    return $e->data->max('einde');
                }, $branches);

                $date = max($dates);
                return [
                    'begin' =>  $plannerRange->begin,//$date - $timeInterval,
                    'end' => $date
                ];
            }
        } else if ($type == 'LATER3MONTHS') {
            $date = time();
            return [
                'begin' => $date-$timeInterval,
                'end' => $date+24*3600
            ];
        } else if ($type == 'CUSTOM') {
            return [
                'begin' => $from,
                'end' => $to
            ];
        }
    }

    public function start(Request $request, $siteId = null) {
        return response($this->getPlannerData($request, 'EARLIEST', null, null, $siteId));
    }

    public function end(Request $request, $siteId = null) {
        return response($this->getPlannerData($request, 'LATEST', null, null, $siteId));
    }

    public function later3Months(Request $request, $siteId = null) {
        return response($this->getPlannerData($request, 'LATER3MONTHS', null, null, $siteId));
    }

    public function custom(Request $request, $from, $to, $siteId = null) {
        return response($this->getPlannerData($request, 'CUSTOM', $from, $to, $siteId));
    }

   
}