<?php

namespace App\Http\Controllers\Planner;

use App\Models\Site\Werf\WerfDetailTeamsPerWerf;
use App\Models\Site\Werf\WerfDetailLedenPerWerf;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeamsMemberController extends Controller
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cellId = $request['cellId'];
        $from = $request['start'];
        $to = $request['end'];
        $teamId = $request['teamId'];
        $teamMemberId = $request['teamMemberId'];

        $plannedTeam = WerfDetailTeamsPerWerf::createPlannedTeam($cellId, $from, $to, $teamId);
        $plannedTeamMemeber = WerfDetailLedenPerWerf::markMemberAsPlanned($plannedTeam->werfdetailteamsperwerfid, $teamMemberId, $from, $to);//planning team member

        return response('{"status": "200", "message": "successs","data":' . $plannedTeamMemeber . '}', '200')->header('Content-Type', 'application/json');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $pl = WerfDetailLedenPerWerf::where("werfdetailledenperwerfid", $id)->firstOrFail();
//        WerfDetailTeamsPerWerf::where("werfdetailteamsperwerfid", $pl->werfdetailteamsperwerfid)->delete();//unplanning team with cell id
        WerfDetailLedenPerWerf::destroy($id);//unplanning member with planned team cell
        return response('{"status": "200", "message": "successs","data":' . 0 . '}', '200')->header('Content-Type', 'application/json');
    }
}
