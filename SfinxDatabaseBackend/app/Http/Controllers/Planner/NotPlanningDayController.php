<?php

namespace App\Http\Controllers\Planner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Site\NotPlanningDay;
use App\Http\Resources\Planner\NotPlanningDayResource;

class NotPlanningDayController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $start, $end)
    {

    }

    public function period(Request $request, $start, $end)
    {
        $query = [];
        if (isset($start)) $query[] = ['afwezighedenbegin', '>=', $start];
        if (isset($end)) $query[] = ['afwezighedeneinde', '<=', $end];

        return NotPlanningDayResource::collection(
            NotPlanningDay::where($query)->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        return null;
    }

    public function update(Request $request, $id)
    {
        return null;
    }

}