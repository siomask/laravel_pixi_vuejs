<?php

namespace App\Http\Controllers\Planner;

use App\Http\Controllers\Controller;
use App\Http\Resources\Planner\Contractor\ContractorResource;
use App\Http\Resources\Planner\SiteResource;
use App\Models\Settings\PlannerRange;
use App\Models\Site\PlanningToShow;
use Illuminate\Http\Request;
use App\Models\Site\Team\Teams;


class PlannerController extends Controller
{

    public function index(Request $request)
    {
        /* Get the daterange selected by the current user */
        $userid = auth()->user()->id;
        $plannerRange = PlannerRange::where('gebruikerid', $userid)->firstOrFail();

        if(!empty($request->query('start'))){
            $plannerRange->begin = $request->query('start');
        }
        if(!empty($request->query('end'))){
            $plannerRange->einde = $request->query('end');
        }
        /* Gather all clients data within the daterange selected by the current user */
        $clientsToShow = PlanningToShow::
        with([
            'client',
            'client.site',
            'client.site.branches',
            'client.site.branches.info',
            'client.site.branches.contact',
            'client.site.branches.data' => function ($q) use ($plannerRange) {
                $q->hasData($plannerRange->begin, $plannerRange->einde);
            }
        ])
            ->where('gebruikerid', $userid)
            ->whereHas('client', function ($q) {
                $q->where('type', '3');
            })
            ->get();


        /* Gather all contractors/internal teams data within the daterange selected by the current user */
        $contractorsToShow = PlanningToShow::
        with([
            'contractor',
            'contractor.branches',
            'contractor.branches.data' => function ($q) use ($plannerRange) {
                $q->hasData($plannerRange->begin, $plannerRange->einde);
            },
        ])
            ->where('gebruikerid', $userid)
            ->whereHas('contractor', function ($q) use ($plannerRange) {
                $q->where('type', '=', '1');
            })
            ->get();

        $teamssToShow = PlanningToShow::
        with([
            'contractor',
            'contractor.branches' => function ($q) use ($plannerRange) {
                $q->whereHas('data', function ($q) use ($plannerRange) {
                    $q->hasData($plannerRange->begin, $plannerRange->einde);
                })->with([
                    'data' => function ($q) use ($plannerRange) {
                        $q->hasData($plannerRange->begin, $plannerRange->einde);
                    },
                    'site.contact'
                ]);
            },
        ])
            ->where('gebruikerid', $userid)
            ->whereHas('contractor', function ($q) use ($plannerRange) {
                $q->where('type', '=', '20');
            })
            ->get();

        //get all plannings day/period id
        $cellsId = [];
        foreach ($clientsToShow as $site) {
            if (isset($site->client) && isset($site->client->site) && isset($site->client->site->branches)) {
                foreach ($site->client->site->branches as $branch) {
                    if (isset($branch->data)) {
                        foreach ($branch->data as $data) {
                            $cellsId[] = $data->werfdetailplanningdataid;
                        }
                    }
                }
            }

        }
        foreach ($contractorsToShow as $sK => $site) {
            if (isset($site->contractor) && isset($site->contractor->branches)) {
                foreach ($site->contractor->branches as $branchKey => $branch) {
                    if (isset($branch->data)) {
                        foreach ($branch->data as $data) {
                            $cellsId[] = $data->werfdetailplanningdataid;
                        }
                    }
                }
            }
        }
        foreach ($teamssToShow as   $site) {
            if (isset($site->contractor) && isset($site->contractor->branches)) {
                foreach ($site->contractor->branches as $branchKey => $branch) {
                    if (isset($branch->data)) {
                        foreach ($branch->data as $data) {
                            $cellsId[] = $data->werfdetailplanningdataid;
                        }
                    }
                }
            }
        }


        $clientsToShow = SiteResource::collection($clientsToShow); // Format data
        $contractorsToShow = ContractorResource::collection($contractorsToShow); // Format data
        $teamssToShow = ContractorResource::collection($teamssToShow); // Format data

        $response = $clientsToShow->merge($contractorsToShow); // Combine both results before responding
        $response = $response->merge($teamssToShow); // Combine both results before responding

        return response([
            'data' => $response,
            "planningsMembers" => Teams::parseActiveTeamMembers($this->loadPlannedMemberOnPlannedDays($cellsId))
        ]);
    }

    private function loadPlannedMemberOnPlannedDays($cellIdAr)
    {
        return Teams::loadAllPlannedTeamsMembers($cellIdAr);//get planned member only for planned days
    }


}