<?php

namespace App\Http\Controllers\Planner;

use App\Models\Site\PlanningData;
use App\Models\Site\NotPlanningDay;
use App\Models\Site\PlanningToShow;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings\PlannerRange;
use Carbon\Carbon;
use App\Models\Site\Werf\WerfDetailTeamsPerWerf;
use App\Models\Site\Werf\WerfDetailLedenPerWerf;

class PlanningDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($this->checkIfInNotPlanning($request['start'], $request['end'])) {
            return response(['state' => 1, "msg" => "Can`t plane in holiday"]);
        }

        $planningData = PlanningData::where('werfdetailatid', $request['branch'])->get()->toArray();

        $dateRange = $this->getDateRange($planningData, $request);
        if ($dateRange) {

            $createdPlanned = PlanningData::create([
                'werfdetailatid' => $request['branch'],
                'begin' => $dateRange['start'],
                'einde' => $dateRange['end'],
                'opmerkingvoorklant' => '',
                'opmerkingvooraannemer' => ''
            ]);
            $this->updateMemberPlanning($dateRange['planningMembers'], $createdPlanned->werfdetailplanningdataid, 0);
            return $createdPlanned;
        } else {
            return response(['state' => 1, "msg" => "Can`t create planned data"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response($this->updateDateRange($request, $id));
    }

    private function updateDateRange($request, $id)
    {
        if ($this->checkIfInNotPlanning($request['start'], $request['end'])) {
            return (['state' => 1, "msg" => "Can`t plane in holiday for branch(" . $request['branch'] . ") from(" . (Carbon::createFromFormat("U", $request['start'])->format('d/m/Y')) . ") to (" . (Carbon::createFromFormat("U", $request['end'])->format('d/m/Y')) . ")"]);
        }
        $result = PlanningData::find($id);
        if (empty($result)) return (['state' => 1, "msg" => "Can`t update un exist Plannings"]);

        $planningData = PlanningData::where('werfdetailatid', $request['branch'])
            ->where('werfdetailplanningdataid', '!=', $id)
            ->get()->toArray();

        $dateRange = $this->getDateRange($planningData, $request);
        if ($dateRange) {
            if (!empty($result)) {
                $daysUpdateTime = empty($request['daysUpdateTime'])?0:$request['daysUpdateTime'];// $dateRange['start'] - $result->begin;
                $result->update([
                    'werfdetailatid' => $request['branch'],
                    'begin' => $dateRange['start'],
                    'einde' => $dateRange['end']
                ]);


                $this->updateMemberPlanning($dateRange['planningMembers'], $id, $daysUpdateTime);
            }

            return !empty($result) ? (['state' => 2, 'plannes' => $dateRange['planningMembers'], 'time' => $daysUpdateTime]) : (['state' => 1, "msg" => "Nothing Update", "data" => $id]);
        } else {
            return (['state' => 1, "msg" => "No DateRange"]);
        }
    }

    private function updateMemberPlanning($planningMembers, $id, $daysUpdateTime)
    {
        if($daysUpdateTime===0){

        }else{
            WerfDetailTeamsPerWerf::where('werfdetailplanningdataid', $id)
                ->update([
                    'werfdetailteamsperwerfbegindatum' => \DB::raw('werfdetailteamsperwerfbegindatum + ' . $daysUpdateTime),
                    'werfdetailteamsperwerfeinddatum' => \DB::raw('werfdetailteamsperwerfeinddatum + ' . $daysUpdateTime),
                ]);

            $updateTimeForSelected = WerfDetailTeamsPerWerf::where('werfdetailplanningdataid', $id)->get();
            $plans = [];
            foreach ($updateTimeForSelected as $planned) {
                $plans[] = $planned->werfdetailteamsperwerfid;
            }
            WerfDetailLedenPerWerf::whereIn('werfdetailteamsperwerfid', $plans)
                ->update([
                    'werfdetailledenperwerfdatumbegin' => \DB::raw('werfdetailledenperwerfdatumbegin + ' . $daysUpdateTime),
                    'werfdetailledenperwerfdatumeinde' => \DB::raw('werfdetailledenperwerfdatumeinde + ' . $daysUpdateTime),
                ]);
        }


        WerfDetailTeamsPerWerf::whereIn('werfdetailplanningdataid', $planningMembers)->update([//just set new parent
            'werfdetailplanningdataid' => $id
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeDate(Request $request)
    {
        if (isset($request['id'])) {
            $data = PlanningData::where('werfdetailplanningdataid', $request['id'])->firstOrFail();
        } else {
            $data = PlanningData::where('werfdetailatid', $request['branch'])
                ->where('begin', '<=', $request['date'])
                ->where('einde', '>=', $request['date'])
                ->firstOrFail();
        }

        $DAY_IN_UTC = 24 * 60 * 60;
        $isSplit = isset($request['isSplit']);

        if ($isSplit) {
            return $this->splitGroup($data, $request);
        } else if ($request['dropall'] || $data->begin == $data->einde || isset($request['id'])) {
            $data->delete();
            return array('msg' => 'group elements was removed,start', 'type' => 1, 'planned' => $this->dropPlanningMembers($data->werfdetailplanningdataid));
        } else if ($request['date'] == $data->begin) {
            $this->dropPlanningMembers($data->werfdetailplanningdataid, $data->begin, $data->begin + $DAY_IN_UTC);
            $data->begin = $request['date'] + $DAY_IN_UTC;
            $data->save();
            return array('msg' => 'group elements was updated', 'type' => 2);
        } else if ($request['date'] == $data->einde) {
            $this->dropPlanningMembers($data->werfdetailplanningdataid, $data->einde, $data->einde + $DAY_IN_UTC);
            $data->einde = $request['date'] - $DAY_IN_UTC;
            $data->save();
            return array('msg' => 'group elements was updated,end', 'type' => 3);
        } else {//remove in the middle or split

            return $this->splitGroup($data, $request,true);
        }
    }

    private function splitGroup($data, $request,$withRemove=false)
    {
        $DAY_IN_UTC = 24 * 60 * 60;
        $data->delete();
        if($withRemove)$this->dropPlanningMembers($data->werfdetailplanningdataid, $request['date'], $request['date'] + $DAY_IN_UTC);


        $data1 = clone $data;
        $data2 = clone $data;
        $res_groups = array();

        unset($data1->werfdetailplanningdataid);
        unset($data2->werfdetailplanningdataid);

        if (isset($request['isSplit'])) {
            /*$data1->einde = $request['date'] == $data1->einde ? $request['date'] - $DAY_IN_UTC : $request['date'];
            if ($data1->einde < $data1->begin) $data1->einde = $data1->begin;

            $data2->begin = $request['date'] == $data2->begin ? $request['date'] + $DAY_IN_UTC : $request['date'];*/

            if($request['date'] == $data1->einde){//split from end
                $data1->einde =    $request['date'] - $DAY_IN_UTC  ;
                if ($data1->einde < $data1->begin) $data1->einde = $data1->begin;

                $data2->begin = $request['date']  ;
                if ($data2->einde < $data2->begin) $data2->einde = $data2->begin;

            } else{//split from begin or split somewhere in the middle
                $data1->einde =   $request['date'];
                if ($data1->einde < $data1->begin) $data1->einde = $data1->begin;

                $data2->begin =  $request['date']+ $DAY_IN_UTC ;
                if ($data2->einde < $data2->begin) $data2->einde = $data2->begin;
            }
        } else {
            $data1->einde = $request['date'] - $DAY_IN_UTC;
            $data2->begin = $request['date'] + $DAY_IN_UTC;
//            if ($data2->einde < $data2->begin) $data2->begin = $data2->einde;
        }

        $res_groups[] = PlanningData::create($data1->toArray());
        $res_groups[] = PlanningData::create($data2->toArray());

        return array('msg' => 'group elements were splited', 'groups' => $res_groups, 'type' => 4,'updates'=>$this->updatePlanningMembers($res_groups, $data));
    }

    private function dropPlanningMembers($cellId, $start = 0, $end = 0)
    {

        $th = $this->getPlannedMemberIds($cellId, $start, $end);
        $teamPlanns = [];
        foreach ($th['plannedTeam'] as $plan) {
            $teamPlanns[] = $plan->werfdetailteamsperwerfid;
        }
        WerfDetailLedenPerWerf::whereIn('werfdetailteamsperwerfid', $teamPlanns)->delete();//clear  all planning team members
        WerfDetailTeamsPerWerf::where($th['cond'])->delete();//clear  all planning teams with cell

        return $teamPlanns;
    }

    private function updatePlanningMembers($groups, $group)
    {
        $th = $this->getPlannedMemberIds($group->werfdetailplanningdataid, $group->begin, $group->einde);

        foreach ($th['plannedTeam'] as $plan) {
            if ($groups[0]->begin <= $plan->werfdetailteamsperwerfbegindatum && $groups[0]->einde >= $plan->werfdetailteamsperwerfbegindatum) {
                $plan->update(['werfdetailplanningdataid' => $groups[0]->werfdetailplanningdataid]);
            } else {
               $plan->update(['werfdetailplanningdataid' => $groups[1]->werfdetailplanningdataid]);
            }
        }
        return $th;
    }

    private function getPlannedMemberIds($cellId, $start = 0, $end = 0)
    {
        $where = [
            ['werfdetailplanningdataid', '=', $cellId]
        ];
        if (!empty($start) && !empty($end)) {
            $where[] = ['werfdetailteamsperwerfbegindatum', '>=', $start];
            $where[] = ['werfdetailteamsperwerfbegindatum', '<=', $end];
//            $where[] = ['werfdetailteamsperwerfeinddatum', '<=', $end];
        }
        $plannedTeam = WerfDetailTeamsPerWerf::select()
            ->where($where)->get();


        return [
            'cond' => $where,
            'plannedTeam' => $plannedTeam
        ];
    }

    public function getDateRange($planningData, $request)
    {
        $start = $request['start'];
        $end = $request['end'];

        $plannedMembers = [];

        if ($this->checkIfWeekend($start, $end)) {
            $_planningData = PlanningData::where('werfdetailatid', $request['branch'])
                ->where('einde', $end)
                ->where('begin', $start)->get()->toArray();

            if (isset($_planningData)) {
                foreach ($_planningData as $datum) {
                    if ($datum['einde'] == $datum['begin']) {
                        PlanningData::destroy($datum['werfdetailplanningdataid']);//if another weekend(s) day in same daterange
                        $plannedMembers [] = $datum['werfdetailplanningdataid'];
                    }

                }
            }
        } else {
            foreach ($planningData as $datum) {
                if ($start > $datum['einde'] || $end < $datum['begin'] || $this->checkIfWeekend($datum['begin'], $datum['einde']))//ignore weekend days
                    continue;
                if ($start > $datum['begin'] && $end < $datum['einde'])
                    return null;

                $start = min($start, $datum['begin']);
                $end = max($end, $datum['einde']);
                PlanningData::destroy($datum['werfdetailplanningdataid']);
                $plannedMembers [] = $datum['werfdetailplanningdataid'];

            }
        }

        return [
            'start' => $start,
            'end' => $end,
            'planningMembers' => $plannedMembers
        ];
    }


    public function move(Request $request)
    {
        $res = [];
        if (  empty($request->daysToMove)) {
            $res = (["state" => 1, "msg" => "Missed required data or nothing to move"]);
        } else {
            $start = !isset($request->start)?-INF :$request->start;
            $end = !isset($request->end)?INF :$request->end;
            $daysToMove = $request->daysToMove;


            if (isset($request->branchId)) {

                $res = (["state" => 2, "data" => [$this->updateBranchonMove($request->branchId, $start, $end, $daysToMove)]]);
            } else if (isset($request->siteId)) {

                $siteId = $request->siteId;
                $userid = auth()->user()->id;
                $plannerRange = PlannerRange::where('gebruikerid', $userid)->firstOrFail();


                /* Gather all contractors/internal teams data within the daterange selected by the current user */
                $contractorsToShow = PlanningToShow::
                with([
                    'contractor',
                    'contractor.branches' => function ($q) use ($plannerRange) {
                        $q->whereHas('data', function ($q) use ($plannerRange) {
                            $q->hasData($plannerRange->begin, $plannerRange->einde);
                        })->with([
                            'data' => function ($q) use ($plannerRange) {
                                $q->hasData($plannerRange->begin, $plannerRange->einde);
                            },
                            'site',
                            'site.contact'
                        ]);
                    },
                ])
                    ->where('gebruikerid', $userid)
                    ->where('tetonencontactid', $siteId)
                    ->whereHas('contractor', function ($q) {
                        $q->where('type', '!=', '3');
                    })->get();
                $results = [];
                foreach ($contractorsToShow as $key => $site) {
                    $branches = $site->contractor->branches;
                    foreach ($branches as $branch) {
                        $results[] = $this->updateBranchonMove($branch->werfdetailatid, $start, $end, $daysToMove);
                    }
                }
                $res = (["state" => 2, "data" => $results]);
            } else {
                $res = (["state" => 1, "msg" => "Missed branch or site ID"]);
            }

        }
        return $res;
    }

    private function updateBranchonMove($branchId, $start, $end, $daysToMove)
    {

        $result = ['state' => 2, "updates" => []];
        $planningData = PlanningData::where('werfdetailatid', $branchId)
            ->where([
                ['begin', '>=', $start],
                ['einde', '<=', $end]
            ])
            ->get()->toArray();

        foreach ($planningData as $plannedD) {
            try {
                $result['updates'][] = $this->updateDateRange([
                    'daysUpdateTime' => $daysToMove,
                    'branch' => $branchId,
                    'start' => ($plannedD['begin'] + $daysToMove),
                    'end' => ($plannedD['einde'] + $daysToMove)
                ], $plannedD['werfdetailplanningdataid']);
            } catch (Exception $e) {
                $result['updates'][] = $e;
            }

        }
        return $result;
    }

    private function checkIfWeekend($start, $end)
    {
        $weekend = array('Sat', 'Sun');
        return $start == $end && !(array_search(gmdate('D', $start), $weekend) === false);

    }

    private function checkIfInNotPlanning($start, $end)
    {

        return NotPlanningDay::
        where(function ($query) use ($start) {
            $query->where([
                ['afwezighedenbegin', '<=', $start],
                ['afwezighedeneinde', '>=', $start]
            ]);//start in the middle of none planning days
        })->orWhere(function ($query) use ($start) {
            $query->where([
                ['afwezighedenbegin', '<=', $start],
                ['afwezighedeneinde', '>=', $start - 24 * 60 * 60]
            ]);//start is btw of none planning days with 1 day of delta
        })->orWhere(function ($query) use ($end, $start) {
            $query->where([
                ['afwezighedenbegin', '<=', $start],
                ['afwezighedeneinde', '>=', $end]
            ]);//start and end is btw of none planning days with 1 day of delta
        })->orWhere(function ($query) use ($end, $start) {
            $query->where([
                ['afwezighedenbegin', '>=', $start],
                ['afwezighedeneinde', '<=', $end]
            ]);//planning days are in start and end
        })->count() > 0;
    }
}
