<?php

namespace App\Http\Middleware;

use DomainException;
use \Firebase\JWT\JWT;
use Closure;
use App\Models\Contact\Client;

class ClientLoad
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $client = Client::where('type','=',Client::$TYPES[0])->find($request->client);
        if($client){
            $request->attributes->add(['_client' => $client]);
            return $next($request);
        }else{
            return  response('{"status": "401", "message": "No Client Found!", "data"=>'.$request->client.'}', '401')->header('Content-Type', 'application/json');
        }
      
    }
}
