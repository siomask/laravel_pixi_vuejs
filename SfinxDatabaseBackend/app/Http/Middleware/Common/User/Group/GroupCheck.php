<?php

namespace App\Http\Middleware\Common\User\Group;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use App\Models\Contact\Contact;
use App\Models\User\UserGroup;

class GroupCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $_group = UserGroup::findOrFail($request->userGroupId);
            $request->attributes->add(['_userGroup' => $_group]);
            return $next($request);
        }catch (\Exception $e){
            return response('{"status": "400", "message": "No User Group Found!", "data"=>' . $request->userGroupId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
