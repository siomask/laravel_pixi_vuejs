<?php

namespace App\Http\Middleware\Contact;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use App\Models\Contact\Contact;


class ContactCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $_contact = auth()->user()
//            ->settings
//            ->selectedConstructionSites()->with([
//                'contact' => function ($q) use ($request) {
//                    $q->where('id', $request->contactId);
//                }
//            ])->first();
//        var_dump('------', $_contact->contact->id);
        $_contact = Contact::find($request->contactId);
        if (!empty($_contact)) {
            $request->attributes->add(['_contact' => $_contact]);
            return $next($request);
        } else {
            return response('{"status": "400", "message": "No contact Found!", "data"=>' . $request->contactId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
