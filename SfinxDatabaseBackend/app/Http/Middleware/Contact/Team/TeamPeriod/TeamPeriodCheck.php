<?php

namespace App\Http\Middleware\Contact\Team\TeamPeriod;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use App\Models\Contact\Contact;


class TeamPeriodCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $_contactTeamPeriod = $request->attributes->get('_contact_team')->period()->find($request->periodId) ;
        if (!empty($_contactTeamPeriod)) {
            $request->attributes->add(['_contact_team_period' => $_contactTeamPeriod]);
            return $next($request);
        } else {
            return response('{"status": "400", "message": "No contact team period Found!", "data"=>' . $request->periodId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
