<?php

namespace App\Http\Middleware\Contact\Team;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;
use App\Models\Contact\Contact;


class TeamCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $_contactTeam = $request->attributes->get('_contact')->teams()->find($request->teamId) ;
        if (!empty($_contactTeam)) {
            $request->attributes->add(['_contact_team' => $_contactTeam]);
            return $next($request);
        } else {
            return response('{"status": "400", "message": "No contact team Found!", "data"=>' . $request->teamId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
