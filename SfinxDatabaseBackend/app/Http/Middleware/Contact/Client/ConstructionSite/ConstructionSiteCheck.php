<?php

namespace App\Http\Middleware\Contact\Client\ConstructionSite;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;

class ConstructionSiteCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $site = ConstructionSite::find($request->constructionSiteId);
        if (!empty($site)) {
            $request->attributes->add(['_construction_site' => $site]);
            return $next($request);
        } else {
            return response('{"status": "400", "message": "No client site Found!", "data"=>' . $request->constructionSiteId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
