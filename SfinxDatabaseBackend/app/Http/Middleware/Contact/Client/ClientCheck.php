<?php

namespace App\Http\Middleware\Contact\Client;

use Closure;
use App\Models\Contact\Contact;


class ClientCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $klientId = Contact::$CLIENT_TYPE;
        $_contact = Contact::with('types')->whereHas('types', function ($q) use ($klientId) {
            return $q->where([['contact_contact_type.contact_type_id', '=', $klientId]]);
        })->find($request->clientId);
        if (!empty($_contact)) {
            $request->attributes->add(['_client' => $_contact]);
            return $next($request);
        } else {
            return response('{"status": "400", "message": "No client Found!", "data"=>' . $request->clientId . '}', '400')->header('Content-Type', 'application/json');
        }

    }
}
