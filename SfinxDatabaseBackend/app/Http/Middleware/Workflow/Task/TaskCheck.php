<?php

namespace App\Http\Middleware\Workflow\Task;

use Closure;
use App\Models\Workflow\Task\WorkflowTask;


class TaskCheck
{
    /**2018_06_25_094331_add_table_absences.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $task = WorkflowTask::find($request->taskId);
        if(!empty($task)){
            $request->attributes->add(['_workflow_task' => $task]);
            return $next($request);
        }else{
            return  response('{"status": "400", "message": "No task Found!", "data"=>'.$request->taskId.'}', '400')->header('Content-Type', 'application/json');
        }

    }
}
