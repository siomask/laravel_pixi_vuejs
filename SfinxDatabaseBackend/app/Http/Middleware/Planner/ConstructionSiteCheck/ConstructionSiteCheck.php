<?php

namespace App\Http\Middleware\Planner\ConstructionSiteCheck;

use Closure;
use App\Models\Planner\ConstructionSite\ConstructionSite;


class ConstructionSiteCheck
{
    /**2018_06_25_094331_add_table_absences.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $site =auth()->user()
//            ->settings
//            ->selectedConstructionSites()->find($request->constructionSiteId);
        $site = ConstructionSite::find($request->constructionSiteId);
        if(!empty($site)){
            $request->attributes->add(['_construction_site' => $site]);
            return $next($request);
        }else{
            return  response('{"status": "400", "message": "No site Found!", "data"=>'.$request->constructionSiteId.'}', '400')->header('Content-Type', 'application/json');
        }

    }
}
