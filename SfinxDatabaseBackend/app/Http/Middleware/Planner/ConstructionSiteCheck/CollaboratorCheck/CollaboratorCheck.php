<?php

namespace App\Http\Middleware\Planner\ConstructionSiteCheck\CollaboratorCheck;

use Closure;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;


class CollaboratorCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        $collaborator =auth()->user()
//            ->settings
//            ->selectedConstructionSites()
//            ->find($request->constructionSiteId)
        $collaborator = $request->attributes->get('_construction_site')->collaborators()
        ->findOrFail($request->collaboratorId);
        if(!empty($collaborator)){
            $request->attributes->add(['_construction_site_collaborator' => $collaborator]);
            return $next($request);
        }else{
            return  response('{"status": "400", "message": "No collaborator Found!!", "data"=>'.$request->collaboratorId.'}', '400')->header('Content-Type', 'application/json');
        }

    }
}
