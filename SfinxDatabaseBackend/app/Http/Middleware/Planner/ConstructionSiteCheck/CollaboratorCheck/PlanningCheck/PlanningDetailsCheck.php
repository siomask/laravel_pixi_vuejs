<?php

namespace App\Http\Middleware\Planner\ConstructionSiteCheck\CollaboratorCheck\PlanningCheck;

use Closure;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;


class PlanningDetailsCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $dataPlanning =$request->attributes->get('_construction_site_collaborator')
        ->data()
        ->find($request->planningId);
        if(!empty($dataPlanning) /*&&
            $request->attributes->get('_construction_site_collaborator')->contact()->firstOrFail()->type_id==1*/){
            $request->attributes->add(['_construction_site_collaborator_data' => $dataPlanning]);
            return $next($request);
        }else{
            return  response('{"status": "400", "message": "No planning Found!", "data"=>'.$request->planningId.'}', '400')->header('Content-Type', 'application/json');
        }

    }
}
