<?php

namespace App\Http\Middleware\Finances\OutgoingInvoice;

use Closure;
use App\Models\Finances\OutgoingInvoice\OutgoingInvoice;

class OutgoingInvoiceCheck
{
    /**2018_06_25_094331_add_table_absences.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $invoice = OutgoingInvoice::findOrFail($request->invoiceId);
            $request->attributes->add(['_outgoing_invoice' => $invoice]);
            return $next($request);
        }catch (\Exception $e){
            return response()->json(["message"=>"No outgoing invoice found"],400);
        }

    }
}
