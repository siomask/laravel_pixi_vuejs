<?php

namespace App\Http\Middleware\Finances\IncomingInvoice;

use Closure;
use App\Models\Finances\IncomingInvoice\IncomingInvoice;

class IncomingInvoiceCheck
{
    /**2018_06_25_094331_add_table_absences.php
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $invoice = IncomingInvoice::findOrFail($request->invoiceId);
            $request->attributes->add(['_invoice' => $invoice]);
            return $next($request);
        }catch (\Exception $e){
            return response()->json(["message"=>"No invoice found"],400);
        }

    }
}
