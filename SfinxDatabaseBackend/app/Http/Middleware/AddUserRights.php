<?php

namespace App\Http\Middleware;

use App\Models\Common\UserGroup;
use Closure;

class AddUserRights
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = $request->attributes->get('jwt_after_decode');
        $rights_level = $token->scope->rechten;

        $gebruikersgroep = UserGroup::find($rights_level)->accessRights;

        $rechten_arr = [];

        foreach ($gebruikersgroep as $recht) {
            $rechten_arr[$recht->attributesToArray()['gebruikersrechtenid']] = $recht->attributesToArray()['gebruikersrechtenitem'];
        }

        $request->attributes->add(['gebruiker_rechten' => $rechten_arr]);

        return $next($request);
    }
}
