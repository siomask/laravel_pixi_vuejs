<?php

namespace App\Models\ConstructionSite;

use Illuminate\Database\Eloquent\Model;

class ConstructionSite extends Model
{
    public function projectLeads() {
        return $this->belongsToMany('App\Models\Common\ProjectLead');
    }
}
