<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

abstract class  AbstractUser extends Model
{
    static $MAX_PERC = 100;
    protected $fillable = ['user_id', 'percentage'];

    public function user()
    {
        return $this->belongsTo('App\Models\Common\User');
    }

    public function constructionSite()
    {
        return $this->belongsTo('App\Models\Planner\ConstructionSite\ConstructionSite');
    }
    public function _constructionSite()
    {
        return $this->hasOne('App\Models\Planner\ConstructionSite\ConstructionSite');
    }

    public static function countItems($model, $originCount = false)
    {
        $c = self::where('construction_site_id', $model->construction_site_id)->count() + 1;
        if ($originCount) {
            $c -= 1;
            if ($c <= 0) $c = 1;
        }
        return $c;
    }
    public static function countSumm($model, $originCount = false)
    {
        $c =  0;
        foreach (self::where('construction_site_id', $model->construction_site_id)->get() as $item){
            $c+= $item->percentage;
        }

        return $c;
    }

    /*public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $c = self::$MAX_PERC / self::countItems($model);
            $totalP = $model->percentage;
            $allow = self::$MAX_PERC / self::countItems($model);
            $delta = 1;
            if (!isset($model->percentage)) {
                $model->percentage = self::$MAX_PERC / self::countItems($model);
                self::where('construction_site_id', $model->construction_site_id)->update(['percentage' => $c]);
            } else if ($totalP > $allow + $delta || $totalP < $allow - $delta) {

            } else {
                $items = self::countItems($model,true);
                $totalS = self::countSumm($model);
                if($items==0 && ($totalS > self::$MAX_PERC+$delta|| $totalS < self::$MAX_PERC-$delta)){

                }else{
                    $c = self::$MAX_PERC / (self::countItems($model));
                    $model->percentage = $c;
                    self::where('construction_site_id', $model->construction_site_id)->update(['percentage' => $c]);
                }

            }
        });


        self::updating(function ($model) {
            $totalP = $model->percentage;
            $allow = self::$MAX_PERC / (self::countItems($model, true));
            $delta = 1;
            if ($totalP > $allow + $delta || $totalP < $allow - $delta) {

            } else {
                $items = self::countItems($model,true);
                $totalS = self::countSumm($model);
                if($items>1 && ($totalS > self::$MAX_PERC+$delta|| $totalS < self::$MAX_PERC-$delta)){

                }else{
                    $c = self::$MAX_PERC / (self::countItems($model, true));
                    self::where('construction_site_id', $model->construction_site_id)->update(['percentage' => $c]);
                }


            }
        });


        self::deleted(function ($model) {
//            foreach (self::all()->get() as $item){
//                self::all()->update(['percentage'=>self::$MAX_PERC/self::countItems()]);
//            }
        });


    }*/
}
