<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable=['start','end','name'];

    public function member(){
        return $this->belongsTo('App\Models\Common\Member','team_member_id');
    }

    static function isInHoliday($_start){
       return  self::where(function ($query) use ($_start) {
            $query->where([
                ['start', '<=', $_start],
                ['end', '>=', $_start]
            ]);//start in the middle of none planning days
        })->count() > 0;
    }
}
