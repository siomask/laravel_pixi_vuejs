<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{


    protected $fillable = ['first_name','last_name'];
    protected $table = 'team_members';
    public $timestamps = true;

    public function teamPeriod(){
        return $this->belongsTo('App\Models\Team\TeamPeriod\TeamPeriod');
    }
    public function plannings(){
        return $this->belongsToMany('App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning','team_member_planning_entries',null,'collaborator_planning_entry_id');
    }
    public function absences(){
        return $this->hasMany('App\Models\Common\Absence','team_member_id');
    }

}
