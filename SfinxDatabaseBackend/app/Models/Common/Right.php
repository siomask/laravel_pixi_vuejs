<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Right  extends Model
{
    public $timestamps = true;
    protected $fillable=['name', 'description', 'sequence', 'parent_right_id'];

    public function parentRight() {
        return $this->hasOne('App\Models\Common\Right','parent_right_id');
    }
    public function groups() {
        return $this->belongsToMany('App\Models\User\UserGroup');
    }


    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (!isset($model->sequence)) {
                $model->sequence = self::count() + 1;
            } else {
                if ($model->sequence > self::count()) {
                    $model->sequence = self::count() + 1;
                } else {
                    self::where('sequence', '>=', $model->sequence)->increment('sequence');
                    if ($model->sequence < 1) $model->sequence = 1;
                }
            }
        });


        self::updating(function ($model) {

            if (!isset($model->sequence)) {
                $model->sequence = self::count();
            } else if ($model->sequence > self::count()) {
                $model->sequence = self::count() + 1;
            } else if ($model->sequence < 1) {
                $model->sequence = 1;
            }
        });


        self::deleted(function ($model) {
            self::where('sequence', '>=', $model->sequence)->decrement('sequence');//not for id
        });


    }
}
