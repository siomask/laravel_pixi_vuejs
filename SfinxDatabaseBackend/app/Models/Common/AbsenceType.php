<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class AbsenceType extends Model
{
    protected $fillable=['name'];

    static function types(){
        return self::all();
    }
}
