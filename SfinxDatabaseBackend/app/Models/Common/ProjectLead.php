<?php

namespace App\Models\Common;
use  App\Http\Resources\v1\Planner\ConstructionSitesResource;
use App\Http\Resources\v1\User\UserResource;

class ProjectLead extends AbstractUser
{

    static function leaders($constractionSits){
        $leaders = ProjectLead::with([
            'user',
            /*'constructionSite' => function ($q) {
                $q->withTrashed()->with([
                    'contact' => function ($q) {
                        $q->with([
                            'teams' => function ($q) {
                                $q->with([
                                    'period' => function ($q) {
                                        $q->where(
                                            [
                                                ['start', '<=', Team::activeTime()],
                                                ['end', '>=', Team::activeTime()],
                                            ]
                                        )->with([
                                                'periodMembers' => function ($q) {
                                                    $q->with([
                                                        'member',
                                                        'plannings'
                                                    ]);
                                                }]
                                        )->whereHas('periodMembers.member');
                                    }

                                ])->whereHas('period.periodMembers.member');
                            }
                        ]);
                    },
                    'collaborators',
                    'collaborators.info',
                    'collaborators.data'
                ]);
            }*/
        ])->whereHas('user', function ($q) {
            $q->whereNotIn('id', [auth()->user()->id]);
        })/*->whereHas('constructionSite', function ($q) use ($constractionSits) {
            $q->whereNotIn('id', $constractionSits);
        })*/->get();

        $users = [];
        $_users = [];
        $leaderSites =  [];
        foreach ($leaders as $leader) {
            if (isset($leader['user'])) {
                if (array_search($leader['user']->id, $users) === false) {
                    $users[] = $leader['user']->id;
                    $_users[] = ($leader['user']);
                    $leaderSites[$leader['user']->id] = [];
                }
                /*if (isset($leader->constructionSite)) {
                    $leaderSites[$leader['user']->id][] = new ConstructionSitesResource($leader->constructionSite);
                }*/
            }

        }
//        $leaderSites = (object)$leaderSites;
        foreach ($_users as $usr) {
            $usr->_sites =  $leaderSites[$usr->id];
        }

        $_users = UserResource::collection(collect($_users));
        return $_users;
    }

    static function mapLeaders($leaders){
        $users = [];
        $_users = [];
        $leaderSites =  [];
        foreach ($leaders as $leader) {
            if (isset($leader['user'])) {
                if (array_search($leader['user']->id, $users) === false) {
                    $users[] = $leader['user']->id;
                    $_users[] = ($leader['user']);
                    $leaderSites[$leader['user']->id] = [];
                }
                if (isset($leader->constructionSite)) {
                    $leaderSites[$leader['user']->id][] = new ConstructionSitesResource($leader->constructionSite);
                }
            }

        }
//        $leaderSites = (object)$leaderSites;
        foreach ($_users as $usr) {
            $usr->_sites =  $leaderSites[$usr->id];
        }

        $_users = UserResource::collection(collect($_users));
        return $_users;
    }
    static function projLeader($leader,$users=[]){
        if (isset($leader['user'])) {
            if (array_search($leader['user']->id, $users) === false) {
                $users[] = $leader['user']->id;
                $_users[] = ($leader['user']);
                $leaderSites[$leader['user']->id] = [];
            }
            if (isset($leader->constructionSite)) {
                $leader['user']->_sites = new ConstructionSitesResource($leader->constructionSite);
            }
            return new UserResource($leader['user']);
        }
    }
}
