<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ConstructionSiteUserSettingType extends Model
{
    static $TYPES=['planner','workflow'];
}
