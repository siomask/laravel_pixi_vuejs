<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class ContractorType extends Model
{
    protected $fillable=['abbreviation','name','sequence'];
    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            if(!isset($model->sequence)){
                $model->sequence = self::count()+1;
            }else{
                if($model->sequence>self::count()){
                    $model->sequence = self::count()+1;
                }else{
                    self::where('sequence','>=',$model->sequence)->increment('sequence');
                    if($model->sequence<1)$model->sequence=1;
                }
            }
        });



        self::updating(function($model){

            if(!isset($model->sequence)){
                $model->sequence = self::count();
            }else if($model->sequence>self::count()){
                $model->sequence = self::count()+1;
            }else if($model->sequence<1){
                $model->sequence=1;
            }
        });


        self::deleted(function($model){
            self::where('sequence','>=',$model->sequence)->decrement('sequence');//not for id
        });


    }
}
