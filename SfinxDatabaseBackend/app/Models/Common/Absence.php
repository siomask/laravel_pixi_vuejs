<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    protected $fillable=['start','end','absence_type_id'];
    public function type(){
        return $this->hasOne('App\Models\Common\AbsenceType');
    }
}
