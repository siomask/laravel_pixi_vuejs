<?php

namespace App\Models\Planner\ConstructionSite;

use App\Models\Common\Member;
use App\Models\Common\Holiday;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Common\ConstructionSiteUserSettingType;
use Carbon\Carbon;
use App\Models\Team\TeamMember\TeamMemberPlanningEntry;

class ConstructionSite extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'contact_id',
        'deleted_at',
        'presented_price',
        'sequence',
        'total_amount_processed',
        'total_amount_orders',
        'date_financial_details',
        'construction_site_statuses_id'
    ];

    public function contact()
    {
        return $this->belongsTo('\App\Models\Contact\Contact', 'contact_id');
    }

    public function collaborators()
    {
        return $this->hasMany('\App\Models\Planner\ConstructionSite\Collaborator\Collaborator');
    }

    public function userSettings()
    {
        return $this->hasMany('\App\Models\Common\UserSetting');
    }

    public function _userSettings()
    {
        return $this->belongsToMany('\App\Models\Common\UserSetting');
    }

    public function projectLeads()
    {
        return $this->hasMany('App\Models\Common\ProjectLead');
    }

    public function estimators()
    {
        return $this->hasMany('\App\Models\Common\Estimator');
    }

    public function workflowTasks()
    {
//        return $this->belongsTo('\App\Models\Workflow\Task\WorkflowTask' );
        return $this->belongsToMany('\App\Models\Workflow\Task\WorkflowTask')
            ->withPivot(['start', 'end', 'task_status', 'description', 'id']);
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Planner\ConstructionSite\ConstructionSiteStatus', 'construction_site_statuses_id');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Planner\ConstructionSite\ConstructionSiteUserSetting');
    }

    public function externals()
    {
        return $this->hasMany('App\Models\Planner\ConstructionSite\External\ConstructionSiteExternal');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $count = self::sequenceC($model);
            if (!isset($model->sequence)) {
                $model->sequence = $count + 1;
            } else {
                if ($model->sequence > $count) {
                    $model->sequence = $count + 1;
                } else {
                    self::where([
//                        ['contact_id', '=', $model['contact_id']],
                        ['construction_site_statuses_id', '=', $model['construction_site_statuses_id']]
                    ])->where('sequence', '>=', $model->sequence)->increment('sequence');
                    if ($model->sequence < 1) $model->sequence = 1;
                }
            }
        });

        self::updating(function ($model) {
            $count = self::sequenceC($model);
            $or = self::findOrFail($model->id);
            $sequence = $model->sequence;
            if (empty($sequence)) {
                $model->sequence = $sequence = $count;
            } else if ($sequence > $or->sequence) {
                self::where([
                    ['sequence', '>', $or->sequence],
                    ['sequence', '<=', $sequence],
                    ['construction_site_statuses_id', '=', $model['construction_site_statuses_id']]
                ])->decrement('sequence');

            } else if ($sequence < $or->sequence) {
                self::where([
                    ['sequence', '<', $or->sequence],
                    ['sequence', '>=', $sequence],
                    ['construction_site_statuses_id', '=', $model['construction_site_statuses_id']]
                ])->increment('sequence');

            }
            $model->sequence = $sequence;
            if ($model->sequence > $count) {
                $model->sequence = $count + 1;

            } else if ($model->sequence < 1) {
                $model->sequence = 1;
            }
        });
        self::deleted(function ($model) {
            self::where([
                ['sequence', '>=', $model->sequence],
                ['construction_site_statuses_id', '=', $model['construction_site_statuses_id']]
            ])->decrement('sequence');//not for id
        });
    }

    private static function sequenceC($model)
    {
        return self::where(
            [
//                ['contact_id', '=', $model['contact_id']],
                ['construction_site_statuses_id', '=', $model['construction_site_statuses_id']]
            ]
        )->count();
    }


    static function plannings($month = null, $memberId)
    {
        try {
            $where = [];
            if (!empty($month)) {
                $start = Carbon::parse($month)->startOfMonth();
                $end = Carbon::parse($month)->endOfMonth();

                $where = TeamMemberPlanningEntry::planningByDate($start, $end, 0);
            }


            $holidays = Holiday::all();
            $member = Member::where('id',$memberId)->with([
                'absences'
            ])->first();
            $sites = self::with([
                'collaborators' => function ($q) use ($memberId, $where) {
                    $q->with([
                        'data',
                        'contact' => function ($q) use ($memberId, $where) {
                            $q->with([
                                'teams' => function ($q) use ($memberId, $where) {
                                    $q->with([
                                        'period' => function ($q) use ($memberId, $where) {
                                            $q->with([
                                                'periodMembers' => function ($q) use ($memberId, $where) {
                                                    $q->where([
                                                        ['team_member_id', '=', $memberId]
                                                    ])->with([
                                                        'plannings' => function ($q) use ($where) {
                                                            $q->where($where)->with([
                                                                'pivotPlannings'
                                                            ]);
                                                        }
                                                    ]);//->whereHas(['plannings','plannings.pivotPlannings']);
                                                }
                                            ])->whereHas('periodMembers.plannings');
                                        }
                                    ])->whereHas('period.periodMembers');
                                }
                            ])->whereHas('teams.period');
                        }
                    ])->whereHas('contact.teams');
                }

            ])
                ->whereHas('collaborators')
                ->whereHas('collaborators.data')
                ->whereHas('collaborators.contact')
                ->get();
//            return $sites;
            $resp = [];
            foreach ($sites as $site) {
                $siteId = $site->id;

                foreach ($site->collaborators as $collaborator) {
                    if (isset($collaborator->contact) && $collaborator->data->count() > 0) {

                        foreach ($collaborator->data as $dataPlanning) {
                            if ($collaborator->id == $dataPlanning->construction_site_collaborator_id) {
                                foreach ($collaborator->contact->teams as $team) {

                                    foreach ($team->period as $period) {
                                        foreach ($period->periodMembers as $periodMember) {
                                            foreach ($periodMember->plannings as $planning) {

                                                if ($planning->pivotPlannings->collaborator_planning_entry_id == $dataPlanning->id) {
                                                    if (empty($resp[$siteId])) $resp[$siteId] = (object)[
                                                        'contact' => [
                                                            'name' => $site->contact->first_name . " " . $site->contact->last_name
                                                        ],
                                                        'id' => $siteId,
                                                        'plannings' => []
                                                    ];
                                                    $planMember = clone $planning;
                                                    $planMember->siteId = $siteId;
                                                    $planMember->collaboratorId = $collaborator->id;
                                                    $planMember->planningId = $planning->pivotPlannings->collaborator_planning_entry_id;
                                                    $planMember->teamPeriodId = $period->id;
                                                    $planMember->teamId = $team->id;

                                                    $planMember =self::checkIfNotShouldBePlanned($member, $holidays, $planMember);
                                                    if($planMember->isShouldBeNotPlanning){
//                                                        $resp[$siteId]->isShouldBeNotPlanning=true;
                                                    }else{
                                                        $resp[$siteId]->plannings[$planning->id] = $planMember;
                                                    }
                                                }

                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            return response()->json([
                'sickDays'=>self::sickDays($member,$holidays),
                'data'=>$resp
            ]);
        } catch (\Exception $e) {
            return response()->json(["message" => "Couldn`t fetch sites", "error" => $e->getMessage()], 400);
        }

    }

    private static function sickDays($member,$holidays){
        $resp = [];
        foreach ($holidays as $holiday) {//including member holidays
            $resp[]=$holiday;
        }
        foreach ($member->absences as $absences) {
            $resp[]=$absences;
        }
        return $resp;
    }
    private static function checkIfNotShouldBePlanned($member, $holidays, $planning)
    {
        $day = Carbon::parse($planning->date);

        foreach ($holidays as $holiday) {//including member holidays
            if ($day->gte(Carbon::parse($holiday->start)) && $day->lte(Carbon::parse($holiday->end))) {
                $planning->isShouldBeNotPlanning = true;
                return $planning;
            }
        }
        foreach ($member->absences as $absences) {
            if ($day->gte(Carbon::parse($absences->start)) && $day->lte(Carbon::parse($absences->end))) {
                $planning->isShouldBeNotPlanning = true;
                return $planning;
            }
        }
        return $planning;
    }
}
