<?php

namespace App\Models\Planner\ConstructionSite\External;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ConstructionSiteExternal extends Model
{
    protected $fillable=['contact_id','ordered','comment','is_assign','is_ordered','visible'];
    public function contact() {
        return $this->belongsTo('\App\Models\Contact\Contact');
    }

    public function site() {
        return $this->belongsTo('\App\Models\Planner\ConstructionSite');
    }


    public static function boot()
    {
        parent::boot();

        /*self::creating(function($model){
            if(!isset($model->ordered_by)){
                $model->ordered_by = self::count()+1;
            }else{
                if($model->ordered_by>self::count()){
                    $model->ordered_by = self::count()+1;
                }else{
                    self::where('ordered_by','>=',$model->ordered_by)->increment('ordered_by');
                    if($model->ordered_by<1)$model->ordered_by=1;
                }
            }
        });



        self::updating(function($model){

            if(!isset($model->ordered_by)){
                $model->ordered_by = self::count();
            }else if($model->ordered_by>self::count()){
                $model->ordered_by = self::count()+1;
            }else if($model->ordered_by<1){
                $model->ordered_by=1;
            }
        });


        self::deleted(function($model){
            self::where('ordered_by','>=',$model->ordered_by)->decrement('ordered_by');//not for id
        });*/


    }


}
