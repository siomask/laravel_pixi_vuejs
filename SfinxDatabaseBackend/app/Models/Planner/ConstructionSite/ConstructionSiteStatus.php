<?php

namespace App\Models\Planner\ConstructionSite;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Common\ConstructionSiteUserSettingType;

class ConstructionSiteStatus extends Model
{
    protected $fillable = ['contact_type_id', 'name'];

    public function contactType()
    {
        return $this->belongsTo('\App\Models\Contact\ContactType', 'contact_type_id');
    }

}
