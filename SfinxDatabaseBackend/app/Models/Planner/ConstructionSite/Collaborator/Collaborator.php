<?php

namespace App\Models\Planner\ConstructionSite\Collaborator;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Collaborator extends Model
{
    use SoftDeletes;
    protected $table = 'construction_site_collaborators';
    protected $fillable = ['start', 'end', 'contractor_type_id', 'contact_id',
        'comment',
        'is_contractor_type_assigned', 'is_contractor_type_ordered', 'is_assigned', 'is_ordered', 'request_date'];

    public function data()
    {
        return $this->hasMany('\App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning', 'construction_site_collaborator_id');
    }

    public function info()
    {
        return $this->belongsTo('\App\Models\Common\ContractorType', 'contractor_type_id');
    }

    public function contact()
    {
        return $this->belongsTo('\App\Models\Contact\Contact', 'contact_id');
    }

    public function constructionSites()
    {
        return $this->belongsTo('App\Models\Planner\ConstructionSite\ConstructionSite', 'construction_site_id');
    }

    public function canPlan($startD, $endD)
    {
        $start = Carbon::parse($this->start);
        $end = Carbon::parse($this->end);
        return $startD->gt($start) && $startD->lt($end) && $endD->lt($end);
    }

}
