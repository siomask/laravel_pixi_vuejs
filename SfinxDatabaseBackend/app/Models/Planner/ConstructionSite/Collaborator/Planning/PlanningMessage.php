<?php

namespace App\Models\Planner\ConstructionSite\Collaborator\Planning;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Common\Holiday;
use App\Models\Team\TeamMember\TeamMemberPlanningEntry;

class PlanningMessage extends Model
{
    protected $table = 'collaborator_planning_messages';
    protected $fillable = ['date', 'description', 'description_to_do', 'collaborator_planning_entry_id'];


    static function updatePlannings($dateRange, $id, $daysUpdateTime)
    {
        $planned = PlanningMessage::where('collaborator_planning_entry_id', $id);
        $planningMembers = $dateRange['planningMembers'];
        if (empty($daysUpdateTime) || $daysUpdateTime === 0) {

        } else {

            //:ToDo need more test
            //update tema planns throught week days , SAT SUN
            foreach ($planned->get() as $_planned) {
                $_newDate = Carbon::parse($_planned->date);
                $curDUpdateTime = 0;

                if ($daysUpdateTime > 0) {
                    while ($curDUpdateTime++ < $daysUpdateTime) {
                        $_newDate->addDays(1);

                        if ($dateRange['isWeekend']) {
                            while (Holiday::isInHoliday($_newDate)) {
                                $_newDate->addDays(1);
                            }
                        } else {
                            while ($_newDate->isWeekend() || Holiday::isInHoliday($_newDate)) {
                                $_newDate->addDays(1);
                            }
                        }
                    }
                } else {
                    while ($curDUpdateTime-- > $daysUpdateTime) {
                        $_newDate->subDays(1);
                        if ($dateRange['isWeekend']) {
                            while (Holiday::isInHoliday($_newDate)) {
                                $_newDate->subDays(1);
                            }
                        } else {
                            while ($_newDate->isWeekend() || Holiday::isInHoliday($_newDate)) {
                                $_newDate->subDays(1);
                            }
                        }
                    }
                }
                $_planned->update(['date' => $_newDate->toDateTimeString()]);
            }


        }

        if (count($planningMembers) > 0) {
            $delete = [];
            $update = [];
            $plannedArDay = array_map(function ($e) {
                return $e['date'];
            }, $planned->get()->toArray());

            foreach (PlanningMessage::whereIn('collaborator_planning_entry_id', $planningMembers)->get() as $_planned) {
                if (
                    array_search($_planned->date, $plannedArDay) === false
                ) {
                    $update[] = $_planned->id;
                } else {
                    $delete[] = $_planned->id;
                }
            }
            if (count($delete) > 0) PlanningMessage::whereIn('id', $delete)->delete();//remove same
            if (count($update) > 0) PlanningMessage::whereIn('id', $update)->update([//just set new parent
                'collaborator_planning_entry_id' => $id
            ]);

        }
    }

    static function updatePlanningMessages($groups, $group, $day = 0)
    {
        $th = PlanningMessage::getPlanning($group->id, null, null);
        $th['plannedMsg'] = $th['plannedMsg']->get();
        $th['upd'] = [];
        foreach ($th['plannedMsg'] as $plan) {
            $th['upd'] = $plan->date;
            if (!empty($day)) {
                if (Carbon::parse($plan->date)->eq($day)) {
                    $plan->delete();
                    continue;
                }
            }
            if (Carbon::parse($groups[0]->start)->lte(Carbon::parse($plan->date)) &&
                Carbon::parse($groups[0]->end)->gte(Carbon::parse($plan->date))) {
                $plan->update(['collaborator_planning_entry_id' => $groups[0]->id]);
            } else {
                $plan->update(['collaborator_planning_entry_id' => $groups[1]->id]);
            }

        }
        return $th;
    }

    static function dropPlanning($cellId, $start, $end, $from = 0)
    {

        $t = PlanningMessage::getPlanning($cellId, $start, $end, $from);
        $t['plannedMsg']->delete();
        return $t;
    }

    static function getPlanning($cellId, $start, $end, $from = 0)
    {
        $where = [
            ['collaborator_planning_entry_id', '=', $cellId]
        ];
        if (!empty($start) && !empty($end)) {
            if ($from == 1) {
                $where[] = ['date', '>=', $start];
                $where[] = ['date', '<', $end];
            } else if ($from == 2) {
                $where[] = ['date', '>', $start];
                $where[] = ['date', '<=', $end];
            } else {
                $where[] = ['date', '>=', $start];
                $where[] = ['date', '<=', $end];
            }

        }
        $plannedTeam = PlanningMessage::select()->where($where);

        return [
            'cond' => $where,
            'plannedMsg' => $plannedTeam
        ];
    }
}
