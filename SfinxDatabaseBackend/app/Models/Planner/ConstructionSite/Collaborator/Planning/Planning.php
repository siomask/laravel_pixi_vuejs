<?php

namespace App\Models\Planner\ConstructionSite\Collaborator\Planning;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Common\Holiday;
use App\Models\Team\TeamMember\TeamMemberPlanningEntry;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;

class Planning extends Model
{
    protected $table = 'collaborator_planning_entry';
    protected $fillable = ['construction_site_collaborator_id', 'start', 'end', 'description', 'description_to_do'];


    private static function checkString($model, $field = 'description')
    {
        $MAX_STR_LENGT = 190;
        if (strlen($model[$field]) > $MAX_STR_LENGT) $model[$field] = substr($model[$field], 0, $MAX_STR_LENGT - 1);
    }

    public function teams()
    {
        return $this->belongsToMany('App\Models\Team\Team', 'collaborator_team_planning_entries', 'collaborator_planning_entry_id', 'team_id');
    }

    public function messages()
    {
        return $this->hasMany('App\Models\Planner\ConstructionSite\Collaborator\Planning\PlanningMessage', 'collaborator_planning_entry_id');
    }

    public function collaborator()
    {
        return $this->hasOne('App\Models\Planner\ConstructionSite\Collaborator\Collaborator');
    }


    private static function updateMinAndMaxDateForCollaborator($model)
    {
        $startDate = null;
        $endDate = null;
        foreach (self::where([['construction_site_collaborator_id', $model->construction_site_collaborator_id]])->get() as $plannings) {
            if (empty($startDate)) $startDate = Carbon::parse($plannings->start);
            if (empty($endDate)) $endDate = Carbon::parse($plannings->end);
            $startDate = $startDate->min(Carbon::parse($plannings->start));
            $endDate = $endDate->max(Carbon::parse($plannings->end));
        }
        Collaborator::findOrFail($model->construction_site_collaborator_id)->update([
            'start' => empty($startDate) ? null : $startDate->toDateTimeString(),
            'end' => empty($endDate) ? null : $endDate->toDateTimeString()
        ]);

    }

    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            self::updateMinAndMaxDateForCollaborator($model);
        });

        self::updated(function ($model) {
            self::updateMinAndMaxDateForCollaborator($model);
        });

        self::deleted(function ($model) {
            self::updateMinAndMaxDateForCollaborator($model);
        });


        self::creating(function ($model) {
            self::checkString($model);
        });


        self::updating(function ($model) {
            self::checkString($model);
        });


        self::deleted(function ($model) {
        });

    }

    static function move($branch, $start, $end, $daysToMove)
    {

        $result = (object)['state' => 2, "updates" => []];
        $planningData = $branch->data()->get();

        if (isset($start) && isset($end)) {
            $_list = $planningData;
            $planningData = [];
            foreach ($_list as $plannedD) {
                $st = Carbon::parse($plannedD->start);
                $en = Carbon::parse($plannedD->end);
                if (
                    ($start->gte($st) &&
                        $start->lte($en)) ||
                    ($end->gte($st) &&
                        $end->lte($en)) ||
                    ($st->gte($start) &&
                        $st->lte($end)) ||
                    ($en->gte($start) &&
                        $en->lte($end))
                ) {
                    $planningData[] = $plannedD;
                }
            }
            $planningData = collect($planningData);
        }


        $ignore = array_map(function ($el) {
            return $el['id'];
        }, $planningData->toArray());

        foreach ($planningData as $plannedD) {
            $result->updates[] = $plannedD->id;
            try {
                $result->updates[] = Planning::onMove([
                    'ignore' => $ignore,
                    'plannedD' => $plannedD,
                    'daysUpdateTime' => $daysToMove,
                    'branch' => $branch,
                    'collaboratorId' => $branch->id,
                    'start' => Carbon::parse($plannedD['start'])->addDays($daysToMove)->toDateTimeString(),
                    'end' => Carbon::parse($plannedD['end'])->addDays($daysToMove)->toDateTimeString()
                ]);
            } catch (\Exception $e) {
                $result->updates[] = $e->getMessage();
            }

        }
        if ($planningData->count() == 0) return (['state' => 1, "msg" => "No planning data", "d" => $planningData]);
        return $result;
    }

    static function checkIfInNotPlanning($start, $end, $branch)
    {

        return false;//
        $_start = Carbon::parse($start)->toDateTimeString();
        $_end = Carbon::parse($end)->toDateTimeString();
        return Holiday::
            where(function ($query) use ($_start) {
                $query->where([
                    ['start', '<=', $_start],
                    ['end', '>=', $_start]
                ]);//start in the middle of none planning days
            })->orWhere(function ($query) use ($_end) {
                $query->where([
                    ['start', '<=', $_end],
                    ['end', '>=', $_end]
                ]);//end in the middle of none planning days
            })->orWhere(function ($query) use ($_start, $_end) {
                $query->where([
                    ['start', '>=', $_start],
                    ['start', '<=', $_end]
                ]);//end in the middle of none planning days
            })->orWhere(function ($query) use ($_start, $_end) {
                $query->where([
                    ['end', '>=', $_start],
                    ['end', '<=', $_end]
                ]);//end in the middle of none planning days
            })->count() > 0 || self::collaboratorCanPlan($branch, Carbon::parse($_start), Carbon::parse($_end));
    }

    static function collaboratorCanPlan($collaborator, $startD, $endD)
    {
//        $start = Carbon::parse($collaborator->start);
//        $end = Carbon::parse($collaborator->end);
        return false;//!($startD->gte($start) && $startD->lte($end) && $endD->lte($end));
    }

    static function onMove($request)
    {
        $end = Carbon::parse($request['plannedD']['end']);
        $start = Carbon::parse($request['plannedD']['start']);
        if ($start->isWeekend() || $end->isWeekend()) {

        } else {
            $days = [$start, $end];
            $daysUpdateTime = $request['daysUpdateTime'];
            for ($i = 0; $i < count($days); $i++) {
                $_newDate = $days[$i];
                $curDUpdateTime = 0;
                if ($daysUpdateTime > 0) {
                    while ($curDUpdateTime++ < $daysUpdateTime) {
                        $_newDate->addDays(1);
                        while ($_newDate->isWeekend()) {
                            $_newDate->addDays(1);
                        }
                    }
                } else {
                    while ($curDUpdateTime-- > $daysUpdateTime) {
                        $_newDate->subDays(1);
                        while ($_newDate->isWeekend()) {
                            $_newDate->subDays(1);
                        }
                    }
                }
                $request[$i == 0 ? 'start' : 'end'] = $_newDate;
            }

        }

        return Planning::updateDateRange($request, $request['plannedD']['id']);
    }

    static function updateDateRange($request, $id)
    {
        if (empty($request['end']) || empty($request['start'])) return (['state' => 1, "msg" => "Required data missed"]);

        $result = $request['plannedD'];
        /*if (Planning::checkIfInNotPlanning($request['start'], $request['end'], $request['branch'])) {
            return [
                'state' => 1,
                "msg" => "Can`t plane in holiday or out of branch daterange for branch(" . $request['collaboratorId'] . ") from(" .
                    $request['start'] . ") to (" . $request['end'] . ")"
            ];
        }*/
        $request['ignore'][] = $id;

        $daysUpdateTime = $request['daysUpdateTime'];
        $_start = Carbon::parse($request['plannedD']['start']);
        $_end = Carbon::parse($request['plannedD']['end']);

        if (isset($daysUpdateTime)) {
            if ($daysUpdateTime == 0) {
                return (['state' => 1, "msg" => "Nothing to update"]);
            } else {
                if ($_start->isWeekend() || $_end->isWeekend()) {
                    if ($daysUpdateTime > 0) {
                        $_start->addDays($daysUpdateTime);
                        $_end->addDays($daysUpdateTime);
                    } else {
                        $daysUpdateTime = abs($daysUpdateTime);
                        $_end->subDays($daysUpdateTime);
                        $_start->subDays($daysUpdateTime);
                    }

                } else {
                    $curDUpdateTime = 0;
                    if ($daysUpdateTime > 0) {
                        while ($curDUpdateTime++ < $daysUpdateTime) {
                            $_start->addDays(1);
                            $_end->addDays(1);
                            while ($_start->isWeekend()) {
                                $_start->addDays(1);
                            }
                            while ($_end->isWeekend()) {
                                $_end->addDays(1);
                            }
                        }
                    } else {
                        while ($curDUpdateTime-- > $daysUpdateTime) {
                            $_start->subDays(1);
                            $_end->subDays(1);
                            while ($_start->isWeekend()) {
                                $_start->subDays(1);
                            }
                            while ($_end->isWeekend()) {
                                $_end->subDays(1);
                            }
                        }
                    }
                }
            }
        }


        $start = Carbon::parse($request['start']);
        $end = Carbon::parse($request['end']);

        if (isset($request['merge'])) {

        } else {
            $allPlannings = Planning::where('construction_site_collaborator_id', $request['collaboratorId'])
                ->whereNotIn('id', $request['ignore'])->get();
            $noPlan = false;
            foreach ($allPlannings as $planedPeriod) {
                $planedPeriod->start = Carbon::parse($planedPeriod->start);
                $planedPeriod->end = Carbon::parse($planedPeriod->end);
                if (!$_start->isWeekend() && $planedPeriod->start->isWeekend() ||
                    $_start->diffInDays($_end) == 0 && $_start->isWeekend() && !$planedPeriod->start->isWeekend()) {
                    continue;
                }

                if (
                    $_start->gte($planedPeriod->start) && $_end->lte($planedPeriod->end) ||//full intersect
                    $planedPeriod->start->gt($_start) && $planedPeriod->start->lt($_end) ||//start is btw
                    $planedPeriod->end->gt($_start) && $planedPeriod->end->lt($_end) || //end is btw


                    $_start->gte($planedPeriod->start) && $_start->lte($planedPeriod->end) ||
                    $_end->gte($planedPeriod->start) && $_end->lte($planedPeriod->end)

                ) {
                    $noPlan = $planedPeriod;
                    break;
                }
            }

            if ($noPlan) {
                return ([
                    'state' => 1,
                    "msg" => "Can`t planned on planned days from " . $planedPeriod->start . " to " . $planedPeriod->end
                ]);
            }
        }

        $planningData = $request['branch']->data()
            ->whereNotIn('id', $request['ignore'])
            ->get()->toArray();

        $dateRange = Planning::getDateRange($planningData, $request);
        if (!empty($dateRange)) {
            if (!empty($result)) {
                $daysUpdateTime = empty($request['daysUpdateTime']) ? 0 : $request['daysUpdateTime'];
                $result->update([
                    'start' => $dateRange['start'],
                    'end' => $dateRange['end']
                ]);

                TeamMemberPlanningEntry::updatePlannings($dateRange, $id, $daysUpdateTime);
            }

            return !empty($result) ? (['state' => 2, 'plannes' => $dateRange['planningMembers'], 'time' => $daysUpdateTime]) : (['state' => 1, "msg" => "Nothing Update", "data" => $id]);
        } else {
            return (['state' => 1, "msg" => "No DateRange"]);
        }
    }

    static function getDateRange($planningData, $request, $isCreating = false)
    {
        $start = $request['start'] = Carbon::parse($request['start']);
        $end = $request['end'] = Carbon::parse($request['end']);

        $isWeekend = Planning::checkIfWeekend($start, $end);
        $plannedMembers = [];

        if ($isWeekend) {
            if (!isset($request['ignore'])) $request['ignore'] = [];
            $_planningData = Planning::where('construction_site_collaborator_id', $request['collaboratorId'])
                ->whereNotIn('id', $request['ignore'])
                ->where([
                    ['end', '<=', $end],
                    ['start', '>=', $start]
                ])->get()->toArray();

            if (isset($_planningData)) {
                foreach ($_planningData as $datum) {
                    $datum['end'] = Carbon::parse($datum['end']);
                    $datum['start'] = Carbon::parse($datum['start']);

                    if ($datum['end']->eq($datum['start'])) {//on weekend can planned only for one day
                        $plannedMembers [] = $datum['id'];
                    }

                }
            }
        } else {

            foreach ($planningData as $datum) {
                $datum['end'] = Carbon::parse($datum['end']);
                $datum['start'] = Carbon::parse($datum['start']);
                if ($start->gt($datum['end'])
                    || $end->lt($datum['start'])
                    || Planning::checkIfWeekend($datum['start'], $datum['end']))//ignore weekend days
                    continue;
                if ($isCreating && $start->gte($datum['start']) && $end->lte($datum['end']) && !isset($request['merge']))//no need to add new planned day
                    return null;

                $start = $start->min($datum['start']);
                $end = $end->max($datum['end']);
                $plannedMembers [] = $datum['id'];

            }
        }
        return [
            'start' => $start,
            'end' => $end,
            'isWeekend' => $isWeekend,
            'planningMembers' => $plannedMembers
        ];
    }

    static function checkIfWeekend($start, $end)
    {

        $weekend = array('Sat', 'Sun');

        return $start == $end && !(array_search(gmdate('D', $start->getTimestamp()), $weekend) === false);

    }
}
