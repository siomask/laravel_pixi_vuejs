<?php

namespace App\Models\Planner\ConstructionSite;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ConstructionSiteUserSetting extends Model
{
    use SoftDeletes;
    protected $table='construction_site_user_setting';
    protected $fillable=['construction_site_id','user_setting_id','construction_site_user_setting_type_id'];
    public function constructionSites() {
        return $this->belongsToMany('\App\Models\Planner\ConstructionSite\ConstructionSite');
    }

}
