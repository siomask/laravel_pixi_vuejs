<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class WorkflowFunctionGroup extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'sequence'];

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model['sequence'] = self::count() + 1;
        });

        self::updating(function($model){
            if(!isset($model->sequence)){
                $model->sequence = self::count();
            }else if($model->sequence>self::count()){
                $model->sequence = self::count()+1;
            }else if($model->sequence<1){
                $model->sequence=1;
            }
        });


        self::deleted(function($model){
            self::where('sequence','>=',$model->sequence)->decrement('sequence');//not for id
        });


    }
    public function workflowTasks()
    {
        return $this->belongsToMany('App\Models\Workflow\Task\WorkflowTask');
    }


}
