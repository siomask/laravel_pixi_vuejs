<?php

namespace App\Models\Workflow\Task;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class WorkflowTask extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'sequence', 'parent_workflow_task_id'];

    public function workflowFunctionGroups()
    {
        return $this->belongsToMany('App\Models\Workflow\WorkflowFunctionGroup');
    }

    function parentWorkflowTask()
    {
        return $this->hasOne('App\Models\Workflow\Task\WorkflowTask','parent_workflow_task_id');
    }


    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model['sequence'] = self::where('parent_workflow_task_id', $model['parent_workflow_task_id'])->count() + 1;
        });
        self::deleted(function($model){
            self::where([
                ['parent_workflow_task_id','=',$model['parent_workflow_task_id']],
                ['sequence','>=',$model->sequence]
            ])->decrement('sequence');//not for id
        });

    }
}
