<?php

namespace App\Models\Workflow;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class ConstructionSiteWorkflowTask extends Model
{
    public $timestamps = true;
    protected $table = 'construction_site_workflow_task';
    protected $fillable = ['start', 'end', 'workflow_task_id', 'task_status', 'description'];

    public function task()
    {
        return $this->hasOne('App\Models\Workflow\Task\WorkflowTask');
    }



}
