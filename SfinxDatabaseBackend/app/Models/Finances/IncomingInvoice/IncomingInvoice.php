<?php

namespace App\Models\Finances\IncomingInvoice;

use  App\Models\Finances\AbstractInvoice;

class IncomingInvoice extends AbstractInvoice
{
    protected $fillable = ['contact_id', 'date', 'invoice_number',
        'serial_number', 'payment_term', 'is_paid', 'financial_discount',
        'discount', 'comment'];

    public function entries()
    {
        return $this->hasMany('\App\Models\Finances\IncomingInvoice\Entrty\IncomingInvoiceEntry');
    }

    public function sites()
    {
        return $this->belongsToMany('\App\Models\Planner\ConstructionSite\ConstructionSite');
    }

}
