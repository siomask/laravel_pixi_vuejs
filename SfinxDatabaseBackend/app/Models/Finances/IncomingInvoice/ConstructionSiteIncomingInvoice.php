<?php

namespace App\Models\Finances\IncomingInvoice;

use Illuminate\Database\Eloquent\Model;

class ConstructionSiteIncomingInvoice extends Model
{
    protected $table='construction_site_incoming_invoices';
    protected $fillable = ['amount','incoming_invoice_id','construction_site_id','contractor_type_id'];

}
