<?php

namespace App\Models\Finances\IncomingInvoice\Entrty;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IncomingInvoiceEntry extends Model
{
    protected $fillable = ['name' ,'amount','vat_percentage','construction_site_id' ];

    public function site()
    {
        return $this->belongsTo('\App\Models\Planner\ConstructionSite\ConstructionSite','construction_site_id');
    }
}
