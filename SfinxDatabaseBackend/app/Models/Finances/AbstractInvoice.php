<?php

namespace App\Models\Finances;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class AbstractInvoice extends Model
{

    public function contact()
    {
        return $this->belongsTo('\App\Models\Contact\Contact');
    }


    public static function lastByDate($contact)
    {
        if (empty($contact)) return null;
        try {
            return self::where([
                ['contact_id', '=', $contact->id]
            ])->orderBy('date')->first();
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function totalInvoices($contact)
    {
        if (empty($contact)) return null;
        $total = 0;
        try {
            foreach (self::where([
                ['contact_id', '=', $contact->id]
            ])->with('entries')->get() as $invoice) {
                foreach ($invoice->entries as $entry) {
                    $total += (float)$entry->amount;
                }
            }
        } catch (\Exception $e) {
            //error
        } finally {
            return $total;
        }
    }
}
