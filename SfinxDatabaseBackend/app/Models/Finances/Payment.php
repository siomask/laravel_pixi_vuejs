<?php

namespace App\Models\Finances;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['amount','date'];

}
