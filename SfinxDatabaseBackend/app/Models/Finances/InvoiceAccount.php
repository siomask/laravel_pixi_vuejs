<?php

namespace App\Models\Finances;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceAccount extends Model
{
    protected $fillable = ['description','amount','sequence','parent_invoice_account_id'];

    public function parentInvoiceAccount(){
        return $this->hasOne('App\Models\Finances\InvoiceAccounts','parent_invoice_account_id');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model['sequence'] = self::where('parent_invoice_account_id', $model['parent_invoice_account_id'])->count() + 1;
        });
        self::deleted(function($model){
            self::where([
                ['parent_invoice_account_id','=',$model['parent_invoice_account_id']],
                ['sequence','>=',$model->sequence]
            ])->decrement('sequence');//not for id
        });

    }
}
