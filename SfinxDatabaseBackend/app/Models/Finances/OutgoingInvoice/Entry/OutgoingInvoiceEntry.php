<?php

namespace App\Models\Finances\OutgoingInvoice\Entry;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutgoingInvoiceEntry extends Model
{
    protected $fillable = ['name', 'amount', 'vat_percentage'];
}
