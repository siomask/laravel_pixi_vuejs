<?php

namespace App\Models\Finances\OutgoingInvoice;

use App\Models\Finances\AbstractInvoice;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OutgoingInvoice extends AbstractInvoice
{
    protected $fillable = ['invoice_number', 'date', 'payment_term', 'comment', 'contact_id', 'invoice_type_id'];

    public function payments()
    {
        return $this->hasMany('App\Models\Finances\Payment');
    }

    public function entries()
    {
        return $this->hasMany('\App\Models\Finances\OutgoingInvoice\Entry\OutgoingInvoiceEntry');
    }

    public function invoiceType()
    {
        return $this->belongsTo('\App\Models\Finances\InvoiceType');
    }

}
