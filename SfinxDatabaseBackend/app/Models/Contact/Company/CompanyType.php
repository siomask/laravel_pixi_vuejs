<?php

namespace App\Models\Contact\Company;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    protected $table = 'company_type';
}
