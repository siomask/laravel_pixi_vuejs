<?php

namespace App\Models\Contact\Company;

use Illuminate\Database\Eloquent\Model;

class CompanyActivity extends Model
{
    protected $table = 'company_activity';
}
