<?php

namespace App\Models\Contact\Company;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'company';
    public function contact() {
        return $this->belongsToMany('App\Models\Contact\Contact');
    }
    public function activity() {
        return $this->belongsToMany('App\Models\Contact\Company\CompanyActivity');
    }
    public function type() {
        return $this->belongsTo('App\Models\Contact\Company\CompanyType','company_type_id');
    }
}
