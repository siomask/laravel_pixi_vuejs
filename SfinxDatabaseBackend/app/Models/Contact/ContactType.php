<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

class ContactType extends Model
{
    static $TYPES=[1,3,9];//clients //contractors //teams
    public function contact() {
        return $this->belongsToMany('App\Models\Contact\Contact');
    }
}
