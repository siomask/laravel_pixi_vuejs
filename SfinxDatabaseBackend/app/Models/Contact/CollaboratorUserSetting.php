<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CollaboratorUserSetting extends Model
{
//    use SoftDeletes;
    protected $table = 'collaborator_user_setting';
    protected $fillable = ['contact_id'];


    public function collaborators(){
        return $this->hasMany('App\Models\Planner\ConstructionSite\Collaborator\Collaborator');
    }
    public function contact(){
        return $this->belongsTo('App\Models\Contact\Contact');
    }
}
