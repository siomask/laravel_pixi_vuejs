<?php

namespace App\Models\Contact;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    static $CLIENT_TYPE = 3;
    static $CONTACT_TYPE = 1;
    protected $table = 'contacts';
    protected $fillable = ['first_name','last_name','address','street','postcode','number_house',/*'type_id',*/'comment'];

    public function collaborators(){
        return $this->hasMany('App\Models\Planner\ConstructionSite\Collaborator\Collaborator');
    }

    public function teams() {
        return $this->hasMany('\App\Models\Team\Team');
    }

    public function types() {
        return $this->belongsToMany('\App\Models\Contact\ContactType');
    }
    public function sites() {
        return $this->hasMany('\App\Models\Planner\ConstructionSite\ConstructionSite');
    }
    public function companyContact() {
        return $this->belongsToMany('\App\Models\Contact\Company\Company','company_contact');
    }



    public function constructionSites() {
        return $this->hasMany('App\Models\Planner\ConstructionSite\ConstructionSite', 'contact_id');
    }

}
