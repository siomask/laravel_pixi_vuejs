<?php

namespace App\Models\Common;

use Illuminate\Database\Eloquent\Model;

class UserSetting extends Model

{
    protected $fillable = [
        'planner_date_start',
        'planner_date_end',
        'planner_client_view',
        'show_workflow_description',
        'show_workflow_func_group'
    ];

    public function selectedConstructionSites()
    {
        return $this->allConstructionSites()->where('construction_site_user_setting_type_id', 1);
    }

    public function workflowConstructionSites()
    {
        return $this->allConstructionSites()->where('construction_site_user_setting_type_id', 2);//->withPivot(['start', 'end', 'task_status', 'id']);
//        return $this->hasMany('App\Models\Planner\ConstructionSite\ConstructionSite')->where('construction_site_user_setting_type_id',2);//->withPivot(['start', 'end', 'task_status', 'id']);
    }
    public function allConstructionSites()
    {
        return $this->belongsToMany('App\Models\Planner\ConstructionSite\ConstructionSite')->withPivot([
            'construction_site_id',
            'construction_site_user_setting_type_id',
            'planner_show_empty_rows'
        ]);
//        return $this->hasMany('App\Models\Planner\ConstructionSite\ConstructionSite')->where('construction_site_user_setting_type_id',2);//->withPivot(['start', 'end', 'task_status', 'id']);
    }


    public function selectedCollaborators()
    {
        return $this->belongsToMany('App\Models\Contact\Contact', 'collaborator_user_setting');
    }

    public function collaborators()
    {
        return $this->hasMany('App\Models\Contact\CollaboratorUserSetting');
    }

    public function settings()
    {
        return $this->hasMany('App\Models\Planner\ConstructionSite\ConstructionSiteUserSetting');
    }


}
