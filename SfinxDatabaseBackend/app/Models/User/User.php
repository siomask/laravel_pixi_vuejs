<?php

namespace App\Models\Common;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    public $timestamps = true;
    protected $fillable=['first_name','name','username','password','user_group_id'];

    public function settings() {
        return $this->hasOne('App\Models\Common\UserSetting');
    }
    public function projLeaders() {
        return $this->hasMany('App\Models\Common\ProjectLead');
    }
    public function estimators() {
        return $this->hasMany('App\Models\Common\Estimator');
    }

//    public function groups() {
//        return $this->belongsToMany('App\Models\User\UserGroups');
//    }
    public function group() {
        return $this->belongsTo('App\Models\User\UserGroup','user_group_id');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
