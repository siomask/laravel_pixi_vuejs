<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserGroup extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'sequence'];

    public function rights()
    {
        return $this->belongsToMany('App\Models\Common\Right');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            if (!isset($model->sequence)) {
                $model->sequence = self::count() + 1;
            } else {
                if ($model->sequence > self::count()) {
                    $model->sequence = self::count() + 1;
                } else {
                    self::where('sequence', '>=', $model->sequence)->increment('sequence');
                    if ($model->sequence < 1) $model->sequence = 1;
                }
            }
        });


        self::updating(function ($model) {

            if (!isset($model->sequence)) {
                $model->sequence = self::count();
            } else if ($model->sequence > self::count()) {
                $model->sequence = self::count() + 1;
            } else if ($model->sequence < 1) {
                $model->sequence = 1;
            }
        });


        self::deleted(function ($model) {
            self::where('sequence', '>=', $model->sequence)->decrement('sequence');//not for id
        });


    }
}
