<?php

namespace App\Models\Team\TeamPeriod;

use Illuminate\Database\Eloquent\Model;

class TeamPeriod extends Model
{

    public $timestamps = true;
    protected $fillable = ['start','end','name'];

    public function teamMember(){
        return $this->belongsTo('App\Models\Common\Member');
    }
//    public function plannings(){
//        return $this->belongsToMany('App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning','team_member_planning_entries',null,'collaborator_planning_entry_id');
//    }
    public function plannings(){
        return $this->hasMany('App\Models\Team\TeamMember\TeamMemberPlanningEntry');
    }
    public function members(){
        return $this->belongsToMany('App\Models\Common\Member','team_period_team_members','team_period_id','team_member_id')->withPivot('member_price');
    }
    public function periodMembers(){
        return $this->hasMany('App\Models\Team\TeamMember\TeamPeriodTeamMember');
    }

    public function team(){
        return $this->belongsTo('App\Models\Team\Team');
    }

}
