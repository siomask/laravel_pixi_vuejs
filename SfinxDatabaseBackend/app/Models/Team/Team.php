<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use App\Http\Resources\v1\Team\TeamPlannedMemberResource;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;

class Team extends Model
{
    public $timestamps = true;
    protected $fillable = ['name', 'abbreviation', 'contact_id'];

    public function period()
    {
        return $this->hasMany('App\Models\Team\TeamPeriod\TeamPeriod');
    }

    public function planningMembers()
    {
        return $this->belongsToMany('App\Models\Team\TeamMember\TeamMemberPlanningEntry', 'collaborator_team_planning_entries', 'collaborator_team_planning_entry_id', 'team_id');
    }

    public function pivotPlanningMembers()
    {
        return $this->hasMany('App\Models\Team\CollaboratorTeamPlanningEntries');
    }

    static function activeTime()
    {
        return Carbon::now()->toDateTimeString();
    }

    static function activeTeams($collaborator, $from)
    {
//        $site=auth()->user()
//        ->settings
//        ->selectedConstructionSites()->firstOrFail();
//        var_dump("------",$site->contact());
        $currentTime = Team::activeTime();

        //:TODO should change to get the teams only of selected sites
        return Team::with(['period.periodMembers.member.absences'])
            ->whereHas('period', function ($q) use ($currentTime, $from) {
                $q->where(
                    [
                        ['start', '<=', $currentTime],
                        ['end', '>=', $currentTime],
                    ]
                );
//                if (isset($from)) {
//                    $q->with(['periodMembers' => function ($q) use ($currentTime, $from) {
//                        $q->with(['member' => function ($q) use ($currentTime, $from) {
//                            $q->with(['absences' => function ($q) use ($currentTime, $from) {
//                                $q->where([
//                                    ['start', '>=', $from],
//                                    ['end', '<=', $from]
//                                ]);
//                            }]);
//                        }]);
//                    }]);
//                }
            })->whereHas('period.periodMembers.member');
    }

    public static function loadAllPlannedTeamsMembers($cellId = -1, $from = -1, $to = -1, $collaborator = null)
    {
        if ($from == -1 || empty($from)) {
            $from = Carbon::now()->subCenturies(10)->toDateTimeString();
        } else {
            $from = Carbon::parse($from)->toDateTimeString();
        }
        if ($to == -1 || empty($to)) {
            $to = Carbon::now()->addCenturies(10)->toDateTimeString();
        } else {
            $to = Carbon::parse($to)->toDateTimeString();
        }


        $plannedTeams = [];

        if (is_array($cellId)) {

        } else {
            $plannedTeams = [
                ['date', '>=', $from],
                ['date', '<=', $to]
            ];
            if ($cellId > -1) $plannedTeams[] = ['collaborator_planning_entry_id', '=', $cellId];//for special planned, if cellId = -1 - for entire day from all sites
        }

        $teams = Team::activeTeams($collaborator, $from);
        if (!empty($collaborator)) {
            $teams = $teams->where('contact_id', $collaborator->contact_id);
        }
        return $teams->with([
            'period.periodMembers.plannings' => function ($q) use ($plannedTeams, $cellId) {
                if (is_array($cellId)) {
                    $q->whereIn("collaborator_planning_entry_id", $cellId);
                } else {
                    $q->where($plannedTeams);
                }
            }
        ])->get();
    }

    public static function parseActiveTeamMembers($teams)
    {
        $res =[];
        foreach ($teams as $teamV) {
            $teamItem = clone $teamV;
            $teamperiod = clone $teamItem->period;

            $members = [];
            foreach ($teamperiod as $teamperiodV) {
                $periodMember = clone $teamperiodV->periodMembers;
                foreach ($periodMember as $t) {
                     $member = clone $t->member;
                    $members[] =$member;
                    $member->periodId = $t->id;
                    $member->name = $member->first_name . " " . $member->last_name;
                    $member->holidays =  isset($member->holidays) ? clone $member->holidays : [];
                    $member->absences =  isset($member->absences) ? clone $member->absences : [];
                    $member->plannings = TeamPlannedMemberResource::collection($t->plannings);
//                    $member->plannings = clone $t->plannings;
//                    $member->plannings->transform(function($planning) { return clone $planning; });

                }
            }

            unset($teamItem->period);
            $teamItem->teamMembers = $members;
            $res[]=$teamItem;
        }
        return ($res);
    }


    public static function bw_tn($contact)
    {
        if (empty($contact) ||1) {
            return 0;
        }
        $list = $contact->teams()->with('period.periodMembers.plannings')->get();

        foreach ($list as $team) {
            foreach ($team->period as $period) {
                foreach ($period->periodMembers as $periodMembers) {
                    foreach ($periodMembers->plannings as $plannings) {

                    }
                }
            }
        }

    }

}
