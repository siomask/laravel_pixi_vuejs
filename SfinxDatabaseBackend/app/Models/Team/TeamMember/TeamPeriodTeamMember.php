<?php

namespace App\Models\Team\TeamMember;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TeamPeriodTeamMember extends Model
{

    public $timestamps = false;
    protected $fillable = ['team_member_id'];

    public function plannings()
    {
        return $this->hasMany('App\Models\Team\TeamMember\TeamMemberPlanningEntry');
    }
    public function period()
    {
        return $this->belongsTo('App\Models\Team\TeamPeriod\TeamPeriod');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Common\Member', 'team_member_id');
    }


}
