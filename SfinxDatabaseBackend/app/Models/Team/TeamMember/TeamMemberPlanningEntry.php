<?php

namespace App\Models\Team\TeamMember;

use App\Models\Common\Holiday;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning;
use App\Models\Planner\ConstructionSite\Collaborator\Planning\PlanningMessage;
use App\Models\Team\CollaboratorTeamPlanningEntries;

class TeamMemberPlanningEntry extends Model
{
    static $SEQUENCE = [1, 2, 3, 4];//1 - First&Last, 2 - First, 3-Last, 4 None
    public $timestamps = true;
    protected $fillable = ['date', 'team_period_team_member_id', 'collaborator_team_planning_entry_id', 'sequence', 'hours'];

    public static function boot()
    {
        parent::boot();
        self::deleted(function ($model) {
            if (TeamMemberPlanningEntry::where('collaborator_team_planning_entry_id', $model->collaborator_team_planning_entry_id)->count() == 0) {
                CollaboratorTeamPlanningEntries::where('id', $model->collaborator_team_planning_entry_id)->delete();
            }
        });


    }

    public function member()
    {
        $this->belongsTo('App\Models\Common\Member');
    }

    public function pivotPlannings()
    {
        return $this->belongsTo('App\Models\Team\CollaboratorTeamPlanningEntries', 'collaborator_team_planning_entry_id');
    }

    public function pivotPlanning()
    {
        return $this->belongsTo('App\Models\Team\CollaboratorTeamPlanningEntries', 'collaborator_team_planning_entry_id');
    }

    static function entries($ids)
    {
        $results = [];
        foreach (CollaboratorTeamPlanningEntries::with('planningMembers')->whereIn('collaborator_planning_entry_id', $ids)->get() as $entry) {
            foreach ($entry->planningMembers as $pl) {
                $results[] = $pl;
            }
        }
        return $results;
    }

    static function updatePlannings($dateRange, $id, $daysUpdateTime = 0)
    {


        $planned = TeamMemberPlanningEntry::entries([$id]);
        $planningMembers = $dateRange['planningMembers'];
        if (empty($daysUpdateTime) || $daysUpdateTime === 0) {

        } else {

            //:ToDo need more test
            //update tema planns throught week days , SAT SUN
            foreach ($planned as $_planned) {
                $_newDate = Carbon::parse($_planned->date);
                $curDUpdateTime = 0;
                if ($daysUpdateTime > 0) {
                    while ($curDUpdateTime++ < $daysUpdateTime) {
                        $_newDate->addDays(1);
                        if ($dateRange['isWeekend']) {
                            while (Holiday::isInHoliday($_newDate)) {
                                $_newDate->addDays(1);
                            }
                        } else {
                            while ($_newDate->isWeekend() || Holiday::isInHoliday($_newDate)) {
                                $_newDate->addDays(1);
                            }
                        }

                    }
                } else {
                    while ($curDUpdateTime-- > $daysUpdateTime) {
                        $_newDate->subDays(1);
                        if ($dateRange['isWeekend']) {
                            while (Holiday::isInHoliday($_newDate)) {
                                $_newDate->subDays(1);
                            }
                        } else {
                            while ($_newDate->isWeekend() || Holiday::isInHoliday($_newDate)) {
                                $_newDate->subDays(1);
                            }
                        }

                    }
                }
                $_planned->update(['date' => $_newDate->toDateTimeString()]);
            }

        }

        PlanningMessage::updatePlannings($dateRange, $id, $daysUpdateTime);

        if (count($planningMembers) > 0) {
            $delete = [];
            $update = [];
            $plannedAr = array_map(function ($e) {
                return $e->team_period_team_member_id;
            }, $planned);
            $plannedArDay = array_map(function ($e) {
                return $e->date;
            }, $planned);

            foreach (TeamMemberPlanningEntry::entries($planningMembers) as $_planned) {
                if (
                    array_search($_planned->team_period_team_member_id, $plannedAr) === false ||
                    array_search($_planned->date, $plannedArDay) === false
                ) {
                    $update[] = $_planned->collaborator_team_planning_entry_id;
                } else {
                    $delete[] = $_planned->id;
                }
            }
            if (count($delete) > 0) TeamMemberPlanningEntry::whereIn('id', $delete)->delete();//remove same member time planned
            if (count($update) > 0) CollaboratorTeamPlanningEntries::whereIn('id', $update)->update([//just set new parent
                'collaborator_planning_entry_id' => $id
            ]);

            Planning::whereIn('id', $planningMembers)->delete();
        }
    }

    static function updatePlanningMembers($groups, $group, $day = 0)
    {
        $th = TeamMemberPlanningEntry::getPlanning($group->id);
        $th['plannedTeam'] = $th['plannedTeam']->get();

        $oldCollaboratorTeamPlanningEntries = CollaboratorTeamPlanningEntries::findOrFail($th['plannedTeam'][0]->collaborator_team_planning_entry_id);
        $listOfCollaboratorTeamPlanningEntries = [];
        foreach ($groups as $key => $group) {
            $pl = clone   $oldCollaboratorTeamPlanningEntries;
            unset($pl->id);
            $listOfCollaboratorTeamPlanningEntries[$key] = CollaboratorTeamPlanningEntries::create($pl->toArray());
        }


        foreach ($th['plannedTeam'] as $plan) {
            if (!empty($day)) {
                if (Carbon::parse($plan->date)->eq($day)) {
                    $plan->delete();
                    continue;
                }
            }
            if (Carbon::parse($groups[0]->start)->lte(Carbon::parse($plan->date)) &&
                Carbon::parse($groups[0]->end)->gte(Carbon::parse($plan->date))) {


                $listOfCollaboratorTeamPlanningEntries[0]->update(['collaborator_planning_entry_id' => $groups[0]->id]);
                $plan->update(['collaborator_team_planning_entry_id' => $listOfCollaboratorTeamPlanningEntries[0]->id]);
            } else {
//                $plan->update(['collaborator_team_planning_entry_id' => $groups[1]->id]);
                $listOfCollaboratorTeamPlanningEntries[1]->update(['collaborator_planning_entry_id' => $groups[1]->id]);
                $plan->update(['collaborator_team_planning_entry_id' => $listOfCollaboratorTeamPlanningEntries[1]->id]);
            }

        }

        $oldCollaboratorTeamPlanningEntries->delete();
        $th['msg'] = PlanningMessage::updatePlanningMessages($groups, $group, $day);
        return $th;
    }

    static function dropPlanning($cellId, $start = 0, $end = 0, $from)
    {

        $th = TeamMemberPlanningEntry::getPlanning($cellId, $start, $end, $from);
        $th['plannedTeam']->delete();
        return [$th, PlanningMessage::dropPlanning($cellId, $start, $end, $from)];
    }

    static function getPlanning($cellId, $start = 0, $end = 0, $from = 0)
    {
        $where = [];
        $plans = array_map(function ($e) {
            return $e->id;
        }, TeamMemberPlanningEntry::entries([$cellId]));

        $where = self::planningByDate($start, $end, $from);
        $plannedTeam = TeamMemberPlanningEntry::where($where)->whereIn('id', $plans);

        return [
            'cond' => $where,
            'plannedTeam' => $plannedTeam
        ];
    }

    static function planningByDate($start = 0, $end = 0, $from = 0)
    {
        $where=[];
        if (!empty($start) && !empty($end)) {
            if ($from == 1) {
                $where[] = ['date', '>=', $start];
                $where[] = ['date', '<', $end];
            } else if ($from == 2) {
                $where[] = ['date', '>', $start];
                $where[] = ['date', '<=', $end];
            } else {
                $where[] = ['date', '>=', $start];
                $where[] = ['date', '<=', $end];
            }

        }
        return $where;
    }

}
