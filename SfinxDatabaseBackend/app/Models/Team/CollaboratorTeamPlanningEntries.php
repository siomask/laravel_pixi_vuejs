<?php

namespace App\Models\Team;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;
use App\Models\Planner\ConstructionSite\Collaborator\Collaborator;

class CollaboratorTeamPlanningEntries extends Model
{
    public $timestamps = false;
    protected $fillable = ['team_id','collaborator_planning_entry_id'];

    public function team()
    {
        return $this->hasOne('App\Models\Team\Team');
    }
    public function planningMembers()
    {
        return $this->hasMany('App\Models\Team\TeamMember\TeamMemberPlanningEntry','collaborator_team_planning_entry_id');
    }

    public function planningEntry()
    {
        return $this->belongsTo('App\Models\Planner\ConstructionSite\Collaborator\Planning\Planning');
    }



}
