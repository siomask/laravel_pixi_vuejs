<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionsiteCheckoffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblconstructionsitecheckoff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('constructionsite_id');
            $table->integer('constructionsitetodo_id');
            $table->integer('checkofftype_id');
            $table->text('comment');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblconstructionsitecheckoff');
    }
}
