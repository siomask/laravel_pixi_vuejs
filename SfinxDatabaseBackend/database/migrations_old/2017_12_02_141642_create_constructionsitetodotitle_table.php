<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionsitetodotitleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblconstructionsitetodotitle', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name');
            $table->text('comment');
            $table->integer('mothertitle_id')->nullable();
            $table->integer('sequence');
            $table->boolean('state');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblconstructionsitetodotitle');
    }
}
