<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionsiteTodoStandardCheckofftypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblconstructionsitetodostandardcheckofftype', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('constructionsitetodo_id');
            $table->integer('checkofftype_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblconstructionsitetodostandardcheckofftype');
    }
}
