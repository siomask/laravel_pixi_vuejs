<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorPlanningEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator_planning_entry', function (Blueprint $table) {
            $table->increments('id');

            $table->date('start');
            $table->date('end');
            $table->string('description')->nullable()->comment('Period message materials');
            $table->string('description_to_do')->nullable()->comment('Period message to do');



            $table->unsignedInteger('construction_site_collaborator_id')->nullable();
            $table->foreign('construction_site_collaborator_id', 'c_s_c_id_foreign')->references('id')->on('construction_site_collaborators')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator_planning_entry');
    }
}
