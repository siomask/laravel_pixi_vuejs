<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncomingInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serial_number')->nullable();
            $table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('invoice_number',25)->nullable();
            $table->string('comment')->nullable();
            $table->timestamp('payment_term')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->float('financial_discount', 8, 2)->nullable()->comment('hope 8 numbers of financial transacrion will be enough');
            $table->float('discount', 8, 2)->nullable();
            $table->boolean('is_paid')->default(false);

            $table->unsignedInteger('contact_id')->nullable()->comment('sender');
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_invoices');
    }
}
