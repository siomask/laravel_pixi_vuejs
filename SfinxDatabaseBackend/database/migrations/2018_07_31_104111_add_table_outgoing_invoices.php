<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableOutgoingInvoices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('invoice_number')->nullable();
            $table->timestamp('date')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('payment_term')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->string('comment')->nullable();

            $table->unsignedInteger('contact_id')->nullable()->default(null);
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

            $table->unsignedInteger('invoice_type_id')->nullable();
            $table->foreign('invoice_type_id')->references('id')->on('invoice_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_invoices');
    }
}
