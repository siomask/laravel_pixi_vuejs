<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableIncomingInvoiceEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('incoming_invoice_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable()->comment('entry_name');
            $table->float('amount',8,2);
            $table->float('vat_percentage',8,2);

            $table->unsignedInteger('construction_site_id')->nullable()->default(null);
            $table->foreign('construction_site_id')->references('id')->on('construction_sites')->onDelete('cascade');

            $table->unsignedInteger('incoming_invoice_id')->nullable();
            $table->foreign('incoming_invoice_id')->references('id')->on('incoming_invoices')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_invoice_entries');
    }
}
