<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableInvoiceAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('description')->nullable();
            $table->float('amount',16,2);
            $table->integer('sequence');

            $table->unsignedInteger('parent_invoice_account_id')->nullable();
            $table->foreign('parent_invoice_account_id')->references('id')->on('invoice_accounts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_accounts');
    }
}
