<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableCollaboratorPlanningMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator_planning_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->string('description')->nullable()->comment('Day message materials');
            $table->string('description_to_do')->nullable()->comment('Day message to do');
            $table->unsignedInteger('collaborator_planning_entry_id');
            $table->foreign('collaborator_planning_entry_id', 'c_p_m_c_p_e_id_foreign')->references('id')->on('collaborator_planning_entry')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator_planning_messages');
    }
}
