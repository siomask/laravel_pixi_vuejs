<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name')->nullable();
            $table->string('vat_number')->nullable();
            $table->string('website')->nullable();
            $table->integer('price')->nullable();
            $table->integer('quality')->nullable();
            $table->integer('technical_expertise')->nullable();
            $table->integer('aesthetic_quality')->nullable();

//            $table->unsignedInteger('company_activity_id');
//            $table->foreign('company_activity_id')->references('id')->on('company_activity')->onDelete('cascade');

            $table->unsignedInteger('company_type_id')->nullable();
            $table->foreign('company_type_id')->references('id')->on('company_type')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company');
    }
}
