<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableConstructionSiteExternal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_site_externals', function (Blueprint $table) {
            $table->increments('id');

//            $table->integer('ordered_by');
            $table->boolean('visible')->default(true);
            $table->string('comment')->nullable();
            $table->boolean('ordered')->default(false)->comment('check if sfinx ordered');
            $table->boolean('is_assign')->default(false);
            $table->boolean('is_ordered')->default(false);

            $table->unsignedInteger('construction_site_id')->nullable();
            $table->foreign('construction_site_id')->references('id')->on('construction_sites')->onDelete('cascade');//->nullable();

            $table->unsignedInteger('contact_id')->nullable();
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');//->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_site_external');
    }
}
