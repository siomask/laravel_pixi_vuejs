<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->unsigned();

            $table->string('name');
            $table->string('first_name');
            $table->string('username')->unique();
            $table->string('password');

            $table->unsignedInteger('user_group_id')->nullable();
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblconstructionsitetodotitle');
    }
}
