<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotIncomingInvoiceConstructionSiteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_site_incoming_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->float('amount')->default(0.0);

            $table->unsignedInteger('incoming_invoice_id')->nullable();
            $table->foreign('incoming_invoice_id','c_s_i_i_i_i_id')->references('id')->on('incoming_invoices')->onDelete('cascade');

            $table->unsignedInteger('construction_site_id')->nullable();
            $table->foreign('construction_site_id','c_s_i_i_c_s_id')->references('id')->on('construction_sites')->onDelete('cascade');

            $table->unsignedInteger('contractor_type_id')->nullable();
            $table->foreign('contractor_type_id','c_s_i_i_c_t_id')->references('id')->on('contractor_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('incoming_invoice_construction_site');
    }
}
