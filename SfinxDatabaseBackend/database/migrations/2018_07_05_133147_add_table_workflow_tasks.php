<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableWorkflowTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->string('description')->nullable();
            $table->integer('sequence')->comment('This is the order in which the task is displayed. This order is per \'level\' of tasks. Meaning subtasks have a sequence together but do not impact the sequence of the main task');

            $table->unsignedInteger('parent_workflow_task_id')->nullable();
            $table->foreign('parent_workflow_task_id')->references('id')->on('workflow_tasks')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_tasks');
    }
}
