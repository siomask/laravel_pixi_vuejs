<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotTeamPeriodTeamMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_period_team_members', function (Blueprint $table) {
            $table->increments('id');
            $table->float('member_price',8,2)->default(0);

            $table->unsignedInteger('team_member_id')->nullable();
            $table->foreign('team_member_id')->references('id')->on('team_members')->onDelete('cascade');

            $table->unsignedInteger('team_period_id')->nullable();
            $table->foreign('team_period_id')->references('id')->on('team_periods')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_period_team_member');
    }
}
