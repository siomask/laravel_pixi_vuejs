<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotConstructionSiteWorkflowTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_site_workflow_task', function (Blueprint $table) {
            $table->increments('id');

            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->integer('task_status')->default(1)->comment("1-not started,1-in progress,2-done,3-n/a");
            $table->string('description')->nullable();

            $table->unsignedInteger('construction_site_id');
            $table->foreign('construction_site_id')->references('id')->on('construction_sites')->onDelete('cascade');
            $table->unsignedInteger('workflow_task_id');
            $table->foreign('workflow_task_id')->references('id')->on('workflow_tasks')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_site_workflow_task');
    }
}
