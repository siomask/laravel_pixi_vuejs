<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableOutgoingInvoiceEntries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outgoing_invoice_entries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->float('amount',8,2)->default(0);
            $table->float('vat_percentage',8,2)->default(0);

            $table->unsignedInteger('outgoing_invoice_id')->nullable();
            $table->foreign('outgoing_invoice_id')->references('id')->on('outgoing_invoices')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outgoing_invoice_entries');
    }
}
