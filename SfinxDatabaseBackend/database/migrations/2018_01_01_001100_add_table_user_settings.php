<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableUserSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->boolean('planner_client_view')->default(false)->comment('1-display normally, 2- client view');
            $table->boolean('show_workflow_description')->default(true)->comment('show description on workflow page');
            $table->boolean('show_workflow_func_group')->default(false)->comment('show function groups on workflow page');

            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->date('planner_date_start')->nullable();
            $table->date('planner_date_end')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
