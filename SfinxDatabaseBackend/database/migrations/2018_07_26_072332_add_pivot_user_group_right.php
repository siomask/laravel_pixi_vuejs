<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotUserGroupRight extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('right_user_group', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('right_id')->nullable();
            $table->foreign('right_id')->references('id')->on('rights')->onDelete('cascade');

            $table->unsignedInteger('user_group_id')->nullable();
            $table->foreign('user_group_id')->references('id')->on('user_groups')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group_right');
    }
}
