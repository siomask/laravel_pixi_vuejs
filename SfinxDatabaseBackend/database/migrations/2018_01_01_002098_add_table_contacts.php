<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('address')->nullable();
            $table->string('street')->nullable();
            $table->string('postcode')->nullable();
            $table->string('number_house')->nullable();
            $table->string('comment')->nullable();

//            $table->unsignedInteger('type_id')->nullable();
//            $table->foreign('type_id')->references('id')->on('contact_types')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('contacts', function(Blueprint $table) {
//           $table->dropForeign(['type_id']);
//        });
        Schema::dropIfExists('contacts');
    }
}
