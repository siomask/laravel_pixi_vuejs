<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAbsences extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absences', function (Blueprint $table) {
            $table->increments('id');
            $table->date('start');
            $table->date('end');

            $table->unsignedInteger('team_member_id')->nullable();
            $table->foreign('team_member_id')->references('id')->on('team_members')->onDelete('cascade');

            $table->unsignedInteger('absence_type_id')->nullable();
            $table->foreign('absence_type_id')->references('id')->on('absence_types')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absences');
    }
}
