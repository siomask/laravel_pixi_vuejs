<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotConstructionSiteUserSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_site_user_setting', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->boolean('planner_show_empty_rows')->default(true)->comment("rows which don`t have any plannings");

            $table->unsignedInteger('construction_site_id');
            $table->foreign('construction_site_id')->references('id')->on('construction_sites')->onDelete('cascade');

            $table->unsignedInteger('user_setting_id');
            $table->foreign('user_setting_id')->references('id')->on('user_settings')->onDelete('cascade');

            $table->unsignedInteger('construction_site_user_setting_type_id');
            $table->foreign('construction_site_user_setting_type_id','c_s_u_s_t__foreign')->references('id')->on('construction_site_user_setting_types')->onDelete('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_site_user_setting');
    }
}
