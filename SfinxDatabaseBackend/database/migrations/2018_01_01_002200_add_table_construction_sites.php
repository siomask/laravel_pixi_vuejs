<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableConstructionSites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_sites', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->float('presented_price')->default(0);
            $table->float('total_amount_orders')->default(0);
            $table->float('total_amount_processed')->default(0);
            $table->date('date_financial_details')->nullable();

            $table->unsignedInteger('contact_id')->nullable()->default(null);
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

            $table->unsignedInteger('construction_site_statuses_id')->nullable();//:TODO hardcoded
            $table->foreign('construction_site_statuses_id')->references('id')->on('construction_site_statuses')->onDelete('cascade');

            $table->integer('sequence')->comment('grouped by contact_id->contact');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_sites');
    }
}
