<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotWorkflowFunctionGroupWorkflowTask extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workflow_function_group_workflow_task', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('workflow_function_group_id');
            $table->foreign('workflow_function_group_id','w_f_g_foreign')->references('id')->on('workflow_function_groups')->onDelete('cascade');
            $table->unsignedInteger('workflow_task_id');
            $table->foreign('workflow_task_id','w_t_foreign')->references('id')->on('workflow_tasks')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workflow_function_group_workflow_task');
    }
}
