<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollaboratorTeamPlanningEntriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collaborator_team_planning_entries', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('collaborator_planning_entry_id')->nullable();
            $table->foreign('collaborator_planning_entry_id','c_t_p_e_c_p_e_i')->references('id')->on('collaborator_planning_entry')->onDelete('cascade');

            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collaborator_team_planning_entries');
    }
}
