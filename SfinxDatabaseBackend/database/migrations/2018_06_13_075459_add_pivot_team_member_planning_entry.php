<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPivotTeamMemberPlanningEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_member_planning_entries', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date');
            $table->integer('sequence')->comment('1 - First&Last, 2 - First, 3 - Last, 4 -None');
            $table->integer('hours')->default(0)->comment('working hours');

            $table->unsignedInteger('team_period_team_member_id')->nullable();
            $table->foreign('team_period_team_member_id','t_p_t_m_id_foreign')->references('id')->on('team_period_team_members')->onDelete('cascade');


            $table->unsignedInteger('collaborator_team_planning_entry_id')->nullable();
            $table->foreign('collaborator_team_planning_entry_id','c_t_p_e_c_t_p_e_fid')->references('id')->on('collaborator_team_planning_entries')->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_member_planning_entry');
    }
}
