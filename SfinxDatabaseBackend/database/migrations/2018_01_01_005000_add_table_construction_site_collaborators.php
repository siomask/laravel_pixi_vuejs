<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableConstructionSiteCollaborators extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_site_collaborators', function(Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->timestamp('request_date')->useCurrent();;

            $table->unsignedInteger('construction_site_id')->nullable();
            $table->foreign('construction_site_id')->references('id')->on('construction_sites')->onDelete('cascade');

            $table->unsignedInteger('contractor_type_id')->nullable();
            $table->foreign('contractor_type_id')->references('id')->on('contractor_types')->onDelete('cascade');


            $table->unsignedInteger('contact_id')->nullable()->default(null);
            $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

            $table->date('start')->nullable();
            $table->date('end')->nullable();
            $table->boolean('is_ordered')->default(0);
            $table->boolean('is_assigned')->default(0);
            $table->boolean('is_contractor_type_ordered')->default(0);
            $table->boolean('is_contractor_type_assigned')->default(0);
            $table->string('comment')->default('');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
