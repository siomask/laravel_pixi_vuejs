<?php
$servername = "localhost";
$username = "demo";
$password = "1111";
$dbname = "sfinxto_sfinxwerk_old";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$resul = [];
$tables = [
    [
        'id' => 'hoofdtypeid',
        'name' => 'tblInkomendefacturenandere_hoofdtype',
        'amount' => 'hoofdtypenr',
        'description' => 'hoofdtypenaam',
    ],
    [
        'id' => 'subtype1id',
        'rel_table' => 'tblInkomendefacturenandere_hoofdtype',
        'name' => 'tblInkomendefacturenandere_subtype1',
        'amount' => 'subtype1nr',
        'parent_invoice_account_id' => 'hoofdtypeid',
        'description' => 'subtype1naam',
    ],
    [
        'id' => 'subtype2id',
        'rel_table' => 'tblInkomendefacturenandere_subtype1',
        'name' => 'tblInkomendefacturenandere_subtype2',
        'amount' => 'subtype2nr',
        'parent_invoice_account_id' => 'subtype1id',
        'description' => 'subtype2naam',
    ]
];

$tableRowsCount = 0;
foreach ($tables as $key => $table) {
    $sql = "SELECT `";
    if (isset($table['parent_invoice_account_id'])) {
        $sql .= $table['parent_invoice_account_id'] . "` as parent_invoice_account_id, `";
    }
    $sql .= $table['id'] . "` as id, `" . $table['amount'] . "` as amount, `" . $table['description'] . "` as description  FROM `" . $table['name'] . "` LIMIT 0,18446744073709551615";
//    var_dump($sql);
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row
        $counts = 0;
        while ($row = $result->fetch_assoc()) {
            if (isset($table['parent_invoice_account_id'])) {
                $row['rel_table'] = $table['rel_table'];
//                $row['parent_invoice_account_id'] = $row['parent_invoice_account_id'] + $tableRowsCount;
            }
            $row['amount'] = (float)$row['amount'];
            $row['table'] =  $table['name'];


            $counts++;
//            $row['_id'] = $counts+$tableRowsCount;
            $resul[] = $row;
        }
        $tableRowsCount += $counts - 1;
    } else {
        var_dump("0 results from " . $table['name']);
    }
}
$conn->close();


foreach ($resul as $item) {
    if (isset($item['parent_invoice_account_id']) && $item['parent_invoice_account_id']) {
        foreach ($resul as $key => $itemI) {
            if ($itemI['id'] == $item['parent_invoice_account_id'] && $item['rel_table'] == $itemI['table']) {
                $item['parent_invoice_account_id'] = $key + 1;
                break;
            }
        }
    }
}

$ress = [];
foreach ($resul as $item) {
    $ress[]=[
        'amount'=>$item['amount'],
        'description'=>$item['description'],
        'parent_invoice_account_id'=>$item['parent_invoice_account_id']
    ];

}
$myfile = fopen("tempOldDbData/invoice_accounts.json", "w") or die("Unable to open file!");
fwrite($myfile, json_encode($ress));
fclose($myfile);