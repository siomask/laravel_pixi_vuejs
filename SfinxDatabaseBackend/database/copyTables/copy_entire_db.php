<?php
$servername = "localhost";
$username = "root";
$password = "unilimes1";
$dbname = "sfinxto_sfinxwerktest";//copy from db
$dbnameNew = "sfinxto_sfinxwerk";//copy into db


//tblAannemingstak = contractor branch
//tblAannemingstakpercontant = contractor branch per contact, a pivot table
//tblAfwezigheden = absences
//tblCodaheader, tblCodevervolg -> these can be removed
//factuur = invoice
//inkomende factuur = incoming invoice
//intellingen = settings
//
//teamleden = members
//verlof = holidays
$LIMIT = "  LIMIT 0,18446744073709551615";//should be 18446744073709551615
$TABLES = [
    'contact_types',
    'construction_site_statuses',//1
    'rights',
    'user_groups',//3
    'right_user_group',
    'users',//5
    'holidays',
    'contractor_types',//7
    'team_members',
    'absences',//9
    'absence_types',
    'invoice_accounts',//11
    'invoice_types',

    'contact_types',//13
    'construction_site_statuses',

    'contacts',//15
    'teams',

    'construction_sites',//17
    'user_settings',
    'construction_site_user_setting_types',//19
    'construction_site_collaborators',
    'team_periods',//21
    'team_period_team_members',
    'collaborator_planning_entry',//23
    'outgoing_invoices',
    'outgoing_invoice_entries',//25
    'collaborator_team_planning_entries',
    'team_member_planning_entries',//27
    'estimators',
    'project_leads',//29
    'incoming_invoices',
    'incoming_invoice_entries',//31
    'construction_site_externals',//32
    'company_type',//33
    'company',
    'company_contact',//35
    'company_activity',
    'company_company_activity',//37
    'contact_contact_type',
];

$relationPrefix = '_RELATED_TO_';
$spce_in_string = '&nbsp;';
$usrPsw = '$2y$10$avvymbWUKIXWThs8.VSMNu0WuGavpCBggEVoqFKabsZAfweK.Dfqq';
//--------field_1 - should be always id
$tables = [
    [
        'from' => [
            'name' => 'tblGebruikersrechten',
            'field_1' => 'gebruikersrechtenid',
            'field_2' => 'gebruikersrechtenitem',
            'field_3' => 'gebruikersrechtenomschrijving',
            //            'field_4' => 'gebruikersrechtensubgroep',
            'field_5' => 'gebruikersrechtenvolgordebinnensubgroep',
            //            'field_2' => 'gebruikersrechtenhoofdgroep'// -----------MISSED
            //            'field_2' => 'gebruikersrechtenextra'// -----------MISSED
            //            'field_2' => 'actief'// -----------MISSED
        ],
        'to_new' => [
            'name' => $TABLES[2],
            'field_1' => 'id',
            'field_2' => 'name',
            'field_3' => 'description',
            //            'field_4' => 'parent_right_id',//-----------MISSED
            'field_5' => 'sequence',
        ]

    ],

    [
        'from' => [
            'name' => 'tblGebruikersgroepen',
            'field_1' => 'gebruikersgroepenid',
            'field_2' => 'gebruikersgroepennaam'
        ],
        'to_new' => [
            'name' => $TABLES[3],
            'field_1' => 'id',
            'field_2' => 'name',
            //            'field_3' => 'description',
            //            'field_4' => 'sequence'       //----------------MISSED
        ],
        'default_v' => [
            'sequence' => true
        ]

    ],
    [
        'from' => [
            'name' => 'tblGebruikersrechten_gebruikersgroepen',
            'field_1' => 'gebruikersrechten_gebruikersgroepenid',
            'field_2' => 'gebruikersrechten_gebruikersgroepen_gebruikersrechtenid',
            'field_3' => 'gebruikersrechten_gebruikersgroepen_gebruikersgroepenid'
        ],
        'to_new' => [
            'name' => $TABLES[4],
            'field_1' => 'id',
            'field_2' => 'right_id',//there are relation with value 28, but it`s missed
            'field_3' => 'user_group_id',
        ]

    ],
    [
        'from' => [
            'name' => 'tblGebruikers',
            'field_1' => 'gebruikerid',
            'field_2' => 'naam',
            'field_3' => 'voornaam',
            'field_4' => 'gebruikersnaam',
            'field_6' => 'rechten',
        ],
        'to_new' => [
            'name' => $TABLES[5],
            'field_1' => 'id',
            'field_2' => 'name',
            'field_3' => 'first_name',
            'field_4' => 'username',
            'field_6' => 'user_group_id',
        ],
        'default_v' => [
            'password' => $usrPsw//admin
        ]

    ],

    [
        'from' => [
            'name' => 'tblVerlof',
            'field_1' => 'verlofid',
            'field_2' => 'naam',
            'field_3' => 'FROM_UNIXTIME(begin)',
            'field_4' => 'FROM_UNIXTIME(einde)'
        ],
        'to_new' => [
            'name' => $TABLES[6],
            'field_1' => 'id',
            'field_2' => 'name',
            'field_3' => 'start',
            'field_4' => 'end'
            //            'field_4' => 'team_member_id'// -------------MISSED, the holiday for one member
        ]
    ],
    [
        'from' => [
            'name' => 'tblAannemingstak',
            'field_1' => 'aannemingsid',
            'field_2' => 'afkorting',
            'field_3' => 'voluit',
            'field_4' => 'opmerkingen',
            'field_5' => 'volgorde'
        ],
        'to_new' => [
            'name' => $TABLES[7],
            'field_1' => 'id',
            'field_2' => 'abbreviation',
            'field_3' => 'name',
            'field_4' => 'description',
            'field_5' => 'sequence'
        ]
    ],

    //planner
    [
        'from' => [
            'name' => 'tblcontacten',
            'field_0' => 'projectnaam',
            'field_1' => 'contactid',
            'field_2' => 'naam',
            'field_4' => 'CONCAT(straat," ", nr ," ",postcode)',
            'field_5' => 'type',// . $relationPrefix . 'tblContacttypes.contacttypesnr_SELECT_tblContacttypes.contacttypesnaam'
            'field_6' => 'straat',
            'field_7' => 'nr',
            'field_8' => 'postcode',
            'field_9' => 'klantenstatus',
            'field_10' => 'opmerkingen',

            //company fields
            'field_11' => 'commercielenaam',
            'field_12' => 'bedrijfsvorm',
            'field_13' => 'btwnummer',
            'field_14' => 'website',
            'field_15' => 'prijs',
            'field_16' => 'kwaliteit',
            'field_17' => 'kwaliteittechnisch',
            'field_18' => 'kwaliteitesthetisch',


            'field_19' => 'specialiteit',

        ],
        'to_new' => [
            'name' => $TABLES[15],
            'field_0' => 'contact_last_name',
            'field_1' => 'id',
            'field_2' => 'last_name',
            'field_4' => 'address',
            'field_5' => 'type_id', //. $relationPrefix . $TABLES[13] . '.id_SELECT_' . $TABLES[13] . '.type'
            'field_6' => 'street',
            'field_7' => 'number_house',
            'field_8' => 'postcode',
            'field_9' => 'construction_site_statuses_id',
            'field_10' => 'comment',

            'field_11' => 'company_name',
            'field_12' => 'company_type_id',
            'field_13' => 'vat_number',
            'field_14' => 'website',
            'field_15' => 'price',
            'field_16' => 'quality',
            'field_17' => 'technical_expertise',
            'field_18' => 'aesthetic_quality',
            'field_19' => 'expertise',
        ]
    ],
    [
        'from' => [
            'name' => 'tblWerfbeheer',
            'field_1' => 'werfbeheerid',
            'field_2' => 'contactid',
            'field_3' => 'bedrageersteofferte',
            'field_4' => 'bedragvoorgelegdaanklant',
            'field_5' => 'projectleider',
            'field_6' => 'FROM_UNIXTIME(datumeersteofferte)',
            'field_7' => 'werfbeheerid'
        ],
        'to_new' => [
            'name' => $TABLES[17],
            'field_1' => 'id',
            'field_2' => 'contact_id',
            'field_3' => 'presented_price',
            'field_4' => 'total_amount_orders',
            'field_5' => 'leader_id',
            'field_6' => 'date_financial_details',
            'field_7' => 'construction_site_statuses_id'
        ],
        'default_v' => [
            'sequence' => true
        ]
    ],

    [
        'from' => [
            'name' => 'tblWerfdetailat',
            'field_1' => 'werfdetailatid',
            'field_2' => 'werfbeheerid',
            'field_3' => 'medewerkerid',
            'field_4' => 'aannemingstak',
            'field_5' => 'besteldmed',
            'field_6' => 'besteldmed',
            'field_7' => 'besteldat',
            'field_8' => 'besteldat',
        ],
        'to_new' => [
            'name' => $TABLES[20],
            'field_1' => 'id',
            'field_2' => 'construction_site_id',
            'field_3' => 'contact_id',
            'field_4' => 'contractor_type_id',
            'field_5' => 'is_ordered',
            'field_6' => 'is_assigned',
            'field_7' => 'is_contractor_type_ordered',
            'field_8' => 'is_contractor_type_assigned',
        ]
    ],
    [
        'from' => [
            'name' => 'tblWerfdetailplanningdata',
            'field_1' => 'werfdetailplanningdataid',
            'field_2' => 'werfdetailatid',
            'field_3' => 'opmerkingvoorklant',
            'field_4' => 'FROM_UNIXTIME(begin)',
            'field_5' => 'FROM_UNIXTIME(einde)',
            'field_6' => 'opmerkingvooraannemer'
        ],
        'to_new' => [
            'name' => $TABLES[23],
            'field_1' => 'id',
            'field_2' => 'construction_site_collaborator_id',
            'field_3' => 'description',
            'field_4' => 'start',
            'field_5' => 'end',
            'field_6' => 'description_to_do'
        ]
    ],


    //planner details
    [
        'from' => [
            'name' => 'tblTeams',
            'field_1' => 'teamid',
            'field_2' => 'teamvoluit',
            'field_3' => 'teamafkorting',
            'field_4' => 'teamgroepid'
        ],
        'to_new' => [
            'name' => $TABLES[16],
            'field_1' => 'id',
            'field_2' => 'name',
            'field_3' => 'abbreviation',
            'field_4' => 'contact_id'
        ]
    ],
    [
        'from' => [
            'name' => 'tblTeamperiode',
            'field_1' => 'teamperiodeid',
            //            'field_2' => 'contactid',
            'field_3' => 'FROM_UNIXTIME(teamperiodestart)',
            'field_4' => 'FROM_UNIXTIME(teamperiodeeinde)',
            'field_5' => 'teamid'
        ],
        'to_new' => [
            'name' => $TABLES[21],
            'field_1' => 'id',
            //            'field_2' => 'name',
            'field_3' => 'start',
            'field_4' => 'end',
            'field_5' => 'team_id'
        ]
    ],

    [
        'from' => [
            'name' => 'tblTeamleden',
            'field_1' => 'teamlidid',
            'field_2' => 'teamlidnaam',
            'field_3' => 'teamlidvoornaam',
            'field_4' => 'teamlidid'
        ],
        'to_new' => [
            'name' => $TABLES[8],
            'field_1' => 'id',
            'field_2' => 'last_name',
            'field_3' => 'first_name',
            'field_4' => 'hour_rate'
        ]
    ],
    [
        'from' => [
            'name' => 'tblTeamperiodeperlid',
            'field_1' => 'teamperiodeperlidid',
            'field_2' => 'teamlidid',
            'field_3' => 'teamperiodeid'
        ],
        'to_new' => [
            'name' => $TABLES[22],
            'field_1' => 'id',
            'field_2' => 'team_member_id',
            'field_3' => 'team_period_id'
        ]
    ],
    [
        'from' => [
            'name' => 'tblAfwezigheden',
            'field_1' => 'afwezighedenid',
            'field_2' => 'FROM_UNIXTIME(afwezighedenbegin)',
            'field_3' => 'FROM_UNIXTIME(afwezighedeneinde)',
            'field_4' => 'werknemerid',
            'field_5' => 'afwezighedentype'
        ],
        'to_new' => [
            'name' => $TABLES[9],
            'field_1' => 'id',
            'field_2' => 'start',
            'field_3' => 'end',
            'field_4' => 'team_member_id',
            'field_5' => 'absence_type_id'
        ]
    ],

    [
        'from' => [
            'name' => 'tblWerfdetailteamsperwerf',
            'field_1' => 'werfdetailteamsperwerfid',
            'field_2' => 'werfdetailplanningdataid',
            'field_3' => 'werfdetailteamsperwerfteamid'
        ],
        'to_new' => [
            'name' => $TABLES[26],
            'field_1' => 'id',
            'field_2' => 'collaborator_planning_entry_id',
            'field_3' => 'team_id'
        ]
    ],
    [
        'from' => [
            'name' => 'tblWerfdetailledenperwerf',
            'field_1' => 'werfdetailledenperwerfid',
            'field_2' => 'werfdetailteamsperwerfid',
            'field_3' => 'FROM_UNIXTIME(werfdetailledenperwerfdatumbegin)',
            //            'field_4' => 'werfdetailledenperwerfdatumeinde',
            'field_5' => 'werfdetailledenperwerfteamlidid',
            'field_6' => 'werfdetailledenperwerfvanafhier',
            'field_7' => 'werfdetailledenperwerftijdstipdag'//all dat are 0, what is that mean
        ],
        'to_new' => [
            'name' => $TABLES[27],
            'field_1' => 'id',
            'field_2' => 'collaborator_team_planning_entry_id',
            'field_3' => 'date',
            //            'field_4' => 'date_end',
            'field_5' => 'team_period_team_member_id',
            'field_6' => 'hours',
            'field_7' => 'sequence'
        ]
    ],


    //finances
    [
        'from' => [
            'name' => 'tblUitgaandefacturen',
            'field_1' => 'factuurid',
            'field_2' => 'factuurnummer',
            'field_3' => 'klant',
            'field_4' => 'FROM_UNIXTIME(factuurdatum)',
            'field_5' => 'FROM_UNIXTIME(vervaldag)',
            'field_6' => 'factuurbedrag',
            'field_7' => 'soortfactuur',
            'field_8' => 'opmerking',
            'field_9' => 'btwpercentage',
        ],
        'to_new' => [
            'name' => $TABLES[24],
            'field_1' => 'id',
            'field_2' => 'invoice_number',
            'field_3' => 'contact_id',
            'field_4' => 'date',
            'field_5' => 'payment_term',
            'field_6' => 'amount',
            'field_7' => 'invoice_type_id',
            'field_8' => 'comment',
            'field_9' => 'vat_percentage',
        ]
    ],
    [
        'from' => [
            'name' => 'tblInkomendefacturen',
            'field_1' => 'factuurid',
            'field_2' => 'factuurnummer',
            'field_3' => 'afzender',
            'field_4' => 'FROM_UNIXTIME(factuurdatum)',
            'field_5' => 'FROM_UNIXTIME(vervaldag)',
            //            'field_6' => 'factuurbedrag',
            //            'field_7' => 'soortfactuur',
            'field_8' => 'opmerking',
            //            'field_9' => 'btwpercentage',
            'field_10' => 'volgnrnummer',
            'field_11' => 'kortingnabtw',
            'field_12' => 'controle',
            'field_13' => 'kortingvoorbtw',
        ],
        'to_new' => [
            'name' => $TABLES[30],
            'field_1' => 'id',
            'field_2' => 'invoice_number',
            'field_3' => 'contact_id',
            'field_4' => 'date',
            'field_5' => 'payment_term',
            //            'field_6' => 'amount',
            //            'field_7' => 'invoice_type_id',
            'field_8' => 'comment',
            //            'field_9' => 'vat_percentage',
            'field_10' => 'serial_number',
            'field_11' => 'financial_discount',
            'field_12' => 'is_paid',
            'field_13' => 'discount',
        ],
    ],
    [
        'from' => [
            'name' => 'tblInkomendefacturenfactuurbedragen',
            'field_1' => 'inkomendefacturenfactuurbedragenid',
            'field_2' => 'factuurbedrag',
            'field_3' => 'btw',
            'field_4' => 'inkomendefacturenid',
        ],
        'to_new' => [
            'name' => $TABLES[31],
            'field_1' => 'id',
            'field_2' => 'amount',
            'field_3' => 'vat_percentage',
            'field_4' => 'incoming_invoice_id',
        ]
    ],


    //contac details
    [
        'from' => [
            'name' => 'tblWerfdetailandere',
            'field_1' => 'werfdetailandereid',
            'field_2' => 'werfbeheerid',
            'field_3' => 'medewerkerid',
            'field_4' => 'tebestellen',
            'field_5' => 'zichtbaar',
            'field_6' => 'opmerkingen',
            'field_7' => 'besteldext',
            'field_8' => 'besteldext',
            'field_9' => 'besteldext',
        ],
        'to_new' => [
            'name' => $TABLES[32],
            'field_1' => 'id',
            'field_2' => 'construction_site_id',
            'field_3' => 'contact_id',
            'field_4' => 'ordered',
            'field_5' => 'visible',
            'field_6' => 'comment',
            'field_7' => 'order_c',
            'field_8' => 'is_ordered',
            'field_9' => 'is_assign',
        ]
    ]
];
$hard_coded = [
    [
        'name' => $TABLES[10],
        'items' => [
            ['name' => 'Sick Days', 'id' => 1],
            ['name' => 'Holiday', 'id' => 2],
            ['name' => 'Not sure what  type of absences is it', 'id' => 3]
        ]
    ],
    [
        'name' => $TABLES[12],
        'items' => [
            ['name' => 'regular'],
            ['name' => 'prepayment'],
            ['name' => 'studyagreement'],
            ['name' => 'other'],
        ]
    ],
    [
        'name' => $TABLES[13],
        'items' => [
            ['old_id' => 0, 'type' => 'aannemer'],
            ['old_id' => 1, 'type' => 'leverancier'],
            ['old_id' => 2, 'type' => 'klant'],
            ['old_id' => 3, 'type' => 'architect'],
            ['old_id' => 4, 'type' => 'veiligheidscoordinator'],
            ['old_id' => 5, 'type' => 'EPB verslaggever'],
            ['old_id' => 6, 'type' => 'stabiliteitsingenieur'],
            ['old_id' => 7, 'type' => 'bouwcoordinator'],
            ['old_id' => -1, 'type' => 'Intern Team'],
            ['old_id' => 8, 'type' => 'Other'],
        ]
    ],
    [
        'name' => $TABLES[14],
        'items' => [
            ['id' => 0, 'name' => 'onbepaald', 'contact_type_id' => 3],//3 is klant from $TABLES[13]
            ['id' => 1, 'name' => 'Offerte in opmaak', 'contact_type_id' => 3],//3 is klant from $TABLES[13]
            ['id' => 2, 'name' => 'in uitvoering', 'contact_type_id' => 3],
            ['id' => 3, 'name' => 'afgewerkt', 'contact_type_id' => 3],
            ['id' => 4, 'name' => 'geen prijs gegeven', 'contact_type_id' => 3],
            ['id' => 5, 'name' => 'niet verkocht', 'contact_type_id' => 3],
            ['id' => 6, 'name' => 'offerte klaar', 'contact_type_id' => 3],
            ['id' => 7, 'name' => 'in nazicht', 'contact_type_id' => 3],
            ['id' => 8, 'name' => 'uitgesteld op lange termijn', 'contact_type_id' => 3],
            ['id' => 12, 'name' => 'in prospectie', 'contact_type_id' => 3],
            ['id' => 13, 'name' => 'ontwerpfase', 'contact_type_id' => 3],
            ['id' => 14, 'name' => 'afgewerkt maar administratief nog af te werken', 'contact_type_id' => 3]
        ]
    ],

    [
        'name' => $TABLES[19],
        'items' => [
            ['name' => 'planner'],
            ['name' => 'workflow'],
        ]
    ],
    [
        'name' => $TABLES[5],
        'items' => [
            ['username' => 'admin', 'password' => $usrPsw, 'first_name' => 'Admin', 'name' => 'adminName'],
        ]
    ],

    [
        'name' => $TABLES[33],
        'items' => [
            ['name' => 'COMM.VA', "id" => 1],
            ['name' => 'BVBA', "id" => 2],
            ['name' => 'CVBA', "id" => 3],
            ['name' => 'EBVBA', "id" => 4],
            ['name' => 'eenmanszaak', "id" => 5],
            ['name' => 'NV', "id" => 6],
            ['name' => 'VOF', "id" => 7],
        ]
    ]
];

$hard_coded_items = [];
$dblink1 = new mysqli($servername, $username, $password, $dbname);
if ($dblink1->connect_error) {
    die("Connection failed: " . $dblink1->connect_error);
}

$dblink2 = new mysqli($servername, $username, $password, $dbnameNew);
if ($dblink2->connect_error) {
    die("Connection failed: " . $dbnameNew->connect_error);
}
// all relatetd id with value =0 will be ignored

$contactItems = [];
//---------------clear tables
if (!$dblink2->query('SET FOREIGN_KEY_CHECKS = 0;')) {
    {
        die("Error description: " . mysqli_error($dblink2));
    }
}
foreach ($TABLES as $table) {
    if (!$dblink2->query('TRUNCATE table ' . $table)) {
        {
            die("Error description: " . mysqli_error($dblink2));
        }
    }
}
if (!$dblink2->query('SET FOREIGN_KEY_CHECKS = 1;')) {
    {
        die("Error description: " . mysqli_error($dblink2));
    }
}
if (!$dblink2->query('SET sql_mode=\'NO_AUTO_VALUE_ON_ZERO\'')) {
    {
        die("Error description: " . mysqli_error($dblink2));
    }
}


var_dump("<h1>---------------------ADD  TRIGGERES--------</h1>");
$TRIGGERS_NAME = [
    'user_settings_auto_trigger',
    'member_period_def_price_trigger',
    'contact_construction_site_default'
];
//CREATE DEFINER=`demo`@`localhost` TRIGGER `member_period_def_price_trigger` BEFORE INSERT ON `team_period_team_members`'
// FOR EACH ROW IF new.member_price is NULL THEN set new.member_price = (select hour_rate from team_members where team_members.id = new.team_member_id limit 0,1); END IF
$TRIGGERS = [
    "CREATE TRIGGER `$TRIGGERS_NAME[0]` AFTER INSERT ON `$TABLES[5]` FOR " .
    "EACH ROW INSERT INTO `$TABLES[18]` (`user_id`) (select `id` from `$TABLES[5]`
         where `id` NOT IN (select `user_id` from `$TABLES[18]`))",

    "CREATE TRIGGER `$TRIGGERS_NAME[1]` BEFORE INSERT ON `$TABLES[22]` FOR EACH ROW IF new.member_price is NULL or  new.member_price =0 THEN 
        set new.member_price = (select hour_rate from team_members where team_members.id = new.team_member_id limit 0,1); END IF"
];
foreach ($TRIGGERS_NAME as $triggerName) {
    if (!$dblink2->query('DROP TRIGGER  IF EXISTS ' . $triggerName . ";")) {
        {
            die("<p style='color:red'>Error on delete triggere: " . $triggerName . mysqli_error($dblink2) . "</p>");
        }
    }
}
foreach ($TRIGGERS as $trigger) {
    if (!$dblink2->query($trigger)) {
        {
            var_dump($trigger);
            die("Error on create trigger: " . mysqli_error($dblink2));
        }
    }
}


var_dump("<h1>---------------------HARDCODED--------</h1>");
//-------------tables not exist in old app
foreach ($hard_coded as $hard_coded_table) {
    foreach ($hard_coded_table['items'] as $items) {
        $keys = '';
        $values = '';
        foreach ($items as $key => $val) {
            if ($key == 'old_id') {

            } else {
                $keys .= (strlen($keys) > 0 ? "," : "") . "`" . $key . "`";
                $values .= (strlen($values) > 0 ? "," : "") . "'" . $val . "'";
            }


        }
        $query = "INSERT INTO `" . $hard_coded_table['name'] . "` (" . $keys . ") VALUES (" . $values . ")";
        $result = $dblink2->query($query);
        if (!$result) {
            {
                var_dump("Error description: " . mysqli_error($dblink2));
                die($query);
            }
        } else {
            if (!isset($hard_coded_items[$hard_coded_table['name']])) $hard_coded_items[$hard_coded_table['name']] = [];
            $hard_coded_items[$hard_coded_table['name']][] = [
                'new_id' => $dblink2->insert_id,
                'old_id' => isset($items['old_id']) ? $items['old_id'] : null
            ];
        }
    }

}

var_dump("<h1>---------------------LOAD ALL CONTACT TYPES--------</h1>");
$contactTypes = [];
foreach ([
             [
                 'table' => 'tblContacttypes',
                 'con' => $dblink1
             ],
             [
                 'table' => $TABLES[13],
                 'con' => $dblink2
             ],
         ] as $contactType) {
    $contactTypesFromOld = [];
    $result = $contactType['con']->query("SELECT * from " . $contactType['table']);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $contactTypesFromOld[] = $row;
        }
    } else {
        var_dump('<p>Empty rows from ' . $contactType['table'] . '</p>');
    }
    $contactTypes[] = $contactTypesFromOld;
}

//var_dump($contactTypesFromOld);die();


var_dump("<h1>---------------------LOAD ALL MEMBER PRICES--------</h1>");
$membersPrice = [];
$result = $dblink1->query("SELECT * from `tblWerknemerskosten`");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $membersPrice[] = $row;
    }
} else {
    var_dump('<p style="color: orange;">Empty rows from tblWerknemerskosten</p>');
}

var_dump("<h1>---------------------LOAD INVOICES TYPE--------</h1>");
$invoicesType = [];
$result = $dblink2->query("SELECT * from `$TABLES[12]`");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $invoicesType[] = $row;
    }
} else {
    var_dump('<p style="color: orange;">Empty rows from ' . $TABLES[12] . '</p>');
}


var_dump("<h1>---------------------LOAD ALL CONTACT CONTACT_TYPES--------</h1>");
$typesPercontact = [];
$result = $dblink1->query("SELECT contactid as contact_id, typeid as type_id ,`verwijderd` as deleted_at from `tblTypespercontact`");
if ($result && $result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $typesPercontact[] = $row;
    }
} else {
    var_dump('<p style="color: orange;">Empty rows from tblTypespercontact</p>');
}


/*var_dump("<h1>---------------------LOAD COMPANY ACTIVITIES--------</h1>");
$companyActivities = [];
$result = $dblink1->query("SELECT `specialiteit`as `name` from `$TABLES[15]` ".$LIMIT);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {

        $_name = $row['name'];
        if($dblink2->query("INSER into `$TABLES[35]` (`name`)VALUES(``)")){
            $companyActivities[] =[
                'id'=>$dblink2->last_inserted_id,
                'name'=>$_name
            ];
        }
    }
} else {
    var_dump('<p style="color: orange;">Empty rows from ' . $TABLES[15] . '</p>');
}

*/


var_dump("<h1>---------------------LOAD RELATION BTW LEADERS AND SITES--------</h1>");
$allleadersPerSite = [];
$result = $dblink1->query("SELECT `projectleiderperwerfid` as `id`,`werfbeheerid` as `siteId`,
`projectleiderid` as `leaderId`,`type`,`procent` as percentage from `tblProjectleiderperwerf`");
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $allleadersPerSite[] = $row;
    }
} else {
    var_dump('<p style="color: orange;">Empty rows from tblProjectleiderperwerf</p>');
}

$newLeaders = [];
$allUsers = [];
function checkMissedUsers()
{
    global $dblink1;
    global $TABLES;
    global $newLeaders;
    global $allUsers;
    global $dblink2;
    global $usrPsw;
    $leader_table = 'tblProjectleiders';
    var_dump("<h1>---------------------Add missed users from projectleadrs table--------</h1>");
    $queries = [
        "SELECT CONCAT(`voornaam`,\" \",`naam`)as user_name,projectleiderid as id, `naam`, `voornaam` from `$leader_table`",
        "SELECT CONCAT(`first_name`,\" \",`name`)as user_name,  id from `$TABLES[5]`"
    ];
    $result = $dblink2->query($queries[1]);
    $result1 = $dblink1->query($queries[0]);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $allUsers[] = $row;
        }
    } else {
        var_dump('<p style="color: orange;">Empty rows from `$TABLES[5]`</p>');
    }
    if ($result1->num_rows > 0) {
        while ($row = $result1->fetch_assoc()) {
            $alreadyHave = false;
            foreach ($allUsers as $user) {
                if ($user['user_name'] == $row['user_name']) {
                    $newLeaders[] = [
                        'id' => $user['id'],
                        'user_name' => $user['user_name'],
                        'old_id' => $row['id']
                    ];
                    $alreadyHave = true;
                }
            }
            if (!$alreadyHave) {
                $query = "INSERT into `$TABLES[5]`(`name`,`first_name`,`username`,`password`) 
    VALUES('" . $row['naam'] . "','" . $row['voornaam'] . "','" . $row['user_name'] . "','$usrPsw')";
                if ($dblink2->query($query)) {
                    $user_inserted = [
                        'id' => $dblink2->insert_id,
                        'user_name' => $row['user_name'],
                        'old_id' => $row['id']
                    ];
                    $newLeaders[] = $user_inserted;
                    $allUsers[] = $user_inserted;
                } else {
                    var_dump('<p style="color: red;">' . $query . '</p>');
                }
            }
        }
    } else {
        var_dump('<p style="color: orange;">Empty rows from tblProjectleiders  </p>');
        var_dump($queries);
    }
    var_dump("<p>Count of poject leaders " . count($newLeaders) . "</p>");
}


var_dump("<h1>---------------------ADOPTING--------</h1>");
//copy values from old tables of app
$contactWithTeamId = 20;
$PARENT_ID_KEY = '_PARENT_ID_';
$CHILD_ID_KEY = '_CHILD_ID_';
$CHILD_CHILD_ID_KEY = '_SUB_CHILD__ID_';
foreach ($tables as $table) {
    $sql = "SELECT ";
    $sqlFields = "";
    $newDb = $table['to_new'];
    $queryAllRows = "";
    $keys_should_take_relations = [];

    $keys = '';
    foreach ($table['from'] as $key => $part) {

        if ($key == 'name') {

        } else {
            if (
                is_numeric(strpos($part, 'FROM_UNIXTIME')) ||
                is_numeric(strpos($part, 'CONCAT('))
            ) {
                $sqlFields .= (strlen($sqlFields) > 0 ? "," : "") . $part . " as " . $newDb[$key];//without semicolons like ``
            } /*else if (is_numeric(strpos($part, $relationPrefix))) {

                    //no need to include in query

                    $oldFiledName = explode($relationPrefix, $part)[0];
                    $newFiledName = explode($relationPrefix, $newDb[$key])[0];

                    $sqlFields .= (strlen($sqlFields) > 0 ? "," : "") . "`" . $oldFiledName . "` as " . $newFiledName;

                    $keys_should_take_relations[] = $key;
                } */ else {
                $sqlFields .= (strlen($sqlFields) > 0 ? "," : "") . "`" . $part . "` as " . $newDb[$key];
            }

            //ignore fields for insert
            if ($table['to_new']['name'] == $TABLES[15]) {//contacts
                if (array_search($key, ['field_9', 'field_5', 'field_0', 'field_11', 'field_12', 'field_13', 'field_14', 'field_15', 'field_16', 'field_17', 'field_18', 'field_19']) !== false
                ) continue;//ignore fields for insert
            } else if ($table['to_new']['name'] == $TABLES[24]) {//outgoing invoice
                if ($key == 'field_6' || $key == 'field_9') continue;//ignore fields for insert
            } else if ($table['to_new']['name'] == $TABLES[17]) {//outgoing invoice
                if ($key == 'field_5') continue;//ignore fields for insert
            } else if ($table['to_new']['name'] == $TABLES[32]) {//externals
                if ($key == 'field_7') continue;
            }
            $keys .= (strlen($keys) > 0 ? "," : "") . "`" . $newDb[$key] . "`";


        }

    }
    if (isset($table['default_v'])) {
        foreach ($table['default_v'] as $key => $part) {
            if ($key == 'sequence') {
                $keys .= (strlen($keys) > 0 ? "," : "") . "`$key`";
            } else if ($key == 'password') {
                $keys .= (strlen($keys) > 0 ? "," : "") . "`$key`";
            }
        }
    }

    $orderByField = $table['from']['field_1'];
    /* if ($table['to_new']['name'] == $TABLES[15]) {//contacts----------------only on developing, on copy entire tables, should remove
     } else if ($table['to_new']['name'] == $TABLES[17]) {
         $orderByField = 'contactid';
     } else if ($table['to_new']['name'] == $TABLES[24]) {
         $orderByField = 'klant';
     } else if ($table['to_new']['name'] == $TABLES[23]) {
         $orderByField = 'werfdetailatid';
     } else if ($table['to_new']['name'] == $TABLES[20]) {
         $orderByField = 'werfbeheerid';
     }*/
    $orderByField .= " ASC";

    $sql .= $sqlFields . " FROM `" . $table['from']['name'] . "`  Order By " . $orderByField . $LIMIT;
    $result = $dblink1->query($sql);
    if (!$result) {
        die($sql);
    }
    var_dump("copy table " . $table['from']['name'] . " " . $table['to_new']['name'] . ", TOTAL: " . $result->num_rows);
    if ($result->num_rows > 0) {
        $index = 0;
        $d_val = "";
        $values = '';

        while ($row = $result->fetch_assoc()) {
            $queries = [];
            $index++;
            $insert_val = '';

            if ($table['to_new']['name'] == $TABLES[15]) {//contacts
                $newTableKey = $table['to_new']['field_5'];
                $haveRelatedId = false;
                /*if ($row['id'] == $contactWithTeamId) {//Hordcoded for SFINX team
                    $relatedType = $hard_coded[2]['items'][8]['type'];
                    foreach ($contactTypes[1] as $contactNewTypeName) {
                        if ($contactNewTypeName['type'] == $relatedType) {//Intern team
                            $row[$newTableKey] = $contactNewTypeName['id'];//get new Id from new table
                            $haveRelatedId = true;
                            break;
                        }
                    }
                    if (!$haveRelatedId) {
                        $row = null;
                        continue;
                    }
                }*/


                $companyField = [
                    'company_name',
                    'company_type_id',
                    'vat_number',
                    'website',
                    'price',
                    'quality',
                    'technical_expertise',
                    'aesthetic_quality',
                ];

                $_keys = '';
                $_values = "";
                foreach ($companyField as $field) {
                    $val = $row[$field];
                    unset($row[$field]);
                    if ($field == $companyField[0]) $field = 'name';
                    $_keys .= (strlen($_keys) > 0 ? "," : "") . "`$field`";
                    if ($field == $companyField[1] && empty($val)) {
                        $_values .= (strlen($_values) > 0 ? "," : "") . " NULL";
                    } else {
                        $_values .= (strlen($_values) > 0 ? "," : "") . "'$val'";
                    }


                }
                if (empty($row['expertise'])) {
                    $queries[] = "INSERT into `$TABLES[34]` (" . $_keys . ") VALUES (" . $_values . ")";
                    $queries[] = "INSERT into `$TABLES[35]` (`contact_id`,`company_id`) VALUES (" . $PARENT_ID_KEY . "," . $CHILD_CHILD_ID_KEY . ")";
                } else {
                    $queries[] = "INSERT into `$TABLES[36]` (`name`) VALUES ('" . $row['expertise'] . "')";
                    $queries[] = "INSERT into `$TABLES[34]` (" . $_keys . ") VALUES (" . $_values . ")";
                    $queries[] = "INSERT into `$TABLES[37]` (`company_activity_id`,`company_id`) VALUES (" . $CHILD_CHILD_ID_KEY . "," . $CHILD_ID_KEY . ")";
                    $queries[] = "INSERT into `$TABLES[35]` (`contact_id`,`company_id`) VALUES (" . $PARENT_ID_KEY . "," . $CHILD_ID_KEY . ")";
                }

                $contact_type = $row['type_id'];
                if ($contact_type == 3) {//klants only
                    $row['last_name'] = $row['contact_last_name'];
                } else if ($contact_type == 20) {//itern teams
                    $row['type_id'] = 9;

                }

//                if ($row['id'] == 20) {//hardcoded
//                    $queries[] = "INSERT into `$TABLES[38]` (`contact_id`,`contact_type_id`) VALUES (" . $row['id'] . ",9)";
//                }
                if ($contact_type == 9) {//contacts which have several types
                    foreach ($typesPercontact as $item) {
                        if ($item['contact_id'] == $row['id']) {
                            $type_id = $item['type_id'];
                            $listOfTypes = $hard_coded_items[$TABLES[13]];
                            foreach ($listOfTypes as $typeItem) {
                                if ($typeItem['old_id'] == $type_id) {
                                    $type_id = $typeItem['new_id'];
                                    break;
                                }
                            }
                            $queries[] = "INSERT into `$TABLES[38]` (`contact_id`,`contact_type_id`,`deleted_at`) VALUES (" . $row['id'] . "," . $type_id . "," . ($item['deleted_at'] == 1 ? date('Y-m-d', strtotime('1999-12-31')) : 'NULL') . ")";
                        }
                    }
                } else {
                    $queries[] = "INSERT into `$TABLES[38]` (`contact_id`,`contact_type_id`) VALUES (" . $row['id'] . "," . $row['type_id'] . ")";
                }

                $contactItems[] = [
                    'id' => $row['id'],
                    'construction_site_statuses_id' => $row['construction_site_statuses_id']
                ];

                if (
                    $contact_type > count($contactTypes[1]) && $contact_type != 20 ||
                    empty($contact_type) ||
                    $contact_type <= 0
                ) continue;

                unset($row['construction_site_statuses_id']);
                unset($row['expertise']);
                unset($row['contact_last_name']);
                unset($row['type_id']);
            } else if ($table['to_new']['name'] == $TABLES[17]) {//construction_sites
//                if ($row['contact_id'] <= 0) {
//                    $row = null;
//                    continue;
//                }
                $leaderId = $row['leader_id'];
                unset($row['leader_id']);
                if ($leaderId > 0) {

                    foreach ($newLeaders as $leaders) {
                        if ($leaders['old_id'] == $leaderId) {
                            foreach ($allleadersPerSite as $rel) {
                                if ($rel['siteId'] == $row['id'] && $rel['leaderId'] == $leaderId) {
                                    $_query = "INSERT into ";
                                    if ($rel['type'] == 0) {
                                        $_query .= $TABLES[29];
                                    } else if ($rel['type'] == 1) {
                                        $_query .= $TABLES[28];
                                    }
                                    $_query .= " (`user_id`,`construction_site_id`,`percentage`) VALUES('" . $leaders['id'] . "','" . $row['id'] . "','" . $rel['percentage'] . "')";
                                    $queries[] = $_query;
                                }
                            }
                            break;
                        }
                    }

                }
                $row['construction_site_statuses_id'] = null;
                foreach ($contactItems as $contactItem) {
                    if ($row['contact_id'] == $contactItem['id']) {
                        $row['construction_site_statuses_id'] = $contactItem['construction_site_statuses_id'];
                        break;
                    }
                }
            } else if ($table['to_new']['name'] == $TABLES[30]) {//incoming_invoices
//                if ($row['contact_id'] <= 0) {
//                    $row = null;
//                    continue;
//                }

            } else if ($table['to_new']['name'] == $TABLES[8]) {//members
                $row['hour_rate'] = 0;
                foreach ($membersPrice as $member) {
                    if ($member['werknemerid'] == $row['id']) {
                        $row['hour_rate'] = $member['nettokostprijs'];
                        break;
                    }
                }
            } else if ($table['to_new']['name'] == $TABLES[20]) {//construction_site_collaborators

		$row['is_ordered'] = $row['is_ordered'] == 2?1:0 ;
		$row['is_assigned'] = $row['is_assigned'] != 0?1:0;

		$row['is_contractor_type_ordered'] = $row['is_contractor_type_ordered'] == 2?1:0 ;
		$row['is_contractor_type_assigned'] = $row['is_contractor_type_assigned'] != 0?1:0;

                /*foreach ([1, 2] as $item) {
                    if ($item == 1) {
                        $row['is_ordered'] = $row['is_ordered'] == 2 ? 1 : 0;
                        $row['is_assigned'] = $row['is_ordered'] == 1 ? 0 : 1;
                    } else {
                        $row['is_contractor_type_ordered'] = $row['is_contractor_type_ordered'] == 2 ? 1 : 0;
                        $row['is_contractor_type_assigned'] = $row['is_contractor_type_ordered'] == 1 ? 0 : 1;
                    }
                }*/
            } else if ($table['to_new']['name'] == $TABLES[24]) {//outgoing invoices
                $queries[] = "INSERT INTO $TABLES[25] (`amount`,`vat_percentage`,`outgoing_invoice_id`) VALUES( '" .
                    $row['amount'] . "','" . $row['vat_percentage'] . "','" . $PARENT_ID_KEY . "')";
                unset($row['amount']);
                unset($row['vat_percentage']);

                $row['invoice_type_id'] = intval($row['invoice_type_id']) + 1;//
            } else if ($table['to_new']['name'] == $TABLES[32]) {//extrnals
//                if ($row['contact_id'] <= 0) {
//                    $row = null;
//                    continue;
//                }

                $row['ordered'] = $row['ordered'] == 0 ? false : true;

                $row['is_ordered'] = $row['is_assign'] = false;
                if ($row['order_c'] == 1) {
                    $row['is_ordered'] = true;
                } else if ($row['order_c'] == 1) {
                    $row['is_assign'] = true;
                }
                unset($row['order_c']);
            }

            foreach ($keys_should_take_relations as $keyItem) {
                $oldTableKey = $table['from'][$keyItem];
                $newTableKey = $table['to_new'][$keyItem];
                if ($table['to_new']['name'] == $TABLES[15]) {//contacts
                    $oldContactypeId = $row[$newTableKey];
                    $haveRelatedId = false;

                    if ($row[$newTableKey] >= count($contactTypes[1])) {//where is id which are > 10?
                        $row = null;//ignore data rows which contact type id not exist
                        break;
                    }

                    if (empty($oldContactypeId) || $oldContactypeId < 1 || $oldContactypeId >= count($contactTypes[1])) {//where is id which are > 10?
                        $row = null;//ignore data rows which contact type id not exist
                        break;
                    }


                    foreach ($contactTypes[0] as $contactTypeName) {
                        if ($contactTypeName['contacttypesid'] == $oldContactypeId) {
                            foreach ($contactTypes[1] as $contactNewTypeName) {
                                if ($contactNewTypeName['type'] == $row['type']) {
                                    $row[$newTableKey] = $contactNewTypeName['id'];//get new Id from new table
                                    $haveRelatedId = true;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    if (!$haveRelatedId) {
                        $row = null;
                        break;
                    }
                }


            }

            if (empty($row) || $row['id'] < 0) continue;

            if ($table['to_new']['name'] == $TABLES[15]) {
//                if (
//                    $row['type_id'] > count($contactTypes[1]) && $row['type_id'] !=20 ||
//                    empty($row['type_id']) ||
//                    $row['type_id'] <= 0
//                ) continue;
            } else if ($table['to_new']['name'] == $TABLES[16]) {
//                $row['contact_id'] = $contactWithTeamId;
            }

            if (isset($row['contact_id']) && $row['contact_id'] <= 0) {
                $row['contact_id'] = null;
            }

            $valuesList = array_values($row);
            foreach ($valuesList as $keyI => $v) {
                $valuesList[$keyI] = $dblink2->real_escape_string($v);
            }
            $insert_val .= (strlen($insert_val) > 0 ? "," : "") . "'" . implode("', '", $valuesList) . "'";
            if (isset($table['default_v'])) {
                foreach ($table['default_v'] as $key => $part) {
                    if ($key == 'sequence') {
                        $insert_val .= (strlen($insert_val) > 0 ? "," : "") . "'$index'";
                    } else if ($key == 'password') {
                        $insert_val .= (strlen($insert_val) > 0 ? "," : "") . "'" . $dblink2->real_escape_string($part) . "'";
                    }
                }
            }


            $insert_val = str_replace($spce_in_string, " ", $insert_val);
            $query = "INSERT INTO `" . $newDb['name'] . "` (" . $keys . ") VALUES (" . $insert_val . ")";

            $res_query = $dblink2->query($query);


            $maxLoop = 5;//make sure our loop will not be infinity
            $curItemInoop = 0;
            do {
                if (!$res_query) {
                    $shouldTryAgain = false;
                    //if missed some relation
                    $error_string = mysqli_error($dblink2);
                    if (is_numeric(strrpos($error_string, "FOREIGN KEY ("))) {//relation to missed contacts
                        $row[substrKey($error_string)] = null;
                        $shouldTryAgain = true;
                    }

                    if ($shouldTryAgain) {
                        $insert_val = '';
                        $valuesList = array_values($row);
                        foreach ($valuesList as $keyI => $v) {
                            $isNull = $v != '0' && (is_null($v) || $v == '');
                            $val = $isNull ? '' : $dblink2->real_escape_string($v);
                            $insert_val .= (strlen($insert_val) > 0 ? "," : "") . ($isNull ? "IS_NULL_VAL" : "'" . $val . "'");
                        }

                        if (isset($table['default_v'])) {
                            foreach ($table['default_v'] as $key => $part) {
                                if ($key == 'sequence') {
                                    $insert_val .= (strlen($insert_val) > 0 ? "," : "") . "'$index'";
                                } else if ($key == 'password') {
                                    $insert_val .= (strlen($insert_val) > 0 ? "," : "") . "'" . $dblink2->real_escape_string($part) . "'";
                                }
                            }
                        }
//                    $insert_val = str_replace($spce_in_string, " ", $insert_val);
                        $insert_val = str_replace("IS_NULL_VAL", "NULL", $insert_val);
                        $query = "INSERT INTO `" . $newDb['name'] . "` (" . $keys . ") VALUES (" . $insert_val . ")";
                        $res_query = $dblink2->query($query);
                    }
                }
            } while (!$res_query && $curItemInoop++ < $maxLoop);


            if (!$res_query) {
                {
//                    if ($TABLES[20] == $newDb['name']) {
                    var_dump("<p style=\"color:red;\">Error description: " . mysqli_error($dblink2) . "</p>");
                    var_dump($query);
//                        var_dump($insert_val);
//                        die();
//                    }
                }
            } else {

                $parentId = $row['id'];//$conn->insert_id;
                $childId = [];
                foreach ($queries as $key => $queryY) {
                    $q = str_replace($PARENT_ID_KEY, $parentId, $queryY);
                    if (!empty($childId[1])) $q = str_replace($CHILD_ID_KEY, $childId[1], $q);
                    if (!empty($childId[0])) $q = str_replace($CHILD_CHILD_ID_KEY, $childId[0], $q);

                    $subQuery = $dblink2->query($q);
                    if (!$subQuery) {
                        {
                            if ($TABLES[15] == $newDb['name']) {
                                var_dump("<p style=\"color:red;\">Error in subquery: " . mysqli_error($dblink2) . "</p>");
                                var_dump("<p > Query " . $q . "</p>");
                            }

                        }
                    } else {
                        $childId[] = $dblink2->insert_id;
                    }
                }
            }
            //            }
        }
        //        if (empty($queryAllRows)) {
        //            if (!$dblink2->query($queryAllRows)) {
        ////                var_dump("<p style=\"color:red;\">Error description: " . mysqli_error($dblink2) . "</p>");
        //            }
        //        }
    } else {
        var_dump("0 results from " . $table['from']['name']);
        var_dump($sql);
    }
    if ($table['to_new']['name'] == $TABLES[5]) {//users
        checkMissedUsers();
    }


}


function query($connect, $query)
{

}

function substrKey($str)
{
    $string = explode("FOREIGN KEY (`", $str)[1];
    return (substr($string, 0, strpos($string, "`")));
}

var_dump("<h1>---------------------RELATION MERGED IN ONE TABLE--------</h1>");
$data = file_get_contents("./tempOldDbData/invoice_accounts.json");
$data = json_decode($data, true);
foreach ($data as $k => $item) {
    $keys = 'sequence';
    $values = $k + 1;
    foreach ($item as $key => $val) {
        $keys .= (strlen($keys) > 0 ? "," : "") . "`" . $key . "`";
        $values .= (strlen($values) > 0 ? "," : "") . (empty($val) && is_null($val) ? "NULL" : "\"" . $val . "\"");

    }
    $query = "INSERT INTO `" . $TABLES[11] . "` (" . $keys . ") VALUES (" . $values . ")";
    if (!$dblink2->query($query)) {
        {
            var_dump("Error description: " . mysqli_error($dblink2));
            die($query);
        }
    }
}


var_dump("<h1>---------------------ADD  TRIGGERES AFTER MIGRATION --------</h1>");
$TRIGGERS = [
    "CREATE TRIGGER `$TRIGGERS_NAME[2]` AFTER INSERT ON `$TABLES[15]` FOR " .
    "EACH ROW INSERT INTO `$TABLES[17]` (`sequence`,contact_id) VALUES(1,NEW.id)"
];
foreach ($TRIGGERS as $trigger) {
    if (!$dblink2->query($trigger)) {
        {
            var_dump($trigger);
            die("Error on create trigger: " . mysqli_error($dblink2));
        }
    }
}

var_dump("<h1>---------------------FINISH--------</h1>");
$dblink1->close();
$dblink2->close();
