<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Common\ConstructionSiteUserSettingType;

class ConstructionSiteTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<count(ConstructionSiteUserSettingType::$TYPES);$i++){
            factory(ConstructionSiteUserSettingType::class)->create(['name' => ConstructionSiteUserSettingType::$TYPES[$i]]);
        }
    }


}
