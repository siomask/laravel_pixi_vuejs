<?php

use App\Models\Common\ProjectLead;
use Illuminate\Database\Seeder;

class OutgoingInvoicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addInvoicesType();
//        $this->addPayments();

        $faker = Faker\Factory::create();
        factory(App\Models\Finances\OutgoingInvoice\OutgoingInvoice::class, 3)->create()->each(function ($invoice) use ($faker) {
            for ($i = 0; $i < random_int(1, 3); $i++) {
                $invoice->entries()->create([
                    'name' => $faker->name,
                    'amount' => mt_rand() / mt_getrandmax(),
                    'vat_percentage' => random_int(1, 100) / 100
                ]);

                for ($i = 1; $i < random_int(1, 3); $i++) {
                    $invoice->payments()->create([
                        'amount' => mt_rand() / mt_getrandmax(),
                        'date' => \Carbon\Carbon::now()->subDays(random_int(1, 100))
                    ]);
                }
            }
        });
    }

    private function addInvoicesType()
    {
        collect([
            ['name' => 'prepayment'],
            ['name' => 'regular']
        ])->each(function ($el) {
            factory(App\Models\Finances\InvoiceType::class)->create($el);
        });
    }

    private function addPayments()
    {
        for ($i = 0; $i < random_int(3, 10); $i++) {
            factory(App\Models\Finances\Payment::class)->create([
                'amount' => mt_rand() / mt_getrandmax(),
                'date' => \Carbon\Carbon::now()->subDays(random_int(1, 100))
            ]);
        }
    }
}
