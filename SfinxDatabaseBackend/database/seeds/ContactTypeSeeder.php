<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ContactTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            /* Main types */
            ['type' => 'aannemer'],
            ['type' => 'leverancier'],
            ['type' => 'klant'],
            ['type' => 'architect'],
            ['type' => 'veiligheidscoordinator'],
            ['type' => 'EPB verslaggever'],
            ['type' => 'stabiliteitsingenieur'],
            ['type' => 'bouwcoordinator'],
            ['type' => 'Intern Team'],
        ];
        $types = [];
        foreach ($data as $contact_type) {
            $types[] = factory(App\Models\Contact\ContactType::class)->create($contact_type);
        }
//        $this->addContacts($types,$this->addConstructionSiteStatus());
    }

    private function addConstructionSiteStatus(){
        $types=[];
        $data = [
            ['name' => 'Offerte in opmaak', 'contact_type_id' => 3],
            ['name' => 'in uitvoering', 'contact_type_id' => 3],
            ['name' => 'afgewerkt', 'contact_type_id' => 3],
            ['name' => 'geen prijs gegeven', 'contact_type_id' => 3],
            ['name' => 'niet verkocht', 'contact_type_id' => 3],
            ['name' => 'offerte klaar', 'contact_type_id' => 3],
            ['name' => 'in nazicht', 'contact_type_id' => 3],
            ['name' => 'uitgesteld op lange termijn', 'contact_type_id' => 3],
            ['name' => 'in prospectie', 'contact_type_id' => 3],
            ['name' => 'ontwerpfase', 'contact_type_id' => 3],
            ['name' => 'afgewerkt maar administratief nog af te werken', 'contact_type_id' => 3]
        ];


        foreach ($data as $contact_type) {
            $types[] = factory(App\Models\Planner\ConstructionSite\ConstructionSiteStatus::class)->create($contact_type);
        }
        return $types;
    }
    private function addContacts($types,$sitesStatus)
    {
        $_data = [
            'contact_out' => []
        ];
        $teams_out = [];
        $faker = Faker\Factory::create();

//        $tasks = DB::table('workflow_function_group_workflow_task')->get();

        for ($iC = 0; $iC < count($types); $iC++) {
            $type_id = ($iC < 2 ? $types[0]->id : ($iC < 3 ? $types[8]->id : ($iC < 5 ? $types[2]->id : ($types[random_int(0, count($types) - 1)]->id))));

            $u = factory(App\Models\Contact\Contact::class)->create([
                //2 contracts ->1 intern team ->2 clients -> any contact
                'type_id' => $type_id,
                'address' => $faker->address,
                'first_name' => $type_id==$types[8]->id && $iC < 3?'Sfinx':$faker->firstName,
                'last_name' =>  $type_id==$types[8]->id&& $iC < 3?'Intern Team':$faker->lastName
            ]);

            if($types[2]->id ==$type_id){
                $siteStatus = $sitesStatus[random_int(0, count($sitesStatus)-1)]->id;
                for($_it =1;$_it<4;$_it++){

                    $site = $u->sites()->create([
                        'construction_site_statuses_id' => random_int(1, count($sitesStatus)),
                        'presented_price' => rand(0, 100)
                    ]);
                    $sites[] = $site->id;

                    $site->externals()->create([
                        'comment'=>$faker->realText($faker->numberBetween(10,20))
                    ]);
//                    for ($i = 1; $i < 4; $i++) {//exept admin
                        $site->projectLeads()->create([
                            'user_id' => $_it
                        ])->make();
                        $site->estimators()->create([
                            'user_id' => rand(1, 3)
                        ])->make();
//                    }
//
                    if ($iC < 6) {
                        $site->settings()->create([
                            'construction_site_user_setting_type_id' => 1,
                            'user_setting_id' => 1
                        ])->make();
                    }


                    for ($id = 0, $countOfBranches = mt_rand(1, 2); $id < $countOfBranches; $id++) {
                        for ($idss = 0; $idss < count($_data['contact_out']); $idss++) {
                            $site->collaborators()->create([
                                'start' => Carbon::now()->subDays(random_int(1,30)),
                                'end' => Carbon::now()->addDays(random_int(1,30)),
                                'contact_id' => $_data['contact_out'][$idss],
                                'contractor_type_id' => mt_rand(1, 38),
                                'is_assigned' => 1,
                            ])->make();
                        }
                    }
                    //collaborator without contact
                    if($iC<2)$site->collaborators()->create([
                        'start' => Carbon::now()->subDays(random_int(1,30)),
                        'end' => Carbon::now()->addDays(random_int(1,30)),
                        'is_assigned' => 1,
                        'contractor_type_id' => mt_rand(1, 38)
                    ]);

                    if($iC<6){


                        //workflow sites

                        for ($i = 0; $i < 1; $i++) {//exept admin
                            $_site = $u->sites()->create([
                                'construction_site_statuses_id' => $siteStatus,
                                'presented_price' => rand(0, 100)
                            ]);
                            for ($i = 0; $i < random_int(1,5); $i++) {//exept admin
                                $_site->workflowTasks()->attach(random_int(1,5),[
                                    'start'=>Carbon::now(),
                                    'end'=>Carbon::now()
                                ]);
                            }

                            $_site->projectLeads()->create([
                                'user_id' => $_it
                            ])->make();
                            $_site->estimators()->create([
                                'user_id' => rand(1, 3)
                            ])->make();

                            $_site->settings()->create([
                                'construction_site_user_setting_type_id' => 2,//workflow type
                                'user_setting_id' => 1
                            ])->make();

//                            foreach ($tasks as $task){
//                                if(random_int(0,10)<5)$_site->workflowTasks()->attach($task->workflow_task_id);
//                            }
                        }

                    }


                    $u = factory(App\Models\Contact\Contact::class)->create([
                        //2 contracts ->1 intern team ->2 clients -> any contact
                        'type_id' => $type_id,
                        'address' => $faker->address,
                        'first_name' => $type_id==$types[8]->id && $iC < 3?'Sfinx':$faker->firstName,
                        'last_name' =>  $type_id==$types[8]->id&& $iC < 3?'Intern Team':$faker->lastName
                    ]);
                }
            }  else if ($type_id == $types[0]->id ||$type_id == $types[8]->id) {//contractor | team
                $_data['contact_out'][] = $u->id;

            }


        }


    }


}
