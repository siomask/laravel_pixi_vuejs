<?php

use Illuminate\Database\Seeder;

class ContractorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'WI', 'name' => 'Werfinrichtingen'                              /*, 'sequence' => 1*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'HA', 'name' => 'Haarden'                                       /*, 'sequence' => 2*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'FV', 'name' => 'Fotovoltaische installatie'                    /*, 'sequence' => 3*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'VO', 'name' => 'Vochtbehandeling'                              /*, 'sequence' => 4*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'AA', 'name' => 'Algemene aanneming'                            /*, 'sequence' => 5*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'TO', 'name' => 'Terrassen en opritten'                         /*, 'sequence' => 6*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'TU', 'name' => 'Tuininrichting'                                /*, 'sequence' => 7*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'DE', 'name' => 'Decoratiewerken'                               /*, 'sequence' => 8*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'SC', 'name' => 'Schilderwerken'                                /*, 'sequence' => 9*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'MM', 'name' => 'Maatmeubilair'                                 /*, 'sequence' => 10*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'GL', 'name' => 'Glaswerken'                                    /*, 'sequence' => 11*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'BD', 'name' => 'Binnendeuren'                                  /*, 'sequence' => 12*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'TR', 'name' => 'Trappen'                                       /*, 'sequence' => 13*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'PH', 'name' => 'Parket en houten vloeren'                      /*, 'sequence' => 14*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'ZB', 'name' => 'Zachte vloer- en wandbekleding'                /*, 'sequence' => 15*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'VL', 'name' => 'Vloer- en wandbetegeling'                      /*, 'sequence' => 16*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'GK', 'name' => 'Gyprocwerken'                                  /*, 'sequence' => 17*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'BI', 'name' => 'Binnenschrijnwerken'                           /*, 'sequence' => 18*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'DV', 'name' => 'Dekvloeren'                                    /*, 'sequence' => 19*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'PL', 'name' => 'Pleisterwerken'                                /*, 'sequence' => 20*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'IS', 'name' => 'Isolatiewerken'                                /*, 'sequence' => 21*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'EL', 'name' => 'Elektrische installatie'                       /*, 'sequence' => 22*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'VT', 'name' => 'Ventilatie'                                    /*, 'sequence' => 23*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'SA', 'name' => 'Sanitaire installatie'                         /*, 'sequence' => 24*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'CV', 'name' => 'Centrale verwarmingsinstallatie'               /*, 'sequence' => 25*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'TI', 'name' => 'Timmerwerken'                                  /*, 'sequence' => 26*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'BU', 'name' => 'Buitenschrijnwerk'                             /*, 'sequence' => 27*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'DK', 'name' => 'Dakwerken'                                     /*, 'sequence' => 28*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'ME', 'name' => 'Metselwerken'                                  /*, 'sequence' => 29*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'ST', 'name' => 'Staalconstruei'                                /*, 'sequence' => 30*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'BE', 'name' => 'Betonwerken'                                   /*, 'sequence' => 31*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'RI', 'name' => 'Rioleringswerken'                              /*, 'sequence' => 32*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'AF', 'name' => 'Afbraakwerken'                                 /*, 'sequence' => 33*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'GR', 'name' => 'Grondwerken'                                   /*, 'sequence' => 34*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'FU', 'name' => 'Funderingswerken'                              /*, 'sequence' => 35*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'AC', 'name' => 'Airconditioning'                               /*, 'sequence' => 36*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'NA', 'name' => 'Natuursteenwerken'                             /*, 'sequence' => 37*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'BH', 'name' => 'Voorzieningen door bouwheer'                   /*, 'sequence' => 38*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => 'RE', 'name' => 'Reinigingswerken'                              /*, 'sequence' => 39*/]);
        factory(App\Models\Common\ContractorType::class)->create(['abbreviation' => '-', 'name' => 'niet verwijderen'                               /*, 'sequence' => 40*/]);
    }
}
