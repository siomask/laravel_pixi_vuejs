<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->addTeam($this->addTeamMember($this->addAbsencesTypes()));
    }

    private function addTeam($members)
    {

        $faker = Faker\Factory::create();
        for ($idT = 0; $idT < 3; $idT++) {

            $team = factory(App\Models\Team\Team::class)->create([
                'contact_id' => 3,//intern team contact
                'abbreviation' => $idT,
                'name' => $faker->company
            ]);
            $teamPeriod = $team->period()->create([
                'start' => Carbon::now()->subYears(2),
                'end' => Carbon::now()->addYears(2)
            ]);
            for ($i = 0, $maxI = rand(1, count($members)); $i < $maxI; $i++) {
                $teamPeriod->periodMembers()->create([
                    'team_member_id' => $members[$i]->id,
                    'member_price' => random_int(0, 10)
                ]);
            }
        }
    }

    private function addAbsencesTypes()
    {
        $abs = [
            ['name' => "Sick Days"],
            ['name' => "Holiday"]
        ];
        $res = [];
        for ($i = 0; $i < count($abs); $i++) {
            $res[] = factory(App\Models\Common\AbsenceType::class)->create($abs[$i]);
        }
        return $res;
    }

    private function addTeamMember($absences)
    {
        $result = [];
        $faker = Faker\Factory::create();
        for ($idT = 0; $idT < 3; $idT++) {
            $member = factory(App\Models\Common\Member::class)->create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName
            ]);
            $result[] = $member;
            for ($i = 0; $i < random_int(1, 4); $i++) {
                $member->absences()->create([
                    'absence_type_id' => $absences[random_int(0,count($absences)-1)]->id,
                    'start' => Carbon::now()->subDays(random_int(1, 40)),
                    'end' => Carbon::now()->addDays(1, 40)
                ]);
            }
        }

        return $result;
    }


}
