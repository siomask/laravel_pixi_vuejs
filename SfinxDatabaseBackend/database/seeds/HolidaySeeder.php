<?php

use Illuminate\Database\Seeder;

class HolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 2; $i++) {
            factory(App\Models\Common\Holiday::class)->create([
                'name' => $faker->company,
                'start' => \Carbon\Carbon::now()->subDays(5),
                'end' => \Carbon\Carbon::now()->subDays(2),
            ]);
        }

    }


}
