<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WorkflowTaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $nested = 0;
    private $countNested = 0;

    public function run()
    {

        $this->nested = 0;
        factory(App\Models\Workflow\Task\WorkflowTask::class, 5)->create()->each(function ($task) {
            $this->nested = 0;
            $this->countNested = 0;
            $this->addSubTask($task);
        });

    }


    public function addSubTask($task, $itemStart = 0)
    {
        /*if(random_int(1, 10)<8)*/
//        if(!empty($task->parent_workflow_task_id))$task->workflowFunctionGroups()->attach(random_int(1, 10));
        for ($i = 0; $i < random_int($itemStart, 3); $i++) {
            $faker = Faker\Factory::create();
            $_n_task = $task->parentWorkflowTask()->create([
                'name' => $faker->company,
                'description' => $faker->text(45)
            ]);
            if ($_n_task->parent_workflow_task_id == $_n_task->id) {
                $_n_task->delete();
            } else {
                if ($this->nested++ < 5 || random_int(0, 10) < 5) {
                    if ($this->countNested++ < 5) $this->addSubTask($_n_task, 1);
                } else {
                    $_n_task->workflowFunctionGroups()->attach(random_int(1, 10));
                }
            }

        }
    }
}
