<?php

use App\Models\Common\ProjectLead;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $countEl = 10;
        factory(App\Models\Common\Right::class,$countEl)->create()->each(function($right,$key)use($countEl){
            if($key-1>0)$right->update(['parent_right_id'=>random_int(1,$key-1)]);
        });;
        factory(App\Models\User\UserGroup::class,$countEl)->create()->each(function($userGroup)use($countEl){
            for($r=random_int(1,$countEl),$maxR =random_int($r,$countEl);$r<$maxR;$r++){
                $userGroup->rights()->attach($r);
            }
        });




        $faker = Faker\Factory::create();
        for ($i = 0; $i < 3; $i++) {
            factory(App\Models\Common\User::class)->create([
                'username' => $i > 0 ? $faker->unique()->userName : 'admin',
                'name' => $faker->lastName,
                'first_name' => $faker->firstName,
                'user_group_id' => random_int(1,$countEl),
                'password' => bcrypt('admin')
            ])->each(function ($u) use($i){
                $u->settings()->create([
                    'planner_date_start' => \Carbon\Carbon::now()->subDays(20),
                    'planner_date_end' => \Carbon\Carbon::now()->addDays(2)
                ])->make();

//                $u->group()->attach($i+1);
            });
        }


    }

}
