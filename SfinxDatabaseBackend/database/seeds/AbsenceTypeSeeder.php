<?php

use Illuminate\Database\Seeder;

class AbsenceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$abs = [
			['name' => "Sick Days"],
			['name' => "Holiday"]
		];
		$res = [];
		for ($i = 0; $i < count($abs); $i++) {
			$res[] = factory(App\Models\Common\AbsenceType::class)->create($abs[$i]);
		}
    }
}
