<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvoiceAccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {

        $data = file_get_contents("./database/copyTables/tempOldDbData/invoice_accounts.json");
        $data = json_decode($data, true);
        foreach ($data as $item) {
            factory(App\Models\Finances\InvoiceAccount::class)->create($item);
        }


    }


}
