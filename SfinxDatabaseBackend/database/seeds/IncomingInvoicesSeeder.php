<?php

use App\Models\Common\ProjectLead;
use Illuminate\Database\Seeder;

class IncomingInvoicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        factory(App\Models\Finances\IncomingInvoice\IncomingInvoice::class, 3)->create()->each(function ($invoice) use ($faker) {
            for ($i = 0; $i < random_int(1, 3); $i++) {
                $invoice->entries()->create([
                    'name' => $faker->name,
                    'amount' => mt_rand() / mt_getrandmax(),
                    'vat_percentage' => random_int(1, 100) / 100
                ]);
            }
        });
    }
}
