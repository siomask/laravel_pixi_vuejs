<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Finances\IncomingInvoice\IncomingInvoice::class, function (Faker $faker) {
    return [
        'contact_id' => random_int(1, 7),
        'serial_number' => $faker->iban(null),
        'comment' => $faker->text(45),
        'invoice_number' => $faker->creditCardNumber,
//        'payment_term' => 60 * 60 * 24 * (random_int(1, 7)),//random days
        'is_paid' => random_int(1, 7) < 3,
        'financial_discount' => mt_rand() / mt_getrandmax(),
        'discount' => mt_rand() / mt_getrandmax()
    ];
});
