<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Common\Right::class, function (Faker $faker) {
    return [
        'description' => $faker->realText($faker->numberBetween(10,20)),
        'name' => $faker->name
    ];
});
