<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Finances\OutgoingInvoice\OutgoingInvoice::class, function (Faker $faker) {
    return [
        'contact_id' => random_int(1, 7),
        'invoice_type_id' => random_int(1, 2),
        'comment' => $faker->text(45),
        'invoice_number' => $faker->creditCardNumber,
//        'payment_term' => 60 * 60 * 24 * (random_int(1, 7)),
    ];
});
