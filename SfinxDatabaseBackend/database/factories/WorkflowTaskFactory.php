<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Workflow\Task\WorkflowTask::class, function (Faker $faker) {
    return [
        'parent_workflow_task_id' => null,
        'name' => $faker->company,
        'description' => $faker->text(45)
    ];
});
