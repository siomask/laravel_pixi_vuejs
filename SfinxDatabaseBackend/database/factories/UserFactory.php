<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Common\User::class, function (Faker $faker) {
    return [
        'username' => $faker->userName,
        'name' => $faker->lastName,
        'first_name' => $faker->firstName,
        'password' => bcrypt('password')
    ];
});
