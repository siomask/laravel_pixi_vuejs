**Login through JWT**
----
  Returns JSON Web Token to be used for authorization during subsequest api calls

* **URL**

  /api/account

* **Method:**

  `GET`
  
*  **URL Params**
 
   None

* **Data Params**

    * **Required:**
   
       ```
        "sfinx_username" => "jente"
        "sfinx_password" => "jente"
       ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "status": "200",
        "message": "Authorized",
        "sfinx_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJsb2NhbGhvc3Q6ODAwMCIsImV4cCI6MTQ5MzE4ODkxMiwiaWF0IjoxNDkzMTYwMTEyLCJ1c2VybmFtZSI6ImplbnRlIiwic2NvcGUiOnsicmVjaHRlbiI6MSwic3BlY2lhbGVyZWNodGVuIjo1LCJzd2l0Y2h2aWV3IjoxfX0.boMnsD2HN1L3tBJzzxr_Y1r1rHdX2bl5ChJZMuVxcGtjTAr7UI-9XVHRvURV8l-5IdxI6JPYebw2Yb97EyltnlpD1Kp6r6XuXNfSv7gv-NYPh2JLc907FkZ809Jl0H5Y4wf_zb5i-7llhcZ0kFMiT1m-EV4TjE5en8nnBKPis2ZvjA2Hy3IZu6-Qi0V05UW7SgPDr7wdtfNnKgWCKl69vi_srQT4VM6L-5IwRRU-FyAAdRfGp1K030Dsrjb26-sQM6ngnTEExAJv58-FIWL8GVtCj14SMENOr1VLxX0wqpwJt00QdIifhcfjCwkxFOrzrSXtwefyS2_rGrA3I29nMA"
    }
    ```
 
* **Error Response:**

  * **Code:** 401 NOT AUTHORIZED <br />
    **Content:** 
    ```
    {
      "status": "401",
      "message": "Not Authorized"
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```
    {
      "status": "400",
      "message": "Bad Request"
    }
    ```

**Add user**
----
  Returns JSON Web Token to be used for authorization during subsequest api calls

* **URL**

  /api/user

* **Method:**

  `POST`
  
*  **URL Params**
 
   None

* **Data Params**

    * **Required:**
   
       ```
        "naam"              => "Naam admin",
        "voornaam"          => "Voornaam admin",
        "gebruikersnaam"    => "admin",
        "paswoord"          => "adminpaswoord",
        "rechten"           => 3,
        "specialerechten"   => 5,
        "switchview"        => 1,
        "is_projectleider"  => 1
       ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "status": "200",
        "message": "User has been added",
    }
    ```
 
* **Error Response:**

  * **Code:** 401 NOT AUTHORIZED <br />
    **Content:** 
    ```
    {
      "status": "401",
      "message": "Not Authorized"
    }
    ```

  OR

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```
    {
      "status": "400",
      "message": "Bad Request"
    }
    ```
  OR

  * **Code:** 400 Bad Request: Malformed JSON <br />
    **Content:** 
    ```
    {
      "status": "400",
      "message": "Bad Request: Malformed JSON"
    }
    ```