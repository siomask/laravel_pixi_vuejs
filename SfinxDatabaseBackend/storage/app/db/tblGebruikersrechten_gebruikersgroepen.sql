-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 01:22 PM
-- Server version: 5.5.59-0+deb8u1
-- PHP Version: 5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sfinxto_sfinxwerktest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblGebruikersrechten_gebruikersgroepen`
--

CREATE TABLE IF NOT EXISTS `tblGebruikersrechten_gebruikersgroepen` (
`gebruikersrechten_gebruikersgroepenid` int(11) NOT NULL,
  `gebruikersrechten_gebruikersgroepen_gebruikersrechtenid` smallint(6) NOT NULL,
  `gebruikersrechten_gebruikersgroepen_gebruikersgroepenid` smallint(6) NOT NULL,
  `gebruikersrechten_gebruikersgroepensetting` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblGebruikersrechten_gebruikersgroepen`
--

INSERT INTO `tblGebruikersrechten_gebruikersgroepen` (`gebruikersrechten_gebruikersgroepenid`, `gebruikersrechten_gebruikersgroepen_gebruikersrechtenid`, `gebruikersrechten_gebruikersgroepen_gebruikersgroepenid`, `gebruikersrechten_gebruikersgroepensetting`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 3, 1),
(4, 1, 4, 1),
(5, 1, 5, 1),
(6, 1, 6, 0),
(7, 2, 1, 1),
(8, 2, 2, 1),
(9, 2, 3, 0),
(10, 2, 4, 0),
(11, 2, 5, 0),
(12, 2, 6, 0),
(13, 3, 1, 1),
(14, 3, 2, 1),
(15, 3, 3, 1),
(16, 3, 4, 1),
(17, 3, 5, 1),
(18, 3, 6, 0),
(19, 4, 1, 1),
(20, 4, 2, 1),
(21, 4, 3, 1),
(22, 4, 4, 1),
(23, 4, 5, 1),
(24, 4, 6, 0),
(25, 5, 1, 1),
(26, 5, 2, 1),
(27, 5, 3, 1),
(28, 5, 4, 1),
(29, 5, 5, 1),
(30, 5, 6, 0),
(31, 6, 1, 1),
(32, 6, 2, 1),
(33, 6, 3, 1),
(34, 6, 4, 1),
(35, 6, 5, 1),
(36, 6, 6, 0),
(37, 7, 1, 1),
(38, 7, 2, 1),
(39, 7, 3, 1),
(40, 7, 4, 1),
(41, 7, 5, 0),
(42, 7, 6, 0),
(43, 8, 1, 1),
(44, 8, 2, 1),
(45, 8, 3, 1),
(46, 8, 4, 1),
(47, 8, 5, 0),
(48, 8, 6, 0),
(49, 9, 1, 1),
(50, 9, 2, 1),
(51, 9, 3, 1),
(52, 9, 4, 1),
(53, 9, 5, 0),
(54, 9, 6, 0),
(55, 10, 1, 1),
(56, 10, 2, 1),
(57, 10, 3, 1),
(58, 10, 4, 0),
(59, 10, 5, 0),
(60, 10, 6, 0),
(61, 11, 1, 1),
(62, 11, 2, 1),
(63, 11, 3, 1),
(64, 11, 4, 0),
(65, 11, 5, 0),
(66, 11, 6, 0),
(67, 12, 1, 1),
(68, 12, 2, 1),
(69, 12, 3, 1),
(70, 12, 4, 1),
(71, 12, 5, 0),
(72, 12, 6, 0),
(73, 13, 1, 1),
(74, 13, 2, 0),
(75, 13, 3, 0),
(76, 13, 4, 0),
(77, 13, 5, 0),
(78, 13, 6, 0),
(79, 14, 1, 1),
(80, 14, 2, 0),
(81, 14, 3, 0),
(82, 14, 4, 1),
(83, 14, 5, 0),
(84, 14, 6, 0),
(85, 15, 1, 1),
(86, 15, 2, 1),
(87, 15, 3, 0),
(88, 15, 4, 0),
(89, 15, 5, 0),
(90, 15, 6, 0),
(91, 16, 1, 1),
(92, 16, 2, 1),
(93, 16, 3, 0),
(94, 16, 4, 0),
(95, 16, 5, 0),
(96, 16, 6, 0),
(97, 17, 1, 1),
(98, 17, 2, 0),
(99, 17, 3, 1),
(100, 17, 4, 0),
(101, 17, 5, 0),
(102, 17, 6, 0),
(103, 18, 1, 1),
(104, 18, 2, 1),
(105, 18, 3, 1),
(106, 18, 4, 0),
(107, 18, 5, 0),
(108, 18, 6, 0),
(109, 19, 1, 1),
(110, 19, 2, 0),
(111, 19, 3, 1),
(112, 19, 4, 0),
(113, 19, 5, 0),
(114, 19, 6, 0),
(115, 20, 1, 1),
(116, 20, 2, 1),
(117, 20, 3, 1),
(118, 20, 4, 0),
(119, 20, 5, 0),
(120, 20, 6, 0),
(121, 21, 1, 1),
(122, 21, 2, 1),
(123, 21, 3, 1),
(124, 21, 4, 0),
(125, 21, 5, 0),
(126, 21, 6, 0),
(127, 22, 1, 1),
(128, 22, 2, 1),
(129, 22, 3, 1),
(130, 22, 4, 0),
(131, 22, 5, 0),
(132, 22, 6, 0),
(133, 23, 1, 1),
(134, 23, 2, 1),
(135, 23, 3, 1),
(136, 23, 4, 0),
(137, 23, 5, 0),
(138, 23, 6, 0),
(139, 24, 1, 1),
(140, 24, 2, 1),
(141, 24, 3, 1),
(142, 24, 4, 0),
(143, 24, 5, 0),
(144, 24, 6, 0),
(145, 25, 1, 1),
(146, 25, 2, 1),
(147, 25, 3, 1),
(148, 25, 4, 0),
(149, 25, 5, 0),
(150, 25, 6, 0),
(151, 26, 1, 1),
(152, 26, 2, 1),
(153, 26, 3, 1),
(154, 26, 4, 0),
(155, 26, 5, 0),
(156, 26, 6, 0),
(157, 27, 1, 1),
(158, 27, 2, 1),
(159, 27, 3, 1),
(160, 27, 4, 0),
(161, 27, 5, 0),
(162, 27, 6, 0),
(163, 28, 1, 1),
(164, 28, 2, 0),
(165, 28, 3, 0),
(166, 28, 4, 0),
(167, 28, 5, 0),
(168, 28, 6, 0),
(169, 1, 7, 0),
(170, 2, 7, 0),
(171, 3, 7, 0),
(172, 4, 7, 0),
(173, 5, 7, 0),
(174, 6, 7, 0),
(175, 7, 7, 0),
(176, 8, 7, 0),
(177, 9, 7, 0),
(178, 10, 7, 0),
(179, 11, 7, 0),
(180, 12, 7, 0),
(181, 13, 7, 0),
(182, 14, 7, 0),
(183, 15, 7, 0),
(184, 16, 7, 0),
(185, 17, 7, 0),
(186, 18, 7, 0),
(187, 19, 7, 0),
(188, 20, 7, 0),
(189, 21, 7, 0),
(190, 22, 7, 0),
(191, 23, 7, 0),
(192, 24, 7, 0),
(193, 25, 7, 0),
(194, 26, 7, 0),
(195, 27, 7, 0),
(196, 28, 7, 0),
(197, 29, 1, 1),
(198, 29, 2, 1),
(199, 29, 3, 1),
(200, 29, 4, 1),
(201, 29, 5, 0),
(202, 29, 6, 0),
(203, 29, 7, 0),
(204, 30, 1, 1),
(205, 30, 2, 1),
(206, 30, 3, 0),
(207, 30, 4, 1),
(208, 30, 5, 0),
(209, 30, 6, 0),
(210, 30, 7, 0),
(211, 31, 1, 1),
(212, 31, 2, 0),
(213, 31, 3, 0),
(214, 31, 4, 0),
(215, 31, 5, 0),
(216, 31, 6, 0),
(217, 31, 7, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblGebruikersrechten_gebruikersgroepen`
--
ALTER TABLE `tblGebruikersrechten_gebruikersgroepen`
 ADD PRIMARY KEY (`gebruikersrechten_gebruikersgroepenid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblGebruikersrechten_gebruikersgroepen`
--
ALTER TABLE `tblGebruikersrechten_gebruikersgroepen`
MODIFY `gebruikersrechten_gebruikersgroepenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=218;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
