SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `tblAannemingstak` (
`aannemingsid` int(11) NOT NULL,
  `afkorting` varchar(5) NOT NULL DEFAULT '',
  `voluit` varchar(150) NOT NULL DEFAULT '',
  `opmerkingen` text NOT NULL,
  `volgorde` mediumint(9) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=135 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblAannemingstakpercontact` (
`aannemingstakpercontact` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `aannemingsid` int(11) NOT NULL DEFAULT '0',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=5172 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblAfwezigheden` (
`afwezighedenid` int(11) NOT NULL,
  `werknemerid` int(11) NOT NULL,
  `afwezighedentype` smallint(6) NOT NULL,
  `afwezighedenbegin` int(11) NOT NULL,
  `afwezighedeneinde` int(11) NOT NULL,
  `afwezighedenjaar` int(11) NOT NULL,
  `afwezighedenopmerking` varchar(100) NOT NULL,
  `afwezighedenduur` tinyint(4) NOT NULL,
  `afwezighedenextra2` text NOT NULL,
  `afwezighedenextra3` text NOT NULL,
  `afwezighedenextra4` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=323 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblCodaheader` (
`codaheaderid` int(11) NOT NULL,
  `codaheaderdatum` int(11) NOT NULL,
  `codaheadertext` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblCodavervolg` (
`codavervolgid` int(11) NOT NULL,
  `codaheaderid` int(11) NOT NULL,
  `codavervolgtext` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblCodes` (
`codeid` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `codetype` int(11) NOT NULL,
  `omschrijving` varchar(250) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblcontacten` (
`contactid` smallint(4) NOT NULL,
  `type` varchar(200) NOT NULL DEFAULT '',
  `projectnaam` varchar(200) NOT NULL DEFAULT '',
  `klantnaam1` varchar(200) NOT NULL DEFAULT '',
  `klantnaam2` varchar(200) NOT NULL DEFAULT '',
  `naam` varchar(200) NOT NULL DEFAULT '',
  `commercielenaam` varchar(200) NOT NULL DEFAULT '',
  `bedrijfsvorm` varchar(50) NOT NULL DEFAULT '',
  `btwnummer` varchar(15) NOT NULL DEFAULT '',
  `rsznummer` varchar(16) NOT NULL DEFAULT '',
  `rekeningnummer` varchar(16) NOT NULL DEFAULT '',
  `registratienummer` varchar(10) NOT NULL DEFAULT '',
  `datumcontroleregnr` date NOT NULL DEFAULT '0000-00-00',
  `straat` varchar(200) NOT NULL DEFAULT '',
  `nr` varchar(10) NOT NULL DEFAULT '',
  `bus` varchar(10) NOT NULL DEFAULT '',
  `postcode` varchar(6) NOT NULL DEFAULT '0',
  `gemeente` varchar(200) NOT NULL DEFAULT '',
  `werfadres_straat` varchar(100) NOT NULL DEFAULT '',
  `werfadres_nr` varchar(10) NOT NULL DEFAULT '',
  `werfadres_bus` varchar(10) NOT NULL DEFAULT '',
  `werfadres_postcode` varchar(5) NOT NULL DEFAULT '',
  `werfadres_gemeente` varchar(100) NOT NULL DEFAULT '',
  `website` varchar(200) NOT NULL DEFAULT '',
  `werkkrachten` varchar(50) NOT NULL DEFAULT '',
  `prijs` varchar(50) NOT NULL DEFAULT '0',
  `kwaliteit` varchar(50) NOT NULL DEFAULT '',
  `kwaliteittechnisch` varchar(50) NOT NULL DEFAULT '',
  `kwaliteitesthetisch` varchar(50) NOT NULL DEFAULT '',
  `planning` varchar(50) NOT NULL DEFAULT '',
  `service` varchar(50) NOT NULL DEFAULT '',
  `aannemingstak` varchar(50) NOT NULL DEFAULT '',
  `bedrijfsactiviteiten` text NOT NULL,
  `specialiteit` text NOT NULL,
  `klantenstatus` varchar(50) NOT NULL DEFAULT '',
  `datum` date NOT NULL DEFAULT '0000-00-00',
  `uitgevoerdeprojecten` varchar(50) NOT NULL DEFAULT '',
  `opmerkingen` text NOT NULL,
  `aannemingsovereenkomst` tinyint(1) NOT NULL DEFAULT '0',
  `standaardprijslijst` tinyint(1) NOT NULL DEFAULT '0',
  `kmvan` varchar(5) NOT NULL DEFAULT '',
  `telefoon` varchar(20) NOT NULL DEFAULT '',
  `infotelefoon` varchar(200) NOT NULL DEFAULT '',
  `gsm` varchar(20) NOT NULL DEFAULT '',
  `infogsm` varchar(200) NOT NULL DEFAULT '',
  `fax` varchar(20) NOT NULL DEFAULT '',
  `infofax` varchar(200) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `infoemail` varchar(200) NOT NULL DEFAULT '',
  `am1` tinyint(1) NOT NULL DEFAULT '0',
  `extra1` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra1` varchar(50) NOT NULL DEFAULT '',
  `infoextra1` varchar(200) NOT NULL DEFAULT '',
  `am2` tinyint(1) NOT NULL DEFAULT '0',
  `extra2` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra2` varchar(50) NOT NULL DEFAULT '',
  `infoextra2` varchar(200) NOT NULL DEFAULT '',
  `am3` tinyint(1) NOT NULL DEFAULT '0',
  `extra3` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra3` varchar(50) NOT NULL DEFAULT '',
  `infoextra3` varchar(200) NOT NULL DEFAULT '',
  `am4` tinyint(1) NOT NULL DEFAULT '0',
  `extra4` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra4` varchar(50) NOT NULL DEFAULT '',
  `infoextra4` varchar(200) NOT NULL DEFAULT '',
  `am5` tinyint(1) NOT NULL DEFAULT '0',
  `extra5` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra5` varchar(50) NOT NULL DEFAULT '',
  `infoextra5` varchar(200) NOT NULL DEFAULT '',
  `am6` tinyint(1) NOT NULL DEFAULT '0',
  `bijlagen` varchar(100) NOT NULL DEFAULT '',
  `datumtoevoegen` bigint(20) NOT NULL,
  `datumwijzigen` bigint(20) NOT NULL,
  `bijnieuwproject` tinyint(4) NOT NULL DEFAULT '0',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0',
  `afstandtotwerf` float(5,1) NOT NULL,
  `inhoudingsplicht` tinyint(1) NOT NULL,
  `verwijderbaar` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12862 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblContacteninhoudingsplicht` (
`contacteninhoudingsplichtid` int(11) NOT NULL,
  `contacteninhoudingsplicht_contactid` int(11) NOT NULL,
  `contacteninhoudingsplicht_waarde` tinyint(4) NOT NULL,
  `contacteninhoudingsplicht_datum` bigint(20) NOT NULL,
  `contacteninhoudingsplicht_opmerking` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=572 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblContacteninstellingen` (
`contactinstellingenid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL,
  `instelling` varchar(50) NOT NULL,
  `waarde` varchar(50) NOT NULL,
  `extra1` varchar(50) NOT NULL,
  `extra2` varchar(50) NOT NULL,
  `extra3` varchar(50) NOT NULL,
  `extra4` varchar(50) NOT NULL,
  `extra5` varchar(50) NOT NULL,
  `extra6` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16665 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblContacttypes` (
`contacttypesid` int(11) NOT NULL,
  `contacttypesnaam` varchar(100) NOT NULL,
  `contacttypesnr` tinyint(4) NOT NULL,
  `contacttypessubnr` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblFactuur` (
`factuurid` int(11) NOT NULL,
  `factuurnummer` varchar(30) NOT NULL DEFAULT '',
  `klant` int(11) NOT NULL DEFAULT '0',
  `factuurdatum` bigint(20) NOT NULL DEFAULT '0',
  `vervaldag` bigint(20) NOT NULL DEFAULT '0',
  `betalingontv` date NOT NULL DEFAULT '0000-00-00',
  `factuurbedrag` float(10,2) NOT NULL DEFAULT '0.00',
  `btwpercentage` tinyint(4) NOT NULL DEFAULT '4',
  `btwattest` tinyint(4) NOT NULL DEFAULT '0',
  `fiscaalattest` tinyint(4) NOT NULL DEFAULT '0',
  `opmerking` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=678 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblFactuur_zoekopties` (
`zoekoptieid` int(11) NOT NULL,
  `zoekoptie` varchar(255) NOT NULL,
  `keuzelijst` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblFilialen` (
`filiaalid` smallint(4) NOT NULL,
  `contactid` smallint(4) NOT NULL DEFAULT '0',
  `commercielenaam` varchar(200) NOT NULL DEFAULT '',
  `straat` varchar(50) NOT NULL DEFAULT '',
  `nr` varchar(10) NOT NULL DEFAULT '',
  `bus` varchar(10) NOT NULL DEFAULT '',
  `postcode` varchar(6) NOT NULL DEFAULT '',
  `gemeente` varchar(50) NOT NULL DEFAULT '',
  `opmerkingen` text NOT NULL,
  `telefoon` varchar(20) NOT NULL DEFAULT '',
  `infotelefoon` varchar(100) NOT NULL DEFAULT '',
  `gsm` varchar(20) NOT NULL DEFAULT '',
  `infogsm` varchar(100) NOT NULL DEFAULT '',
  `fax` varchar(20) NOT NULL DEFAULT '',
  `infofax` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `infoemail` varchar(100) NOT NULL DEFAULT '',
  `am1` tinyint(1) NOT NULL DEFAULT '0',
  `extra1` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra1` varchar(50) NOT NULL DEFAULT '',
  `infoextra1` varchar(100) NOT NULL DEFAULT '',
  `am2` tinyint(1) NOT NULL DEFAULT '0',
  `extra2` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra2` varchar(50) NOT NULL DEFAULT '',
  `infoextra2` varchar(100) NOT NULL DEFAULT '',
  `am3` tinyint(1) NOT NULL DEFAULT '0',
  `extra3` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra3` varchar(50) NOT NULL DEFAULT '',
  `infoextra3` varchar(100) NOT NULL DEFAULT '',
  `am4` tinyint(1) NOT NULL DEFAULT '0',
  `extra4` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra4` varchar(50) NOT NULL DEFAULT '',
  `infoextra4` varchar(100) NOT NULL DEFAULT '',
  `am5` tinyint(1) NOT NULL DEFAULT '0',
  `extra5` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra5` varchar(50) NOT NULL DEFAULT '',
  `infoextra5` varchar(100) NOT NULL DEFAULT '',
  `am6` tinyint(1) NOT NULL DEFAULT '0',
  `datumtoevoegen` date NOT NULL DEFAULT '0000-00-00',
  `datumwijzigen` date NOT NULL DEFAULT '0000-00-00',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=9071 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblGebruikers` (
`gebruikerid` smallint(4) NOT NULL,
  `naam` varchar(40) NOT NULL DEFAULT '',
  `voornaam` varchar(40) NOT NULL DEFAULT '',
  `gebruikersnaam` varchar(40) NOT NULL DEFAULT '',
  `paswoord` varchar(100) NOT NULL DEFAULT '',
  `rechten` tinyint(4) NOT NULL,
  `specialerechten` smallint(6) NOT NULL,
  `switchview` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblGebruikersgroepen` (
`gebruikersgroepenid` tinyint(4) NOT NULL,
  `gebruikersgroepennaam` varchar(255) NOT NULL,
  `gebruikersgroepenvolgorde` tinyint(4) NOT NULL,
  `gebruikersgroepenomschrijving` varchar(255) NOT NULL,
  `gebruikersgroepenextra` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblGebruikersrechten` (
`gebruikersrechtenid` int(11) NOT NULL,
  `gebruikersrechtenhoofdgroep` smallint(6) NOT NULL,
  `gebruikersrechtensubgroep` smallint(6) NOT NULL,
  `gebruikersrechtenvolgordebinnensubgroep` smallint(6) NOT NULL,
  `gebruikersrechtenitem` varchar(255) NOT NULL,
  `gebruikersrechtenomschrijving` text NOT NULL,
  `actief` tinyint(1) NOT NULL,
  `gebruikersrechtenextra` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblGebruikersrechten_gebruikersgroepen` (
`gebruikersrechten_gebruikersgroepenid` int(11) NOT NULL,
  `gebruikersrechten_gebruikersgroepen_gebruikersrechtenid` smallint(6) NOT NULL,
  `gebruikersrechten_gebruikersgroepen_gebruikersgroepenid` smallint(6) NOT NULL,
  `gebruikersrechten_gebruikersgroepensetting` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblGeuploadefiles` (
`fileid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `pad` varchar(100) NOT NULL DEFAULT '',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturen` (
`factuurid` int(11) NOT NULL,
  `volgnrjaar` int(11) NOT NULL,
  `volgnrmaand` int(11) NOT NULL,
  `volgnrnummer` int(15) NOT NULL DEFAULT '0',
  `factuurnummer` varchar(30) NOT NULL DEFAULT '',
  `afzender` int(11) NOT NULL DEFAULT '0',
  `factuurdatum` bigint(20) NOT NULL DEFAULT '0',
  `betalingstermijn` bigint(20) NOT NULL,
  `vervaldag` bigint(20) NOT NULL DEFAULT '0',
  `betalinggedaan` date NOT NULL DEFAULT '0000-00-00',
  `factuurbedrag` float(10,2) NOT NULL DEFAULT '0.00',
  `btwpercentage` tinyint(4) NOT NULL DEFAULT '0',
  `controle` tinyint(4) NOT NULL DEFAULT '0',
  `kortingvoorbtw` float(10,2) NOT NULL DEFAULT '0.00',
  `kortingnabtw` varchar(10) NOT NULL DEFAULT '0',
  `tax` float(10,2) NOT NULL DEFAULT '0.00',
  `opmerking` text NOT NULL,
  `betaaltermijn` int(11) NOT NULL DEFAULT '0',
  `vervaldagbepaald` bigint(20) NOT NULL DEFAULT '0',
  `factuurbedragincl` float(10,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=16492 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenandere` (
`inkomendefacturenandereid` int(11) NOT NULL,
  `inkomendefacturenid` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `subtype1` int(11) NOT NULL,
  `subtype2` int(11) NOT NULL,
  `factuuranderebedrag` float(10,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5414 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenandere_hoofdtype` (
`hoofdtypeid` int(11) NOT NULL,
  `hoofdtypenr` varchar(20) NOT NULL,
  `hoofdtypenaam` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenandere_subtype1` (
`subtype1id` int(11) NOT NULL,
  `hoofdtypeid` int(11) NOT NULL,
  `subtype1nr` varchar(20) NOT NULL,
  `subtype1naam` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenandere_subtype2` (
`subtype2id` int(11) NOT NULL,
  `subtype1id` int(11) NOT NULL,
  `subtype2nr` varchar(20) NOT NULL,
  `subtype2naam` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenatnaam` (
`inkomendefacturenatnaamid` int(11) NOT NULL,
  `inkomendefacturenwerfnaamid` int(11) NOT NULL,
  `atid` int(11) NOT NULL,
  `factuuratbedrag` float(10,2) NOT NULL,
  `extra1` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13362 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenbetalingen` (
`inkomendefacturenbetalingenid` int(11) NOT NULL,
  `inkomendefacturenid` int(11) NOT NULL,
  `bedrag` float(10,2) NOT NULL,
  `betaalwijze` tinyint(1) NOT NULL,
  `datum` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14778 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturendubbels` (
`inkomendefacturendubbelsid` int(11) NOT NULL,
  `inkomendefacturenid` int(11) NOT NULL,
  `inkomendefactureniddubbels` int(11) NOT NULL,
  `inkomendefacturendubbelsdatumtoevoegen` bigint(11) NOT NULL,
  `inkomendefacturendubbelstype` tinyint(4) NOT NULL,
  `inkomendefacturendubbelsjaarorigineel` varchar(10) NOT NULL,
  `inkomendefacturendubbelsmaandorigineel` varchar(10) NOT NULL,
  `inkomendefacturendubbelsvolgnrorigineel` varchar(10) NOT NULL,
  `inkomendefacturendubbelsfactuurnr` varchar(50) NOT NULL,
  `inkomendefacturendubbelsafzender` int(11) NOT NULL,
  `inkomendefacturendubbelsfactuurdatum` bigint(11) NOT NULL,
  `inkomendefacturendubbelsfactuurbedrag` float(10,2) NOT NULL,
  `inkomendefacturendubbelsfactuurbedragincl` float(10,2) NOT NULL,
  `inkomendefacturendubbelsbtwtarief` tinyint(4) NOT NULL,
  `inkomendefacturendubbelsomcontrolegetallen` text NOT NULL,
  `inkomendefacturendubbelsgoedgekeurd` varchar(10) NOT NULL,
  `inkomendefacturendubbelsreden` text NOT NULL,
  `inkomendefacturendubbelsomids` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=67209 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturendubbelscontrole` (
`inkomendefacturendubbelscontroleid` int(11) NOT NULL,
  `inkomendefacturendubbelscontrolefactuurid` int(11) NOT NULL,
  `inkomendefacturendubbelscontrolegetal` int(11) NOT NULL,
  `inkomendefacturendubbelscontrolesomids` int(11) NOT NULL,
  `extra2` varchar(50) NOT NULL,
  `extra3` varchar(50) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenfactuurbedragen` (
`inkomendefacturenfactuurbedragenid` int(11) NOT NULL,
  `factuurbedrag` float(10,2) NOT NULL DEFAULT '0.00',
  `btw` smallint(6) NOT NULL,
  `inkomendefacturenid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2107 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInkomendefacturenwerfnaam` (
`inkomendefacturenwerfnaamid` int(11) NOT NULL,
  `inkomendefacturenid` int(11) NOT NULL,
  `contactidwerfnaam` int(11) NOT NULL,
  `controle` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=13223 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblInstellingen` (
`Instellingid` smallint(6) NOT NULL,
  `begin` int(11) NOT NULL,
  `einde` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblLaatsteupdate` (
`laatsteupdateid` int(11) NOT NULL,
  `laatsteupdatepagina` smallint(6) NOT NULL,
  `tijd` int(11) NOT NULL,
  `paginaomschrijving` varchar(255) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=31122 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblLogintijd` (
`logintijdid` int(11) NOT NULL,
  `logintijdhashid` varchar(100) NOT NULL,
  `logintijdeindtijd` int(11) NOT NULL,
  `logintijdlogin` int(11) NOT NULL,
  `logintijdlogintijd` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10792 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblNacalc` (
`nacalcid` int(11) NOT NULL,
  `projectnaamid` int(11) NOT NULL,
  `datumnacalc` int(11) NOT NULL,
  `totaalbestelling` float(10,2) NOT NULL,
  `totaaluitgevoerd` float(10,2) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3404 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblNacalcfastload` (
  `nacalcid` int(11) NOT NULL,
  `projectnaamid` int(11) NOT NULL,
  `projectnaam` varchar(150) NOT NULL,
  `datumnacalc` int(11) NOT NULL,
  `totaalbestelling` float(10,2) NOT NULL,
  `totaaluitgevoerd` float(10,2) NOT NULL,
  `datumlaatstefactuur` int(11) NOT NULL,
  `somallefacturen` float(10,2) NOT NULL,
  `totaaluitgavefacturen` float(10,2) NOT NULL,
  `nettowerknemerskost` float(10,2) NOT NULL,
  `brutowerknemerskost` float(10,2) NOT NULL,
  `totaalinclnettowerknemerskost` float(10,2) NOT NULL,
  `totaalinclbrutowerknemerskost` float(10,2) NOT NULL,
  `marge` float(3,2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblPostnummers` (
`postnummerid` mediumint(9) NOT NULL,
  `postnummer` varchar(4) NOT NULL DEFAULT '',
  `gemeente` varchar(200) NOT NULL DEFAULT '',
  `geheel` varchar(200) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=2906 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblProjecten` (
`projectid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `opstartdatum` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblProjectleiderperwerf` (
`projectleiderperwerfid` int(11) NOT NULL,
  `werfbeheerid` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0 voor projectleiders, 1 voor calculatoren',
  `projectleiderid` int(11) NOT NULL,
  `procent` tinyint(4) NOT NULL COMMENT 'geeft percentage aan dat deze voor project heeft gewerkt',
  `functie` varchar(50) NOT NULL DEFAULT '1',
  `opmerking` varchar(250) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2352 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblProjectleiders` (
`projectleiderid` mediumint(9) NOT NULL,
  `voornaam` varchar(50) NOT NULL DEFAULT '',
  `naam` varchar(100) NOT NULL DEFAULT '',
  `gsm` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `fax` varchar(50) NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL,
  `opmerking` varchar(250) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblProjectleidertypes` (
`projectleidertypeid` smallint(6) NOT NULL,
  `projectleiderid` smallint(6) NOT NULL,
  `typenaam` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSetting_begineinde` (
`begineindeid` smallint(6) NOT NULL,
  `gebruikerid` int(11) NOT NULL,
  `begin` int(11) NOT NULL,
  `einde` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3614 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSetting_breedtecontacten` (
`breedtecontactenid` smallint(6) NOT NULL,
  `gebruikerid` smallint(6) NOT NULL,
  `breedte` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSetting_grootteplanning` (
`grootteschermid` smallint(6) NOT NULL,
  `gebruikerid` smallint(6) NOT NULL,
  `grootte` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSetting_planningentonen` (
`planningtonenid` int(11) NOT NULL,
  `gebruikerid` int(11) NOT NULL,
  `tetonencontactid` int(11) NOT NULL,
  `tetonencontacttype` int(11) NOT NULL,
  `groep` int(11) NOT NULL,
  `typeweergave` varchar(10) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8828 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSetting_rijenweergeven` (
`rijenweergevenid` int(11) NOT NULL,
  `gebruikerid` int(11) NOT NULL,
  `medewerkerklant` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22030 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblSpecifiekecontactgegevens` (
`contactgegevenid` smallint(6) NOT NULL,
  `contactid` smallint(4) NOT NULL DEFAULT '0',
  `filiaalcontact` tinyint(1) NOT NULL DEFAULT '0',
  `voornaam2` varchar(50) NOT NULL DEFAULT '',
  `infovoornaam2` varchar(200) NOT NULL DEFAULT '',
  `naam2` varchar(50) NOT NULL DEFAULT '',
  `infonaam2` varchar(200) NOT NULL DEFAULT '',
  `functie2` varchar(50) NOT NULL DEFAULT '',
  `infofunctie2` varchar(200) NOT NULL DEFAULT '',
  `telefoon2` varchar(20) NOT NULL DEFAULT '',
  `infotelefoon2` varchar(200) NOT NULL DEFAULT '',
  `gsm2` varchar(20) NOT NULL DEFAULT '',
  `infogsm2` varchar(200) NOT NULL DEFAULT '',
  `fax2` varchar(20) NOT NULL DEFAULT '',
  `infofax2` varchar(200) NOT NULL DEFAULT '',
  `email2` varchar(50) NOT NULL DEFAULT '',
  `infoemail2` varchar(200) NOT NULL DEFAULT '',
  `am12` tinyint(1) NOT NULL DEFAULT '0',
  `extra12` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra12` varchar(50) NOT NULL DEFAULT '',
  `infoextra12` varchar(200) NOT NULL DEFAULT '',
  `am22` tinyint(1) NOT NULL DEFAULT '0',
  `extra22` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra22` varchar(50) NOT NULL DEFAULT '',
  `infoextra22` varchar(200) NOT NULL DEFAULT '',
  `am32` tinyint(1) NOT NULL DEFAULT '0',
  `extra32` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra32` varchar(50) NOT NULL DEFAULT '',
  `infoextra32` varchar(200) NOT NULL DEFAULT '',
  `am42` tinyint(1) NOT NULL DEFAULT '0',
  `extra42` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra42` varchar(50) NOT NULL DEFAULT '',
  `infoextra42` varchar(200) NOT NULL DEFAULT '',
  `am52` tinyint(1) NOT NULL DEFAULT '0',
  `extra52` varchar(50) NOT NULL DEFAULT '',
  `gegevenextra52` varchar(50) NOT NULL DEFAULT '',
  `infoextra52` varchar(200) NOT NULL DEFAULT '',
  `am62` tinyint(1) NOT NULL DEFAULT '0',
  `verwijderd2` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=493 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblStatistics` (
`statisticsid` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `gebruiktepagina` int(11) NOT NULL,
  `gebruikteoptie` int(11) NOT NULL,
  `extra1` int(11) NOT NULL,
  `extra2` varchar(100) NOT NULL,
  `extra3` int(11) NOT NULL,
  `extra4` int(11) NOT NULL,
  `extra5` int(11) NOT NULL,
  `extra6` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=28695 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamleden` (
`teamlidid` int(11) NOT NULL,
  `teamlidvoornaam` varchar(30) NOT NULL,
  `teamlidnaam` varchar(50) NOT NULL,
  `teamlidstraatnr` varchar(50) NOT NULL,
  `teamlidpostnr` varchar(10) NOT NULL,
  `teamlidgemeente` varchar(80) NOT NULL,
  `teamlidindienst` date NOT NULL,
  `teamliduitdienst` date NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamledenperwerfperdag` (
`teamledenperwerfperdagid` int(11) NOT NULL,
  `werfdetailledenperwerfid` int(11) NOT NULL,
  `teamledenperwerfperdagdatum` int(11) NOT NULL,
  `teamledenperwerfperdagurenauto` float(3,2) NOT NULL,
  `teamledenperwerfperdagurenhandm` float(3,2) NOT NULL,
  `teamledenperwerfperdagcouren` float(4,2) NOT NULL,
  `teamledenperwerfperdagopmerking` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7222 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamledenperwerfperdag2` (
`teamledenperwerfperdagid` int(11) NOT NULL,
  `teamledenperwerfperdagteamlid` int(11) NOT NULL,
  `teamledenperwerfperdagdag` int(11) NOT NULL,
  `teamledenperwerfperdagproject` int(11) NOT NULL,
  `teamledenperwerfperdaguren` float(10,2) NOT NULL,
  `teamledenperwerfperdagcouren` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamledenperwerfperdagbackup` (
`teamledenperwerfperdagid` int(11) NOT NULL,
  `werfdetailledenperwerfid` int(11) NOT NULL,
  `teamledenperwerfperdagdatum` int(11) NOT NULL,
  `teamledenperwerfperdagurenauto` float(3,2) NOT NULL,
  `teamledenperwerfperdagurenhandm` float(3,2) NOT NULL,
  `teamledenperwerfperdagcouren` float(4,2) NOT NULL,
  `teamledenperwerfperdagopmerking` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9953 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamperiode` (
`teamperiodeid` int(11) NOT NULL,
  `teamid` int(11) NOT NULL,
  `teamperiodestart` int(11) NOT NULL DEFAULT '0',
  `teamperiodeeinde` int(11) NOT NULL DEFAULT '0',
  `teamperiodeopmerking` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=495 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeamperiodeperlid` (
`teamperiodeperlidid` int(11) NOT NULL,
  `teamperiodeid` int(11) NOT NULL,
  `teamlidid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=430 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTeams` (
`teamid` int(11) NOT NULL,
  `teamgroepid` int(11) NOT NULL COMMENT 'verwijssleutel contactid type 20',
  `teamafkorting` varchar(30) NOT NULL,
  `teamvoluit` varchar(50) NOT NULL,
  `teamopmerking` text NOT NULL,
  `actief` tinyint(1) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tbltest` (
`id` smallint(6) NOT NULL,
  `dag` varchar(30) NOT NULL DEFAULT '',
  `dagnr` varchar(30) NOT NULL DEFAULT '',
  `a` varchar(30) NOT NULL DEFAULT '',
  `aa` varchar(30) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=784 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTijdstip` (
`tijdstipid` int(11) NOT NULL,
  `script` tinyint(4) NOT NULL,
  `tijdstiptijd` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30794 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTypes` (
  `typeid` int(11) NOT NULL DEFAULT '0',
  `nr` int(11) NOT NULL DEFAULT '0',
  `naam` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblTypespercontact` (
`typespercontactid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `typeid` int(11) NOT NULL DEFAULT '0',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=357 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblUitgaandefacturen` (
`factuurid` int(11) NOT NULL,
  `factuurnummer` varchar(30) NOT NULL DEFAULT '',
  `klant` int(11) NOT NULL DEFAULT '0',
  `factuurdatum` bigint(20) NOT NULL DEFAULT '0',
  `vervaldag` bigint(20) NOT NULL DEFAULT '0',
  `betalingontv` bigint(20) NOT NULL,
  `factuurbedrag` float(10,2) NOT NULL DEFAULT '0.00',
  `btwpercentage` tinyint(4) NOT NULL DEFAULT '4',
  `btwattest` tinyint(4) NOT NULL DEFAULT '0',
  `fiscaalattest` tinyint(4) NOT NULL DEFAULT '0',
  `opmerking` text NOT NULL,
  `factuurblokkeren` tinyint(1) NOT NULL,
  `betalingskorting` tinyint(4) NOT NULL,
  `betalingskortingbedrag` float(10,2) NOT NULL,
  `soortfactuur` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5086 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblUitgaandefacturen2014` (
`factuurid` int(11) NOT NULL,
  `factuurnummer` varchar(30) NOT NULL DEFAULT '',
  `klant` int(11) NOT NULL DEFAULT '0',
  `factuurdatum` bigint(20) NOT NULL DEFAULT '0',
  `vervaldag` bigint(20) NOT NULL DEFAULT '0',
  `betalingontv` bigint(20) NOT NULL,
  `factuurbedrag` float(10,2) NOT NULL DEFAULT '0.00',
  `btwpercentage` tinyint(4) NOT NULL DEFAULT '4',
  `btwattest` tinyint(4) NOT NULL DEFAULT '0',
  `fiscaalattest` tinyint(4) NOT NULL DEFAULT '0',
  `opmerking` text NOT NULL,
  `factuurblokkeren` tinyint(1) NOT NULL,
  `betalingskorting` tinyint(4) NOT NULL,
  `betalingskortingbedrag` float(10,2) NOT NULL,
  `soortfactuur` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3208 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblUitgaandefacturenbetalingen` (
`uitgaandefacturenbetalingenid` int(11) NOT NULL,
  `uitgaandefacturenid` int(11) NOT NULL,
  `bedrag` float(10,2) NOT NULL,
  `datumbetaling` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5543 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblUitgaandefacturen_zoekopties` (
`zoekoptieid` int(11) NOT NULL,
  `zoekoptie` varchar(255) NOT NULL,
  `keuzelijst` tinyint(4) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblVerlof` (
`verlofid` int(11) NOT NULL,
  `verloftype` smallint(6) NOT NULL,
  `begin` int(11) NOT NULL,
  `einde` int(11) NOT NULL,
  `jaar` int(11) NOT NULL,
  `naam` varchar(100) NOT NULL,
  `tekst1` text NOT NULL,
  `tekst2` text NOT NULL,
  `tekst3` text NOT NULL,
  `tekst4` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22985 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfbeheer` (
`werfbeheerid` mediumint(9) NOT NULL,
  `contactid` mediumint(9) NOT NULL DEFAULT '0',
  `gebruikersnaam` varchar(50) NOT NULL DEFAULT '',
  `paswoord` varchar(30) NOT NULL DEFAULT '',
  `rolsfinx` smallint(6) NOT NULL DEFAULT '0',
  `projectleider` smallint(6) NOT NULL DEFAULT '0',
  `bedrageersteofferte` float(10,2) NOT NULL DEFAULT '0.00',
  `datumeersteofferte` bigint(20) NOT NULL,
  `bedragvoorgelegdaanklant` float(10,2) NOT NULL,
  `datumvoorgelegdaanklant` bigint(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1386 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailandere` (
`werfdetailandereid` int(11) NOT NULL,
  `werfbeheerid` int(11) NOT NULL DEFAULT '0',
  `type` smallint(4) NOT NULL DEFAULT '0',
  `medewerkerid` int(11) NOT NULL DEFAULT '0',
  `besteldext` tinyint(4) NOT NULL DEFAULT '0',
  `tebestellen` tinyint(4) NOT NULL DEFAULT '0',
  `zichtbaar` tinyint(4) NOT NULL DEFAULT '0',
  `opmerkingen` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=387 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailat` (
`werfdetailatid` int(11) NOT NULL,
  `werfbeheerid` int(11) NOT NULL DEFAULT '0',
  `type` smallint(4) NOT NULL DEFAULT '0',
  `aannemingstak` mediumint(9) NOT NULL DEFAULT '0',
  `besteldat` tinyint(9) NOT NULL DEFAULT '0',
  `medewerkerid` int(11) NOT NULL DEFAULT '0',
  `besteldmed` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=6930 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailledenperwerf` (
`werfdetailledenperwerfid` int(11) NOT NULL,
  `werfdetailteamsperwerfid` int(11) NOT NULL,
  `werfdetailledenperwerfteamlidid` int(11) NOT NULL,
  `werfdetailledenperwerftijdstipdag` tinyint(1) NOT NULL,
  `werfdetailledenperwerfdatumbegin` int(11) NOT NULL,
  `werfdetailledenperwerfdatumeinde` int(11) NOT NULL,
  `werfdetailledenperwerfvanafhier` tinyint(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=24034 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailoffertedata` (
`werfdetailoffertedataid` mediumint(9) NOT NULL,
  `werfdetailatid` int(9) NOT NULL DEFAULT '0',
  `aangevraagddatum` bigint(20) NOT NULL DEFAULT '0',
  `gevraagdklaardatum` bigint(20) NOT NULL DEFAULT '0',
  `ontvangendatum` bigint(20) NOT NULL DEFAULT '0',
  `opmerkingen` text NOT NULL,
  `besteld` tinyint(9) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=7960 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailplanningdata` (
`werfdetailplanningdataid` mediumint(9) NOT NULL,
  `werfdetailatid` mediumint(9) NOT NULL DEFAULT '0',
  `besteld` bigint(20) NOT NULL DEFAULT '0',
  `begin` bigint(20) NOT NULL DEFAULT '0',
  `einde` bigint(20) NOT NULL DEFAULT '0',
  `teamid` int(11) NOT NULL DEFAULT '0',
  `opmerkingvoorklant` text CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `opmerkingvooraannemer` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=22831 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailplanningopmerkingen` (
`werfdetailplanningopmerkingenid` int(11) NOT NULL,
  `werfdetailplanningdataid` int(11) NOT NULL,
  `datum` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  `werfdetailplanningopmerkingenvoorklanten` text NOT NULL,
  `werfdetailplanningopmerkingenvooraannemers` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=1033 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerfdetailteamsperwerf` (
`werfdetailteamsperwerfid` int(11) NOT NULL,
  `werfdetailplanningdataid` int(11) NOT NULL,
  `werfdetailteamsperwerfteamid` int(11) NOT NULL,
  `werfdetailteamsperwerfteamvolledig` tinyint(1) NOT NULL,
  `werfdetailteamsperwerfteamlidid` int(11) NOT NULL,
  `werfdetailteamperwerfvanafhier` tinyint(1) NOT NULL,
  `werfdetailteamsperwerfbegindatum` int(11) NOT NULL,
  `werfdetailteamsperwerfeinddatum` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=29610 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerknemerevaluatie` (
`evaluatieid` int(11) NOT NULL,
  `teamledenperwerfperdagid` int(11) NOT NULL,
  `teamlidid` int(11) NOT NULL,
  `maand` tinyint(4) NOT NULL,
  `evaluatiescore` float(3,1) NOT NULL,
  `evaluatietekst` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=765 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerknemerskosten` (
`werknemerskostenid` int(11) NOT NULL,
  `werknemerid` int(11) NOT NULL,
  `nettokostprijs` float(4,2) NOT NULL,
  `kostmagazijn` float(4,2) NOT NULL,
  `procent` float(3,2) NOT NULL DEFAULT '0.06'
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerknemersperploeg` (
`werknemersperploegid` smallint(6) NOT NULL,
  `contactid` int(11) NOT NULL,
  `contactgegevenid` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9009 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerkregio` (
`werkregioid` int(11) NOT NULL,
  `regio` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `tblWerkregiopercontact` (
`werkregiopercontactid` int(11) NOT NULL,
  `contactid` int(11) NOT NULL DEFAULT '0',
  `werkregioid` int(11) NOT NULL DEFAULT '0',
  `verwijderd` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=1790 DEFAULT CHARSET=latin1;


ALTER TABLE `tblAannemingstak`
 ADD PRIMARY KEY (`aannemingsid`);

ALTER TABLE `tblAannemingstakpercontact`
 ADD PRIMARY KEY (`aannemingstakpercontact`);

ALTER TABLE `tblAfwezigheden`
 ADD PRIMARY KEY (`afwezighedenid`);

ALTER TABLE `tblCodaheader`
 ADD PRIMARY KEY (`codaheaderid`);

ALTER TABLE `tblCodavervolg`
 ADD PRIMARY KEY (`codavervolgid`);

ALTER TABLE `tblCodes`
 ADD PRIMARY KEY (`codeid`);

ALTER TABLE `tblcontacten`
 ADD PRIMARY KEY (`contactid`);

ALTER TABLE `tblContacteninhoudingsplicht`
 ADD PRIMARY KEY (`contacteninhoudingsplichtid`);

ALTER TABLE `tblContacteninstellingen`
 ADD PRIMARY KEY (`contactinstellingenid`);

ALTER TABLE `tblContacttypes`
 ADD PRIMARY KEY (`contacttypesid`);

ALTER TABLE `tblFactuur`
 ADD PRIMARY KEY (`factuurid`);

ALTER TABLE `tblFactuur_zoekopties`
 ADD PRIMARY KEY (`zoekoptieid`);

ALTER TABLE `tblFilialen`
 ADD PRIMARY KEY (`filiaalid`);

ALTER TABLE `tblGebruikers`
 ADD PRIMARY KEY (`gebruikerid`);

ALTER TABLE `tblGebruikersgroepen`
 ADD PRIMARY KEY (`gebruikersgroepenid`);

ALTER TABLE `tblGebruikersrechten`
 ADD PRIMARY KEY (`gebruikersrechtenid`);

ALTER TABLE `tblGebruikersrechten_gebruikersgroepen`
 ADD PRIMARY KEY (`gebruikersrechten_gebruikersgroepenid`);

ALTER TABLE `tblGeuploadefiles`
 ADD PRIMARY KEY (`fileid`);

ALTER TABLE `tblInkomendefacturen`
 ADD PRIMARY KEY (`factuurid`);

ALTER TABLE `tblInkomendefacturenandere`
 ADD PRIMARY KEY (`inkomendefacturenandereid`);

ALTER TABLE `tblInkomendefacturenandere_hoofdtype`
 ADD PRIMARY KEY (`hoofdtypeid`);

ALTER TABLE `tblInkomendefacturenandere_subtype1`
 ADD PRIMARY KEY (`subtype1id`);

ALTER TABLE `tblInkomendefacturenandere_subtype2`
 ADD PRIMARY KEY (`subtype2id`);

ALTER TABLE `tblInkomendefacturenatnaam`
 ADD PRIMARY KEY (`inkomendefacturenatnaamid`);

ALTER TABLE `tblInkomendefacturenbetalingen`
 ADD PRIMARY KEY (`inkomendefacturenbetalingenid`);

ALTER TABLE `tblInkomendefacturendubbels`
 ADD PRIMARY KEY (`inkomendefacturendubbelsid`);

ALTER TABLE `tblInkomendefacturendubbelscontrole`
 ADD PRIMARY KEY (`inkomendefacturendubbelscontroleid`);

ALTER TABLE `tblInkomendefacturenfactuurbedragen`
 ADD PRIMARY KEY (`inkomendefacturenfactuurbedragenid`);

ALTER TABLE `tblInkomendefacturenwerfnaam`
 ADD PRIMARY KEY (`inkomendefacturenwerfnaamid`);

ALTER TABLE `tblInstellingen`
 ADD PRIMARY KEY (`Instellingid`);

ALTER TABLE `tblLaatsteupdate`
 ADD PRIMARY KEY (`laatsteupdateid`);

ALTER TABLE `tblLogintijd`
 ADD PRIMARY KEY (`logintijdid`);

ALTER TABLE `tblNacalc`
 ADD PRIMARY KEY (`nacalcid`);

ALTER TABLE `tblNacalcfastload`
 ADD PRIMARY KEY (`projectnaamid`);

ALTER TABLE `tblPostnummers`
 ADD PRIMARY KEY (`postnummerid`);

ALTER TABLE `tblProjecten`
 ADD PRIMARY KEY (`projectid`);

ALTER TABLE `tblProjectleiderperwerf`
 ADD PRIMARY KEY (`projectleiderperwerfid`);

ALTER TABLE `tblProjectleiders`
 ADD PRIMARY KEY (`projectleiderid`);

ALTER TABLE `tblProjectleidertypes`
 ADD PRIMARY KEY (`projectleidertypeid`);

ALTER TABLE `tblSetting_begineinde`
 ADD PRIMARY KEY (`begineindeid`);

ALTER TABLE `tblSetting_breedtecontacten`
 ADD PRIMARY KEY (`breedtecontactenid`);

ALTER TABLE `tblSetting_grootteplanning`
 ADD PRIMARY KEY (`grootteschermid`);

ALTER TABLE `tblSetting_planningentonen`
 ADD PRIMARY KEY (`planningtonenid`);

ALTER TABLE `tblSetting_rijenweergeven`
 ADD PRIMARY KEY (`rijenweergevenid`);

ALTER TABLE `tblSpecifiekecontactgegevens`
 ADD PRIMARY KEY (`contactgegevenid`);

ALTER TABLE `tblStatistics`
 ADD PRIMARY KEY (`statisticsid`);

ALTER TABLE `tblTeamleden`
 ADD PRIMARY KEY (`teamlidid`);

ALTER TABLE `tblTeamledenperwerfperdag`
 ADD PRIMARY KEY (`teamledenperwerfperdagid`);

ALTER TABLE `tblTeamledenperwerfperdag2`
 ADD PRIMARY KEY (`teamledenperwerfperdagid`);

ALTER TABLE `tblTeamledenperwerfperdagbackup`
 ADD PRIMARY KEY (`teamledenperwerfperdagid`);

ALTER TABLE `tblTeamperiode`
 ADD PRIMARY KEY (`teamperiodeid`);

ALTER TABLE `tblTeamperiodeperlid`
 ADD PRIMARY KEY (`teamperiodeperlidid`);

ALTER TABLE `tblTeams`
 ADD PRIMARY KEY (`teamid`);

ALTER TABLE `tbltest`
 ADD PRIMARY KEY (`id`);

ALTER TABLE `tblTijdstip`
 ADD PRIMARY KEY (`tijdstipid`);

ALTER TABLE `tblTypes`
 ADD PRIMARY KEY (`typeid`);

ALTER TABLE `tblTypespercontact`
 ADD PRIMARY KEY (`typespercontactid`);

ALTER TABLE `tblUitgaandefacturen`
 ADD PRIMARY KEY (`factuurid`);

ALTER TABLE `tblUitgaandefacturen2014`
 ADD PRIMARY KEY (`factuurid`);

ALTER TABLE `tblUitgaandefacturenbetalingen`
 ADD PRIMARY KEY (`uitgaandefacturenbetalingenid`);

ALTER TABLE `tblUitgaandefacturen_zoekopties`
 ADD PRIMARY KEY (`zoekoptieid`);

ALTER TABLE `tblVerlof`
 ADD PRIMARY KEY (`verlofid`);

ALTER TABLE `tblWerfbeheer`
 ADD PRIMARY KEY (`werfbeheerid`);

ALTER TABLE `tblWerfdetailandere`
 ADD PRIMARY KEY (`werfdetailandereid`);

ALTER TABLE `tblWerfdetailat`
 ADD PRIMARY KEY (`werfdetailatid`);

ALTER TABLE `tblWerfdetailledenperwerf`
 ADD PRIMARY KEY (`werfdetailledenperwerfid`);

ALTER TABLE `tblWerfdetailoffertedata`
 ADD PRIMARY KEY (`werfdetailoffertedataid`);

ALTER TABLE `tblWerfdetailplanningdata`
 ADD PRIMARY KEY (`werfdetailplanningdataid`);

ALTER TABLE `tblWerfdetailplanningopmerkingen`
 ADD PRIMARY KEY (`werfdetailplanningopmerkingenid`);

ALTER TABLE `tblWerfdetailteamsperwerf`
 ADD PRIMARY KEY (`werfdetailteamsperwerfid`);

ALTER TABLE `tblWerknemerevaluatie`
 ADD PRIMARY KEY (`evaluatieid`);

ALTER TABLE `tblWerknemerskosten`
 ADD PRIMARY KEY (`werknemerskostenid`);

ALTER TABLE `tblWerknemersperploeg`
 ADD PRIMARY KEY (`werknemersperploegid`);

ALTER TABLE `tblWerkregio`
 ADD PRIMARY KEY (`werkregioid`);

ALTER TABLE `tblWerkregiopercontact`
 ADD PRIMARY KEY (`werkregiopercontactid`);


ALTER TABLE `tblAannemingstak`
MODIFY `aannemingsid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=135;
ALTER TABLE `tblAannemingstakpercontact`
MODIFY `aannemingstakpercontact` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5172;
ALTER TABLE `tblAfwezigheden`
MODIFY `afwezighedenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=323;
ALTER TABLE `tblCodaheader`
MODIFY `codaheaderid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `tblCodavervolg`
MODIFY `codavervolgid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
ALTER TABLE `tblCodes`
MODIFY `codeid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `tblcontacten`
MODIFY `contactid` smallint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12862;
ALTER TABLE `tblContacteninhoudingsplicht`
MODIFY `contacteninhoudingsplichtid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=572;
ALTER TABLE `tblContacteninstellingen`
MODIFY `contactinstellingenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16665;
ALTER TABLE `tblContacttypes`
MODIFY `contacttypesid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
ALTER TABLE `tblFactuur`
MODIFY `factuurid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=678;
ALTER TABLE `tblFactuur_zoekopties`
MODIFY `zoekoptieid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `tblFilialen`
MODIFY `filiaalid` smallint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9071;
ALTER TABLE `tblGebruikers`
MODIFY `gebruikerid` smallint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
ALTER TABLE `tblGebruikersgroepen`
MODIFY `gebruikersgroepenid` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
ALTER TABLE `tblGebruikersrechten`
MODIFY `gebruikersrechtenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
ALTER TABLE `tblGebruikersrechten_gebruikersgroepen`
MODIFY `gebruikersrechten_gebruikersgroepenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=218;
ALTER TABLE `tblGeuploadefiles`
MODIFY `fileid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
ALTER TABLE `tblInkomendefacturen`
MODIFY `factuurid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16492;
ALTER TABLE `tblInkomendefacturenandere`
MODIFY `inkomendefacturenandereid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5414;
ALTER TABLE `tblInkomendefacturenandere_hoofdtype`
MODIFY `hoofdtypeid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
ALTER TABLE `tblInkomendefacturenandere_subtype1`
MODIFY `subtype1id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
ALTER TABLE `tblInkomendefacturenandere_subtype2`
MODIFY `subtype2id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
ALTER TABLE `tblInkomendefacturenatnaam`
MODIFY `inkomendefacturenatnaamid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13362;
ALTER TABLE `tblInkomendefacturenbetalingen`
MODIFY `inkomendefacturenbetalingenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14778;
ALTER TABLE `tblInkomendefacturendubbels`
MODIFY `inkomendefacturendubbelsid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67209;
ALTER TABLE `tblInkomendefacturendubbelscontrole`
MODIFY `inkomendefacturendubbelscontroleid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
ALTER TABLE `tblInkomendefacturenfactuurbedragen`
MODIFY `inkomendefacturenfactuurbedragenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2107;
ALTER TABLE `tblInkomendefacturenwerfnaam`
MODIFY `inkomendefacturenwerfnaamid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13223;
ALTER TABLE `tblInstellingen`
MODIFY `Instellingid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
ALTER TABLE `tblLaatsteupdate`
MODIFY `laatsteupdateid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31122;
ALTER TABLE `tblLogintijd`
MODIFY `logintijdid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10792;
ALTER TABLE `tblNacalc`
MODIFY `nacalcid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3404;
ALTER TABLE `tblPostnummers`
MODIFY `postnummerid` mediumint(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2906;
ALTER TABLE `tblProjecten`
MODIFY `projectid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `tblProjectleiderperwerf`
MODIFY `projectleiderperwerfid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2352;
ALTER TABLE `tblProjectleiders`
MODIFY `projectleiderid` mediumint(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
ALTER TABLE `tblProjectleidertypes`
MODIFY `projectleidertypeid` smallint(6) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tblSetting_begineinde`
MODIFY `begineindeid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3614;
ALTER TABLE `tblSetting_breedtecontacten`
MODIFY `breedtecontactenid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
ALTER TABLE `tblSetting_grootteplanning`
MODIFY `grootteschermid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
ALTER TABLE `tblSetting_planningentonen`
MODIFY `planningtonenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8828;
ALTER TABLE `tblSetting_rijenweergeven`
MODIFY `rijenweergevenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22030;
ALTER TABLE `tblSpecifiekecontactgegevens`
MODIFY `contactgegevenid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=493;
ALTER TABLE `tblStatistics`
MODIFY `statisticsid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28695;
ALTER TABLE `tblTeamleden`
MODIFY `teamlidid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
ALTER TABLE `tblTeamledenperwerfperdag`
MODIFY `teamledenperwerfperdagid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7222;
ALTER TABLE `tblTeamledenperwerfperdag2`
MODIFY `teamledenperwerfperdagid` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `tblTeamledenperwerfperdagbackup`
MODIFY `teamledenperwerfperdagid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9953;
ALTER TABLE `tblTeamperiode`
MODIFY `teamperiodeid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=495;
ALTER TABLE `tblTeamperiodeperlid`
MODIFY `teamperiodeperlidid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=430;
ALTER TABLE `tblTeams`
MODIFY `teamid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
ALTER TABLE `tbltest`
MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=784;
ALTER TABLE `tblTijdstip`
MODIFY `tijdstipid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30794;
ALTER TABLE `tblTypespercontact`
MODIFY `typespercontactid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=357;
ALTER TABLE `tblUitgaandefacturen`
MODIFY `factuurid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5086;
ALTER TABLE `tblUitgaandefacturen2014`
MODIFY `factuurid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3208;
ALTER TABLE `tblUitgaandefacturenbetalingen`
MODIFY `uitgaandefacturenbetalingenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5543;
ALTER TABLE `tblUitgaandefacturen_zoekopties`
MODIFY `zoekoptieid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
ALTER TABLE `tblVerlof`
MODIFY `verlofid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22985;
ALTER TABLE `tblWerfbeheer`
MODIFY `werfbeheerid` mediumint(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1386;
ALTER TABLE `tblWerfdetailandere`
MODIFY `werfdetailandereid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=387;
ALTER TABLE `tblWerfdetailat`
MODIFY `werfdetailatid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6930;
ALTER TABLE `tblWerfdetailledenperwerf`
MODIFY `werfdetailledenperwerfid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24034;
ALTER TABLE `tblWerfdetailoffertedata`
MODIFY `werfdetailoffertedataid` mediumint(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7960;
ALTER TABLE `tblWerfdetailplanningdata`
MODIFY `werfdetailplanningdataid` mediumint(9) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22831;
ALTER TABLE `tblWerfdetailplanningopmerkingen`
MODIFY `werfdetailplanningopmerkingenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1033;
ALTER TABLE `tblWerfdetailteamsperwerf`
MODIFY `werfdetailteamsperwerfid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29610;
ALTER TABLE `tblWerknemerevaluatie`
MODIFY `evaluatieid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=765;
ALTER TABLE `tblWerknemerskosten`
MODIFY `werknemerskostenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=65;
ALTER TABLE `tblWerknemersperploeg`
MODIFY `werknemersperploegid` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9009;
ALTER TABLE `tblWerkregio`
MODIFY `werkregioid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
ALTER TABLE `tblWerkregiopercontact`
MODIFY `werkregiopercontactid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1790;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
