-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 01:22 PM
-- Server version: 5.5.59-0+deb8u1
-- PHP Version: 5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sfinxto_sfinxwerktest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblGebruikersrechten`
--

CREATE TABLE IF NOT EXISTS `tblGebruikersrechten` (
`gebruikersrechtenid` int(11) NOT NULL,
  `gebruikersrechtenhoofdgroep` smallint(6) NOT NULL,
  `gebruikersrechtensubgroep` smallint(6) NOT NULL,
  `gebruikersrechtenvolgordebinnensubgroep` smallint(6) NOT NULL,
  `gebruikersrechtenitem` varchar(255) NOT NULL,
  `gebruikersrechtenomschrijving` text NOT NULL,
  `actief` tinyint(1) NOT NULL,
  `gebruikersrechtenextra` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblGebruikersrechten`
--

INSERT INTO `tblGebruikersrechten` (`gebruikersrechtenid`, `gebruikersrechtenhoofdgroep`, `gebruikersrechtensubgroep`, `gebruikersrechtenvolgordebinnensubgroep`, `gebruikersrechtenitem`, `gebruikersrechtenomschrijving`, `actief`, `gebruikersrechtenextra`) VALUES
(1, 1, 1, 1, 'mag inloggen op de database', '', 1, 0),
(2, 2, 1, 1, 'heeft toegang tot de homepagina', '', 1, 0),
(3, 3, 1, 1, 'heeft toegang tot de contactenpagina', '', 1, 0),
(4, 3, 2, 1, 'kan contacten toevoegen/wijzigen', '', 1, 0),
(5, 3, 2, 2, 'kan contacten verwijderen', '', 0, 0),
(6, 4, 1, 1, 'heeft toegang tot de planning', '', 1, 0),
(7, 5, 1, 1, 'heeft toegang tot de inkomende facturen', '', 1, 0),
(8, 6, 1, 1, 'heeft toegang tot de uitgaande facuren', '', 1, 0),
(9, 7, 1, 1, 'heeft toegang tot het financieel overzicht', '', 1, 0),
(10, 8, 1, 1, 'heeft toegang tot de afwezigheden', '', 1, 0),
(11, 9, 1, 1, 'heeft toegang tot de teamverdeling', '', 1, 0),
(12, 10, 1, 1, 'heeft toegang tot de planning van de individuele WN', '', 1, 0),
(13, 11, 1, 1, 'Heeft toegang tot de AT', '', 1, 0),
(14, 12, 1, 1, 'Heeft toegang tot de rechtenpagina', '', 0, 0),
(15, 1, 1, 2, 'Heeft toegang tot de rechtenswitcher (switch view)', '', 0, 0),
(16, 3, 2, 3, 'Kan verwijderde contacten (vuilbakje) raadplegen', '', 0, 0),
(17, 5, 2, 1, 'Kan inkomende facturen toevoegen', '', 0, 0),
(18, 5, 2, 2, 'Kan inkomende facturen verwijderen', '', 0, 0),
(19, 5, 2, 3, 'Kan inkomende facturen aanpassen', '', 0, 0),
(20, 6, 2, 1, 'Kan uitgaande facturen toevoegen', '', 1, 0),
(21, 6, 2, 2, 'Kan uitgaande facturen verwijderen', '', 1, 0),
(22, 6, 2, 3, 'Kan uitgaande facturen wijzigen', '', 1, 0),
(23, 5, 2, 4, 'Kan alle inkomende facturen zien<br>(Indien niet ziet deze persoon enkel de facturen van zijn eigen projecten)', '', 0, 0),
(24, 6, 2, 4, 'Kan alle uitgaande facturen zien (en selecteren op project)<br>(Indien niet ziet deze persoon enkel de facturen van zijn eigen projecten)', '', 0, 0),
(25, 7, 2, 1, 'kan ingegeven nacalculatiebedragen verwijderen', '', 1, 0),
(26, 7, 2, 2, 'kan nacalculatiebedragen toevoegen', '', 1, 0),
(27, 7, 2, 3, 'kan alle projecten zien', '', 0, 0),
(29, 10, 2, 3, 'heeft toegang tot de maandbonnen', '', 1, 0),
(30, 10, 2, 1, 'heeft toegang tot de werknemerevaluaties', '', 1, 0),
(31, 10, 2, 2, 'Kan alle werknemerevaluaties aanpassen<br>(Indien niet kan dit contacttype enkel zijn eigen projecten beoordelen)', '', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblGebruikersrechten`
--
ALTER TABLE `tblGebruikersrechten`
 ADD PRIMARY KEY (`gebruikersrechtenid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblGebruikersrechten`
--
ALTER TABLE `tblGebruikersrechten`
MODIFY `gebruikersrechtenid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
