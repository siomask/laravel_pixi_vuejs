-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 05, 2018 at 01:22 PM
-- Server version: 5.5.59-0+deb8u1
-- PHP Version: 5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sfinxto_sfinxwerktest`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblGebruikersgroepen`
--

CREATE TABLE IF NOT EXISTS `tblGebruikersgroepen` (
`gebruikersgroepenid` tinyint(4) NOT NULL,
  `gebruikersgroepennaam` varchar(255) NOT NULL,
  `gebruikersgroepenvolgorde` tinyint(4) NOT NULL,
  `gebruikersgroepenomschrijving` varchar(255) NOT NULL,
  `gebruikersgroepenextra` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblGebruikersgroepen`
--

INSERT INTO `tblGebruikersgroepen` (`gebruikersgroepenid`, `gebruikersgroepennaam`, `gebruikersgroepenvolgorde`, `gebruikersgroepenomschrijving`, `gebruikersgroepenextra`) VALUES
(1, 'superuser', 1, 'superusers hebben alles rechten op de toepassing', 0),
(2, 'zaakvoerder', 2, 'zaakvoerders Sfinx=Jonas, Ludwig en Tim', 0),
(3, 'management assistent', 3, 'job secretaresse', 0),
(4, 'projectleider', 4, '', 0),
(5, 'calculator', 5, '', 0),
(6, 'reserveaccount', 6, '', 0),
(7, 'uit dienst', 7, '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tblGebruikersgroepen`
--
ALTER TABLE `tblGebruikersgroepen`
 ADD PRIMARY KEY (`gebruikersgroepenid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tblGebruikersgroepen`
--
ALTER TABLE `tblGebruikersgroepen`
MODIFY `gebruikersgroepenid` tinyint(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
