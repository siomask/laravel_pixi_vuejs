const exec = require('child_process').exec;
var cron = require('node-cron');
/*
* Warning whis cron remove entire project and restore again if catch any changes from git
*
*
* */
var lastComitHash = '';

var cmd = '' +
    'cd ../sfinxdatabase;' +
    'git fetch;' +
    'git rev-parse origin/master;';
var task = cron.schedule('*/1 * * * *', function () {//every 1 minute
    console.log('running a task every minute-------');//test
    exec(cmd, (error, stdout, stderr) => {//the cron file in same dir as project
        if (error !== null) {
            return console.log(`exec move error: ${error}`);
        }
        console.log(`check status: ${stdout}`);
        if (lastComitHash != stdout) {
            lastComitHash = stdout;
            exec('./cronSetup.sh',
                (error, stdout, stderr) => {
                    console.log(`stdout: ${stdout}`);
                    // console.log(`stderr: ${stderr}`);
                    if (error !== null) {
                        console.log(`exec sh error: ${error}`);
                    }
                    console.log('finish a task every midnight-------');
                });
        }
    });


}, false);
//test
task.start();