#!/bin/bash
#!clear
echo `date` "Refresh installing SFINX--------------------------------" >> ./cronjobs.log

cd ../;
sudo rm -rf ./sfinxdatabase;
git clone git@bitbucket.org:theblueglaucus/sfinxdatabase.git;

#!git reset --hard;
#!git pull;
sudo chmod -R 777 ./sfinxdatabase/SfinxDatabaseBackend;
sudo chmod -R 777 ./sfinxdatabase/SfinxDatabaseBackend/*;

cp ./cron/.env ./sfinxdatabase/SfinxDatabaseBackend;
cd ./sfinxdatabase/SfinxDatabaseBackend;

php composer.pchar install;
sudo php /usr/local/bin/composer install;
php artisan key:generate;
yes | php artisan jwt:secret;
sudo php /usr/local/bin/composer  dump-autoload;
php artisan migrate:fresh --seed;
cd ../SfinxDatabaseFrontend;
#!npm install --max_old_space_size=2048;
npm install --unsafe-perm=true --max_old_space_size=2048;
npm run build --max_old_space_size=2048;

echo  `date` "Done--------------------------------" >> ./cronjobs.log
