import {getDay, Help} from '@/utils'
import {VUtils} from '../VUtils'
import store from "../../../../../../../store";

export class DaysMemberPlanning {
    constructor(sites) {
        this.data = sites || {};
    }

    updatePlannedDayState(opt) {
        let {type, cell, planning, teamMember, teamId, cellId} = opt,
            day = cell._daysUTC[cell._currentItemHor],
            _branch = cell._currentItemBranch,
            _periodId = cell._periodContainer ? cell._periodContainer.id : (cellId || -1),
            hasChanges = false;
        switch (type) {
            case VUtils.PLANNING_ACTIONS.DRAG: {//on drag
                let {dayShift} = opt,
                    _newDate = day + dayShift * VUtils.DAY_IN_UTC;

                this.traverse((_planningD) => {
                    if (
                        _periodId == (_planningD.collaborator_planning_entry_id) &&
                        (_planningD.date == day || Math.abs(_planningD.date - day) <= VUtils.DAY_IN_UTC / 6)) {
                        _planningD.date = _newDate;
                    }
                });
                break;
            }
            case VUtils.PLANNING_ACTIONS.SPLIT: {//on split
                let {groups, removeDay, oldId} = opt;

                groups.forEach((el) => {
                    el.start = Date._toUTCSeconds(el.start);
                    el.end = Date._toUTCSeconds(el.end);
                });

                this.traverse((_pl, _teamMember, ki) => {
                    if (_pl.collaborator_planning_entry_id == oldId) {

                        if (removeDay && (_pl.date == day || Math.abs(_pl.date - day) < VUtils.DAY_IN_UTC / 6)) {
                            _teamMember.plannings.splice(ki, 1);
                            return ki - 1;
                        } else {
                            if (/*_pl.date >= groups[0].start &&*/ (_pl.date <= groups[0].end || _pl.date - VUtils.DAY_IN_UTC / 6 <= groups[0].end)) {
                                _pl.collaborator_planning_entry_id = groups[0].id;
                            } else {
                                _pl.collaborator_planning_entry_id = groups[1].id;
                            }
                        }
                    }
                });
                break;
            }
            case VUtils.PLANNING_ACTIONS.REMOVE_ALL: {//on remove cell(s) planned
                let {removeDay} = opt;

                this.traverse((_pl, _teamMember, ki) => {
                    if (_pl.collaborator_planning_entry_id == _periodId) {
                        if (removeDay) {
                            if ((_pl.date == day || Math.abs(_pl.date - day) <= VUtils.DAY_IN_UTC / 6)) {
                                _teamMember.plannings.splice(ki, 1);
                                return ki - 1;
                            }
                        } else {
                            _teamMember.plannings.splice(ki, 1);
                            return ki - 1;
                        }

                    }
                });

                break;
            }
            case VUtils.PLANNING_ACTIONS.ADD_OR_UPDATE: {//on add or update
                let {oldId} = opt;
                this.traverse((_pl, _teamMember, ki) => {
                    if (oldId.indexOf(_pl.collaborator_planning_entry_id) > -1) {
                        hasChanges = true;
                        _pl.collaborator_planning_entry_id = _periodId;
                    }
                });

                break;
            }
            case VUtils.PLANNING_ACTIONS.CREATE: {//on planned team member

                planning.date = Date._toUTCSeconds(planning.date.split(" ")[0]);
                for (let i = 0; i < _branch.teams.length; i++) {
                    let team = _branch.teams[i];
                    if (team.id == teamId) {
                        for (let di = 0; di < team.teamMembers.length; di++) {
                            let _teamMember = team.teamMembers[di];
                            if (_teamMember.id == teamMember.id) {
                                for (let fd = 0; fd < _teamMember.plannings.length; fd++) {

                                    if (planning.collaborator_planning_entry_id == _teamMember.plannings[fd].collaborator_planning_entry_id &&
                                        planning.date == _teamMember.plannings[fd].data) {
                                        return _teamMember.plannings[fd].sequence = planning.sequence;
                                    }
                                }
                                return _teamMember.plannings.push(planning);
                            }
                        }
                    }
                }

                break;
            }
            case VUtils.PLANNING_ACTIONS.DELETE_MEMBER: {//on unplanned team member
                this.traverse((_pl, _teamMember, ki) => {
                    if (_teamMember.plannings[ki].id == planning.id) return _teamMember.plannings.splice(ki, 1);
                });
                break;
            }
            case VUtils.PLANNING_ACTIONS.EDIT_MEMBER: {//on edit team member
                this.traverse((_pl, _teamMember, ki) => {
                    if (_teamMember.plannings[ki].id == planning.id) return Object.assign(_teamMember.plannings[ki], planning);
                });
                break;
            }
        }

        return hasChanges;
    }

    traverse(callback, isTeeam) {
        for (let sdi = 0; sdi < this.data.length; sdi++) {
            let site = this.data[sdi];
            if (typeof site.site == 'undefined') continue;
            for (let jk = 0; jk < site.branches.length; jk++) {
                let _branch = site.branches[jk];
                for (let i = 0; i < _branch.teams.length; i++) {
                    let team = _branch.teams[i];
                    if (isTeeam) {
                        callback(0, 0, 0, team);
                    } else {
                        for (let di = 0; di < team.teamMembers.length; di++) {
                            let _teamMember = team.teamMembers[di];
                            for (let ki = 0; ki < _teamMember.plannings.length; ki++) {
                                let _reslt = (callback(_teamMember.plannings[ki], _teamMember, ki));
                                if (typeof _reslt == 'number') {
                                    ki--;
                                } else if (_reslt === false) {
                                    return;
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    hasPlannings(cellElement) {
        let hasPlannings = false,
            day = cellElement._daysUTC[cellElement._currentItemHor],
            _periodId = cellElement._periodContainer ? cellElement._periodContainer.id : (-1);
        return _periodId > -1;
        /*this.traverse((planning) => {
            if (planning.collaborator_planning_entry_id == _periodId &&
                (planning.date == day || Math.abs(day - planning.date) < VUtils.DAY_IN_UTC / 6)) {
                hasPlannings = true;
                return false;//stop loop
            }
        });
        return hasPlannings;*/
    }

    checkConflicts() {
        store.commit('planner/WARNING_CONFLICT_CLEAN', 0);

        let _plannings = {}, cells = {}, msg = {},
            _planingsTeamMember = VUtils.PLANNING_MEMBER_SEQUENCE.concat([]);
        for (let sdi = 0; sdi < this.data.length; sdi++) {
            let site = this.data[sdi];
            if (typeof site.site == 'undefined') continue;
            for (let jk = 0; jk < site.branches.length; jk++) {
                let _branch = site.branches[jk];


                for (let j = 0, plannings = {}; j < _branch._rowElements[2].children.length; j++) {
                    let _cell = _branch._rowElements[2].children[j],
                        _dayUTC = _cell._daysUTC[_cell._currentItemHor];
                    if (_cell._periodContainer && _cell._hasToBeNoPlanned /*&& (_planningD.date ==_dayUTC || Math.abs(_dayUTC - _planningD.date) < VUtils.DAY_IN_UTC / 6)*/ ) {
                        if (!plannings[_cell._currentItemHor] && this.hasPlannings(_cell)) {
                            plannings[_cell._currentItemHor] = true;
                            if (_cell.isOutOfBranchRange) {
                                store.commit('planner/WARNING_CONFLICT', "The plannings in " + Date._formatDate(_cell._daysUTC[_cell._currentItemHor]) + " are out of collaborator daterange");
                            } else {
                                store.commit('planner/WARNING_CONFLICT', "In holiday " + Date._formatDate(_cell._daysUTC[_cell._currentItemHor]) + " have plannings");
                            }

                        }
                    }
                }
                //check if same planning details have in same day for several collaborattors(branches)
                let _memberSick = {};
                for (let i = 0; i < _branch.teams.length; i++) {
                    let team = _branch.teams[i];
                    for (let di = 0; di < team.teamMembers.length; di++) {
                        let _teamMember = team.teamMembers[di];
                        for (let ki = 0; ki < _teamMember.plannings.length; ki++) {
                            let _planningD = _teamMember.plannings[ki];




                            if (!_plannings[_planningD.date]) _plannings[_planningD.date] = {};
                            if (!_plannings[_planningD.date][team.id]) _plannings[_planningD.date][team.id] = {};
                            if (!_plannings[_planningD.date][team.id][_teamMember.id]) _plannings[_planningD.date][team.id][_teamMember.id] = {
                                sequence: [],
                                cellsId: []
                            };

                            let _cellPlanned = _plannings[_planningD.date][team.id][_teamMember.id],
                                hasConflict = false;

                            if (_plannings[_planningD.date][team.id][_teamMember.id].cellsId.indexOf(_planningD.collaborator_planning_entry_id) > -1) {
                                continue;
                            }
                            if (hasConflict = _plannings[_planningD.date][team.id][_teamMember.id].sequence.indexOf(_planningD.sequence) > -1) {
                                let _day = new Date(0),
                                    seq = '';
                                _day.setUTCSeconds(_planningD.date);
                                for (let i = 0; i < _planingsTeamMember.length; i++) {
                                    if (_planningD.sequence == _planingsTeamMember[i].value) {
                                        seq = _planingsTeamMember[i].name;
                                        break;
                                    }
                                }

                                let text = 'In day (' +
                                    _day.toUTCString()
                                        .split(' ')
                                        .splice(1, 2)
                                        .join(' ') +
                                    `) have same planning(<b>${seq}</b>) for user :<b>${_teamMember.first_name} ${_teamMember.last_name}</b> from team <b>${team.name}</b>`;

                                if (!msg[`${_planningD.date}${team.id}${_teamMember.id}${_planningD.sequence}`]) store.commit('planner/WARNING_CONFLICT', text.substr(0, text.length - 1));
                                msg[`${_planningD.date}${team.id}${_teamMember.id}${_planningD.sequence}`] = true;

                            }

                            _cellPlanned.sequence.push(_planningD.sequence);
                            _cellPlanned.cellsId.push(_planningD.collaborator_planning_entry_id);
                            if (hasConflict) {
                                for (let cellsid = 0; cellsid < _cellPlanned.cellsId.length; cellsid++) {
                                    if (!cells[_cellPlanned.cellsId[cellsid]]) cells[_cellPlanned.cellsId[cellsid]] = {days: []};
                                    cells[_cellPlanned.cellsId[cellsid]].days.push(_planningD.date);
                                }
                            }

                            //check if planned member got sick or in holliday
                            let cellsid = _planningD.collaborator_planning_entry_id;
                            DaysMemberPlanning.checkMemberHollidayConflict(_teamMember, _planningD.date, () => {
                                if (!_memberSick[_teamMember.id+"_"+_planningD.date]) {
                                    _memberSick[_teamMember.id+"_"+_planningD.date] = true;
                                    store.commit('planner/WARNING_CONFLICT', `Member ${_teamMember.name} got sick/holiday in ${Date._formatDate(_planningD.date)}`);
                                    if (!cells[cellsid]) cells[cellsid] = {days: []};
                                    cells[cellsid].days.push(_planningD.date);
                                }

                            });

                        }


                    }
                }

            }
        }

        return cells;

    }

    static checkMemberHollidayConflict(teamM, startT, callback) {
        ['absences', 'holidays'].forEach((el) => {
            if (teamM[el]) {
                for (let jd = 0; jd < teamM[el].length; jd++) {
                    let _start = Date._toUTCSeconds(teamM[el][jd].start),
                        _end = Date._toUTCSeconds(teamM[el][jd].end);
                    if ((startT >= _start || Math.abs(_start - startT) < VUtils.DAY_IN_UTC / 6) &&
                        startT <= _end || Math.abs(_end - startT) < VUtils.DAY_IN_UTC / 6) {
                        teamM._noPlanned = true;
                        if (callback) callback(teamM);
                    }
                }
            }
        })
    }

}