
export class VLevel {
    constructor(opt) {
        this.opt = opt;
        this.container = new PIXI.Container();
        opt.parent.stage.addChild(this.container);
    }

    show(show) {
        this.container.renderable = this.container.visible = !!show;
    }

    onResize() {
    }
    _updateVisibleEls(){}
    resetViews(){}
}


