import {VUtils} from "../VUtils";
import {PrevPlannedAction} from "../vlevel/PrevPlannings";

export class HistoryPlanning {
    constructor(parent) {
        this.parent = parent;
    }

    add(opt, withouHistory) {
        if (!!withouHistory) return;
        let {parent, self, dayBackG, isNoUserAction = false, groups} = opt,
            pixiviewer = this.parent,
            axios = this.parent.opt.domeEl.axios,
            _planning = this.parent.opt.conflicts._planning,
            history = this.parent.opt.domeEl.$store.state.planner.history,
            prevState = {
                onFinish: () => {
                    pixiviewer.refreshRender();
                },
                isNoUserAction: isNoUserAction
            };

        switch (opt.type) {
            case VUtils.PLANNING_ACTIONS.DRAG: {//on drag
                let {prevGroups} = opt;
                prevState.callback = ((d, s, e, _item) => {
                    return async () => {
                        let _period = parent.children[_item],
                            _els = _period._periodContainer._elements,
                            startAt = _els[0]._currentItemHor,
                            endAt = _els[_els.length - 1]._currentItemHor,
                            _itemRedo = _item + d,
                            _dRedo = -d;
                        return await _period.customeEvents._setNewPst(d, s, e).then(() => {
                            prevGroups.restore().then((groups) => {
                                history.addRedo({
                                    callback: ((d, s, e, _item) => {
                                        return async () => {
                                            return await parent.children[_item].customeEvents._setNewPst(d, s, e, true, true);
                                        }
                                    })(_dRedo, startAt, endAt, _itemRedo)
                                });
                            });
                        });
                    }
                })(opt.daysShift, opt.start, opt.end, opt.index,)
                break;
            }
            case VUtils.PLANNING_ACTIONS.SPLIT: {//on split
                let _firstEl = groups[0]._elements[0],
                    last = groups[1]._elements[groups[1]._elements.length - 1];
                prevState.callback = ((_item, s, e, _startFromOr,_endWithOr ) => {
                    return async () => {
                        let el = parent.children[_item];
                        return await el.customeEvents.updatePlannedPeriod({
                            detailsData: {
                                site: el.parent.siteId,
                                branch: el.parent.branchId,
                                id: el._periodContainer.id
                            },
                            dataRequest: {
                                start: s,
                                end: e,
                            },
                            _elsInGroup: el._periodContainer._elements,
                            merge: true,
                            daysShift: 0,
                            _elsInGroupClone: el._periodContainer._elements.concat([]),
                            // nextItem,
                            replaceCellId: [],
                            isNoUserAction: true,
                            withHistory: false,
                            _endWithOr,
                            _startFromOr
                        }).then(()=>{
                            history.addRedo({
                                callback: async () => {
                                    return await parent.children[_item].customeEvents._split();
                                }
                            });
                        });

                    }
                })(
                    dayBackG._currentItemHor,
                    _firstEl._daysUTC[_firstEl._currentItemHor],
                    last._daysUTC[last._currentItemHor],
                    _firstEl._currentItemHor,
                    last._currentItemHor
                )
                break;
            }
            case VUtils.PLANNING_ACTIONS.REMOVE_ALL: {

                let prevGroups = new PrevPlannedAction(pixiviewer),
                    isSimpleDay = !opt.removeDay;
                prevGroups.add(dayBackG, isSimpleDay);
                prevState.callback = ((_item, prevGroup) => {
                    return async () => {
                        return await prevGroup.restore().then(() => {
                            history.addRedo({
                                callback: async () => {
                                    return await parent.children[_item].customeEvents._removeItem(isSimpleDay, true, true);
                                }
                            });

                        });
                    }
                })(dayBackG._currentItemHor, prevGroups)
                break;
            }
            case VUtils.PLANNING_ACTIONS.CREATE: {//on planning
                prevState.callback = ((_item) => {
                    return async () => {
                        let
                            _dayPlanned = parent.children[_item],
                            prevGroups = new PrevPlannedAction(pixiviewer);
                        prevGroups.add(_dayPlanned, false);
                        return await _dayPlanned.customeEvents._removeItem(false, true).then(() => {
                            history.addRedo({
                                callback: async () => {
                                    return prevGroups.restore(true);
                                }
                            });
                        });
                    }
                })(dayBackG._currentItemHor)
                break;
            }
            case VUtils.PLANNING_ACTIONS.TOGGLE_EMTY_ROWS: {
                let {table} = opt;
                prevState.callback = async () => {
                    history.addRedo({
                        callback: async () => {
                            return self.toggleEmptyRows(table, null, null, false, true);
                        }
                    });
                    return await self.toggleEmptyRows(table, null, null, true);
                }
                break;
            }
            case VUtils.PLANNING_ACTIONS.TOGGLE_EMTY_ROWS_ALL: {
                prevState.callback = async () => {
                    history.addRedo({
                        callback: async () => {
                            return self.toggleEmptyRowsFromAllSites(null, null, false, true);
                        }
                    });
                    return await self.toggleEmptyRowsFromAllSites(null, null, true);
                }
                break;
            }
            default: {
                return console.warn("no action found");
            }
        }

        history.add(prevState);
    }

}