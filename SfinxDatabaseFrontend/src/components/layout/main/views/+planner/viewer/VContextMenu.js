
import { VUtils } from './VUtils'
export class VContextMenu {
	constructor(viewer) {
		function attachEv(el) {
			el.interactive = true
			el.children[0].alpha = 0.6
			el.mouseover = function() {
				el.children[0].alpha = 1
				viewer.stage.renderer.view.style.cursor = 'pointer'
			}
			el.mouseout = function() {
				el.children[0].alpha = 0.6
				viewer.stage.renderer.view.style.cursor = ''
			}
			el.mouseup = function(e) {
				switch (e.data.originalEvent.button) {
					case VUtils.BUTTONS.LEFT: {
						el._onMouseUp()
						break
					}
				}
			}
		}

        this.container = new PIXI.Container();
		this.viewer = viewer;

		/*---red cell remove---*/
		let onDateCell = new PIXI.Container()
		onDateCell.__category = VUtils.DOM_ELS.CNTX_MENU_DAY_CELL
		this.contextMenuItems = [
			{
				name: 'Split',
				mouseUp: () => {
					viewer._events.mouse._lastHover.customeEvents._split()
				}
			},
			{
				name: 'Remove 1',
				mouseUp: () => {
					viewer._events.mouse._lastHover.customeEvents._removeItem()
				}
			},
			{
				name: 'Remove Group',
				mouseUp: () => {
					viewer._events.mouse._lastHover.customeEvents._removeItem(true)
				}
			},
			{
				name: 'Show Details',
				mouseUp: () => {
					viewer.showDetails()
				}
			},
			{
				name: 'Move',
				mouseUp: () => {
					viewer.opt.domeEl.selectEditItemSite(viewer._events.mouse._lastHover)
				}
			},
			{
				name: 'Available workers',
				mouseUp: () => {
					viewer.opt.domeEl.showAvailableWorkers(
						viewer._events.mouse._lastHover._selectedDay
					)
				}
			}
		]
		this.contextMenuItems.forEach((option, keys) => {
			let item = VUtils.drawCell(210, 30, { type: VUtils.TEXT_TYPE.CNTX_EL,category:16 })
			item.addChild(
				VUtils.drawText({ type: VUtils.TEXT_TYPE.CNTX_EL, text: option.name })
			)
			onDateCell.addChild(item)
			item._onMouseUp = option.mouseUp
			option.item = item
			attachEv(item)
			if (keys == 3) {
				item.visible = item.renderable = false
			}
			item.position.y = keys * item.height
		})

		/*---all types of context menu -----*/
		this.items = [onDateCell]
	}

	show(show, category, ev) {
		this.visible = !!show
		if (this.container.parent) this.viewer.stage.removeChild(this.container)
		this.container.removeChild(this.container.children[0])
		if (!show) {
			return
		}

		let _cell = this.viewer._events.mouse._lastHover,
			isGroup = !!_cell._periodContainer,
			isDetails =
				isGroup /*&& (_cell._currentItemBranch.contact.type == VUtils.TYPES.CONTACT_TEAM */  ,
			isMove = _cell._category != VUtils.TYPES.ALL_AVAILABLE_WORKERS,
			isShowAvailableWorkers =
				_cell._category == VUtils.TYPES.ALL_AVAILABLE_WORKERS && false,
			showItems = 0

        this.contextMenuItems.forEach((e, key) => {
			if (key <= 3) {
				e.item.visible = e.item.renderable = isGroup
				if (key == 3) e.item.visible = e.item.renderable = isDetails
			} else if (key == 4) {
				e.item.visible = e.item.renderable = isMove
			} else if (key == 5) {
				e.item.visible = e.item.renderable = isShowAvailableWorkers
			}

			if (e.item.visible) {
				e.item.position.y = showItems++ * e.item.height
			}
		})

		switch (category) {
			case VUtils.DOM_ELS.CNTX_MENU_DAY_CELL: {
				this.container.addChild(this.items[0])
				this.viewer.stage.addChild(this.container)
				break
			}
		}
		this.container.position.x = ev.x
		this.container.position.y = ev.y
	}
}
