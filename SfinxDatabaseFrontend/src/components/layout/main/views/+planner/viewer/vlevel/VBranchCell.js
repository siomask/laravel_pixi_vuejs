import {VUtils} from "../VUtils";
import {PixiViewer} from "../PixiViewer";
import {VConsole, Help} from '@/utils';
import {PrevPlannedAction} from './PrevPlannings';
import {PeriodGroup} from './PeriodGroup';

export class VBranchCell {
    constructor(parent, opt) {
        this.parent = parent;

    }

    _attachToCellEvs(dayBackG) {
        // if (dayBackG.isWeekEnd)return;
        dayBackG.findCloser = function (dir) {
            let el = this;
            while (el.isWeekEnd || el.hasToBeNoPlanned) {
                let _el = el.parent.children[el._currentItemHor + dir];
                if (!_el) return el;
                el = _el;
            }
            return el;
        }
        dayBackG.unPlan = function () {
            if (this._periodContainer) {
                this._periodContainer.removeCell(this);
                this._periodContainer = this.isPlanned = this.isGroup = this.hasToBeNoPlanned = null;
            }
            return this;
        }
        dayBackG.checkNeigbor = function () {
            //check previous neigbor


            let previous = this.__parent.children[this._currentItemHor - 1],
                next = this.__parent.children[this._currentItemHor + 1],
                category = VUtils.DOM_ELS.CELL_WITH_NEIGHBOR;

            // while (this.children.length) this.removeChild(this.children[0]);
            for (let i = 0; i < this.children.length; i++) {
                if (this.children[i].__category == category) {
                    this.removeChild(this.children[i--]);
                }
            }
            if (previous) {
                if (previous._periodContainer && this._periodContainer && previous._periodContainer.id != this._periodContainer.id) {
                    this.addChild(
                        VUtils.drawCell(
                            5,
                            this.height - 4,
                            {
                                type: VUtils.TEXT_TYPE.TABLE_CELL_BORDER_SPLIT,
                            }
                        )
                    );
                    this.children[this.children.length - 1].position.x = -2;
                    this.children[this.children.length - 1].position.y = 2;
                    this.children[this.children.length - 1].__category = category;
                }
                // if (previous._periodContainer)previous.checkNeigbor();
            }

            if (next) {
                if (this._periodContainer) {
                    if (next._periodContainer) {
                        if (next._periodContainer.id == this._periodContainer.id) {

                            for (let i = 0; i < next.children.length; i++) {
                                if (next.children[i].__category == category) {
                                    next.removeChild(next.children[i--]);
                                }
                            }
                            next._periodContainer._elements.forEach((el) => {
                                if (el.onComment) el.onComment();
                            });

                        } else {
                            // next.addChild(
                            //     VUtils.drawCell(
                            //         5,
                            //         next.height - 4,
                            //         {
                            //             type: VUtils.TEXT_TYPE.TABLE_CELL_BORDER_SPLIT,
                            //         }
                            //     )
                            // );
                            // next.children[next.children.length-1].position.x = -2;
                            // next.children[next.children.length-1].position.y = 2;
                            // next.children[next.children.length-1].__category = category;
                        }
                    }
                } else if (next._periodContainer) {

                    for (let i = 0; i < next.children.length; i++) {
                        if (next.children[i].__category == category) {
                            next.removeChild(next.children[i--]);
                        }
                    }
                    next._periodContainer._elements.forEach((el) => {
                        if (el.onComment) el.onComment();
                    });
                }
            }

            if (this.onComment) this.onComment();
            return this;
        }
        if (dayBackG.hasToBeNoPlanned || dayBackG._category == VUtils.TYPES.ALL_AVAILABLE_WORKERS) return;

        let self = this.parent,
            _self = dayBackG._actionCell,
            _parent = self.opt.parent,
            plannteInfo = _parent.opt.domeEl.$refs.plannteInfo,
            parent = dayBackG.parent,
            _planning = _parent.opt.conflicts._planning;
        dayBackG.interactive = dayBackG.isInteractive = true;
        // dayBackG._alpha = dayBackG._category == VUtils.TYPES.ALL_AVAILABLE_WORKERS ? 1 : (dayBackG.isWeekEnd ? 0.8 : 0.6);
        dayBackG._alpha = (dayBackG.isWeekEnd ? 0.8 : 0.6);
        if (dayBackG._periodContainer) dayBackG._alpha = 1;
        // dayBackG.alpha = dayBackG._category == VUtils.TYPES.ALL_AVAILABLE_WORKERS ? 1 : dayBackG._alpha;
        dayBackG.mouseover = function (e) {
            if (this._category != VUtils.TYPES.SIMPLE_BTN) {
                _parent._events.mouse.lastHover = this;
                _parent._events.mouse.lastHover.__e = e;
            }
            if (_parent._events.mouse.canHover) {
                this.alpha = dayBackG._alpha;
                _parent.stage.renderer.view.style.cursor = 'pointer';
            }
        };
        dayBackG.mousemove = function (e) {
            if (e.target) {
                _parent.stage.renderer.view.style.cursor = 'pointer';
                if (e.target._category != VUtils.TYPES.SIMPLE_BTN) {
                    _parent._events.mouse.lastHover = e.target;
                    _parent._events.mouse.lastHover.__e = e;
                }
            }
            // plannteInfo.show();
        };
        dayBackG.mouseout = function () {
            _parent._events.mouse.lastHover = null;
            if (_parent._events.mouse.canHover) {
                this.alpha = 1;
            }
            _parent.stage.renderer.view.style.cursor = '';
        };

        if (
            dayBackG._category == VUtils.TYPES.ALL_AVAILABLE_WORKERS ||
            dayBackG._category == VUtils.TYPES.SIMPLE_BTN
        ) return;
        if (dayBackG._periodContainer) {
            dayBackG.customeEvents = {
                _removeItem: function (dropall, withoutHistory, isNoUserAction) {

                    return new Promise((reslove, reject) => {
                        let dataReq = {
                            type: VUtils.REUESTS.CELL.DELETE_SIGNLE,
                            detailsData: {
                                site: parent.siteId,
                                branch: parent.branchId,
                                id: dayBackG._periodContainer.id
                            },
                            data: {
                                branch: dayBackG.parent.branchId,
                                date: self.daysUTC[dayBackG._currentItemHor]
                            },
                            next: (res) => {
                                let _els = dayBackG._periodContainer._elements,
                                    _id = dayBackG._periodContainer.id,
                                    parent = dayBackG.parent;

                                switch (res.type) {
                                    case 1://remove all
                                    {

                                        let _lastEl = _els[_els.length - 1];
                                        _parent._historyPlanning.add({
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            parent,
                                            isNoUserAction,
                                            dayBackG,
                                            self
                                        }, withoutHistory);
                                        while (_els.length) {
                                            new VBranchPlannedCell(self, {
                                                isNew: true,
                                                parent: parent,
                                                cell: _els.pop().unPlan().checkNeigbor()
                                            });
                                        }
                                        if (_lastEl) _lastEl.checkNeigbor();
                                        _planning.updatePlannedDayState({
                                            cell: dayBackG,
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            cellId: _id
                                        });

                                        break;
                                    }
                                    case 2://remove from left
                                    {
                                        _parent._historyPlanning.add({
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            parent,
                                            isNoUserAction,
                                            dayBackG,
                                            removeDay: true,
                                            self
                                        }, withoutHistory);
                                        new VBranchPlannedCell(self, {
                                            isNew: true,
                                            parent: parent,
                                            cell: _els.shift().unPlan().checkNeigbor()
                                        });
                                        _planning.updatePlannedDayState({
                                            cell: dayBackG,
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            removeDay: true,
                                            cellId: _id
                                        });
                                        break;
                                    }
                                    case 3://remove from right
                                    {
                                        _parent._historyPlanning.add({
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            parent,
                                            isNoUserAction,
                                            dayBackG,
                                            removeDay: true,
                                            self
                                        }, withoutHistory);
                                        new VBranchPlannedCell(self, {
                                            isNew: true,
                                            parent: parent,
                                            cell: _els.pop().unPlan().checkNeigbor()
                                        });
                                        _planning.updatePlannedDayState({
                                            cell: dayBackG,
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            removeDay: true,
                                            cellId: _id
                                        });
                                        break;
                                    }
                                    case 4://split in two newElems
                                    {
                                        _parent._historyPlanning.add({
                                            type: VUtils.PLANNING_ACTIONS.REMOVE_ALL,
                                            parent,
                                            isNoUserAction,
                                            dayBackG,
                                            removeDay: true,
                                            self
                                        }, withoutHistory);
                                        for (let i = 0; i < _els.length; i++) {
                                            let _e = _els[i];
                                            if (dayBackG._currentItemHor == _e._currentItemHor) {
                                                res.groups[0]._elements = _els.splice(0, i);
                                                res.groups[1]._elements = _els.splice(1, _els.length);

                                                for (let j = 0; j < 2; j++) {
                                                    let _g = new PeriodGroup(res.groups[j]);
                                                    for (let dj = 0; dj < res.groups[j]._elements.length; dj++) {
                                                        res.groups[j]._elements[dj]._periodContainer = _g;
                                                    }
                                                }
                                                new VBranchPlannedCell(self, {
                                                    isNew: true,
                                                    parent: parent,
                                                    cell: _e.unPlan()
                                                });
                                                if (res.groups[1]._elements[0]) res.groups[1]._elements[0].checkNeigbor();
                                                break;
                                            }
                                        }
                                        _planning.updatePlannedDayState({
                                            cell: dayBackG,
                                            oldId: _id,
                                            groups: res.groups,
                                            type: VUtils.PLANNING_ACTIONS.SPLIT,
                                            removeDay: true,
                                            cellId: _id
                                        });
                                        break;
                                    }
                                    default: {
                                        return VConsole.warn('No Action found');
                                    }
                                }

                                dayBackG.__parent._compare();
                                self.updateConflicts(_parent.opt.conflicts._planning.checkConflicts());
                                reslove();
                            }
                        };
                        if (dropall || dayBackG._periodContainer._elements.length < 2) dataReq.data.dropall = true;
                        if (dataReq.isWeekEnd) dataReq.data.id = dataReq._periodContainer.id;
                        self._actions._requests(dataReq);
                    })

                },
                _setNewPst: function (daysShift, startFrom, endWith, withHistory, isNoUserAction) {
                    if (daysShift === 0) return VConsole.warn('no chagnes');

                    return new Promise((resolve, reject) => {
                        let _curGroup = dayBackG._periodContainer,
                            _elsInGroup = _curGroup._elements,
                            _elsInGroupClone = _elsInGroup.concat([]),
                            parent = dayBackG.parent,
                            nextItem = dayBackG.parent.children[_elsInGroup[_elsInGroup.length - 1]._currentItemHor],
                            detailsData = {
                                site: parent.siteId,
                                branch: parent.branchId,
                                id: dayBackG._periodContainer.id
                            },
                            _startFromOr = _elsInGroup[0]._currentItemHor,
                            _endWithOr = _elsInGroup[_elsInGroup.length - 1]._currentItemHor,

                            _startFrom = typeof startFrom == 'undefined' ? _startFromOr + daysShift : startFrom,
                            _endWith = typeof startFrom == 'undefined' ? _endWithOr + daysShift : endWith,
                            dataRequest = {
                                description: '',
                                description_to_do: '',
                                start: self.daysUTC[_startFrom],
                                end: self.daysUTC[_endWith],
                                daysUpdateTime: daysShift //* 24 * 60 * 60
                            },
                            prev = parent.children[_startFrom - 1],
                            next = parent.children[_endWith + 1]
                        ;

                        if (_curGroup.description) dataRequest.description += _curGroup.description + ", ";
                        if (_curGroup.description_to_do) dataRequest.description_to_do += _curGroup.description + ", ";
                        /*---get UTC times for member planning----*/
                        let _daysMemberPlannedInUTC = _elsInGroup.map((cellElement) => {
                            return cellElement._daysUTC[cellElement._currentItemHor];
                        }), replaceCellId = [];

                        /*--set the limits---*/
                        if (!dataRequest.start) {
                            dataRequest.start = dataRequest.end >= _parent.opt.daterange.start.end ? _parent.opt.daterange.end : _parent.opt.daterange.start;
                        }
                        if (!dataRequest.end) {
                            dataRequest.end = dataRequest.start <= _parent.opt.daterange.start ? _parent.opt.daterange.start : _parent.opt.daterange.end;
                        }

                        /*---check neighbors----*/
                        if (dataRequest.start == dataRequest.end &&
                            self.daysWeekEndUTC.indexOf(dataRequest.start) > -1
                        ) {
                            //weekends
                            console.log("Weekend drag");
                        } else {
                            if (prev) {
                                prev = prev.findCloser(-1);
                                if (prev && prev._periodContainer && prev._periodContainer.id != detailsData.id) {
                                    dataRequest.start = self.daysUTC[prev._periodContainer._elements[0]._currentItemHor];
                                    if (prev._periodContainer.description) dataRequest.description += prev._periodContainer.description + ", ";
                                    if (prev._periodContainer.description_to_do) dataRequest.description_to_do += prev._periodContainer.description + ", ";
                                }

                            }
                            if (next) {
                                next = next.findCloser(1);
                                if (next && next._periodContainer && next._periodContainer.id != detailsData.id) {
                                    dataRequest.end = self.daysUTC[next._periodContainer._elements[next._periodContainer._elements.length - 1]._currentItemHor];
                                    if (next._periodContainer.description) dataRequest.description += next._periodContainer.description + ", ";
                                    if (next._periodContainer.description_to_do) dataRequest.description_to_do += next._periodContainer.description + ", ";
                                }
                            }

                        }


                        // if (dataRequest.end - dataRequest.start <= 0 )return VConsole.warn('Maybe eed to fix');
                        this.updatePlannedPeriod({
                            detailsData,
                            dataRequest,
                            _elsInGroup,
                            daysShift,
                            _elsInGroupClone,
                            nextItem,
                            replaceCellId,
                            isNoUserAction,
                            withHistory,
                            _endWithOr,
                            _startFromOr
                        }).then(resolve);
                    });
                },
                _split: function () {
                    let _elements = dayBackG._periodContainer._elements;
                    if (_elements.length < 2 || !_elements.length) {
                        return VConsole.warn('There are nothing to split');
                    } else {

                        let dataReq = {
                            type: VUtils.REUESTS.CELL.DELETE_SIGNLE,
                            detailsData: {
                                site: parent.siteId,
                                branch: parent.branchId,
                                id: dayBackG._periodContainer.id
                            },
                            data: {
                                isSplit: true,
                                date: self.daysUTC[dayBackG._currentItemHor]
                            },
                            next: (res) => {
                                let _els = dayBackG._periodContainer._elements,
                                    cellId = dayBackG._periodContainer.id;

                                for (let i = 0; i < _els.length; i++) {
                                    let _e = _els[i];
                                    if (dayBackG._currentItemHor == _e._currentItemHor) {
                                        res.groups[0]._elements = _els.splice(0, (i == 0 ? 1 : (i >= _els.length - 1 ? i : i + 1)));
                                        res.groups[1]._elements = _els;


                                        for (let j = 0; j < res.groups.length; j++) {
                                            let _g = res.groups[j],
                                                group = new PeriodGroup(_g);
                                            for (let dj = 0; dj < _g._elements.length; dj++) {
                                                _g._elements[dj]._periodContainer = group;
                                                // _g._elements[dj].__parent.removeChild(_g._elements[dj]);
                                                // console.log("sdas");
                                            }
                                        }
                                        if (res.groups[1]._elements[0]) res.groups[1]._elements[0].checkNeigbor();
                                        break;
                                    }
                                }

                                _parent._historyPlanning.add({
                                    type: VUtils.PLANNING_ACTIONS.SPLIT,
                                    groups: res.groups,
                                    parent,
                                    dayBackG,
                                    self
                                });
                                _planning.updatePlannedDayState({
                                    cell: dayBackG,
                                    oldId: cellId,
                                    groups: res.groups,
                                    type: VUtils.PLANNING_ACTIONS.SPLIT
                                });
                                dayBackG.__parent._compare();
                            }
                        };

                        self._actions._requests(dataReq);
                    }
                },
                updatePlannedPeriod: function (opt) {
                    let {detailsData, dataRequest, _elsInGroup, daysShift, _elsInGroupClone, nextItem, replaceCellId, isNoUserAction, withHistory, _endWithOr, _startFromOr} = opt;
                    return new Promise((resolve, reject) => {
                        self._actions._requests({
                            detailsData: detailsData,
                            data: dataRequest,
                            type: VUtils.REUESTS.CELL.EDIT,
                            next: (res) => {
                                if (res == VUtils.RESPONCE_STATUS.SUCCESS || res.state == 2 || res.id) {

                                    if (daysShift > 0) _elsInGroupClone = _elsInGroupClone.reverse();
                                    _elsInGroupClone.forEach((el) => {
                                        if(el.onFinishDrag)el.onFinishDrag(daysShift);
                                    });

                                    let prevGroups = new PrevPlannedAction(self.opt.parent),
                                        periodContainer = new PeriodGroup({
                                            start: dataRequest.start,
                                            end: dataRequest.end,
                                            id: dayBackG._periodContainer.id,
                                            messages: dayBackG._periodContainer.messages,
                                            description: res.description,
                                            description_to_do: res.description_to_do,
                                            _elements: []
                                        }, null, {isNew: true});
                                    for (let i = 0, counts = parent.children.length; i < counts; i++) {
                                        let cell = parent.children[i],
                                            _cellUpdate = cell;
                                        if (cell.isWeekEnd && !dayBackG.isWeekEnd /*|| cell._hasToBeNoPlanned*/) {
                                            // if(dayBackG._periodContainer) console.log(dayBackG._currentItemHor);
                                            cell.checkNeigbor();
                                            continue;
                                        }

                                        if (dataRequest.start <= self.daysUTC[cell._currentItemHor] && dataRequest.end >= self.daysUTC[cell._currentItemHor]) {

                                            if (cell._periodContainer) {
                                                if (cell._periodContainer.id != periodContainer.id) {
                                                    prevGroups.add(cell, true);
                                                    replaceCellId.push(cell._periodContainer.id);
                                                }
                                                if (cell._periodContainer.id == periodContainer.id) {
                                                    // debugger
                                                    // _cellUpdate= new VBranchPlannedCell(self,{
                                                    //     isNew: true,
                                                    //     parent: parent,
                                                    //     cell: cell,
                                                    //     periodContainer: periodContainer
                                                    // });
                                                    if (cell.isWeekEnd) {
                                                        (new VBranchPlannedCell(self, {
                                                            isNew: true,
                                                            parent: parent,
                                                            cell: cell.unPlan(true)
                                                        })).cell().checkNeigbor();
                                                        continue;
                                                    }

                                                    periodContainer.addCell(cell);
                                                } else if (cell.isWeekEnd || cell._hasToBeNoPlanned) {
                                                    // cell._periodContainer = periodContainer;
                                                    // periodContainer._elements = [cell];
                                                    continue;
                                                } else {
                                                    for (var j = 0, _elss = cell._periodContainer._elements; j < _elss.length; j++) {
                                                        periodContainer.addCell(_elss[j]);
                                                    }

                                                    if (!cell._periodContainer._elements.length) {
                                                        periodContainer.addCell(cell);
                                                    }

                                                }

                                            } else {
                                                _cellUpdate = (new VBranchPlannedCell(self, {
                                                    isNew: true,
                                                    parent: parent,
                                                    periodContainer: cell.isWeekEnd && _startFrom != cell._currentItemHor ? null : periodContainer,
                                                    cell: cell.unPlan(true)
                                                })).cell();
                                            }

                                        } else {
                                            for (let j = 0; j < _elsInGroup.length; j++) {
                                                if (cell._currentItemHor == _elsInGroup[j]._currentItemHor) {
                                                    _elsInGroup.splice(j, 1);
                                                    cell.hasToBeNoPlanned = cell._hasToBeNoPlanned;
                                                    _cellUpdate = (new VBranchPlannedCell(self, {
                                                        isNew: true,
                                                        parent: parent,
                                                        cell: cell.unPlan()
                                                    })).cell();


                                                    break;
                                                }
                                            }
                                        }

                                        if (_cellUpdate && _cellUpdate.checkNeigbor) _cellUpdate.checkNeigbor();
                                    }
                                    periodContainer._elements.sort((a, b) => a._currentItemHor > b._currentItemHor ? 1 : -1);


                                    if (nextItem) nextItem.checkNeigbor();
                                    if (replaceCellId.length) {
                                        _planning.updatePlannedDayState({
                                            cell: _elsInGroupClone[0],
                                            oldId: replaceCellId,
                                            type: VUtils.PLANNING_ACTIONS.ADD_OR_UPDATE
                                        });//on merge
                                    }

                                    _parent._historyPlanning.add({
                                        type: VUtils.PLANNING_ACTIONS.DRAG,
                                        parent,
                                        isNoUserAction,
                                        prevGroups: prevGroups,
                                        daysShift: daysShift * -1,
                                        start: _startFromOr,
                                        end: _endWithOr,
                                        index: periodContainer._elements[0]._currentItemHor
                                    }, !withHistory);

                                    _elsInGroupClone[0].__parent._compare();
                                    self.updateConflicts(_parent.opt.conflicts._planning.checkConflicts());
                                } else {
                                    VConsole.warn('Something bad on update', res);
                                }

                                resolve();
                            }
                        });
                    });
                }

            };

            dayBackG.mousedown = function (e) {
                switch (e.data.originalEvent.button) {
                    case VUtils.BUTTONS.LEFT: {
                        let
                            tempDragContainer = new PIXI.Container(),
                            _listOfCells = this.parent.children,
                            maxnX = 0,
                            limits = this.findLimits(),
                            maxWidth = limits.right,//(this.parent.children.length - (dayBackG.isWeekEnd ? 0 : self.weekends.length)) * this.width,//ignore last 2 weekend days
                            minWidth = limits.left,//0,
                            minnX = Infinity,
                            weekendsAfter = 0,
                            ignoreItemsFromEnd = 0;


                        tempDragContainer._widthCell = dayBackG.width;
                        tempDragContainer._iteIgnored = 0;
                        tempDragContainer.lastPst = tempDragContainer._lastPst = {
                            x: e.data.global.x,
                            _x: dayBackG.position.x
                        };
                        for (let i = 0, _elsInGroup = dayBackG._periodContainer._elements, prev; i < _elsInGroup.length; i++) {
                            let tempCell = VUtils.drawCell(
                                dayBackG.width, dayBackG.height,
                                {type: VUtils.TEXT_TYPE.TABLE_CELL, backGround: 0x008000, category: 15}
                            );
                            tempCell.position.x = _elsInGroup[i].position.x - dayBackG.position.x;//dayBackG.width*(i);
                            if (tempCell.position.x == 0) {
                                tempDragContainer._itemDrag = i;
                            }
                            // if (prev && dayBackG.position.x < _elsInGroup[i].position.x && _elsInGroup[i]._currentItemHor - prev._currentItemHor > 1) weekendsAfter += self.weekends.length;
                            minnX = Math.min(minnX, tempCell.position.x);
                            maxnX = Math.max(maxnX, tempCell.position.x + dayBackG.width);
                            prev = _elsInGroup[i];
                            tempDragContainer.addChild(tempCell);
                            tempDragContainer._lastItem = i;
                            tempCell._currentItemHor = _elsInGroup[i]._currentItemHor;
                        }
                        // maxnX -= weekendsAfter * dayBackG.width;

                        tempDragContainer.position.__x = tempDragContainer.position.__xS = tempDragContainer.position.x = dayBackG.position.x;

                        tempDragContainer.children.forEach((cell, key) => {
                            cell.position.startX = cell.position.x;
                        });

                        dayBackG.parent._temp.addChild(tempDragContainer);
                        tempDragContainer.checkIfNoPlanning = function (curTempCell) {
                            return _listOfCells[curTempCell._currentItemHor]._hasToBeNoPlanned ||
                                (!dayBackG.isWeekEnd && _listOfCells[curTempCell._currentItemHor].isWeekEnd);
                        }
                        tempDragContainer.checkNeighbors = function (dir) {
                            /*---check if next day in weekend----*/
                            for (let i = 0, _els = this.children, prev; i < _els.length; i++) {
                                let curTempCell = _els[i],
                                    _index = Math.floor((curTempCell.position.x + this.position.x) / this._widthCell);

                                if (_index < 0 || _index > _listOfCells.length - 1) {
                                    console.warn("out of limits--", _index);
                                    continue;
                                }
                                curTempCell._currentItemHor = _index;
                                //loop until it`s not holiday or weekend for none weekend planned
                                while (this.checkIfNoPlanning(curTempCell)) {
                                    let _newPst = curTempCell.position.x + dir * this._widthCell,
                                        _index = Math.ceil((_newPst + this.position.x) / this._widthCell);
                                    if (_index < 0 || _index > _listOfCells.length - 1) {
                                        console.warn("out of limits", _index);
                                        break;
                                    }
                                    curTempCell.position.x += dir * this._widthCell;
                                    curTempCell._currentItemHor = _index;
                                    // if (!dayBackG.isWeekEnd && _listOfCells[_els[i]._currentItemHor].isWeekEnd ) {
                                    //     _els[i].position.x += dir * self.weekends.length * this._widthCell;
                                    //     _els[i]._currentItemHor = Math.round((_els[i].position.x + this.position.x) / this._widthCell);
                                    // }
                                }

                                // if (!dayBackG.isWeekEnd &&
                                //     self.daysWeekEndUTC.indexOf(self.daysUTC[_els[i]._currentItemHor]) > -1) {
                                //     _els[i].position.x += dir * self.weekends.length * this._widthCell;
                                //     _els[i]._currentItemHor = Math.round((_els[i].position.x + this.position.x) / this._widthCell);
                                // }
                                // console.log(curTempCell._currentItemHor);
                            }

                        };

                        tempDragContainer.checkLimit = function (type) {
                            switch (type) {
                                case 1: {//max
                                    return _listOfCells[this.children[this.children.length - 1]._currentItemHor].position.x;
                                    break;
                                }
                                default: {
                                    return _listOfCells[this.children[0]._currentItemHor].position.x;
                                }
                            }
                            let _start = this.position.x + this.children[this.children.length - 1].position.x;
                            // for (let i = 0, _elsInGroup = this.children, prev; i < _elsInGroup.length; i++) {
                            //     if (i >= this._itemDrag) {
                            //         _start = this.position.x + _elsInGroup[i].position.x + dayBackG.width +
                            //             (prev && _elsInGroup[i]._currentItemHor - prev._currentItemHor > 1 ? self.weekends.length * dayBackG.width : 0);
                            //     }
                            //
                            //     prev = _elsInGroup[i];
                            // }
                            return _start;
                        }
                        tempDragContainer.checkCell = function (cell) {
                            return cell && (cell.isWeekEnd || cell._hasToBeNoPlanned);
                        }
                        tempDragContainer.updateOnDrag = function (event) {
                            let
                                _startDragedPst = this.children[this._itemDrag].position,
                                _draggedPosition = this.position.x + _startDragedPst.x,
                                start_X = 1,
                                lastS = this._dragShift;
                            if (event.x >= this._lastPst.x + this._widthCell) {//check if move to right
                                let difference = event.x - this._lastPst.x,
                                    itemDayDelta = Math.floor(difference / this._widthCell),
                                    rowIndex = Math.floor((_draggedPosition + itemDayDelta * this._widthCell) / this._widthCell);

                                // console.log(rowIndex);
                                if (this.checkCell(_listOfCells[rowIndex])) {
                                    //weekend
                                    if (this._dragShift < 0) {
                                        this._dragShift = 1;//switch from left to right dir
                                    } else {
                                        this._dragShift = itemDayDelta;// >= self.weekends.length ? self.weekends.length : 1;
                                    }
                                    console.warn("weekend or not planned")
                                } else {
                                    if (this.checkLimit(1) >= maxWidth) {
                                        return console.warn("out from row max width");
                                    }
                                    this._dragShift = 1;

                                    while (start_X <= lastS) {
                                        if (this.checkLimit(1) < maxWidth) {
                                            this.lastPst.x += this._widthCell;
                                            this.position.x += this._widthCell;

                                            // _draggedPosition = this.position.x + _startDragedPst.x;
                                            this.checkNeighbors(1);
                                            this.updateGlobalPosition();
                                        }
                                        start_X++;
                                        if (lastS > 1) {
                                            this.lastPst.x += lastS * this._widthCell;
                                            break;
                                        }

                                    }


                                }


                            } else if (event.x <= this._lastPst.x - this._widthCell) {//check if move to left
                                let itemDayDelta = Math.abs(Math.floor((event.x - this._lastPst.x + this._widthCell) / this._widthCell)),
                                    rowIndex = Math.floor((_draggedPosition - itemDayDelta * this._widthCell) / this._widthCell);

                                if (this.checkCell(_listOfCells[rowIndex])) {
                                    //weekend
                                    if (this._dragShift > 0) {
                                        this._dragShift = -1;//switch from right to lef dir
                                    } else {
                                        this._dragShift = -itemDayDelta;//>= self.weekends.length ? -self.weekends.length : -1;
                                    }
                                } else {
                                    if (this.position.x + minnX - this._widthCell < minWidth) {
                                        return console.warn("out from row min width");
                                    }
                                    this._dragShift = -1;
                                    while (start_X <= Math.abs(lastS)) {
                                        if (/*this.position.x + minnX - this._widthCell*/ this.checkLimit() >= minWidth) {
                                            // this.position.__xS -= this._widthCell;
                                            this.lastPst.x -= this._widthCell;
                                            this.position.x -= this._widthCell;
                                            this.checkNeighbors(-1);
                                            this.updateGlobalPosition();
                                        }
                                        start_X++;
                                        if (lastS < -1) {
                                            this.lastPst.x += lastS * this._widthCell;
                                            break;
                                        }
                                    }
                                }

                            }
                            // this.position.__x = this.position.x;
                        };

                        tempDragContainer._dragShift = 1;
                        tempDragContainer.updateGlobalPosition = function (_x = 0) {
                            // let pos = {x: this.position.__xS},
                            //     parent = this.parent;
                            // while (parent) {
                            //     pos.x += parent.position.x;
                            //     parent = parent.parent;
                            // }
                            //
                            // this.positionGlob = pos;
                        };
                        tempDragContainer.setNewPst = function () {
                            this.parent.removeChild(this);
                            dayBackG.customeEvents._setNewPst(
                                Math.round((tempDragContainer.position.x - dayBackG.position.x) / this._widthCell),
                                tempDragContainer.children[0]._currentItemHor,
                                tempDragContainer.children[tempDragContainer.children.length - 1]._currentItemHor,
                                true
                            );

                        };
                        tempDragContainer.updateGlobalPosition();
                        _parent._events.mouse.dragElement = tempDragContainer;
                        _parent._events.mouse.canHover = false;

                        break;
                    }
                }

            };

            dayBackG.checkMovedPlanning = function (dayShift) {
                let _from = this._daysUTC[this._currentItemHor],
                    _itemHor = this._currentItemHor,
                    _item = 0,
                    _maxItems = Math.abs(dayShift),
                    _shiftDays = 0,
                    _dir = dayShift < 0 ? -1 : 1;

                while (_item++ < _maxItems) {
                    _from += VUtils.DAY_IN_UTC * _dir;
                    _itemHor += _dir;
                    _shiftDays += _dir;

                    let _d = new Date(0);
                    _d.setUTCSeconds(_from);
                    let nextIsNotPlanned = this.isNotPlanned(_d.getTime() / 1000);
                    //should step out weekends only for none weekend planning
                    while (!this.isWeekEnd && (
                        // self.weekends.indexOf(_d.toDateString().split(" ")[0]) > -1||
                        nextIsNotPlanned
                    ) || nextIsNotPlanned) {
                        _from += VUtils.DAY_IN_UTC * _dir;
                        _itemHor += _dir;
                        _shiftDays += _dir;
                        _d = new Date(0);
                        _d.setUTCSeconds(_from);
                        nextIsNotPlanned = this.isNotPlanned(_d.getTime() / 1000);
                    }
                }
                return {_itemHor, _shiftDays};
            }
            dayBackG.isNotPlanned = function (dayUTC) {

                for (let i = 0; i < this.daysNoPlanningUTC.length; i++) {
                    if (this.daysNoPlanningUTC[i] == dayUTC || dayUTC >= this.daysNoPlanningUTC[i] && dayUTC < this.daysNoPlanningUTC[i] + VUtils.DAY_IN_UTC) {
                        return true;
                    }
                }
                return false;
            }
            dayBackG.onFinishDrag = function (dayShift, hard, replaceCellId) {

                if (dayShift) {
                    let {_itemHor, _shiftDays} = this.checkMovedPlanning(dayShift);

                    if (this._description || this._currentItemBranch.teams.length) {


                        let _cell = this.__parent.children[_itemHor];
                        if (_cell && _cell._periodContainer) {

                            if (this._description /*&& _cell._description*/) {
                                // if(_cell._description)_cell.onFinishDrag(dayShift,true);
                                _cell._description = this._description;
                                _cell._description.date = this._daysUTC[_itemHor];
                                this._description = null;
                                this.onComment();
                                _cell.onComment();
                            }

                        }

                    }

                    // if (this._currentItemBranch.teams.length) {
                    _planning.updatePlannedDayState({
                        cell: this,
                        dayShift: _shiftDays,
                        type: VUtils.PLANNING_ACTIONS.DRAG
                    });
                    // }
                }
            }
            dayBackG.onComment = function (type, data, withRelation) {
                if (type && data) {
                    switch (type) {
                        case 2: {
                            this._description = data;
                            data.date = Date._toUTCSeconds(data.date);
                            break;
                        }
                        default: {
                            this._periodContainer.description = data.description;
                            this._periodContainer.description_to_do = data.description_to_do;
                        }
                    }
                    //
                    if (!withRelation &&
                        dayBackG.__parent._relativeSameRow &&
                        dayBackG.__parent._relativeSameRow.children[dayBackG._currentItemHor]._periodContainer) {

                        // debugger
                        dayBackG.__parent._relativeSameRow.children[dayBackG._currentItemHor].onComment(type, data, true);
                    }
                    return this._periodContainer._elements.forEach((cell) => {
                        cell.onComment();
                    })
                }
                let category = VUtils.DOM_ELS.CELL_WITH_DESC;
                for (let i = 0; i < this.children.length; i++) {
                    if (this.children[i].__category == category) {
                        this.removeChild(this.children[i--]);
                    }
                }
                if (this._periodContainer &&
                    (
                        ((this._periodContainer.description || this._periodContainer.description_to_do) && this._periodContainer._elements.length &&
                            this._currentItemHor == this._periodContainer._elements[0]._currentItemHor)
                        || (this._description && (this._description.description || this._description.description_to_do))
                    )) {
                    let _l = VUtils.drawCell(25, 10, {
                        type: VUtils.TEXT_TYPE.TABLE_CELL_PLUS
                    });
                    this.addChild(_l);
                    _l.__category = category;
                    _l.position.x = this.width - _l.width;
                    _l.position.y = this.height / 2 - _l.height / 2;
                }
                return this;
            }
        } else {
            dayBackG.mouseup = function (e) {
                // e.preventDefault();
                // e.preventBubble();
                switch (e.data.originalEvent.button) {
                    case VUtils.BUTTONS.LEFT: {
                        if (!_parent._events.mouse.hasMove) {
                            dayBackG.onPlanning();
                        }
                        break;
                    }
                }

            };
        }
        dayBackG.onPlanned = function (options) {
            let {prevGroup, nextGroup, res, addInBgn, group, opt, isNoUserAction} = options,
                dayBackGNew, dayBackGNews = [];
            if (opt && opt.prevGroup) {
                let start = opt.prevGroup.start,//_elements[0]._daysUTC[opt.prevGroup._elements[0]._currentItemHor],
                    _end = opt.prevGroup.end;//start+VUtils.DAY_IN_UTC*(opt.prevGroup._elements.length-1);
                opt.prevGroup.id = res.id;
                opt.prevGroup._elements = [];

                let
                    _index = dayBackG._daysUTC.indexOf(start);
                while (start <= _end) {
                    dayBackGNews.push((new VBranchPlannedCell(self,
                        {
                            isNew: true,
                            parent: dayBackG.__parent,
                            cell: dayBackG.__parent.children[_index++],
                            periodContainer: opt.prevGroup
                        }
                    )).cell());
                    start += VUtils.DAY_IN_UTC;
                }
            } else {
                let
                    _startDate = new Date((res.begin || res.start)).getTime() / 1000,
                    _endDate = new Date((res.einde || res.end)).getTime() / 1000;
                dayBackGNew = (new VBranchPlannedCell(self,
                    {
                        isNew: true,
                        parent: dayBackG.parent,
                        cell: dayBackG,
                        periodContainer: new PeriodGroup({
                            start: _startDate,
                            end: _endDate,
                            id: res.id,
                            messages: res.messages,
                            description: res.description,
                            _elements: []
                        })
                    }
                )).cell();
                dayBackGNews.push(dayBackGNew);
                // dayBackGNew._periodContainer._elements = [];

                if (group) {
                    dayBackGNew._periodContainer = group;
                    if (addInBgn) {
                        group._elements.splice(0, 0, dayBackGNew);
                    } else {
                        group._elements.push(dayBackGNew);
                    }
                } else {
                    let replcarId = [];
                    if (prevGroup || nextGroup) {
                        if (!prevGroup || !nextGroup) VConsole.warn('UNEXPECTED CASE');

                        for (let i = 0, groupsEls = [prevGroup, nextGroup]; i < groupsEls.length; i++) {
                            let _group = groupsEls[i]._periodContainer._elements;
                            for (let j = 0, counts = _group.length; j < counts; j++) {
                                replcarId.push(_group[j]._periodContainer.id);
                                _group[j]._periodContainer = dayBackGNew._periodContainer;
                                dayBackGNew._periodContainer._elements.push(_group[j]);
                            }
                            if (i == 0) dayBackGNew._periodContainer._elements.push(dayBackGNew);
                        }

                        _planning.updatePlannedDayState({
                            cell: dayBackGNew,
                            oldId: replcarId,
                            type: 4
                        });
                    } else {
                        dayBackGNew._periodContainer._elements.push(dayBackGNew);
                    }
                }


                if (!dayBackGNew) dayBackGNew = dayBackGNews[0];
                _parent._historyPlanning.add({
                    type: VUtils.PLANNING_ACTIONS.CREATE,
                    parent,
                    isNoUserAction,
                    dayBackG: dayBackGNew
                }, opt);


            }

            dayBackGNews[0]._periodContainer.checkNeigborsWithDelta();
            dayBackGNews[0].__parent._compare();
            return dayBackGNews;
        }
        dayBackG.onPlanning = function (opt, isNoUserAction = false) {
            return new Promise((resolve, reject) => {
                // debugger;
                let parent = dayBackG.parent,
                    requestType = VUtils.REUESTS.CELL.CREATE,
                    detailsData = {
                        site: parent.siteId,
                        branch: parent.branchId
                    },
                    dataRequest = {
                        description: '',
                        description_to_do: '',
                        start: self.daysUTC[dayBackG._currentItemHor],
                        end: self.daysUTC[dayBackG._currentItemHor]
                    },
                    group,
                    addInBgn = false,
                    prev = parent.children[dayBackG._currentItemHor - 1],
                    next = parent.children[dayBackG._currentItemHor + 1],
                    isWeekend = self.daysWeekEndUTC.indexOf(self.daysUTC[dayBackG._currentItemHor]) > -1,
                    prevGroup, nextGroup;

                if (!isWeekend) {
                    if (prev) {
                        prev = prev.findCloser(-1);
                        if (prev._periodContainer) {
                            prevGroup = prev;
                            group = prev._periodContainer;
                            detailsData.id = prev._periodContainer.id;
                            dataRequest.start = self.daysUTC[prev._periodContainer._elements[0]._currentItemHor];
                            if (prev._periodContainer.description) dataRequest.description += prev._periodContainer.description + ", ";
                            if (prev._periodContainer.description_to_do) dataRequest.description_to_do += prev._periodContainer.description + ", ";
                            requestType = VUtils.REUESTS.CELL.EDIT;
                        }
                    }

                    if (next) {
                        next = next.findCloser(1);
                        if (next && next._periodContainer) {
                            nextGroup = next;
                            if (prevGroup) {
                                group = null;
                                dataRequest.start = self.daysUTC[prev._periodContainer._elements[0]._currentItemHor];
                                dataRequest.end = self.daysUTC[next._periodContainer._elements[next._periodContainer._elements.length - 1]._currentItemHor];
                                requestType = VUtils.REUESTS.CELL.CREATE;
                            } else {
                                addInBgn = true;
                                requestType = VUtils.REUESTS.CELL.EDIT;
                                group = next._periodContainer;
                                detailsData.id = next._periodContainer.id;
                                dataRequest.end = self.daysUTC[next._periodContainer._elements[next._periodContainer._elements.length - 1]._currentItemHor];
                            }

                            if (nextGroup._periodContainer.description) dataRequest.description += nextGroup._periodContainer.description + ", ";
                            if (nextGroup._periodContainer.description_to_do) dataRequest.description_to_do += nextGroup._periodContainer.description + ", ";
                        }
                    }
                }
                if (opt) {
                    if (opt.prevGroup) {
                        dataRequest = opt.prevGroup;
                    } else {
                        dataRequest.merge = true;
                    }

                }
                // console.log('cell was clicked', isWeekend);


                dayBackG.onWaiting();
                self._actions._requests({
                    detailsData: detailsData,
                    data: dataRequest,
                    type: requestType,
                    onError: () => {
                        this.finishWaiting();
                    },
                    _action: () => {
                        dayBackG.onPlanning(opt, isNoUserAction);
                    },
                    next: (res) => {
                        if (res === VUtils.RESPONCE_STATUS.SUCCESS || typeof res.id == 'number' || res.state == 2) {
                            resolve(dayBackG.onPlanned({
                                prevGroup, nextGroup, res, addInBgn, group, opt, isNoUserAction
                            }));
                        } else {
                            VConsole.warn('Some bad request', res);
                        }
                    }
                });
            })
        }
        dayBackG.finishWaiting = function () {
            for (let i = 0; i < this.children.length; i++) {
                if (this.children[i]._category == VUtils.DOM_ELS.LOADING) return;
            }
        }
        dayBackG.onWaiting = function () {
            this.finishWaiting();
            VUtils.loadingImg().then((arrayBuffer) => {
                // var start = performance.now();
                let gif = new GIF(arrayBuffer),
                    SCALE = 0.21,
                    frames = gif.decompressFrames(true),
                    dims = frames[0].dims,
                    canvas = VUtils.createCanvas(dims),
                    ctx = canvas.getContext('2d'),
                    tmpCanvas = VUtils.createCanvas(dims),
                    tmpCtx = tmpCanvas.getContext('2d'),
                    gifSprite = new PIXI.Sprite(PIXI.Texture.fromCanvas(canvas)),
                    uuid = Date.now(),
                    lastTime = Date.now(),
                    _index = 0;

                gifSprite._category = VUtils.DOM_ELS.LOADING;
                gifSprite.height = dayBackG.height;
                gifSprite.width = dims.width / dims.height * gifSprite.height;
                // gifSprite.scale.set(SCALE);
                dayBackG.addChild(gifSprite);
                gifSprite.position.x = dayBackG.width / 2 - gifSprite.width / 2;
                gifSprite.position.y = dayBackG.height / 2 - gifSprite.height / 2;

                self.opt.parent.animations.gifs.push({
                    callback: () => {
                        let frame = frames[_index],
                            fdims = frame.dims,
                            _t = Date.now();
                        if (frame.delay - (_t - lastTime) > 0) return;

                        let frameImageData = tmpCtx.createImageData(fdims.width, fdims.height);
                        frameImageData.data.set(frame.patch);
                        tmpCtx.putImageData(frameImageData, 0, 0);

                        ctx.drawImage(tmpCanvas, fdims.left, fdims.top);
                        gifSprite.texture.update();
                        _index = ++_index % frames.length;
                        lastTime = _t;
                        if (!dayBackG.parent) {
                            for (let i = 0; i < self.opt.parent.animations.gifs.length; i++) {
                                if (self.opt.parent.animations.gifs[i].uuid == uuid) {
                                    return self.opt.parent.animations.gifs.splice(i, 1);
                                }
                            }

                        }
                    },
                    uuid: uuid
                });
            })
        }
        dayBackG.findLimits = function () {
            let _listOfCells = this.parent.children,
                startFrom = 0,
                endFrom = _listOfCells.length - 1;

            while (_listOfCells[endFrom] && (_listOfCells[endFrom].isWeekEnd || _listOfCells[endFrom]._hasToBeNoPlanned)) {
                endFrom--;
            }
            while (_listOfCells[startFrom] && (_listOfCells[startFrom].isWeekEnd || _listOfCells[startFrom]._hasToBeNoPlanned)) {
                startFrom++;
            }
            return {
                left: _listOfCells[startFrom].position.x,
                right: _listOfCells[endFrom].position.x
            }
        }

        dayBackG._category = PixiViewer.ELEMENTS.CELL;
    }
}

export class VBranchButtonsCell extends VBranchCell {
    constructor(parent, dayBackG) {
        super(parent);
        this._attachToCellEvs(dayBackG);
    }
}

export class VBranchPlannedCell extends VBranchCell {
    constructor(parent, opt) {
        super(parent);
        this._cell = this._drawSell(opt);
        this._cell._actionCell = this;
    }

    cell() {
        return this._cell;
    }

    _drawSell(opt) {
        let
            fromSell = opt.cell,
            _actCell = VUtils.drawCell(
                fromSell.width,
                fromSell.height,
                {
                    isPlanned: fromSell.isPlanned,
                    isWeekEnd: fromSell.isWeekEnd,
                    hasToBeNoPlanned: fromSell.hasToBeNoPlanned || (opt.isNew ? fromSell._hasToBeNoPlanned : false),
                    type: opt.type || VUtils.TEXT_TYPE.TABLE_CELL,
                    isGroup: opt.periodContainer, category: 6
                }
            );
        // if (opt.isNew) {
        if (fromSell.__parent) {
            fromSell.__parent.removeChild(fromSell);
        } else {
            opt.parent.removeChild(fromSell);
        }
        // }
        opt.parent.removeChild(fromSell);
        opt.parent.removeChild(fromSell.__new);
        opt.parent.addChild(_actCell);
        if (fromSell._isTopHeaderEl) {
            _actCell._isTopHeaderEl = fromSell._isTopHeaderEl;

            opt.parent._table.el_cells.splice(fromSell._currentItemInElCells, 1, _actCell);
            opt.parent._table.el_days_header_cells.splice(fromSell._currentItemHor, 1, _actCell);
            opt.parent.children.splice(fromSell._currentItemHor, 0, opt.parent.children.pop());
        } else if (opt.isNew) {
            opt.parent.children.splice(fromSell._currentItemHor, 0, opt.parent.children.pop());
            opt.parent._table.el_cells.splice(fromSell._currentItemInElCells, 1, _actCell);
        } else {
            opt.parent._table.el_cells.push(_actCell);
        }

        // _actCell._id = fromSell._currentItemHor;
        fromSell.__new = _actCell;
        _actCell.__parent = opt.parent;
        _actCell._id = fromSell._currentItemHor;
        _actCell._category = fromSell._category;
        _actCell._selectedDay = fromSell._selectedDay;
        _actCell.hasToBeNoPlanned = fromSell.hasToBeNoPlanned;
        _actCell._hasToBeNoPlanned = fromSell._hasToBeNoPlanned;
        _actCell._daysUTC = fromSell._daysUTC;
        _actCell._actionCell = fromSell._actionCell;
        _actCell.isPlanned = fromSell.isPlanned;
        _actCell.position.x = fromSell.position.x;
        _actCell.isOutOfBranchRange = fromSell.isOutOfBranchRange;
        _actCell._currentItemHor = fromSell._currentItemHor;
        _actCell.daysWeekEndUTC = this.parent.daysWeekEndUTC;
        _actCell.daysNoPlanningUTC = this.parent.daysNoPlanningUTC;
        _actCell.isWeekEnd = this.parent.daysWeekEndUTC.indexOf(this.parent.daysUTC[_actCell._currentItemHor]) > -1;

        _actCell._currentItemType = fromSell._currentItemType;
        _actCell._currentItemBranch = fromSell._currentItemBranch;
        _actCell._currentItemVer = fromSell._currentItemVer;
        _actCell._currentItemInElCells = fromSell._currentItemInElCells;
        if (opt.periodContainer) {
            _actCell._description = fromSell._description;
            _actCell._periodContainer = new PeriodGroup(opt.periodContainer, _actCell, opt);
            // if(_actCell.isWeekEnd){
            //     opt.periodContainer._elements=[_actCell];
            // }else{

            // }

        }

        this._attachToCellEvs(_actCell, this.parent);
        return _actCell;
    }
}