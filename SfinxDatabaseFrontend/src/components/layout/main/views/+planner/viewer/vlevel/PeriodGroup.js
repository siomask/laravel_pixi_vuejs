export class PeriodGroup {
    constructor(opt, _actCell = null, _opt) {
        let period = opt, _same;


        this._elements = period._elements;
        this.id = period.id;
        this.description = period.description;
        this.messages = period.messages;
        Object.assign(this, period);


        if (_opt && _opt.isNew) {
            if (period instanceof PeriodGroup) {
                period.addCell(_actCell);
                return period;
            }
            PeriodGroup._items = {};
        }
        if (_same = this.checkSame(_actCell)) {
            // this._elements = _same._elements;
            // this.description = _same.description;
            // this.uuid = _same.uuid;
            _same.addCell(_actCell);
            return _same;
        } else {
            this.uuid = ++PeriodGroup.uuid;
            this.addCell(_actCell);
        }

    }

    lastDay(){
        return  this._elements[0]._daysUTC[this._elements[this._elements.length-1]._currentItemHor];
    }
    firstDay(){
        return this._elements[0]._daysUTC[this._elements[0]._currentItemHor];
    }
    checkSame(_actCell) {
        if (_actCell) {
            let brances = _actCell._currentItemType,
                _branch = _actCell._currentItemBranch,
                itemId = (this.id + "_" + brances.siteId + "_" + (_branch.id ? _branch.id : 'no_collaborator' + PeriodGroup.uuid)),
                _item = PeriodGroup._items[itemId];
            if (!_item) {
                PeriodGroup._items[itemId] = this;
            }
            /*ielse if(_item._elements.length==0){
                           _item = false;
                           PeriodGroup._items[itemId] = this;
                       }else*/
            if (!_actCell.isWeekEnd) {
                let
                    _els = _actCell.__parent.children,
                    prev = _els[_actCell._currentItemHor - 1],
                    next = _els[_actCell._currentItemHor + 1],
                    hasPrev = false;


                if (prev) {
                    if (prev.isWeekend) {
                        prev = PeriodGroup._items[itemId] = _els[prev._currentItemHor - 2];
                    }
                    if (prev && prev._periodContainer && prev._periodContainer.id == this.id) {
                        _item = hasPrev = PeriodGroup._items[itemId] = prev._periodContainer;
                    }
                }
                if (!hasPrev && next) {
                    if (next.isWeekend) {
                        next = _els[next._currentItemHor + 2];
                    }
                    if (next && next._periodContainer && next._periodContainer.id == this.id) {
                        _item = PeriodGroup._items[itemId] = next._periodContainer;
                    }
                }
            }
            return _item;
        }
    }

    compare(period) {
        return this.uuid == period.uuid;
    }

    addCell(_actCell) {
        let shouldAdd = true,
            haveIndex = [];
        if (_actCell) {
            for (let i = 0; i < this._elements.length; i++) {
                if (this._elements[i]._currentItemHor == _actCell._currentItemHor) {
                    shouldAdd = false;
                    break;
                }

            }
            if (shouldAdd) {
                this._elements.push(_actCell);
            }
            _actCell._periodContainer = this;
        }

        if(this._elements.length){
            this._elements = this._elements.filter((a) => {
                let _isUsed = haveIndex.indexOf(a._currentItemHor) < 0;
                if (_isUsed) haveIndex.push(a._currentItemHor);

                return a._periodContainer && _isUsed;
            });
            this._elements.sort((a, b) => a._currentItemHor > b._currentItemHor ? 1 : -1);
        }

        return this;
    }

    removeCell(_actCell) {
        if (_actCell) {
            for (let i = 0; i < this._elements.length; i++) {
                if (this._elements[i]._currentItemHor == _actCell._currentItemHor) {
                    this._elements.splice(i, 1);
                    break;
                }
            }
        }

        this._elements.sort((a, b) => a._currentItemHor > b._currentItemHor ? 1 : -1);
        return this.remove();
    }

    checkNeigbors() {
        this._elements[0].checkNeigbor();
        this._elements[this._elements.length - 1].checkNeigbor();
        return this;
    }

    checkNeigborsWithDelta() {
        let _els = this._elements;
        _els.sort((a, b) => a._currentItemHor > b._currentItemHor ? 1 : -1);
        let _listOfEls = _els[0].__parent.children,
            startFrom = _els[0]._currentItemHor,
            endTo = _els[_els.length - 1]._currentItemHor + 1;

        for (let i = startFrom; i <= endTo; i++) {
            if (_listOfEls[i] && _listOfEls[i]._periodContainer) {
                _listOfEls[i].checkNeigbor();
            }
        }
        return this;
    }

    remove() {
        let _same = this.checkSame(this._elements[0]);
        if (this._elements.length > 0 || !_same) return this;
        PeriodGroup._items[_same] = null;
        delete PeriodGroup._items[_same];
        return this;
    }

    checMatchedCell(id) {
        for (let i = 0; i < this._elements.length; i++) {
            if (this._elements[i]._currentItemHor == id) return true
        }
        return false;
    }

    static _reset(){
        PeriodGroup.uuid = 0;
        PeriodGroup._items = {};
    }

}

PeriodGroup.uuid = 0;
PeriodGroup._items = {};