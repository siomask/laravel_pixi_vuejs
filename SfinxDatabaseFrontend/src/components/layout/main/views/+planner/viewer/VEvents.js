import {VUtils} from './VUtils'
import {PixiViewer} from './PixiViewer'

export class VEvents {
    constructor(viewer) {
        this.viewer = viewer
        this.keys = []
        this._els = [
            {target: window, ev_name: ['resize'], callback: () => this.onResize()},
            {
                target: window,
                ev_name: ['mouseup'],
                callback: e => this.onWindowMouseUp(e)
            },
            {
                target: viewer.stage.renderer.view,
                ev_name: ['mousedown'],
                callback: e => this.onMouseDown(e)
            },
            {
                target: viewer.stage.renderer.view,
                ev_name: ['mouseup'],
                callback: e => this.onMouseUp(e)
            }, {
                target: viewer.stage.renderer.view,
                ev_name: ['mouseout'],
                callback: e => this.onMouseOut(e)
            },
            {
                target: viewer.stage.renderer.view,
                ev_name: ['mousemove'],
                callback: e => this.onMouseMove(e)
            },
            {
                target: viewer.stage.renderer.view,
                ev_name: ['contextmenu'],
                callback: e => this.onCntxMenu(e)
            },
            {
                target: document.body,
                ev_name: ['keydown'],
                callback: e => this.onKeyDown(e)
            },
            {
                target: document.body,
                ev_name: ['keyup'],
                callback: e => this.onKeyUp(e)
            },
            {
                target: viewer.stage.renderer.view,
                ev_name: ['wheel'/*, 'mousewheel', 'MozMousePixelScroll', 'onmousewheel'*/],
                callback: (e) => this.onWheel(e)
            }
        ]
        this._els.forEach((el, key) => {
            if (key == 0) el.callback()
            let handler = (el.target.addEventListener || el.target.attachEvent).bind(
                el.target
            )
            el.ev_name.forEach(evName => {
                handler(evName, el.callback)
            })
        })
        this.mouse = {canHover: true, dragElement: null}
        this.swift = {left: 0, top: 0}
        this.lastPointer = {x: 0, y: 0}
    }

    onWindowMouseUp(e) {
        // if (this.viewer._contextMenu.visible) {
        //     this.viewer._contextMenu.show();
        // }
        this.viewer.opt.domeEl.$refs.plannerCntxMenu.show();
        this.mouse.canHover = true;
        this.mouse._lastHover = null;
        this.viewer.stage.renderer.view.style.cursor = '';
    }

    onCntxMenu(e) {
        this.viewer.refreshRender()
        return VUtils.preventEvent(e)
    }

    onMouseOut(e) {
        this.viewer.opt.domeEl.$refs.plannteInfo.show();
    }

    onMouseDown(e) {
        this.viewer.refreshRender()
        if (!this.mouse.isScrollManually && this.viewer._scrollBar.currentScroll)
            this.viewer._scrollBar.resetScroll()
        this._getAbsoluteCoordinates(e, true)
        this.mouse.canHover = !(
            this.viewer._scrollBar.currentScroll || this.mouse.dragElement
        )
        this.mouse.down = e
    }

    onMouseUp(e) {
        this.viewer.refreshRender()
        let needToprevent = false,
            dragEl = this.mouse.dragElement;
        this.mouse.down = null;
        this.mouse.canHover = true;
        this.mouse.hasMove = false;
        this.mouse.dragElement = null;

        switch (e.button) {
            case VUtils.BUTTONS.LEFT: {
                //left
                if (dragEl) {
                    dragEl.setNewPst()
                }
                break
            }
            case VUtils.BUTTONS.RIGHT: {
                //right
                if (this.mouse.lastHover) {
                    this.mouse._lastHover = this.mouse.lastHover;
                    // if (this.mouse.lastHover._periodContainer) {

                    // this.viewer._contextMenu.show(
                    //     true,
                    //     VUtils.DOM_ELS.CNTX_MENU_DAY_CELL,
                    //     this._getAbsoluteCoordinates(e)
                    // )
                    this.viewer.opt.domeEl.$refs.plannerCntxMenu.show(this.mouse.lastHover, e);

                    // } else {
                    //     this.viewer._contextMenu.show(true, VUtils.DOM_ELS.CNTX_MENU_DAY_CELL, this._getAbsoluteCoordinates(e));
                    //     this.mouse.canHover = false;
                    //     needToprevent = true;
                    // }
                    this.mouse.canHover = false;
                    needToprevent = true;
                }

                break
            }
        }
        this.viewer._scrollBar.resetScroll()
        if (needToprevent) return VUtils.preventEvent(e)
    }

    onMouseMove(e) {
        this.viewer.refreshRender()
        let _p = this._getAbsoluteCoordinates(e),
            _delta = 10;

        this.viewer.opt.domeEl.$refs.plannteInfo.show();
        if (this.mouse.down) {
            if (Math.abs(e.clientX - this.mouse.down.clientX) > _delta ||
                Math.abs(e.clientY - this.mouse.down.clientY) > _delta) this.mouse.hasMove = true;
            let _drag = this.mouse.dragElement,
                _scrollDrag = this.viewer._scrollBar.currentScroll;
            if (_drag) {
                _drag.updateOnDrag(_p)
            } else if (_scrollDrag) {
                this.viewer._scrollBar.upddateOnDrag(e, _p);
            }

        } else {
            if (this.mouse.lastHover &&
                this.mouse.lastHover._periodContainer
            ) {
                this.viewer.opt.domeEl.$refs.plannteInfo.show({cell: this.mouse.lastHover}, this.mouse.lastHover.__e, e);
            }
        }
        this.lastPointer = _p;


    }

    _getAbsoluteCoordinates(e, hard) {
        if (hard) this.swift = e.target.getBoundingClientRect()
        return {x: e.clientX - this.swift.left, y: e.clientY - this.swift.top}
    }

    onKeyDown(e) {
        if (this.keys.indexOf(e.keyCode) < 0) {
            this.keys.push(e.keyCode)
        }
        switch (e.keyCode) {
            case PixiViewer.KEYS.SHIFT: {
                this.keys.SHIFT = true
                break
            }
        }
    }

    onKeyUp(e) {
        if (this.keys.indexOf(e.keyCode) > -1)
            this.keys.splice(this.keys.indexOf(e.keyCode), 1)
        switch (e.keyCode) {
            case PixiViewer.KEYS.SHIFT: {
                this.keys.SHIFT = false
                break
            }
        }
    }

    omDestroy() {
        this._els.forEach(el => {
            let handler = (
                el.target.removeEventListener || el.target.detachEvent
            ).bind(el.target)
            el.ev_name.forEach(evName => {
                handler(evName, el.callback)
            })
        })
    }

    onWheel(event) {
        var e = event || window.event;
        var delta = e.deltaY || e.detail || e.wheelDelta;
        // console.log(delta);
        if (this.mouse.dragElement) return;
        this.scrollHorizontal = this.keys.indexOf(PixiViewer.KEYS.SHIFT) > -1;
        // this.viewer.scroll.velocity = this.scrollHorizontal ? this.viewer.opt.domeEl._data.horSpeed : this.viewer.opt.domeEl._data.vertSpeed;
        this.viewer.scroll.velocity = 50;
        this.viewer.scroll.dir = delta < 0 ? 1 : -1;
        this.viewer._scrollBar.setCurrentScroll(this.scrollHorizontal);
        e.preventDefault ? e.preventDefault() : (e.returnValue = false);
        this.viewer.refreshRender();
        this.onWindowMouseUp();
    }

    onResize() {
        var ratio = 1 //window.devicePixelRatio;
        var width = this.viewer._W() * ratio
        var height = window.innerHeight-3 ; //*/this.viewer._H() * ratio;
        //                    sideNav.position.x = mainElem.width = width * 0.8;
        this.viewer._pages.forEach(page => {
            if (page) page.onResize(width, height)
        })
        this.viewer.stage.renderer.resize(width, height)
        this.viewer._scrollBar.updateScrolls()
        this.viewer.refreshRender()
    }
}
