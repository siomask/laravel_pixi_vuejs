import {Help} from "../../../../../../utils/help";

export class VUtils {
    static HOUR_IN_UTC = 60 * 60;
    static DAY_IN_UTC = 24 * Help.HOUR_IN_UTC;
    static DAY_PLANNED_PARTS = 5;


    static PLANNING_MEMBER_SEQUENCE=Help.PLANNING_MEMBER_SEQUENCE;
    static PLANNING_ACTIONS = {
        DRAG:1,
        SPLIT:2,
        REMOVE_ALL:3,
        ADD_OR_UPDATE:4,
        CREATE:5,
        DELETE_MEMBER:6,
        EDIT_MEMBER:10,
        TOGGLE_EMTY_ROWS:8,
        TOGGLE_EMTY_ROWS_ALL:9,
    };
    static TEXT_TYPE = {
        MAIN: 1,
        TABLE_CELL: 2,
        TABLE_CELL_HEAD: 3,
        SCROLLBAR: 4,
        SCROLLBAR_EL: 5,
        CNTX_EL: 6,
        TABLE_CELL_DAY_HEAD: 7,
        TABLE_CELL_BORDER_SPLIT: 8,
        TABLE_CELL_PLUS: 9
    };
    static DOM_ELS = {
        SCROLL_BAR_RIGHT: 1,
        CONTAINER_ALL: 2,
        CNTX_MENU_DAY_CELL: 3,
        CNTX_MENU_CELL: 4,
        CELL_WITH_DESC: 5,
        CELL_WITH_NEIGHBOR: 6,
        LOADING: 97
    };
    static SOME_SETTINGS = {
        HEIGHT_PADDING: 0.9,
        WIDTH_PADDING: 0.9
    };
    static GRAPHS = {};
    static RESPONCE_STATUS = {
        SUCCESS: 1
    };
    static BUTTONS = {
        LEFT: 0,
        MIDDLE: 1,
        RIGHT: 2
    };
    static REUESTS = {
        CELL: {
            CREATE: 1,
            EDIT: 2,
            CHECK_DATE_RANGE: 3,
            DELETE_SIGNLE: 4
        }
    };
    static TYPES = {
        DETAILS: 8,//20,
        CONTACT_CLIENT: 3,//20,
        CONTACT_COLLABORATOR: 1,//20,
        CONTACT_TEAM: 9,//20,
        ALL_AVAILABLE_WORKERS: 21,
        SIMPLE_BTN: 22

    };

    static createCanvas(dims){
        var canvas = document.createElement('canvas');

        canvas.width = dims.width;
        canvas.height = dims.height;

        return canvas;
    }

    static preventEvent(e) {
        e.preventDefault();
        e.stopPropagation();
        return false;
    }
    static loadingImg(e) {
        return new Promise((resolve)=>{
            if(VUtils._loadedImgArrayBuffer)return resolve(VUtils._loadedImgArrayBuffer);
            var oReq = new XMLHttpRequest();
            oReq.open("GET", 'static/img/UnitedSmartBinturong.gif', true);
            oReq.responseType = "arraybuffer";
            oReq.onload = function (oEvent) {
                var arrayBuffer = oReq.response;
                if (arrayBuffer) {
                    VUtils._loadedImgArrayBuffer = arrayBuffer;
                    resolve(VUtils._loadedImgArrayBuffer);
                }
            }
            oReq.send(null);
        })
        return false;
    }

    static getGraphic(width, height, _data, flag) {
        let borderWidth = 2,
            borderHeight = 2,
            beginFill = 0x002179,
            lineStyle = 0xffffff,
            _lineStyle = '#ffffff',
            _beginFill = '#bebebe',
            data = _data || {};

        // if(!height)console.log(width);

        switch (data.type) {
            case VUtils.TEXT_TYPE.TABLE_CELL_DAY_HEAD:
            case VUtils.TEXT_TYPE.TABLE_CELL_HEAD:
            case VUtils.TEXT_TYPE.TABLE_CELL: {
                borderWidth = 2;
                beginFill = data.backGround || 0xbebebe;
                break;
            }
            case VUtils.TEXT_TYPE.SCROLLBAR: {
                beginFill = data.backGround || beginFill;
                break;
            }
            case VUtils.TEXT_TYPE.TABLE_CELL_BORDER_SPLIT: {
                borderWidth = 0;

                beginFill = data.backGround || 0x5d5d5d;
                lineStyle = beginFill;
                break;
            }
            case VUtils.TEXT_TYPE.SCROLLBAR_EL: {
                beginFill = data.backGround || beginFill;
                break;
            }
            case VUtils.TEXT_TYPE.TABLE_CELL_PLUS:
            case VUtils.TEXT_TYPE.CNTX_EL: {
                beginFill = data.backGround ||0xffffff;
                break;
            }
        }

        if (data.isAnimation) {
            beginFill = 0xd8ff00;
            _beginFill = '#d8ff00';
        }
        if (data.hasToBeNoPlanned) {
            beginFill = 0x5d5d5d;
            _beginFill = '#5d5d5d';
        } else if (data.isGroup) {
            beginFill = 0xf31123;
            _beginFill = '#f31123';
        } else if (data.isPlanned) {
            beginFill = 0x0796ad;
            _beginFill = '#0796ad';
        } else if (data.isWeekEnd) {
            beginFill = 0x9b9b9b;
            _beginFill = '#9b9b9b';
        }
        _beginFill = beginFill.toString(16);
        while (_beginFill.length < 6) _beginFill = "0" + _beginFill;
        _beginFill = "#" + _beginFill;

        let _graph = Math.ceil(width) + '_' + Math.ceil(height) + "_" + data.type + "_" + beginFill + (data.category ? "_" + data.category : '');
        if (flag) {
            let box = new PIXI.Graphics();
            box.beginFill(beginFill);
            box.lineStyle(borderWidth, lineStyle);
            box.drawRect(0, borderHeight, width - borderWidth, height - borderHeight);
            box.endFill();
            // box.position.x = x + borderWidth/2;
            // box.position.y = y + borderWidth/2;
            return box;
        } else {

            if (VUtils.GRAPHS[_graph]) {

            } else {
                // let canvas = document.createElement('canvas');
                // canvas.width = width;
                // canvas.height = height;
                // let ctx = canvas.getContext('2d');
                // ctx.strokeStyle = _lineStyle;
                // ctx.strokeRect(0, 0, width, height);
                // ctx.fillStyle = _beginFill;
                // ctx.fillRect(1, 1, width - 2, height - 2);
                // VUtils.GRAPHS[_graph] = canvas.toDataURL();
                switch (data.type) {
                    case VUtils.TEXT_TYPE.TABLE_CELL_PLUS: {
                        // let canvas = document.createElement('canvas');
                        // canvas.width = width ||100;
                        // canvas.height = height||100;
                        // let ctx = canvas.getContext('2d');
                        // ctx.beginPath();
                        // ctx.lineWidth = width*0.1;
                        // ctx.strokeStyle = _lineStyle;
                        // ctx.moveTo(canvas.width/2, 0);
                        // ctx.lineTo(canvas.width/2, canvas.height);
                        // ctx.moveTo(0, canvas.height/2);
                        // ctx.lineTo(canvas.width, canvas.height/2);
                        // ctx.stroke();
                        // ctx.closePath();

                        // let line = new PIXI.Graphics();
                        // line.lineStyle(borderWidth, lineStyle);
                        // line.moveTo(width/2, 0);
                        // line.lineTo(width/2, height);
                        // line.moveTo(0, height/2);
                        // line.lineTo(width, height/2);
                        // line.stroke();
                        // line.closePath();
                        // line.drawStar(width/2,height/2,4,width/2);

                        let _p = PIXI.Sprite.fromImage('static/img/baseline-add-24px.svg');
                        _p.width = _p.height = width;
                        return _p
                    }
                    default:{
                        let box = new PIXI.Graphics();
                        box.beginFill(beginFill);
                        box.lineStyle(borderWidth, lineStyle);
                        box.drawRect(0, 0, width - borderWidth, height - borderWidth);
                        box.endFill();
                        VUtils.GRAPHS[_graph] =box.generateTexture();
                    }
                }

            }
            // let _p = PIXI.Sprite.fromImage(VUtils.GRAPHS[_graph]);
            let _p =new PIXI.Sprite(VUtils.GRAPHS[_graph]);
            if (width) _p.width = width;
            if (height) _p.height = height;
            return _p;
        }
    }

    static drawCell(width, height, _data) {
        // if (!_data || !_data.uniq) {
        return VUtils.getGraphic(width, height, _data, !!_data.isGraphic);
        // }

        // return new PIXI.Sprite(VUtils.getGraphic(width, height, _data));
    }

    static drawText(data) {
        let fontSize = 24,
            fill = 0x868686,
            fontWeight = 100;

        switch (data.type) {
            case VUtils.TEXT_TYPE.TABLE_CELL_HEAD: {
                fontSize = 18;
                // fontWeight = 900
                // fill = 0xffffff
                break;
            }
            case VUtils.TEXT_TYPE.TABLE_CELL_DAY_HEAD: {
                fontSize = 14;
                fontWeight = 600;
                // fill = 0xffffff
                if (data.isWeekEnd) {
                    fill = 0xcecece;
                }
                if (data.isCurrentDay) {
                    fill = 0xff0000;
                }
                break;
            }
            case VUtils.TEXT_TYPE.TABLE_CELL: {
                fontSize = 16;
                // fill = 0xffffff
                break;
            }
            case VUtils.TEXT_TYPE.CNTX_EL: {
                fill = 0x000000;
                break;
            }
        }
        return new PIXI.Text(data.text, {
            fontFamily: 'Arial',
            fontSize: fontSize,
            fontWeight: fontWeight,
            fill: fill,
            align: 'center'
        });
    }

    static randomColor(item) {
        let rgb = [],
            maxS = 255,
            counts = 3,
            maxIntensity = Math.max(VUtils.getRandomInt(item, maxS), counts);
        for (let i = 0; i < counts; i++) {
            rgb[i] = VUtils.getRandomInt(i, maxIntensity);
        }
        return 'rgb(' + rgb.join(',') + ')';
    }

    static rgb2hex(rgb) {
        rgb = rgb.match(
            /^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i
        );
        return rgb && rgb.length === 4
            ? '#' +
            ('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
            ('0' + parseInt(rgb[3], 10).toString(16)).slice(-2)
            : '';
    }

    hexTox(hex) {
        return hex.replace('#', '0x');
    }

    static getRandomInt(min, max) {
        return Math.round(min - 0.5 + Math.random() * (max - min + 1));
    }
}
