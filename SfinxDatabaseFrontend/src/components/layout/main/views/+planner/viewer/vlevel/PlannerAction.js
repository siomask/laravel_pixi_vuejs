import {VUtils} from "../VUtils";
import Vue from 'vue';
import {VConsole, Help} from '@/utils';

export class PlannerAction {
    constructor(parent){
        this.parent = parent;
        this.inProgress = false;
        this.pool = [];
    }
    _requests(opt) {
        if (!opt) return VConsole.warn('request require options');
        if (this.inProgress) {
            if(opt._action){
                this.pool.push(opt._action);
            }else{
                this.pool.push(()=>{
                    this._requests(opt);
                });
            }

            return VConsole.warn('some ajax already in progress, you should wait untill it finish');
        }
        this.inProgress = true;
        let onFinish = (res) => {
                let _d = res.data;
                this.inProgress = false;
                if (_d && _d.state == 1) {
                    return this.parent.opt.parent.opt.domeEl.$refs.popupAlert.show(_d.msg);
                }
                if (opt.next) opt.next(_d?(_d.data||_d):{id: Date.now()});
                if(this.pool.length)this.pool.shift()();

            },
            onError = (res) => {
                if ( res.data &&res.data.message) {
                    return this.parent.opt.parent.opt.domeEl.$refs.popupAlert.show(res.data.message);
                }
                if (opt.onError) opt.onError();
                this.inProgress = false;
                console.log(res);
            };

        let _old = opt.data;
        opt.data = {};
        Object.assign(opt.data, _old);
        ['date', 'start', 'end'].forEach((attr) => {
            if (_old[attr]) opt.data[attr] = Date._toCustomDate(_old[attr]);
        });
        let _url = `${process.env.API_URL}/api/v1/planner/construction-site/${opt.detailsData.site}/collaborator/${opt.detailsData.branch}/planning`;
        switch (opt.type) {
            case VUtils.REUESTS.CELL.CREATE: {

                Vue.axios.post(_url, opt.data).then((res) => {
                    onFinish(res);
                }).catch(onError);

                break;
            }
            case VUtils.REUESTS.CELL.EDIT: {
                if (opt.detailsData.id == undefined) {
                    return onError('missed reuired id');
                }
                Vue.axios.put(`${_url}/${opt.detailsData.id}`, opt.data).then((res) => {
                    onFinish(res);
                }).catch(onError);
                break;
            }
            case VUtils.REUESTS.CELL.DELETE_SIGNLE: {
                if (opt.detailsData.id == undefined) return onError('missed reuired id');
                _url += `/${opt.detailsData.id}?`;
                ['date', 'dropall', 'isSplit'].forEach((attr, index) => {
                    if (_old[attr]) _url += attr + "=" + (index == 0 ? opt.data[attr] : _old[attr]) + "&";
                });
                Vue.axios.delete(_url, opt.data).then((res) => {
                    onFinish(res);
                }).catch(onError);
                break;
            }
            default: {
                onFinish();
                return VConsole.warn('no method provided');
            }
        }
    }

}