import router from '@/routes';
import {VConsole, Help} from '@/utils';
import {VBranchPlannedCell, VBranchButtonsCell} from './VBranchCell';
import {PeriodGroup} from './PeriodGroup';
import {VLevel} from './VLevel';
import {PixiViewer} from '../PixiViewer';
import {VUtils} from '../VUtils';
import {PlannerAction} from './PlannerAction';

PIXI.RowContainer = function () {
    let _container = new PIXI.Container();
    _container.min_X = function () {
        if (this.minHorAvailable) return this.minHorAvailable;
        this.minHorAvailable = 0;
        for (let i = 0; i < this.children.length; i++) {
            if (!this.children[i].isOutOfBranchRange) return this.minHorAvailable;
            this.minHorAvailable += this.children[0].width;
        }
    }
    _container.max_X = function () {
        if (this.maxHorAvailable) return this.maxHorAvailable;
        this.maxHorAvailable = this.children.length * this.children[0].width;
        for (let i = this.children.length - 1; i >= 0; i--) {
            if (!this.children[i].isOutOfBranchRange) return this.maxHorAvailable;
            this.maxHorAvailable -= this.children[0].width;
        }
    }
    return _container;
}
PIXI.RowMirrorContainer = function (self) {
    let _container = new PIXI.RowContainer();
    _container._compare = function () {
        let _self = this,
            relativeItem = _self._relativeSameRow,
            plannings = {};

        if (!relativeItem) return;
        for (let i = 0; i < _self.children.length; i++) {
            let cell = _self.children[i],
                opositeCell = relativeItem.children[i],
                nextPpositeCell = relativeItem.children[i + 1],
                prevOpositeCell = relativeItem.children[i - 1];//oposite cell which should change

            opositeCell._description = cell._description;
            if (cell._periodContainer) {
                if (opositeCell._periodContainer) {
                    if (opositeCell._periodContainer.id == cell._periodContainer.id) {
                        let periodContainer;
                        if (plannings[cell._periodContainer.id]) {
                            periodContainer = plannings[cell._periodContainer.id];
                        } else if (
                            nextPpositeCell &&
                            !nextPpositeCell.isWeekEnd &&
                            nextPpositeCell._periodContainer &&
                            nextPpositeCell._periodContainer.id == cell._periodContainer.id
                        ) {
                            periodContainer = nextPpositeCell._periodContainer;
                        } else if (prevOpositeCell &&
                            !prevOpositeCell.isWeekEnd &&
                            prevOpositeCell._periodContainer &&
                            prevOpositeCell._periodContainer.id == cell._periodContainer.id) {
                            periodContainer = prevOpositeCell._periodContainer;
                        }

                        if (periodContainer && !opositeCell.isWeekEnd) {
                            opositeCell._periodContainer = periodContainer.addCell(opositeCell).checkNeigbors();
                        }

                        if (cell.isWeekEnd && opositeCell.isWeekEnd) {
                            opositeCell.checkNeigbor();
                        }
                    }
                    else {
                        let periodContainer;
                        if (plannings[cell._periodContainer.id]) {
                            periodContainer = plannings[cell._periodContainer.id];
                        } else if (nextPpositeCell && !nextPpositeCell.isWeekEnd && nextPpositeCell._periodContainer &&
                            nextPpositeCell._periodContainer.id == cell._periodContainer.id &&
                            ((opositeCell._periodContainer && !opositeCell._periodContainer.checMatchedCell(nextPpositeCell._currentItemHor))
                                || !opositeCell._periodContainer)
                        ) {
                            periodContainer = nextPpositeCell._periodContainer;
                        } else if (prevOpositeCell && !prevOpositeCell.isWeekEnd && prevOpositeCell._periodContainer &&
                            prevOpositeCell._periodContainer.id == cell._periodContainer.id &&
                            ((opositeCell._periodContainer && !opositeCell._periodContainer.checMatchedCell(nextPpositeCell._currentItemHor))
                                || !opositeCell._periodContainer)) {
                            periodContainer = prevOpositeCell._periodContainer;
                        } else {
                            periodContainer = plannings[cell._periodContainer.id] = cell._periodContainer.customCloneNoDeep;
                            periodContainer._elements = [];
                        }

                        let _c = (new VBranchPlannedCell(self, {
                            isNew: true,
                            parent: opositeCell.__parent,
                            cell: opositeCell,
                            periodContainer
                        }))._cell.checkNeigbor();
                        _c.isInteractive = _c.visible = opositeCell.visible;
                    }

                }
                else {
                    let periodContainer;
                    if (plannings[cell._periodContainer.id]) {
                        periodContainer = plannings[cell._periodContainer.id];
                    }

                    if (!periodContainer && nextPpositeCell) {
                        if (nextPpositeCell.isWeekEnd) {
                            nextPpositeCell = nextPpositeCell.__parent.children[nextPpositeCell._currentItemHor + 2];
                        }
                        if (nextPpositeCell && nextPpositeCell._periodContainer && nextPpositeCell._periodContainer.id == cell._periodContainer.id) {
                            periodContainer = nextPpositeCell._periodContainer;
                        }

                    }
                    if (!periodContainer && prevOpositeCell) {
                        if (prevOpositeCell.isWeekEnd) {
                            prevOpositeCell = prevOpositeCell.__parent.children[prevOpositeCell._currentItemHor - 2];
                        }
                        if (prevOpositeCell && prevOpositeCell._periodContainer && prevOpositeCell._periodContainer.id == cell._periodContainer.id) {
                            periodContainer = prevOpositeCell._periodContainer;
                        }

                        //test
                    }

                    if (!periodContainer) {
                        periodContainer = plannings[cell._periodContainer.id] = cell._periodContainer.customCloneNoDeep;
                        periodContainer._elements = [];
                    }

                    let _c = (new VBranchPlannedCell(self, {
                        isNew: true,
                        parent: opositeCell.__parent,
                        cell: opositeCell,
                        periodContainer
                    }))._cell.checkNeigbor();
                    _c.isInteractive = _c.visible = opositeCell.visible;
                    if (cell.isWeekEnd && _c.isWeekEnd) {
                        periodContainer.checkNeigborsWithDelta();
                    }
                }

            }
            else {
                if (opositeCell._periodContainer) {
                    let _c = new VBranchPlannedCell(self, {
                        isNew: true,
                        parent: opositeCell.__parent,
                        cell: opositeCell.unPlan().checkNeigbor()
                    });
                    _c.isInteractive = _c.visible = opositeCell.visible;
                }
            }
        }

    }
    return _container;
}

export class VBranches extends VLevel {
    constructor(opt) {
        super(opt);
        let urlObj = location.href.split('?')[1],
            urlParams = this.urlParams = {days: 121};
        if (urlObj) urlObj = urlObj.split('&');
        if (urlObj) {
            for (let i = 0; i < urlObj.length; i++) {
                let _obj = urlObj[i].split('=');
                if (_obj.length > 1) {
                    urlParams[_obj[0]] = _obj[1];
                }
            }
        }
        this.lastHeight = 0;
        this.lastWidth = 0;
        this.show_empty_rows = true;
        this._actions = new PlannerAction(this);
        this._allSites = new PIXI.Container();
        this._allSites._header = new PIXI.Container();
        this._allSites._body = new PIXI.Container();
        this._allSites._header._scnColumnHeader = new PIXI.Container();
        this._allSites._body._header = this._allSites._header;
        this._allSites.addChild(this._allSites._body);
        this._allSites.addChild(this._allSites._header);
        this.maxTop = 0;
        this._allSites.position.y = this.maxTop;
        this._allSites.__category = this._allSites._body.__category = VUtils.DOM_ELS.CONTAINER_ALL;
        this.heightBTWTables = this._allSites.heightBTWTables = 0;
        this.daysUTC = [];
        this.daysWeekEndUTC = [];
        this.daysNoPlanningUTC = [];
        this.weekends = ['Sat', 'Sun'];

        /*---default header for all sites---*/
        let _def = [{info: {full: ''}, data: [], _type: 10}];
        this.allTables = [];
        PeriodGroup._reset();
        this.createTable(_def, true);//create default table
        this._allSites._header._scnColumnHeader.addChild(_def.table.secondColumnHeadersContainer);
        this._allSites._header.addChild(this._allSites._header._scnColumnHeader);
        this._allSites._header.addChild(_def.table.firstColumnContainer);
        this.table = null;
        this.ids = 0;
    }

    show(show, els) {
        super.show(show);
        this.update(els);
        if (!show && !this.isAll) this.resetViews();

    }

    updateOnScroll(_shouldRecalc) {
        let mainElem = this.table,
            _parent = this.opt.parent,
            _fullWidth = _parent._W(),
            _w = _fullWidth * 0.5,
            __W = _fullWidth * 1,
            _heights = this.opt.parent._H(),//* VUtils.SOME_SETTINGS.HEIGHT_PADDING,
            scrollHoriz = _parent._events.scrollHorizontal,
            speedMove = _parent.scroll.dir * _parent.scroll.velocity;

        if (!mainElem) return;

        if (this.isAll) {
            let _width = this.opt.parent._W() * VUtils.SOME_SETTINGS.WIDTH_PADDING,
                shouldRecalc = _shouldRecalc;
            if (scrollHoriz) {
                let _allHeader = this._allSites._header._scnColumnHeader.children[0];
                if (_allHeader.position.x < -(this._allSites.maxWidth - __W)) _allHeader.position.x = -(this._allSites.maxWidth - __W);
                if (_allHeader.position.x > 0) _allHeader.position.x = 0;
                if (_allHeader.position.x <= 0 && _allHeader.position.x >= -(this._allSites.maxWidth - __W)) {
                    _allHeader.position.x += speedMove;
                }

            } else {
                let mainElem = this._allSites._body,
                    maxHeight = this._allSites.maxHeight;

                if (mainElem.position.y < -(maxHeight - _heights)) mainElem.position.y = -(maxHeight - _heights) + 1;
                if (mainElem.position.y > this.maxTop) mainElem.position.y = this.maxTop;
                if (mainElem.position.y <= this.maxTop && mainElem.position.y >= -maxHeight) {
                    mainElem.position.y += _parent.scroll.dir * _parent.scroll.velocity;

                    if (_parent.scroll.velocity && Math.abs(this.lastHeight - mainElem.position.y) > 20) {
                        this.lastHeight = mainElem.position.y;
                        shouldRecalc = true;
                    }
                }
            }
            for (let i = 0,
                     _maxHeight = this._allSites.maxHeight,
                     elementCnt = this._allSites._body,
                     _maxTop = elementCnt.position.y,
                     totalHeight = 0,
                     tables = elementCnt.children; i < tables.length; i++) {
                let table = tables[i],
                    mainElem = table;

                if (scrollHoriz) {
                    mainElem = table.secondColumn;
                    if (mainElem.position.x < -(mainElem.maxWidth - __W)) mainElem.position.x = -(mainElem.maxWidth - __W);
                    if (mainElem.position.x > 0) mainElem.position.x = 0;
                    if (mainElem.position.x <= 0 && mainElem.position.x >= -(mainElem.maxWidth - __W)) {
                        mainElem.position.x += speedMove;
                    }


                    if (!mainElem.__lastWidth) mainElem.__lastWidth = 0;
                    if (_parent.scroll.velocity && Math.abs(mainElem.__lastWidth - mainElem.position.x) > 30 && mainElem) {
                        mainElem.__lastWidth = mainElem.position.x;
                        this._updateVisibleEls(table, _width, _heights, _maxHeight, _maxTop, totalHeight);
                    }
                } else {
                    if (shouldRecalc) {
                        this._updateVisibleEls(table, _width, _heights, _maxHeight, _maxTop, totalHeight);
                    }
                }
                totalHeight += table.maxHeight - table.maxHeightHead;// + this.heightBTWTables;
                // totalHeight -=500;
            }
        } else {
            if (scrollHoriz) {
                mainElem = this.table.secondColumn;
                if (mainElem.position.x < -(mainElem.maxWidth - _w)) mainElem.position.x = -(mainElem.maxWidth - _w);
                if (mainElem.position.x > 0) mainElem.position.x = 0;
                if (mainElem.position.x <= 0 && mainElem.position.x >= -mainElem.maxWidth) {
                    mainElem.position.x += _parent.scroll.dir * _parent.scroll.velocity;
                }

                if (_parent.scroll.velocity && Math.abs(this.lastWidth - mainElem.position.x) > 30) {
                    this.lastWidth = mainElem.position.x;
                    this._updateVisibleEls(this.table._verticalContainersScroll[0]);
                }
            } else {
                for (let i = 0, _els = mainElem._verticalContainersScroll; i < _els.length; i++) {
                    let mainElem = _els[i];
                    // mainElem.maxHeight = mainElem.height;
                    if (mainElem.position.y < -(mainElem.maxHeight - _heights)) mainElem.position.y = -(mainElem.maxHeight - _heights) + 1;
                    if (mainElem.position.y > this.maxTop) mainElem.position.y = this.maxTop;
                    if (mainElem.position.y <= this.maxTop && mainElem.position.y >= -mainElem.maxHeight) {
                        mainElem.position.y += speedMove;
                        if (i > 0 && _parent.scroll.velocity && Math.abs(this.lastHeight - mainElem.position.y) > 20) {
                            this.lastHeight = mainElem.position.y;
                            this._updateVisibleEls(mainElem);
                        }
                    }
                }

            }
        }

    }

    onResize() {
        if (this.isAll) {
            //TODO
        } else {
            if (this.table) this._updateVisibleEls();
        }

    }

    _updateVisibleEls(_table, maxWidth, maxHeight, tableMaxH, tableTop, someHeight) {
        let
            _maxWidth = maxWidth || this.opt.parent._W(),
            _maxHeight = maxHeight || this.opt.parent._H(),
            _someHeight = someHeight || 0,
            table = _table || this.table,
            top = table.position.y;

        if (maxWidth) {
            top = tableTop;
        }
        for (let i = 0,
                 // _wC = PixiViewer.KEYS.WEEK_CELL,
                 els = table.el_cells,
                 left = table.secondColumn.position.x,
                 tableMaxHeight = (tableMaxH || table.maxHeight) * 0.2,
                 _length = els.length; i < _length; i++) {
            let button = els[i];

            if (!button.__parent && button.parent) button.__parent = button.parent;

            if (button._isTopHeaderEl) {
                button.visible = button.renderable =
                    ((button.position.x + left) > -720 && (button.position.x + left) < _maxWidth);
            } else {
                button.visible = button.renderable =
                    ((button.position.x + left) > -170 && (button.position.x + left) < _maxWidth)
                    && ((button.__parent.position.y + _someHeight + top) > -tableMaxHeight && (button.__parent.position.y + _someHeight + top) < _maxHeight);
            }

            button.interactive = button.isInteractive && button.visible;
            // if(button.visible){
            //     if(!button.parent)button.__parent.addChild(button);
            // }else{
            //     if(button.parent)button.__parent.removeChild(button);
            // }

            // if (button.visible)countVisible++;
            // els.updateTransform();
        }
    }

    storeAllTables() {
        for (let i = 0, totalHeight = 0, _sites = this.opt.parent.stage.loadedData.data; i < _sites.length; i++) {
            let _table = this.update(_sites[i].branches, true);
            if (!_table) return;
            if (i == 0) totalHeight = _table.maxHeightHead;
            if (_table) {
                _table.secondColumnHeadersContainer.visible = _table.secondColumnHeadersContainer.renderable = _table.firstColumnContainer.visible = _table.firstColumnContainer.renderable = false;
                _table.nameTableContainer.visible = _table.nameTableContainer.renderable = true;
                _table.position.y = totalHeight;
                totalHeight += _table.maxHeight + (_table.maxHeight > 30 ? this.heightBTWTables - _table.maxHeightHead : 0);//+;

                if (_table._COMPLEX_SITE_firstRow && _table._HEADER_EL) {
                    _table._COMPLEX_SITE_firstRow.addChild(_table._HEADER_EL);
                    _table._HEADER_EL.position.x = _table._COMPLEX_SITE_firstRow.parent.width / 2 - _table._HEADER_EL.width / 2;
                }
                this._allSites._body.addChild(_table);
            }

            this._allSites.maxHeight = this._allSites._body.maxHeight = totalHeight;
        }

        this.isAll = this._allSites.renderable = this._allSites.visible = true;
        this.container.addChild(this._allSites);

        if (!this.storeAllTables_calls) this.storeAllTables_calls = 0;
        if (this.storeAllTables_calls++ > 1) this.toggleEmptyRowsFromAllSites(null, true);

        this.updateConflicts(this.opt.parent.opt.conflicts._planning.checkConflicts());
    }

    resetListOfSites() {
        this.toggleEmptyRowsFromAllSites(true);
        this.resetViews();
        this.isAll = this._allSites.renderable = this._allSites.visible = false;
        while (this._allSites._body.children.length) {
            let _table = this._allSites._body.children[0];
            _table.position.y = _table._maxTop;
            this._allSites._body.removeChild(_table);

            _table.secondColumnHeadersContainer.visible = _table.secondColumnHeadersContainer.renderable = _table.firstColumnContainer.visible = _table.firstColumnContainer.renderable = true;
            _table.nameTableContainer.visible = _table.nameTableContainer.renderable = false;

            if (_table._SIMPLE_SITE_firstRow && _table._HEADER_EL) {
                _table._SIMPLE_SITE_firstRow.addChild(_table._HEADER_EL);
                _table._HEADER_EL.position.x = 0;// _table._SIMPLE_SITE_firstRow.parent.width / 2 - _table._HEADER_EL.width / 2;
            }
        }
    }

    resetTable() {
        while (this.container.children.length) {
            this.container.removeChild(this.container.children[0]);
        }
    }

    _checkDate(date, isMin) {
        let _d = date instanceof Date ? date : new Date(date),
            _dI = _d.getDate(),
            _day = 'Mon',
            _step = isMin ? -1 : 1;
        while (_day != (_d.toUTCString().split(",")[0])) {
            _d.setDate(_dI += _step);
            _dI = _d.getDate();
        }
        return (_d.toUTCString());
    }

    _findSameRow(rowKey) {
        for (let i = 0; i < this._allSites._body.children.length; i++) {
            if (this._allSites._body.children[i]._rows[rowKey]) return this._allSites._body.children[i]._rows[rowKey];
        }

        return false
    }

    _resetSameRows() {
        for (let i = 0; i < this._allSites._body.children.length; i++) {
            this._allSites._body.children[i]._rows = {};
        }
    }

    createTable(brances, noNeedToSave) {
        let self = this,
            paddingLeft = 10,
            paddingTop = 10,
            firstColumnFirsColWidth = 40,
            dayWidth = 35,
            _todayTime = (new Date()).toDateString(),
            _parent = this.opt.parent,
            viewer = _parent.opt.domeEl,
            noplanning = _parent.opt.noplanning || [],
            conflicts = _parent.opt.conflicts || {},
            firstColumnSecColWidth = _parent.MAX_WIDTH_FIRST_COLUMN_SECND_COLMN || 210,
            weekDays = 7,
            isSprite = !!this.urlParams.useSprite,
            plannedByAllTeams = _parent.opt.plannedDays,
            isDetailed = brances._info && brances._info.type == VUtils.TYPES.DETAILS;

        if (conflicts.conflicts) conflicts = conflicts.conflicts;

        _parent.opt.daterange.start = this._checkDate(_parent.opt.daterange.start, true);
        _parent.opt.daterange.end = this._checkDate(_parent.opt.daterange.end);
        /*---table---*/
        let table = brances.table = self.table = new PIXI.Container(),
            height = 10,
            _weekends = this.weekends,
            needToFillDatesUTS = this.daysUTC.length == 0,
            daysUTC = !needToFillDatesUTS ? this.daysUTC : [],
            DAY = 1000 * 60 * 60 * 24,
            DAY_IN_UTC = 60 * 60 * 24,
            _dateUTCSeconds = new Date(_parent.opt.daterange.start).getTime() / 1000,
            maxDate = new Date(_parent.opt.daterange.end),
            minDate = new Date(_parent.opt.daterange.start),
            days = Date.daysBetween(minDate, maxDate),
            maxWidth = Math.min(days * (dayWidth + 2 * paddingLeft) + firstColumnSecColWidth + 2 * paddingLeft + firstColumnFirsColWidth + 2 * paddingLeft, this.opt.parent._W(true));

        if (days === 0) {
            days++;
        }

        self.allTables.push(table);
        table._conflicts = [];
        table.brances = brances;
        table.position.y = table._maxTop = this._allSites._maxTop = this._allSites._body._maxTop = 0;

        self.table._rows = {};
        self.table.el_cells = [];
        self.table.el_days_header_cells = [];
        this.daysUTC = daysUTC;

        /*---FIRST COLUMN---*/
        /*---first row, HEAD---*/
        let
            firstColumnContainer = this.table.firstColumnContainer = new PIXI.Container(),
            nameTableContainer = this.table.nameTableContainer = new PIXI.Container(),
            headerData = new PIXI.Container(),
            firstRow = new PIXI.Container(),
            firstRowFirstColumn = new PIXI.Container()
        ;

        /*--name table---*/
        // firstColumnContainer.visible = firstColumnContainer.renderable = false;
        nameTableContainer.visible = nameTableContainer.renderable = false;
        nameTableContainer.addChild(VUtils.drawCell(
            maxWidth,
            (height + 2 * paddingTop),
            {type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD, category: 7}
        ));

        let buttonsContainer = new PIXI.Container();
        nameTableContainer.children[0].addChild(buttonsContainer);
        nameTableContainer.children[0].addChild(headerData);

        headerData.addChild(VUtils.drawText(
            {
                text: brances.name,
                type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD
            }
        ));
        let buttons = [];
        if (brances.length) {
            buttons = buttons.concat([
                {
                    icon: 'static/img/menu-2-512.png',
                    action: () => {
                        self.toggleEmptyRows(table);
                    }
                }
            ]);


        }
        if (typeof brances.siteId == 'number') {
            buttons = buttons.concat([
                {
                    icon: 'static/img/settings-512.png',
                    action: () => {
                        router.push({path: `client/${brances._info.contact.id}/details`})
                    }
                },
                {
                    icon: 'static/img/baseline-fast_forward-24px.svg',
                    action: () => {
                        viewer.axios
                            .post(`${process.env.API_URL}/api/v1/planner/daterange`, {
                                action: 'EARLIEST',
                                siteID: brances.siteId
                            })
                            .then(res => {
                                viewer.$store.dispatch(
                                    'navigation/changePLanerViewer',
                                    res.data
                                );
                            });
                    },
                    rotate: Math.PI
                },
                {
                    icon: 'static/img/baseline-fast_forward-24px.svg',
                    action: () => {
                        viewer.axios
                            .post(`${process.env.API_URL}/api/v1/planner/daterange`, {
                                action: 'LATEST',
                                siteID: brances.siteId
                            })
                            .then(res => {
                                viewer.$store.dispatch(
                                    'navigation/changePLanerViewer',
                                    res.data
                                );
                            });
                    }
                },
                {
                    icon: 'static/img/baseline-navigate_next-24px.svg',
                    action: () => {
                        viewer.selectEditItemSite({
                            _daysUTC: this.daysUTC
                        }, null, {
                            id: brances.siteId,
                            name: brances.name
                        });
                    }
                }
            ]);
        }

        buttons.forEach((el, key) => {
            let helpBtn = PIXI.Sprite.fromImage(el.icon);

            helpBtn.width = helpBtn.height = 15;
            helpBtn.position.x = (buttonsContainer.children.length) * helpBtn.width * 1.5;
            helpBtn.interactive = true;
            buttonsContainer.addChild(helpBtn);
            if (el.rotate) {
                helpBtn.anchor.set(0.5);
                helpBtn.rotation = el.rotate;
                helpBtn.position.x += helpBtn.width / 2;
                helpBtn.position.y += helpBtn.height / 2;
            }
            helpBtn.mouseup = function () {
                el.action();
            };
        });
        buttonsContainer.children.forEach((el, key) => {
            el._category = VUtils.TYPES.SIMPLE_BTN;
            new VBranchButtonsCell(this, el);
        });


        buttonsContainer.position.y = 6;
        buttonsContainer.position.x = 10;

        nameTableContainer.children[0].position.y = nameTableContainer.children[0].height;

        headerData.position.y = nameTableContainer.children[0].height / 2 - headerData.height / 2;
        headerData.position.x = nameTableContainer.children[0].width / 2 - headerData.width / 2;

        /*--branches---*/
        firstRow.addChild(firstRowFirstColumn);

        firstRowFirstColumn.addChild(VUtils.drawCell(
            firstColumnFirsColWidth + 2 * paddingLeft + (firstColumnSecColWidth + 2 * paddingLeft),
            height + 2 * paddingTop,
            {type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD, category: 8}
        ));
        self.table._HEADER_EL = headerData;
        self.table._SIMPLE_SITE_firstRow = firstRowFirstColumn.children[0];
        self.table._COMPLEX_SITE_firstRow = nameTableContainer.children[0];

        /*---second row, HEAD---*/
        let secondRow = new PIXI.Container(),
            secondRowFirstColumn = new PIXI.Container(),
            secondRowSecondColumn = new PIXI.Container()
        ;

        secondRow.addChild(secondRowFirstColumn);
        secondRow.addChild(secondRowSecondColumn);
        secondRowFirstColumn.addChild(VUtils.drawCell(
            firstColumnFirsColWidth + 2 * paddingLeft,
            height + 2 * paddingTop,
            {type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD, category: 8}
        ));
        secondRowFirstColumn.children[0].addChild(VUtils.drawText({
            text: 'AT',
            type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD
        }));
        secondRowFirstColumn.children[0].children[0].position.y = secondRowFirstColumn.children[0].height / 2 - secondRowFirstColumn.children[0].children[0].height / 2;
        secondRowFirstColumn.children[0].children[0].position.x = secondRowFirstColumn.children[0].width / 2 - secondRowFirstColumn.children[0].children[0].width / 2;

        secondRowSecondColumn.addChild(VUtils.drawCell(
            firstColumnSecColWidth + 2 * paddingLeft,
            height + 2 * paddingTop,
            {type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD, category: 8}
        ));
        secondRowSecondColumn.children[0].addChild(VUtils.drawText({
            text: 'Contractor/Client',
            type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD
        }));
        secondRowSecondColumn.children[0].children[0].position.y = secondRowSecondColumn.children[0].height / 2 - secondRowSecondColumn.children[0].children[0].height / 2;
        secondRowSecondColumn.children[0].children[0].position.x = secondRowSecondColumn.children[0].width / 2 - secondRowSecondColumn.children[0].children[0].width / 2;
        secondRow.position.y = height + 2 * paddingTop;
        secondRowSecondColumn.position.x = secondRowFirstColumn.children[0].width;

        /*---SECOND  COLUMN ------*/

        let secondColumn = self.table.secondColumn = new PIXI.Container(),
            secondColumnDates = self.table.secondColumnDates = new PIXI.Container(),
            secondColumnHeaders = self.table.secondColumnHeaders = new PIXI.Container(),
            firstColumnRows = self.table.firstColumnRows = new PIXI.Container(),
            secondColumnContainer = new PIXI.Container(),
            secondColumnDatesContainer = new PIXI.Container(),
            secondColumnHeadersContainer = self.table.secondColumnHeadersContainer = new PIXI.Container(),
            firstColumnRowsContainer = new PIXI.Container(),
            firstRowSecondColumnTable = new PIXI.Container(),
            secondRowSecondColumnTable = new PIXI.Container()
        ;
        secondColumnContainer.position.x = this._allSites._header._scnColumnHeader.position.x = secondRowSecondColumn.position.x + secondRowSecondColumn.width;

        secondRowSecondColumnTable.position.y = secondRow.position.y;
        secondColumn.addChild(secondColumnDatesContainer);
        secondColumn.addChild(secondColumnHeadersContainer);
        secondColumnHeadersContainer.addChild(firstRowSecondColumnTable);
        secondColumnHeadersContainer.addChild(secondRowSecondColumnTable);
        table.addChild(secondColumnContainer);
        table.addChild(firstColumnRowsContainer);
        table.addChild(nameTableContainer);

        table.addChild(firstColumnContainer);
        firstColumnContainer.addChild(firstRow);
        firstColumnContainer.addChild(secondRow);

        secondColumnContainer.addChild(secondColumn);
        firstColumnRowsContainer.addChild(firstColumnRows);
        secondColumnDatesContainer.addChild(secondColumnDates);
        secondColumnHeadersContainer.addChild(secondColumnHeaders);

        /*---next rows, GENERATED---*/

        /*-------generate planned rows for table---*/
        for (let i = 0,
                 hasNoHeader2Column = true;
             i < brances.length; i++) {

            let _branch = brances[i],
                rowKey = (brances.siteId ? brances.siteId : _branch.siteId) + "_" + (_branch.id ? _branch.id : 'no_collaborator' + i + Math.random()),
                sameRow = self._findSameRow(rowKey),
                secondRow = new PIXI.Container(),
                secondRowFirstColumn = new PIXI.Container(),
                secondRowSecondColumn = new PIXI.Container()
            ;

            secondRow.addChild(secondRowFirstColumn);
            secondRow.addChild(secondRowSecondColumn);
            firstColumnRows.addChild(secondRow);

            secondRowFirstColumn.addChild(VUtils.drawCell(
                firstColumnFirsColWidth + 2 * paddingLeft,
                height + 2 * paddingTop,
                {type: VUtils.TEXT_TYPE.TABLE_CELL, category: 9}
            ));
            secondRowFirstColumn.children[0].addChild(VUtils.drawText({
                text: _branch.info.abbreviation,
                type: VUtils.TEXT_TYPE.TABLE_CELL
            }));
            secondRowFirstColumn.children[0].children[0].position.y = secondRowFirstColumn.children[0].height / 2 - secondRowFirstColumn.children[0].children[0].height / 2;
            secondRowFirstColumn.children[0].children[0].position.x = secondRowFirstColumn.children[0].width / 2 - secondRowFirstColumn.children[0].children[0].width / 2;

            secondRowSecondColumn.addChild(VUtils.drawCell(
                firstColumnSecColWidth + 2 * paddingLeft,
                height + 2 * paddingTop,
                {type: VUtils.TEXT_TYPE.TABLE_CELL, category: 9}
            ));
            secondRowSecondColumn.children[0].addChild(VUtils.drawText({
                // text: _branch.contact ? (_branch.contact.name.length > 31 ? _branch.contact.name.substr(0, 31) : _branch.contact.name) : '',
                // text: _branch.contact ? (_branch.contact.name.length > 28 ? _branch.contact.name.substr(0, 25) : _branch.contact.name) : '',
                text: PixiViewer.checkTextLength(_branch.__name),
                type: VUtils.TEXT_TYPE.TABLE_CELL
            }));
            secondRowSecondColumn.children[0].children[0].position.y = secondRowSecondColumn.children[0].height / 2 - secondRowSecondColumn.children[0].children[0].height / 2;
            secondRowSecondColumn.children[0].children[0].position.x = secondRowSecondColumn.children[0].width / 2 - secondRowSecondColumn.children[0].children[0].width / 2;
            _branch.__row = secondRowSecondColumn;

            // self.table.el_cells.push(secondRowFirstColumn.children[0]);
            // self.table.el_cells.push(secondRowSecondColumn.children[0]);

            // secondRowFirstColumn.children[0].children[0].position.x = secondRowSecondColumn.children[0].children[0].position.x = paddingLeft;
            // secondRowFirstColumn.children[0].children[0].position.y = secondRowSecondColumn.children[0].children[0].position.y = paddingTop;
            // if(secondRowSecondColumn.children[0].children[0].width>firstColumnSecColWidth)secondRowSecondColumn.children[0].children[0].width=firstColumnSecColWidth;
            secondRow.position.y = (i + 2) * (height + 2 * paddingTop);
            secondRowSecondColumn.position.x = secondRowFirstColumn.width;

            table.maxHeight = secondRow.position.y;

            /*----second column dates---*/
            let nextCRowInsecondColumn = new PIXI.RowMirrorContainer(self);
            let nextCRowInsecondColumnTemp = new PIXI.RowMirrorContainer(self);
            nextCRowInsecondColumnTemp.position.y =nextCRowInsecondColumn.position.y = secondRow.position.y;
            nextCRowInsecondColumn._allCells = [];
            nextCRowInsecondColumn.siteId = _branch.siteId;
            nextCRowInsecondColumn.branchId = _branch.id;
            nextCRowInsecondColumn._branch = _branch;
            secondColumnDates.addChild(nextCRowInsecondColumn);
            secondColumnDates.addChild(nextCRowInsecondColumnTemp);
            nextCRowInsecondColumn._temp =nextCRowInsecondColumnTemp;
            if (!_branch._rowElements) _branch._rowElements = [secondRowFirstColumn, secondRowSecondColumn, nextCRowInsecondColumn];

            /*----mark periods in branch with different color--*/
            /*for (let p = 0; p < _branch.data.length; p++) {
             _branch.data[p]._back_color = VUtils.hexTox(VUtils.rgb2hex(VUtils.randomColor(p)));
             }*/
            // nextCRowInsecondColumn.branchId = _branch.branch;
            nextCRowInsecondColumn._table = secondRowSecondColumnTable._table = _branch._table = secondColumnHeadersContainer._table = self.table;
            if (!sameRow) {
                self.table._rows[rowKey] = nextCRowInsecondColumn;
            } else {
                _branch.teams = sameRow._branch.teams;
                sameRow._relativeSameRow = nextCRowInsecondColumn;
                nextCRowInsecondColumn._relativeSameRow = sameRow;
            }

            for (let d = 0,
                     weeks = 1,
                     _day,
                     totalWeekWidth = 0;
                 d < days; d++) {
                _day = new Date(minDate.getTime() + d * DAY);
                let _dayStr = _day.toUTCString(),
                    isCurrentDay = _todayTime == _day.toDateString(),
                    isWeekEnd = false,
                    hasToBeNoPlanned = false,
                    isOutOfBranchRange = false,
                    _selecteDay = _dateUTCSeconds + d * DAY_IN_UTC,
                    _selectedEndDay = _dateUTCSeconds + (d - 1) * DAY_IN_UTC,
                    _selectedPrevDay = _dateUTCSeconds + (d - 1) * DAY_IN_UTC,
                    isFullPlanned = /*isDetailed &&*/ plannedByAllTeams.indexOf(_selecteDay) > -1;

                for (let j = 0; j < _weekends.length; j++) {
                    if (_dayStr.indexOf(_weekends[j]) > -1) {
                        isWeekEnd = true;
                        break;
                    }
                }
                for (let j = 0; j < noplanning.length; j++) {
                    if (
                        noplanning[j].start <= _selecteDay && _selecteDay <= noplanning[j].end ||//group or entire day
                        noplanning[j].start >= _selecteDay && _selecteDay >= noplanning[j].end//simple
                    ) {
                        hasToBeNoPlanned = true;
                        break;
                    }
                }

                /*----CREATE HEADER ELS FOR SECOND TABLE COLUMN-------*/
                if (hasNoHeader2Column) {
                    if (d % weekDays == 0) {
                        let weekEl = VUtils.drawCell(
                            (dayWidth + 2 * paddingLeft) * weekDays,
                            height + 2 * paddingTop,
                            {type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD, uniq: isSprite, category: 1}
                            ),
                            weekText = VUtils.drawText({
                                text: 'Week ' + _day.getWeek() + '/' + _day.getFullYear(),
                                type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD
                            });

                        weekEl._isTopHeaderEl = true;
                        firstRowSecondColumnTable.addChild(weekEl);
                        weekEl.addChild(weekText);
                        weekEl.position.x = totalWeekWidth;
                        weekText.position.x = weekEl.width / 2 - weekText.width / 2;
                        weekText.position.y = weekEl.height / 2 - weekText.height / 2;
                        totalWeekWidth += weekEl.width;
                        secondColumn.maxWidth = table.maxWidth = this._allSites.maxWidth = this._allSites._body.maxWidth = totalWeekWidth + table.firstColumnContainer.width;

                        self.table.el_cells.push(weekEl);

                    }

                    _dayStr = _dayStr.split(' ').splice(1, 2);
                    _dayStr[0] = parseInt(_dayStr[0]);
                    let dayBack = VUtils.drawCell(
                        (dayWidth + 2 * paddingLeft),
                        height + 2 * paddingTop,
                        {
                            type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD,
                            isWeekEnd: isWeekEnd,
                            isPlanned: isFullPlanned,
                            uniq: isSprite, category: 2
                        }
                        ),
                        dayText = VUtils.drawText({
                            text: _dayStr.join(' ').toLowerCase(),
                            isWeekEnd: isWeekEnd,
                            isCurrentDay: isCurrentDay,
                            type: VUtils.TEXT_TYPE.TABLE_CELL_DAY_HEAD
                        });

                    if (needToFillDatesUTS) {
                        daysUTC.push(_selecteDay);
                        if (isWeekEnd) this.daysWeekEndUTC.push(daysUTC[daysUTC.length - 1]);
                        if (isWeekEnd || hasToBeNoPlanned) this.daysNoPlanningUTC.push(daysUTC[daysUTC.length - 1]);
                    }
                    dayBack._category = VUtils.TYPES.ALL_AVAILABLE_WORKERS;
                    dayBack._selectedDay = _selecteDay;
                    new VBranchButtonsCell(this, dayBack);

                    dayBack.position.x = d * dayBack.width;
                    dayText.position.x = dayBack.width / 2 - dayText.width / 2;
                    dayText.position.y = dayBack.height / 2 - dayText.height / 2;
                    secondRowSecondColumnTable.addChild(dayBack);
                    dayBack.addChild(dayText);
                    dayBack._isTopHeaderEl = true;
                    dayBack._currentItemHor = self.table.el_days_header_cells.length;
                    dayBack._currentItemInElCells = self.table.el_cells.length;
                    dayBack.isPlanned = isFullPlanned;
                    self.table.el_cells.push(dayBack);
                    self.table.el_days_header_cells.push(dayBack);

                }

                /*----CREATE CELLS FOR ROW-------*/
                if (noNeedToSave) continue;

                let period;
                for (let j = 0; j < _branch.data.length; j++) {
                    if (_branch.data[j].start <= daysUTC[d] && _branch.data[j].end >= daysUTC[d]) {
                        period = _branch.data[j];
                        if (!period._elements) period._elements = [];
                        if (period._elements.length > 0 && (isWeekEnd /*|| hasToBeNoPlanned*/)) {
                            period = null;
                        } else if (isWeekEnd) {
                            break;
                        }
                    }
                }


                nextCRowInsecondColumn._allCells.push((new VBranchPlannedCell(
                    this,
                    {
                        parent: nextCRowInsecondColumn,
                        cell: {
                            width: (dayWidth + 2 * paddingLeft),
                            height: height + 2 * paddingTop,
                            isWeekEnd: isWeekEnd,
                            //isPlanned: isFullPlanned,
                            hasToBeNoPlanned: period ? false : hasToBeNoPlanned,
                            _hasToBeNoPlanned: hasToBeNoPlanned,
                            isOutOfBranchRange: isOutOfBranchRange,
                            _daysUTC: daysUTC,
                            _currentItemType: brances,
                            _currentItemBranch: _branch,
                            _currentItemHor: d,
                            _currentItemVer: i,
                            position: {x: d * (dayWidth + 2 * paddingLeft)},
                            _currentItemInElCells: self.table.el_cells.length
                        },
                        periodContainer: period
                    }
                )).cell());
                if (period) {
                    //get day message
                    for (let df = 0; df < period.messages.length; df++) {
                        let _item = period.messages[df];
                        if (_item.date >= daysUTC[d] && _item.date < daysUTC[d] + DAY_IN_UTC) {
                            nextCRowInsecondColumn._allCells[d]._description = _item;
                            break;
                        }
                    }
                    nextCRowInsecondColumn._allCells[nextCRowInsecondColumn._allCells.length - 1].checkNeigbor();
                }
            }
            hasNoHeader2Column = false;

        }

        if (!table.maxHeight) table.maxHeight = 0;
        table.maxHeightHead = (height + 2 * paddingTop);
        table.maxHeight += table.maxHeightHead;
        firstColumnRows.el_cells = secondColumnDates.el_cells = table.el_cells;
        firstColumnRows.maxHeight = secondColumnDates.maxHeight = table.maxHeight;
        firstColumnRows._maxTop = secondColumnDates._maxTop = table._maxTop;
        firstColumnRows.secondColumn = secondColumnDates.secondColumn = table.secondColumn;
        table._verticalContainersScroll = [firstColumnRows, secondColumnDates];
        // secondColumnDates._shiftTop = firstColumnRows._shiftTop = -210;
        this.table = brances.table;
        this.table.siteId = brances.siteId;
        if (brances._info && (brances._info.pivot && !brances._info.pivot.planner_show_empty_rows ||!brances._info.pivot)) self.toggleEmptyRows(table, 2, true, true);
        if (!noNeedToSave) {
            self.container.addChild(table);
            this._updateVisibleEls();
        }
        return this.table;
    }

    updateDayAfterPlanning(cell) {
        if (!cell) return;
        let index = cell._currentItemHor,
            store = cell._currentItemType.table.secondColumnDates,
            _parent = this.opt.parent,
            days = _parent.opt.plannedDays,
            Day_IN_UTC = 60 * 60 * 24,
            selectedDay = cell._daysUTC[index],
            needToMark = false,
            self = this;

        for (let i = 0; i < days.length; i++) {
            if (days[i] >= selectedDay && days[i] < selectedDay + Day_IN_UTC) needToMark = true;
        }

        function updateD(cellInDay) {
            if (!cellInDay) return;
            if (needToMark) {
                if (cellInDay.isPlanned) {
                    return;//already marked
                } else {
                    cellInDay.isPlanned = true;
                }
            } else {
                if (!cellInDay.isPlanned) {
                    return;//already un  marked
                } else {
                    cellInDay.isPlanned = false;
                }
            }

            let _n = (new VBranchPlannedCell(self,
                {
                    parent: cellInDay.parent,
                    type: VUtils.TEXT_TYPE.TABLE_CELL_HEAD,
                    isNew: true,
                    cell: cellInDay
                }
            )).cell().checkNeigbor();
            _n.addChild(cellInDay.children[0]);
        }

        for (let i = 0, paddingLeft = 10; i < _parent.opt.jsonData.data.length; i++) {
            let _dataSite = _parent.opt.jsonData.data[i];
            if (_dataSite.branches && _dataSite.branches.table) {
                updateD(_dataSite.branches.table.el_days_header_cells[index]);
            }
        }
        updateD(this._allSites._header._scnColumnHeader.children[0]._table.el_days_header_cells[index]);

    }

    updateConflicts(newConflictsList) {
        let _parent = this.opt.parent;
        //remove old
        _parent.animations.conflicts = [];
        for (let i = 0; i < this.allTables.length; i++) {
            let table = this.allTables[i];

            while (table._conflicts.length) {
                let cell = table._conflicts.shift();
                cell.parent.removeChild(cell);
            }

        }

        // debugger
        // return  console.log(cellIds,days);

        for (let i = 0, plannings = {}; i < this.allTables.length; i++) {
            let table = this.allTables[i];
            for (let j = 0, lists = table._verticalContainersScroll[1]; j < lists.children.length; j++) {
                let rowWithCells = lists.children[j];
                for (let sj = 0; sj < rowWithCells.children.length; sj++) {
                    let cellElement = rowWithCells.children[sj];

                    if (cellElement._periodContainer) {
                        let _conflict = newConflictsList[cellElement._periodContainer.id];
                        if (_conflict) {
                            // debugger
                            for (let d = 0, _dayIntUtc = cellElement._daysUTC[cellElement._currentItemHor]; d < _conflict.days.length; d++) {
                                if ((_dayIntUtc == _conflict.days[d] || Math.abs(_dayIntUtc - _conflict.days[d]) <= VUtils.DAY_IN_UTC / 6)) {

                                    _conflict.days.splice(d, 1);

                                    let _actCell = VUtils.drawCell(
                                        cellElement.width,
                                        cellElement.height,
                                        {
                                            isAnimation: true,
                                            type: VUtils.TEXT_TYPE.TABLE_CELL,
                                            category: 4
                                        }
                                    );
                                    // _actCell.position.x = cellElement.position.x;
                                    cellElement.addChild(_actCell);
                                    _parent.animations.conflicts.push(function (delta) {
                                        _actCell.alpha = delta;
                                    });
                                    table._conflicts.push(_actCell);
                                    break;
                                }
                            }
                        } else if (cellElement._hasToBeNoPlanned) {
                            // if (!plannings[cellElement._currentItemHor] /*&& _parent.opt.conflicts._planning.hasPlannings(cellElement)*/) {
                            //     plannings[cellElement._currentItemHor] = true;
                            let _actCell = VUtils.drawCell(
                                cellElement.width,
                                cellElement.height,
                                {
                                    isAnimation: true,
                                    type: VUtils.TEXT_TYPE.TABLE_CELL,
                                    category: 4
                                }
                            );
                            // _actCell.position.x = cellElement.position.x;
                            cellElement.addChild(_actCell);
                            _parent.animations.conflicts.push(function (delta) {
                                _actCell.alpha = delta;
                            });
                            table._conflicts.push(_actCell);
                            // }

                        }


                    }
                }
            }
        }

    }

    resetViews() {
        if (this.isAll) {
            this._allSites._body.position.y = this._allSites._maxTop;
            this._allSites._header._scnColumnHeader.children[0].position.x = 0;
            for (let i = 0, tables = this._allSites._body.children; i < tables.length; i++) {
                tables[i].position.y = tables[i]._maxTop;
                tables[i].secondColumn.position.x = 0;
            }
        } else {
            if (this.table) {
                this.table.position.y = this.table._maxTop;
                this.table.secondColumn.position.x = 0;
            }
        }
        this.updateOnScroll(true);
    }

    update(brances, flag) {
        if (this.isAll) return;
        if (!brances) return;
        this.branches = brances;
        if (!flag) this.resetTable();
        if (brances.table) {
            this.table = brances.table;
            this._updateVisibleEls();
            if (flag) {
                return this.table;
            }
            return this.container.addChild(brances.table);
        }

        return this.createTable(brances);

    }

    destroy() {
        this.container.parent.removeChild(this.container);
    }

    toggleEmptyRows(table, _type, noneedToRecreateScrollbards, withoutHistory, isNoUserAction) {
        let show = false,
            self = this,
            noAction = [1, 2].indexOf(_type) < 0;

        if (!withoutHistory) self.opt.parent._historyPlanning.add({
            type: VUtils.PLANNING_ACTIONS.TOGGLE_EMTY_ROWS,
            self,
            isNoUserAction,
            table
        });
        if (noAction) {
            show = this.updateSiteSettings(table,undefined,table.is_emty_rows);
        }
        for (let i = 0; i < table.brances.length; i++) {
            let _branch = table.brances[i],
                toggle = true;

            for (let j = 0; j < _branch._rowElements[2].children.length; j++) {
                let obj = _branch._rowElements[2].children[j];
                if (obj._periodContainer) {
                    toggle = false;
                    break;
                }
            }

            if (toggle) {
                let _rowColumn = _branch._rowElements[0];

                if (_type == 1) {//show anyway
                    if (_rowColumn.visible) continue;
                    show = true;
                } else if (_type == 2) {//hide anyway
                    if (!_rowColumn.visible) continue;
                    show = false;
                } else {
                    // show = !_rowColumn.visible;
                }

                if (show === _rowColumn.visible) {
                    "no need to refresh"
                    continue;
                }

                let shift = (show ? 1 : -1) * _rowColumn.height;
                _branch._rowElements.forEach((el) => {
                    el.visible = el.renderable = show;
                });

                for (let is = i; is < table.brances.length; is++) {
                    table.brances[is]._rowElements.forEach((el) => {
                        el.position.y += shift;
                    });
                }

                table.maxHeight += shift;
                table._maxTop += shift;
                table._verticalContainersScroll[0].maxHeight = table._verticalContainersScroll[1].maxHeight = table.maxHeight;
                table._verticalContainersScroll[0]._maxTop = table._verticalContainersScroll[1]._maxTop = table._maxTop;
            }

        }
        table.is_emty_rows = show;
        this._checkSpacesAfterToggleEmptyRows();

        if (noAction && !noneedToRecreateScrollbards) {
            this.opt.parent._scrollBar.updateScrolls();
            this.updateOnScroll();
        }
        return show;

    }

    _checkSpacesAfterToggleEmptyRows() {
        if (this.isAll) {
            let totalHeight = 0;//test
            for (let i = 0, tables = this._allSites._body.children; i < tables.length; i++) {
                let _table = tables[i];
                if (i == 0) totalHeight = _table.maxHeightHead;

                _table.position.y = totalHeight;
                totalHeight += _table.maxHeight + (_table.maxHeight > 30 ? this.heightBTWTables - _table.maxHeightHead : 0);//+;
            }
            this._allSites.maxHeight = this._allSites._body.maxHeight = totalHeight;
        }
    }

    toggleEmptyRowsFromAllSites(hard, hard1, withoutHistory, isNoUserAction) {
        let self = this,
            show = this.show_empty_rows = hard ? this.show_empty_rows : !this.show_empty_rows;
        // if (!show && hard) return;
        // if (!hard) {
        //
        // }
        if (hard1) show = false;

        for (let i = 0, tables = this._allSites._body.children; i < tables.length; i++) {
            this.updateSiteSettings(tables[i], show);
            this.toggleEmptyRows(tables[i], show ? 1 : 2, false, true);
        }
        if (!withoutHistory) self.opt.parent._historyPlanning.add({
            type: VUtils.PLANNING_ACTIONS.TOGGLE_EMTY_ROWS_ALL,
            self,
            isNoUserAction
        });

        this.opt.parent._scrollBar.updateScrolls();
        this.updateOnScroll();

    }

    updateSiteSettings(table, _show, prev) {
        let _site = table.brances._info,
            show = _show;
        if (!_site) return;
        if (typeof _show == "undefined") {
            if(!_site.pivot)_site.pivot={};
            show = _site.pivot.planner_show_empty_rows = !_site.pivot.planner_show_empty_rows;
            if (prev === show) show = _site.pivot.planner_show_empty_rows = !show;
            console.log("upate");
        }
        this.opt.parent.opt.domeEl.$store.dispatch('constructionSite/editItem', {
            planner_show_empty_rows: show,
            clientId: _site.contact.id,
            id: _site.id
        });
        return show;
    }
}

export class BranchRow {
    constructor() {
        let row = new PIXI.Container();
        row._compare = function () {
            let _self = this,
                relativeItem = _self._relativeSameRow,
                plannings = {};

            if (!relativeItem) return;
            for (let i = 0; i < _self.children.length; i++) {
                let cell = _self.children[i],
                    opositeCell = relativeItem.children[i],
                    nextPpositeCell = relativeItem.children[i + 1],
                    prevOpositeCell = relativeItem.children[i - 1];//oposite cell which should change

                // if (opositeCell &&opositeCell.isWeekEnd && opositeCell._periodContainer){
                //     debugger
                //     // console.log(12);
                // }
                opositeCell._description = cell._description;
                if (cell._periodContainer) {
                    if (opositeCell._periodContainer) {
                        if (opositeCell._periodContainer.id == cell._periodContainer.id) {
                            let periodContainer;
                            if (plannings[cell._periodContainer.id]) {
                                periodContainer = plannings[cell._periodContainer.id];
                            } else if (nextPpositeCell && !nextPpositeCell.isWeekEnd && nextPpositeCell._periodContainer
                            ) {
                                periodContainer = nextPpositeCell._periodContainer;
                            } else if (prevOpositeCell && !prevOpositeCell.isWeekEnd && prevOpositeCell._periodContainer) {
                                periodContainer = prevOpositeCell._periodContainer;
                            }

                            if (periodContainer && !opositeCell.isWeekEnd) {
                                opositeCell._periodContainer = periodContainer.addCell(opositeCell).checkNeigbors();
                            }
                        } else {
                            let periodContainer;

                            if (plannings[cell._periodContainer.id]) {
                                periodContainer = plannings[cell._periodContainer.id];
                            } else if (nextPpositeCell && !nextPpositeCell.isWeekEnd && nextPpositeCell._periodContainer &&
                                nextPpositeCell._periodContainer.id == cell._periodContainer.id &&
                                ((opositeCell._periodContainer && !opositeCell._periodContainer.checMatchedCell(nextPpositeCell._currentItemHor)) || !opositeCell._periodContainer)
                            ) {
                                periodContainer = nextPpositeCell._periodContainer;
                            } else if (prevOpositeCell && !prevOpositeCell.isWeekEnd && prevOpositeCell._periodContainer &&
                                prevOpositeCell._periodContainer.id == cell._periodContainer.id &&
                                ((opositeCell._periodContainer && !opositeCell._periodContainer.checMatchedCell(nextPpositeCell._currentItemHor)) || !opositeCell._periodContainer)) {
                                periodContainer = prevOpositeCell._periodContainer;
                            } else {
                                periodContainer = plannings[cell._periodContainer.id] = cell._periodContainer.customCloneNoDeep;
                                periodContainer._elements = [];
                            }

                            let _c = (new VBranchPlannedCell(self, {
                                isNew: true,
                                parent: opositeCell.__parent,
                                cell: opositeCell,
                                periodContainer
                            }))._cell.checkNeigbor();
                            _c.isInteractive = _c.visible = opositeCell.visible;
                        }

                    } else {
                        let periodContainer;
                        if (plannings[cell._periodContainer.id]) {
                            periodContainer = plannings[cell._periodContainer.id];
                        } else if (nextPpositeCell && !nextPpositeCell.isWeekEnd && nextPpositeCell._periodContainer &&
                            nextPpositeCell._periodContainer.id == cell._periodContainer.id
                        ) {
                            periodContainer = nextPpositeCell._periodContainer;
                        } else if (prevOpositeCell && !prevOpositeCell.isWeekEnd && prevOpositeCell._periodContainer &&
                            prevOpositeCell._periodContainer.id == cell._periodContainer.id
                        ) {
                            periodContainer = prevOpositeCell._periodContainer;
                        } else {
                            periodContainer = plannings[cell._periodContainer.id] = cell._periodContainer.customCloneNoDeep;
                            periodContainer._elements = [];
                        }

                        let _c = (new VBranchPlannedCell(self, {
                            isNew: true,
                            parent: opositeCell.__parent,
                            cell: opositeCell,
                            periodContainer
                        }))._cell.checkNeigbor();
                        _c.isInteractive = _c.visible = opositeCell.visible;
                    }
                } else {
                    if (opositeCell._periodContainer) {
                        let _c = new VBranchPlannedCell(self, {
                            isNew: true,
                            parent: opositeCell.__parent,
                            cell: opositeCell.unPlan().checkNeigbor()
                        });
                        _c.isInteractive = _c.visible = opositeCell.visible;
                    }
                }
            }

        }
        return row;
    }
}


