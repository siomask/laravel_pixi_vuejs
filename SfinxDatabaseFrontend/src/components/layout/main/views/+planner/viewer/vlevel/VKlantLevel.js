
import {VLevel} from './VLevel';
import {PixiViewer} from '../PixiViewer';

export class VKlantLevel extends VLevel {
    constructor(opt) {
        super(opt);

        let _self = this,
            mainElem = this.mainElem = new PIXI.Container(),
            sideNav = this.sideNav = new PIXI.Container(),
            sideNavBack = this.sideNav.sideNavBack = new PIXI.Sprite.from(PIXI.Texture.WHITE),
            _child = new PIXI.Text('Details', {
                fontFamily: 'Arial',
                fontSize: 24,
                fill: 0x000000,
                align: 'center'
            });

        mainElem.position.y = 50;
        this.container.addChild(mainElem);
        this.container.addChild(sideNav);
        sideNav.addChild(sideNavBack);
        sideNav.addChild(_child);

        (function addElements() {
            if (opt.jsonData) {
                for (let i = 0, width = 0, y = 0, els = opt.jsonData.data; i < els.length; i++) {
                    let curEl = els[i];
                    let _s = _self._createEl({
                        text: curEl.info ? curEl.info.title : curEl.site, onMouseUp: ()=> {
                            opt.parent.updatePage(PixiViewer.PAGES.BRANCHES, curEl.branches);
                        }
                    }, width, y);
                    width = _s.position.x + _s._textInfo.width;
                    if (_s.sholdBeNextRow || width > _self.opt.parent._W() * 0.8) {
                        width = _s.sholdBeNextRow || 0;
                        y++
                    }
                }
            }
        })()
    }

    _createEl(data, x, y) {
        var padding = 10;
        var paddingLeft = 10;
        var paddingTop = 50;
        var sprite = new PIXI.Sprite();
        let self = this;

        var _child = new PIXI.Text(data.text, {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'center'
        });

        var textbg = new PIXI.Graphics();
        textbg.beginFill(0xe1e1e1, 1);
        textbg.drawRoundedRect(-padding, -padding, _child.width + 2 * padding, _child.height + 2 * padding, 2, 2);
        textbg.endFill();
        sprite._textbg = textbg;
        sprite._textInfo = _child;
        sprite.addChild(textbg);
        sprite.addChild(_child);

        var nextX = paddingLeft + x + ( 2 * padding + 5),
            nexRow = nextX + _child.width > self.opt.parent._W() * 0.8;
        sprite.position.x = paddingLeft + (nexRow ? 0 : x) + ( 2 * padding + 5);
        sprite.position.y = paddingTop + (y + (nexRow ? 1 : 0)) * (_child.height + 2 * padding + 5);
        sprite.sholdBeNextRow = nexRow ? sprite.position.x + _child.width : 0;

        this.mainElem.addChild(sprite);
        sprite.interactive = true;
        sprite.alpha = 0.8;
        sprite.mouseover = function () {
            this.alpha = 1;
            self.opt.parent.stage.renderer.view.style.cursor = 'pointer';
        }
        sprite.mouseout = function () {
            this.alpha = 0.8;
            self.opt.parent.stage.renderer.view.style.cursor = '';
        }
        sprite.mouseup = function () {
            self.opt.parent.stage.renderer.view.style.cursor = '';
            if (data.onMouseUp)return data.onMouseUp();
            // if (!data._childs || !data._childs.length)return alert('Nothing here');
            // self.updateViewElms(data._childs, data._parent);
        }
        return sprite;

    }

    onResize(width, height) {
        this.sideNav.sideNavBack.width = width * 0.2;
        this.sideNav.sideNavBack.height = /*mainElem.height = */height;
        this.sideNav.position.x = width * 0.8;
    }
}
