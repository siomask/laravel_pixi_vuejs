import {VUtils} from "../VUtils";

export class PrevPlannedAction {
    constructor(_parent) {
        this.indexes = [];
        this.groups = [];
        this._parent = _parent;
        this.id = Math.random();
    }

    add(cell, withoutRestoreGroup) {
        if (this.indexes.indexOf(cell._periodContainer.id) < 0) {
            this.indexes.push(cell._periodContainer.id);
            this.groups.push(new PrevPlannedGroup(cell, withoutRestoreGroup));
        }
    }

    restore(isNoUserAction) {
        let self = this,
            pixiviewer = self._parent,//.opt.parent
            _planning = pixiviewer.opt.conflicts._planning,
            axios = pixiviewer.opt.domeEl.axios,
            resultGroups = [];
        return new Promise((reslove, reject) => {
            (function restorePlanningGroups(listOfgroups) {
                if (listOfgroups.length) {
                    let group = listOfgroups.shift(),
                        _cell = group.cell.__parent.children[group.cell._currentItemHor],
                        planningD ={};
                    group.cell = null;
                    if (group.withoutRestoreGroup) planningD={prevGroup :group};
                    _cell.onPlanning((isNoUserAction && !group.withoutRestoreGroup?null:planningD),isNoUserAction).then((el) => {
                        let _firstElem = el[0],
                            planningData = group.planningData,
                            _url = `${process.env.API_URL}/api/v1/planner/construction-site/${
                                _firstElem.parent.siteId}/collaborator/${_firstElem.parent.branchId}/planning/${
                                _firstElem._periodContainer.id}/details`;
                        resultGroups.push(el);
                        (function restorePlanningData(data) {
                            if (data.length) {
                                let _item = data.shift(),
                                    _index = el.length - (data.length + 1),
                                    cell = el[(_index < 0 ? 0 : _index)];
                                if (!cell) return console.warn('Bad action!!!');

                                // noinspection JSAnnotator
								function restorePlanningMember() {

                                    if (_item.plannings.length) {
                                        let _planningMember = _item.plannings.shift(),
                                            teamMember = _planningMember.teamMember,
                                            teamId = _planningMember.teamId;

                                        _planningMember.hard = true;
                                        _planningMember.date = Date._toCustomDate(_planningMember.date);
                                        axios.post(`${_url}`, _planningMember).then(function (res) {
                                            _planning.updatePlannedDayState({
                                                cell,
                                                teamMember,
                                                teamId: teamId,
                                                type: VUtils.PLANNING_ACTIONS.CREATE,
                                                planning: res.data.data
                                            });
                                            restorePlanningMember();
                                        })
                                    } else {
                                        restorePlanningData(data);
                                    }
                                }

                                if (_item.description && _item.description.description) {
                                    delete _item.description.id;
                                    _item.description.date = Date._toCustomDate(_item.description.date);
                                    axios.post(`${_url}/message`, _item.description).then(function (res) {
                                        cell.onComment(2, res.data);
                                        restorePlanningMember();
                                    })
                                } else {
                                    restorePlanningMember()
                                }
                            } else {
                                restorePlanningGroups(listOfgroups);
                            }

                        })(planningData)
                    });
                } else {
                    pixiviewer._pages[0].updateConflicts(_planning.checkConflicts());
                    console.warn("fininsh restore");
                    reslove(resultGroups);
                }
            })(this.groups);

        })

    }
}

export class PrevPlannedGroup {
    constructor(cell, withoutRestoreGroup) {
        let period = cell._periodContainer,
            _start = period._elements[0]._daysUTC[period._elements[0]._currentItemHor],
            items = [],
            _c = period._elements.sort((a, b) => a._currentItemHor > b._currentItemHor ? 1 : -1).filter((el) => {
                let hasAlready = items.indexOf(el._currentItemHor) < 0;
                if (hasAlready) items.push(el._currentItemHor);
                return hasAlready;
            });
        this.withoutRestoreGroup = withoutRestoreGroup;
        this.description = period.description;
        this.start = _start;
        this.end = _start + VUtils.DAY_IN_UTC * (_c.length - 1);
        this.cell = {
            __parent: cell.__parent,
            _currentItemHor: cell._currentItemHor
        };
        this.planningData = [];


        for (let i = 0; i < period._elements.length; i++) {
            let _item = period._elements[i];
            if(!withoutRestoreGroup && cell._currentItemHor !=_item._currentItemHor){
                continue;//only day planned
            }
            this.planningData.push(new PrevPlaningDayPeriod({
                teams: _item._currentItemBranch.teams.customClone,
                period_id: period.id,
                day: _item._daysUTC[_item._currentItemHor],
                description: _item._description
            }));
        }


    }
}

export class PrevPlaningDayPeriod {
    constructor(dayOpt) {
        this.period_id = dayOpt.id || dayOpt.period_id;
        this.day = dayOpt.day;
        this.description = dayOpt.description;
        this.plannings = [];

        let teams = dayOpt.teams;
        for (let i = 0; i < teams.length; i++) {
            let team = teams[i];
            for (let j = 0; j < team.teamMembers.length; j++) {
                let teamM = team.teamMembers[j];
                teamM.plannings.filter((planning) => {
                    return planning.date == dayOpt.day && planning.collaborator_planning_entry_id == dayOpt.period_id;
                }).forEach((planning) => {
                    let _p = planning.customClone;
                    _p.teamId = team.id;
                    _p.teamMember = {id: teamM.id};
                    this.plannings.push(_p);
                })
            }
        }
    }
}