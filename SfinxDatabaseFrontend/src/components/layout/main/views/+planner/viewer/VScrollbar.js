
import {VUtils} from './VUtils';
export class VScrollbar {
    constructor(viewer) {
        this.viewer = viewer;
        this._size = 0.2;
        this.backColor = 0xeaeaea;
        this.barColor = 0x3e3e3e;
        this.paddind = 2;
        this.size = 10;
        this.borderRadius = 10;
        this.visible = false;
        this.children = [];
        this.currentScroll = this.mainElem = null;
    }


    setCurrentScroll(isShift) {
        if (isShift && (!this.bottomScroll || !this.bottomScroll.visible))return;
        if (!isShift && (!this.rightScroll || !this.rightScroll.visible))return;
        this._setScroll(isShift ? this.bottomScroll.children[0] : this.rightScroll.children[0]);
    }

    _setScroll(scroll, manually) {
        this.currentScroll = scroll;
        this.viewer._events.mouse.isScrollManually = manually;
    };

    resetScroll() {
        this.viewer._scrollBar.currentScroll = null
    }

    updateScrolls() {
        while (this.children.length)this.viewer.stage.removeChild(this.children.shift());
        if (!this.mainElem)return;
        this.updateRightScrollBar();
        this.updateBottomScrollBar();
        this._updateRendereable();
    }

    updateOnWheel() {
        if (this.mainElem && this.currentScroll) {
            let mainElem = this.mainElem;
            if (this.currentScroll.__category == VUtils.DOM_ELS.SCROLL_BAR_RIGHT) {
                if (mainElem.__category != VUtils.DOM_ELS.CONTAINER_ALL) {
                    mainElem = mainElem.firstColumnRows;
                }
                if (mainElem.position.y <= mainElem._maxTop && mainElem.position.y >= -(mainElem.maxHeight )) {
                    this.rightScroll.children[0].position.y = Math.abs((mainElem.position.y) / mainElem.maxHeight) * this.rightScroll.height;
                }

            } else {
                let _width = this.viewer._W();
                if (mainElem.__category == VUtils.DOM_ELS.CONTAINER_ALL) {
                    if (mainElem.children.length) {
                        let _subEl = mainElem.children[0].secondColumn;
                        if (_subEl.position.x <= 0 && _subEl.position.x >= -mainElem.maxWidth) {
                            this.bottomScroll.children[0].position.x = Math.abs((_subEl.position.x) / mainElem.maxWidth) * this.bottomScroll.width;
                        }
                        if (this.bottomScroll.children[0].position.x + this.bottomScroll.children[0].width > _width) {
                            this.bottomScroll.children[0].position.x = _width - this.bottomScroll.children[0].width;
                        }
                    }
                } else {
                    mainElem = mainElem.secondColumn;
                    if (mainElem.position.x <= 0 && mainElem.position.x >= -mainElem.maxWidth) {
                        this.bottomScroll.children[0].position.x = Math.abs((mainElem.position.x) / mainElem.maxWidth) * this.bottomScroll.width;
                    }
                    if (this.bottomScroll.children[0].position.x + this.bottomScroll.children[0].width > _width) {
                        this.bottomScroll.children[0].position.x = _width - this.bottomScroll.children[0].width;
                    }
                }
            }
        }
    }

    upddateOnDrag(e, _p) {
        // if(this.inProcess)return console.log(11);
        // this.inProcess = true;
        let mainElem = this.mainElem,
            updateElsVisiilityOnPage = (...args)=> {
                if (this.viewer.currentPage){
                    this.viewer.currentPage._updateVisibleEls(...args);
                }
            };
        if (mainElem && this.currentScroll) {
            if (this.currentScroll.__category == VUtils.DOM_ELS.SCROLL_BAR_RIGHT) {
                let _heights = this.viewer._H() * VUtils.SOME_SETTINGS.HEIGHT_PADDING,
                    _width = this.viewer._W() * VUtils.SOME_SETTINGS.WIDTH_PADDING,
                    __w = _width*1,
                    _all = mainElem.__category == VUtils.DOM_ELS.CONTAINER_ALL,
                    con_1 = _p.y >= 0,
                    con_2,
                    con_3,
                    lastPosY = this.currentScroll.position.y;


                if (con_1) {
                    this.currentScroll.position.y += _p.y - this.viewer._events.lastPointer.y;
                    if (_all) {
                        this.mainElem.position.y = -(this.mainElem.maxHeight * this.currentScroll.position.y / e.target.clientHeight);
                    } else {
                        for (let i = 0; i < this.mainElem._verticalContainersScroll.length; i++) {
                            this.mainElem._verticalContainersScroll[i].position.y = -(this.mainElem.maxHeight * this.currentScroll.position.y / e.target.clientHeight);
                        }
                    }

                }

                if (con_2 = e.target.clientHeight < this.currentScroll.position.y + this.currentScroll.height) {//max top
                    this.currentScroll.position.y = e.target.clientHeight - this.currentScroll.height;
                    if (_all) {
                        this.mainElem.position.y = -(this.mainElem.maxHeight - _heights);
                    } else {
                        for (let i = 0; i < this.mainElem._verticalContainersScroll.length; i++) {
                            this.mainElem._verticalContainersScroll[i].position.y = -(this.mainElem.maxHeight - _heights);
                        }
                    }


                } else if (con_3 = this.currentScroll.position.y < 0) {//min top
                    this.currentScroll.position.y = 0;
                    if (_all) {
                        this.mainElem.position.y = this.mainElem._maxTop;
                    } else {
                        for (let i = 0; i < this.mainElem._verticalContainersScroll.length; i++) {
                            this.mainElem._verticalContainersScroll[i].position.y = this.mainElem._maxTop;
                        }
                    }

                }
                let shouldUpdate = con_1 || con_2 || con_3;

                if (mainElem.__category == VUtils.DOM_ELS.CONTAINER_ALL) {
                    for (let i = 0, _els = mainElem.children,
                             _maxHeight = mainElem.maxHeight,
                             _maxTop = mainElem.position.y,
                             totalHeight = 0; i < _els.length; i++) {

                        if (shouldUpdate) {
                            updateElsVisiilityOnPage(_els[i]._verticalContainersScroll[0] , _width, _maxHeight, _maxHeight, _maxTop, totalHeight);
                        }
                        totalHeight += _els[i].maxHeight - _els[i].maxHeightHead ;
                    }

                } else {
                    if (shouldUpdate) {
                        updateElsVisiilityOnPage(this.mainElem._verticalContainersScroll[0]);
                    }
                }
            } else {
                let _width = this.viewer._W() ,//* VUtils.SOME_SETTINGS.WIDTH_PADDING,
                    delta = _p.x - this.viewer._events.lastPointer.x,
                    con_1 = _p.x >= 0,
                    con_2 = e.target.clientWidth <= this.currentScroll.position.x + this.currentScroll.width+delta,
                    con_3 = this.currentScroll.position.x < 0,
                    shouldUpdate = con_1 || con_2 || con_3;

                if (con_1) {
                    this.currentScroll.position.x += delta;
                }

                if (con_2) {//max right
                    this.currentScroll.position.x = e.target.clientWidth - this.currentScroll.width;

                } else if (con_3) {//min right
                    this.currentScroll.position.x = 0;
                }

                if (mainElem.__category == VUtils.DOM_ELS.CONTAINER_ALL) {
                    if (con_1) {
                        mainElem._header._scnColumnHeader.children[0].position.x = -(mainElem.maxWidth * (this.currentScroll.position.x) / e.target.clientWidth);
                    }
                    if (con_2) {//max right
                        mainElem._header._scnColumnHeader.children[0].position.x = -(mainElem.maxWidth - _width*1);
                    } else if (con_3) {//min top
                        mainElem._header._scnColumnHeader.children[0].position.x = 0;
                    }

                    for (let i = 0, _els = mainElem.children,
                             _maxHeight = mainElem.maxHeight,
                             _maxTop = mainElem.position.y,
                             totalHeight = 0; i < _els.length; i++) {
                        let table = _els[i],
                            mainElem = table.secondColumn;

                        if (con_1) {
                            mainElem.position.x = -(mainElem.maxWidth * (this.currentScroll.position.x) / e.target.clientWidth);
                        }
                        if (con_2) {//max right
                            mainElem.position.x = -(mainElem.maxWidth - _width*1);
                        } else if (con_3) {//min top
                            mainElem.position.x = 0;
                        }
                        if (shouldUpdate)updateElsVisiilityOnPage(_els[i]._verticalContainersScroll[0], _width, _maxHeight, _maxHeight, _maxTop, totalHeight);
                        totalHeight += table.maxHeight-table.maxHeightHead;
                    }
                } else {
                    let mainElem = this.mainElem.secondColumn;
                    if (con_1) {
                        mainElem.position.x = -(mainElem.maxWidth * (this.currentScroll.position.x) / e.target.clientWidth);
                    }
                    if (con_2) {//max right
                        mainElem.position.x = -(mainElem.maxWidth - _width*1);
                    } else if (con_3) {//min top
                        mainElem.position.x = 0;
                    }
                    if (shouldUpdate)updateElsVisiilityOnPage(this.mainElem._verticalContainersScroll[0]);
                }
            }

        }

        // setTimeout(()=>{
        //     this.inProcess = false;
        // },100)
    }

    updateRightScrollBar() {

        let self = this,
            height = this.viewer._H(true),
            scrollElement = VUtils.drawCell(this.size - this.paddind, height * height / this.mainElem.maxHeight, {
                type: VUtils.TEXT_TYPE.SCROLLBAR_EL,
                backGround: this.barColor
            }),
            _top = 0;
        if (this.rightScroll && this.rightScroll.visible)_top = this.rightScroll.children[0].position.y;

        scrollElement.interactive = true;
        scrollElement.alpha = 0.6;
        scrollElement.mouseover = function () {
            this.alpha = 1;
        }
        scrollElement.mouseout = function () {
            this.alpha = 0.6;
        }
        scrollElement.mousedown = function (e) {
            self._setScroll(this, true);
        }

        scrollElement.__category = VUtils.DOM_ELS.SCROLL_BAR_RIGHT;
        this.rightScroll = VUtils.drawCell(this.size, this.viewer._H(true), {
            type: VUtils.TEXT_TYPE.SCROLLBAR,
            backGround: this.backColor
        });
        this.rightScroll.addChild(scrollElement);
        scrollElement.position.x = this.paddind / 2;
        scrollElement.position.y = _top;
        this.rightScroll.position.x = this.viewer._W(true) - this.size;


        this.rightScroll.visible = this.rightScroll.renderable = this.mainElem.maxHeight > height;
        if (this.rightScroll.visible) {
            this.viewer.stage.addChild(this.rightScroll);
            this.children.push(this.rightScroll);
        }

    }


    updateBottomScrollBar() {
        let self = this,
            width = this.viewer._W(true),
            scrollElement = VUtils.drawCell(width * width / this.mainElem.maxWidth, this.size - this.paddind, {
                type: VUtils.TEXT_TYPE.SCROLLBAR_EL,
                backGround: this.barColor
            }),
            _left = 0;

        // console.log(width * width / this.mainElem.maxWidth);
        if (this.bottomScroll && this.bottomScroll.visible)_left = this.bottomScroll.children[0].position.x;
        scrollElement.interactive = true;
        scrollElement.alpha = 0.6;
        scrollElement.mouseover = function () {
            this.alpha = 1;
        }
        scrollElement.mouseout = function () {
            this.alpha = 0.6;
        }
        scrollElement.mousedown = function (e) {
            self._setScroll(this, true);
        }

        scrollElement.position.x = _left;
        scrollElement.position.y = this.paddind / 2;

        let _delta = +Math.abs(this.viewer.stage.position.x)*2;
        this.bottomScroll = VUtils.drawCell(this.viewer._W(true)+_delta, this.size, {
            type: VUtils.TEXT_TYPE.SCROLLBAR,
            backGround: this.backColor
        });

        this.bottomScroll.addChild(scrollElement);
        this.bottomScroll.position.y = this.viewer._H(true) - this.bottomScroll.height+_delta;

        this.bottomScroll.visible = this.bottomScroll.renderable = this.mainElem.maxWidth > width;
        if (this.bottomScroll.visible) {
            this.viewer.stage.addChild(this.bottomScroll);
            this.children.push(this.bottomScroll);
        }
    }

    show(flag) {
        this.visible = flag;
        if (flag)this.updateScrolls();
        this._updateRendereable();
    }

    _updateRendereable() {
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].visible = this.children[i].renderable = this.visible;
        }
    }
}
