import {VConsole} from '@/utils';
import {VUtils} from './VUtils';
import {VContextMenu} from './VContextMenu';
import {VScrollbar} from './VScrollbar';
import {VBranches} from './vlevel/VBranches';
import {VEvents} from './VEvents';
import {DaysMemberPlanning} from './utils/DaysMemberPlanning';
import {HistoryPlanning} from "./utils/HistoryPlanning";

export class PixiViewer {
    static PAGES = {
        KLANT: 1,
        BRANCHES: 2,
        SOMETHIRDPAGE: 3
    };

    static ELEMENTS = {
        CELL: 1,
        WEEK_CELL: 2
    };

    static KEYS = {
        SHIFT: 16
    };

    constructor(options) {
        if (!options) throw new Error('no options provided');
        if (!options.container) throw new Error('no container provided');
        if (!options.jsonData) throw new Error('no jsonData provided');


        this.noCanAnimate = this.hasAnimations = false;
        this.opt = options || {};
        this.scroll = {velocity: 0, dir: 0};
        this._pages = [];
        this.visible = true;
        this.canAnimate = true;
        this.lastTimeAnimate = 0;
        this.maxItemsRender = 80;
        let stage;
        let _self = this;
        _self._historyPlanning = new HistoryPlanning(_self);

        (function init(_data) {
            let data = _data.jsonData,
                interactive = true,
            planningConflicts =[];
            stage = _self.stage = new PIXI.Stage(0x000000, interactive);

            _self.mainContainer = new PIXI.Container();
            stage.addChild(_self.mainContainer);
            _self.mainContainer.position.x = 10;
            stage.position.x = stage.position.y = -2;

            _self._parseJson(data, _data);

            if (options.conflicts) {
                options.conflicts._planning = new DaysMemberPlanning(options.conflicts.sites );
                // planningConflicts= options.conflicts._planning.checkConflicts();
            }
            // create a renderer instance.
            let renderer = (stage.renderer = PIXI.autoDetectRenderer(620, 400, {
                view: options.container
            }));

            renderer.autoResize = true;
            renderer.backgroundColor = 0xffffff;

            // let stats = (_self.stats = new Stats());
            // options.container.parentNode.appendChild(stats.dom);
            // stats.dom.style.top = stats.dom.style.left = '';
            // stats.dom.style.bottom = stats.dom.style.right = '10px';

            _self._updatePages();
            _self.animations = {conflicts: [],gifs:[]};
            // _self._contextMenu = new VContextMenu(_self);
            _self._scrollBar = new VScrollbar(_self);
            _self._events = new VEvents(_self);
            _self.refreshRender();
            _self.animate();
        })(options);
    }

    _parseJson(data, allData) {
        let _self = this;
        this.stage.loadedData = data;
        //find max text length
        let maxWidth = 100,
            maxAvailableWidth = 210,
            paddingLeft = 10;

        let _text = VUtils.drawText({
            text: 'Contractor/Client',
            type: VUtils.TEXT_TYPE.TABLE_CELL
        });
        maxWidth = Math.max(maxWidth, _text.width + 2 * paddingLeft);

        for (let i = 0; i < data.data.length; i++) {
            let _dataSite = data.data[i];
            _dataSite.branches._info = _dataSite;
            _dataSite.branches.siteId = _dataSite.site;
            if (!_dataSite.info) _dataSite.info = {title: 'NONE'};
            _dataSite.branches.name = _dataSite.info.title;

            for (let j = 0; j < _dataSite.branches.length; j++) {
                let _branch = _dataSite.branches[j];
                if (_dataSite.site) _branch.siteId = _dataSite.site;
                if (!_branch.info || !_branch.contact.name) continue;

                if(_branch.start_date){
                    _branch.start = Date._toUTCSeconds(_branch.start_date);
                    _branch.end = Date._toUTCSeconds(_branch.end_date);
                }

                _branch.data.forEach(planned => {
                    planned.start = Date._toUTCSeconds(planned.start);
                    planned.end = Date._toUTCSeconds(planned.end);
                    planned.messages.forEach((message)=>{
                        message.date = Date._toUTCSeconds(message.date);
                    })
                });
                _branch.teams.forEach(team => {
                    team.teamMembers.forEach(teamMember => {
                        teamMember.plannings.forEach(planning => {
                            planning.date = Date._toUTCSeconds(planning.date);
                        })
                    })
                });
                _branch.__name = allData.daterange.planner_client_view?_branch.info.full:_branch.contact.name;
                let _text = VUtils.drawText({
                    text:  _branch.info
                        ? PixiViewer.checkTextLength(_branch.__name)
                        : '',
                    type: VUtils.TEXT_TYPE.TABLE_CELL
                });

                maxWidth = Math.max(maxWidth, _text.width + 2 * paddingLeft);
            }

            if (_dataSite.contact && _dataSite.contact.contact_types_indexes.indexOf(VUtils.TYPES.DETAILS)>-1) {
                //collaborators
            } else {
                _dataSite.branches.sort((a, b) => {
                    return a.info.sequence > b.info.sequence ? 1 : -1;
                });
            }
        }
        _self.MAX_WIDTH_FIRST_COLUMN_SECND_COLMN = Math.min(
            maxWidth,
            maxAvailableWidth
        );
        if (allData) {
            allData.noplanning = allData.noplanning.map(item => {
                return {
                    start: new Date(item.start).getTime() / 1000,
                    end: new Date(item.end).getTime() / 1000
                };
            });
        }
    }

    static checkTextLength(text){
        return !text?'':text.length>28?text.substr(0, 25):text;
    }
    _updatePages() {
        this._pages = [new VBranches({parent: this})];
    }

    refreshRender() {
        // this.canAnimate = true;
        this.lastTimeAnimate = 0;
    }

    refresh(jsondata) {
        if (!jsondata) return VConsole.warn('no data');
        this.stop();
        if (jsondata.daterange) this.opt.daterange = jsondata.daterange;
        if (jsondata.plannedDays) this.opt.plannedDays = jsondata.plannedDays;
        if (jsondata.noplanning)
            this.opt.noplanning = jsondata.noplanning.map(item => {
                return {
                    start: new Date(item.start).getTime() / 1000,
                    end: new Date(item.end).getTime() / 1000
                };
            });
        if (jsondata.conflicts) {
            // this.opt.conflicts = jsondata.conflicts.conflicts;
            this.opt.conflicts._planning = new DaysMemberPlanning(
                jsondata.conflicts.sites
            );

        } //new DaysMemberPlanning(daysInUtcPlanned),
        if (jsondata.jsonData) {
            this._parseJson(jsondata.jsonData,jsondata);
        } else {
            for (let i = 0; i < this.stage.loadedData.data.length; i++) {
                for (
                    let j = 0;
                    j < this.stage.loadedData.data[i].branches.length;
                    j++
                ) {
                    this.stage.loadedData.data[i].branches.table = null;
                }
            }
        }
        if (this._pages)
            this._pages.forEach(page => {
                if (page) page.destroy();
            });
        this.animations = {conflicts: [],gifs:[]};
        this._updatePages();
    }

    animate() {
        if (this.hasAnimations) return;
        this.hasAnimations = true;
        let _self = this,
            item = 4,
            _scroll = _self.scroll,
            _c_1 = PixiViewer.PAGES.BRANCHES,
            _page_1 = _self._pages[0],
            _scrollbar = _self._scrollBar,
            _stage = _self.stage,
            _stats = _self.stats,
            _renderer = _stage.renderer,
            _renderId = Date.now(),
            _speed = 1 / 300,
            _dirAn = _speed,
            anItems = 0;

        this.refreshRender();

        function animate() {
            if (_self.noCanAnimate) {
                _self.hasAnimations = false;
                return;
            }
            // _stats.update();
            for (let i = 0; i < _self.animations.conflicts.length; i++) {
                _self.animations.conflicts[i](Math.sin((anItems += _dirAn)));
            }
            for (let i = 0; i < _self.animations.gifs.length; i++) {
                _self.animations.gifs[i].callback();
            }
            if (anItems > 1) {
                _dirAn = -_speed;
            } else if (anItems < 0) {
                _dirAn = _speed;
            }
            if (_scroll.velocity > 0) {
                _scroll.velocity -= 2.5; //options.domeEl._data.velocity/10;
                if (_self.currentPageType == _c_1) {
                    // debugger;
                    _page_1.updateOnScroll();
                    _scrollbar.updateOnWheel();
                }
            }

            requestAnimationFrame(animate);
            item++;
            if (item % 4 == 0) {
                item = 0;

                if (_self.lastTimeAnimate < _self.maxItemsRender) {
                    _self.lastTimeAnimate++;
                    _renderer.render(_stage);
                }
            }
        }

        animate();
    }

    showDetails() {
        this.opt.domeEl.selectTypePage(-1);
    }

    play() {
        this.noCanAnimate = false;
        this._events.onResize();
        this.animate();
    }

    stop() {
        this.noCanAnimate = true;
    }

    updatePage(type, levelEls) {
        this.stop();
        let pages = this._pages;
        pages.forEach(page => {
            if (page) page.show(false);
        });
        this.currentPageType = type;
        // this.stage.backBtn.renderable = this.stage.backBtn.visible = this.stage.showAllBtn.visible = this.stage.showAllBtn.visible = false;
        this._scrollBar.mainElem = this.currentPage = null;
        this._scrollBar.show();
        switch (type) {
            case PixiViewer.PAGES.KLANT: {
                this._pages[0].resetListOfSites();
                break;
            }
            case PixiViewer.PAGES.BRANCHES: {
                // this._pages[0].resetListOfSites();
                // this.stage.backBtn.renderable = this.stage.backBtn.visible = true;
                pages[0].show(true, levelEls);
                this._scrollBar.mainElem = pages[0].isAll
                    ? pages[0]._allSites._body
                    : pages[0].table;
                this._scrollBar.show(true);
                this.currentPage = pages[0];
                break;
            }
            default: {
                VConsole.warn('No Page Found!!!');
            }
        }
        setTimeout(() => {
            this.play();
        }, 100);
    }

    createEl(data, x, y) {
        var padding = 10;
        var paddingLeft = 10;
        var paddingTop = 50;
        var sprite = new PIXI.Sprite();
        let self = this,
            stage = data.parent || this.mainContainer;

        var _child = new PIXI.Text(data.text, {
            fontFamily: 'Arial',
            fontSize: 24,
            fill: 0x000000,
            align: 'center'
        });

        var textbg = new PIXI.Graphics();
        textbg.beginFill(0xe1e1e1, 1);
        textbg.drawRoundedRect(
            -padding,
            -padding,
            _child.width + 2 * padding,
            _child.height + 2 * padding,
            2,
            2
        );
        textbg.endFill();
        sprite._textbg = textbg;
        sprite._textInfo = _child;
        sprite.addChild(textbg);
        sprite.addChild(_child);

        var nextX = paddingLeft + x + (2 * padding + 5),
            nexRow = nextX + _child.width > this._W() * 0.8;
        sprite.position.x = paddingLeft + (nexRow ? 0 : x) + (2 * padding + 5);
        sprite.position.y =
            paddingTop + (y + (nexRow ? 1 : 0)) * (_child.height + 2 * padding + 5);
        sprite.sholdBeNextRow = nexRow ? sprite.position.x + _child.width : 0;

        stage.addChild(sprite);
        sprite.interactive = true;
        sprite.alpha = 0.8;
        sprite.mouseover = function () {
            this.alpha = 1;
            self.stage.renderer.view.style.cursor = 'pointer';
        };
        sprite.mouseout = function () {
            this.alpha = 0.8;
            self.stage.renderer.view.style.cursor = '';
        };
        sprite.mouseup = function () {
            if (data.onMouseUp) return data.onMouseUp();
        };
        return sprite;
    }

    destroy() {
        this.stop();
        this._events.omDestroy();
        // this.stage.renderer.view.parentNode.removeChild(this.stats.dom);
    }

    _W(canvas) {
        if (canvas && this.stage.renderer.view.clientWidth)
            return this.stage.renderer.view.clientWidth;
        return this._parent().clientWidth || window.innerWidth;
    }

    _H(canvas) {
        // debugger;
        if (canvas && this.stage.renderer.view.clientHeight)
            return this.stage.renderer.view.clientHeight;
        return this._parent().clientHeight || window.innerHeight;
    }

    _parent() {
        return this.stage.renderer.view.parentNode;
    }

    toggleActiveRows() {
        this._pages[0].toggleEmptyRowsFromAllSites();
        this.refreshRender();
    }
}
