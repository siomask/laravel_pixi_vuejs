class Auth {
	static logout(context) {
		context.$auth.logout({
			makeRequest: false,
			success: function() {},
			redirect: '/login'
		})
	}
}

export { Auth }
