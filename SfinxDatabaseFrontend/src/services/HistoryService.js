class HistoryService {
    pool = [];
    poolRedo = [];
    currentRedoItem = 0;
    currentItem = 0;
    static MAX_LENGTH = 15;

    constructor() {
        this.isUndo = this.isRedo = false;
    }

    add(toPrevState) {
        // this.pool.splice(this.currentItem);
        this.pool.push(toPrevState);
        if (this.pool.length > HistoryService.MAX_LENGTH) this.pool.shift();

        if(!toPrevState.isNoUserAction)this.poolRedo=[];
        this.currentItem = this.pool.length;
        this._isRedo();
        this._isUndo();
        // console.log("history back refresh");
    }

    addRedo(currentState){
        // this.poolRedo.splice(this.currentItem);
        this.poolRedo.push(currentState);
        if (this.poolRedo.length > HistoryService.MAX_LENGTH) this.poolRedo.shift();
        this._isRedo();
        this._isUndo();
        // console.log("history next refresh");
    }


    undo() {
        this.currentItem--;
        this._isRedo();
        this._isUndo();
    }

    redo() {
        this.currentItem++;
        this._isRedo();
        this._isUndo();

    }

    _isRedo() {
        // return this.isRedo = this.currentItem < this.pool.length && this.pool.length;
        return this.isRedo =  this.poolRedo.length>0;
    }

    _isUndo() {
        // return this.isUndo = this.currentItem > 0 && this.pool.length;
        return this.isUndo =  this.pool.length >0;
    }
}

class PlannerHistoryService extends HistoryService {

    static ACTIONS = {
        NEW_PLANNING: 1,
        REMOVE_PLANNING: 2,
        REMOVE_PLANNING_ALL: 3,
        SPLIT_PLANNING: 4,
    }

    constructor() {
        super();
    }

    move(dirrection = 1) {
        switch (dirrection) {
            case 1: {//back
                this.undo();
                break;
            }
            case 2: {//forward
                break;
            }
            default: {
                //custom
            }
        }
    }

    undo() {
        if (!this.isUndo) {
            super.undo();
            return console.warn('history back is empty');
        }
        // let action = this.pool[this.currentItem - 1];
        let action = this.pool.pop();
        if (!action) {
            return console.warn('no back history');
        } else if (action.callback) {
            this.timeOut();
            action.callback().then(res => {
                super.undo();
                if(action.onFinish)action.onFinish();
            })
        }
    }

    redo() {
        if (!this.isRedo) {
            return console.warn('history next is empty');
        }
        let action = this.poolRedo.pop();
        if (!action) {
            return console.warn('no next history ');
        } else if (action.callback) {
            this.timeOut();
            action.callback().then(res => {
                super.redo();
                if(action.onFinish)action.onFinish();
            })
        }
    }
    timeOut(){
        this.isRedo = this.isUndo =false;
    }
}

export {HistoryService, PlannerHistoryService}
