class BrowserStorageService {
    static PREFIX={
        MAIN:'sf',
        PLANNER_BRANCH:'_branch',
        WORKFLOW:'_workflow_',
        PLANNER:'_planner'
    }
    static setItem(key,value){
        localStorage.setItem(key,value);
    }
    static getItem(key){
        return localStorage.getItem(key);
    }
    static removeItem(key){
        localStorage.removeItem(key);
    }
}

export { BrowserStorageService }
