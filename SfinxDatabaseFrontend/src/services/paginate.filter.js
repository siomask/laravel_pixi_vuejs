export class PaginateFilter {
    constructor(vueComponent) {
        // setTimeout(()=>{
        let el = document.querySelector(`#component_${vueComponent._uid} tbody`),
            handler = (el.addEventListener || el.attachEvent).bind(el);
        handler('scroll', (e) => {
            this.onScroll(e);
        });

        this.vueComponent = vueComponent;
        this.vueComponent.onChangeFilter = () => this.onChangeFilter();
        this.loadMoreItems(true);
        // },1000)

    }

    onScroll(e) {
        let _d = e.target,
            totalHeight = 0;
        [].forEach.call(_d.children, (node) => {
            totalHeight += node.clientHeight;
        })
        let _h = totalHeight - e.target.clientHeight;
        if (_h <= _d.scrollTop + 10) {
            this.lastScrollTop = _d.scrollTop;
            this.loadMoreItems();
        }
    }

    onChangeFilter() {
        this.vueComponent.paginate = null;
        this.loadMoreItems(true);
    }

    loadMoreItems(onMounted) {

        let queries = ``,
            link,
            filter = this.vueComponent.filter;
        for (let i = 0; i < filter.keys.length; i++) {
            let _key = filter.keys[i],
                _value = filter[_key];
            if (_key && _value) queries += (queries.length ? '&' : '') + `${_key}=${_value}`;
        }
        if (this.vueComponent.allContactTypes) {
            let filterTypes = '';
            for (let i = 0, _d = this.vueComponent.allContactTypes; i < _d.length; i++) {
                if (_d[i].selected) {
                    filterTypes += (filterTypes.length ? ',' : '') + (_d[i].id);
                }
            }
            if (filterTypes) queries += (queries.length ? '&' : '') + `type=${filterTypes}`;
        }

        if (this.vueComponent.paginate) {
            if (!this.vueComponent.paginate.finish) return console.warn('in progress');
            this.vueComponent.paginate.finish = false;
            link = this.vueComponent.paginate.next_page_url + (queries?'&'+queries:'');
        }
        this.vueComponent.$forceUpdate();
        if ((this.vueComponent.paginate && !this.vueComponent.paginate.next_page_url)) {
            if (this.vueComponent.paginate) {
                this.vueComponent.paginate.finish = true;
                this.vueComponent.paginate.isAll = true;
            }
            return console.warn('No more items');
        }
        this.vueComponent.paginate = {};
        this.vueComponent.$store.dispatch(`${filter.query_link}`, {link, queries}).then((res) => {
            this.vueComponent.paginate = res.data;
            this.vueComponent.paginate.finish = true;
            if (this.vueComponent.onLoadItems) this.vueComponent.onLoadItems(res);
            let _d = document.querySelector(`#component_${this.vueComponent._uid} tbody`),
                totalHeight = 0;

            if (onMounted) {
                setTimeout(() => {
                    if (!_d || !_d.children) return;
                    [].forEach.call(_d.children, (node) => {
                        totalHeight += node.clientHeight;
                    })
                    if (_d.clientHeight > totalHeight) {
                        this.loadMoreItems(onMounted);
                    }
                }, 1000);//wait untill items will render

            }
        });

    }
}