import store from '../store/index'

export const parseAllMembersForConflctPlanning = (sites, _dateRange, parent) => {
    let teamMemberAvailable = {},
        daysPlanned = {},
        daysInUtcPlanned = {},
        HOUR_IN_UTC = 60 * 60,
        self = this,
        dayParts = 5,
        members = {};

    for (let i = 0; i < sites.length; i++) {
        let site = sites[i];
        for (let jk = 0; jk < site.branches.length; jk++) {
            for (let jks = 0; jks < site.branches[jk].teams.length; jks++) {
                let team = site.branches[jk].teams[jks];

                for (let j = 0; j < team.teamMembers.length; j++) {
                    let teamM = team.teamMembers[j],
                        teamMember = daysPlanned[teamM.id] || {
                            id: teamM.id,
                            plannedDays: {}
                        };
                    members[teamM.id] = teamM;
                    daysPlanned[teamM.id] = teamMember;
                    if (!teamM.plannings) {

                    } else {
                        for (let d = 0; d < teamM.plannings.length; d++) {
                            let plannedD = teamM.plannings[d],
                                start = Math.floor(new Date(plannedD.date).getTime() / 1000),
                                dayInUTC = start;

                            if (!teamMember.plannedDays[dayInUTC]) {
                                teamMember.plannedDays[dayInUTC] = {}
                            }

                            if (!daysInUtcPlanned[dayInUTC]) daysInUtcPlanned[dayInUTC] = {};
                            if (!daysInUtcPlanned[dayInUTC][plannedD.collaborator_planning_entry_id]) daysInUtcPlanned[dayInUTC][plannedD.collaborator_planning_entry_id] = {};
                            if (!daysInUtcPlanned[dayInUTC][plannedD.collaborator_planning_entry_id][teamM.id]) daysInUtcPlanned[dayInUTC][plannedD.collaborator_planning_entry_id][teamM.id] = [];
                            daysInUtcPlanned[dayInUTC][plannedD.collaborator_planning_entry_id][teamM.id].push(plannedD);

                            let _dayMember = teamMember.plannedDays[dayInUTC];
                            if (!_dayMember[plannedD.collaborator_planning_entry_id]) {
                                _dayMember[plannedD.collaborator_planning_entry_id] = {dayPlannedParts: []};
                            }
                            let _d = _dayMember[plannedD.collaborator_planning_entry_id];

                            _d.fullDay = true;
                        }
                    }
                }
            }
        }

    }

    let conflicts = {};
    for (let memberId in daysPlanned) {
        let _m = daysPlanned[memberId]

        for (let dayInUTC in _m.plannedDays) {
            let dayrows = _m.plannedDays[dayInUTC]

            for (let _row in dayrows) {
                let row = +_row;
                let _dayPlannedCell = dayrows[row];
                if (_dayPlannedCell.fullDay || _dayPlannedCell.dayPlannedParts.length) {
                    if (_m.conflics) {
                        if (_m.conflics[dayInUTC]) {


                            if (_dayPlannedCell.fullDay) {
                                _m.conflics[dayInUTC].fullDay.push(row);
                            }

                            if (_m.conflics[dayInUTC].fullDay.length) {
                                if (!conflicts[dayInUTC]) conflicts[dayInUTC] = {}
                                if (!conflicts[dayInUTC][memberId])
                                    conflicts[dayInUTC][memberId] = []

                                if (conflicts[dayInUTC][memberId].indexOf(row) < 0) conflicts[dayInUTC][memberId].push(row);
                                if (_m.conflics[dayInUTC].fullDay.length == 1) {
                                    conflicts[dayInUTC][memberId].push(_m.conflics[dayInUTC].fullDay[0]);
                                }

                                for (let i = 0, _l = _m.conflics[dayInUTC].dayParts; i < _l.length; i++) {
                                    if (conflicts[dayInUTC][memberId].indexOf(_l[i].cellId) < 0) conflicts[dayInUTC][memberId].push(_l[i].cellId);
                                }
                            } else {
                                for (let i = 0, _l = _m.conflics[dayInUTC].dayParts; i < _l.length; i++) {
                                    for (let df = 0, _ls = _dayPlannedCell.dayPlannedParts; df < _ls.length; df++) {
                                        let _p = _l[i];
                                        if (_p.ar.indexOf(_ls[df]) > -1) {
                                            if (!conflicts[dayInUTC]) conflicts[dayInUTC] = {};
                                            if (!conflicts[dayInUTC][memberId]) {
                                                conflicts[dayInUTC][memberId] = [];
                                            }
                                            if (conflicts[dayInUTC][memberId].indexOf(row) < 0) conflicts[dayInUTC][memberId].push(row);
                                            if (conflicts[dayInUTC][memberId].indexOf(_p.cellId) < 0) conflicts[dayInUTC][memberId].push(_p.cellId);
                                        }
                                    }
                                }
                            }


                            _m.conflics[dayInUTC].dayParts.push({
                                cellId: row,
                                ar: _dayPlannedCell.dayPlannedParts
                            });

                        } else {
                            _m.conflics[dayInUTC] = {
                                fullDay: [],
                                dayParts: [{cellId: row, ar: _dayPlannedCell.dayPlannedParts}]
                            }
                            if (_dayPlannedCell.fullDay)
                                _m.conflics[dayInUTC].fullDay.push(row)
                        }
                    } else {
                        _m.conflics = {}
                        _m.conflics[dayInUTC] = {
                            fullDay: [],
                            dayParts: [{cellId: row, ar: _dayPlannedCell.dayPlannedParts}]
                        }
                        if (_dayPlannedCell.fullDay) _m.conflics[dayInUTC].fullDay.push(row)
                    }
                }
            }
        }
    }

    for (let days in conflicts) {
        let _day = new Date(0);
        _day.setUTCSeconds(days);
        let text =
            'In day (' +
            _day
                .toUTCString()
                .split(' ')
                .splice(1, 2)
                .join(' ') +
            ') have conflicts for user(s):'

        for (let member in conflicts[days]) {
            text +=
                members[member].first_name +
                ' ' +
                members[member].last_name +
                ','
        }
        // store.commit('planner/WARNING_CONFLICT', text.substr(0, text.length - 1));
    }
    // if (warnings.length) {
    //   parent.$refs.popupAlert.show(warnings)
    // }
    return {
        daysInUtcPlanned,
        conflicts: {}
    }
}

export const getDay = (start, end, dateRange) => {
    let DAY_IN_UTC = 60 * 60 * 24,
        dateRangeEnd = dateRange.end,
        dateRangeStart = dateRange.start

    for (
        let _curURCScnd = dateRangeStart;
        _curURCScnd < dateRangeEnd;
        _curURCScnd += DAY_IN_UTC
    ) {
        if (start >= _curURCScnd && start < _curURCScnd + DAY_IN_UTC)
            return _curURCScnd
    }
}


