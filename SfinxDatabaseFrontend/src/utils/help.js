export class Help {
    static UTC_MILI_SCNDS = 1000;
    static HOUR_IN_UTC = 60 * 60;
    static DAY_IN_UTC = 24 * Help.HOUR_IN_UTC;
    static DAY_PLANNED_PARTS = 5;
    static MAX_DAUS_IN_MONTH = 32;
    static PLANNING_MEMBER_SEQUENCE=[
        {name: '1', value: 1},
        {name: '2', value: 2},
        {name: '3', value: 3},
        {name: '4', value: 4},
        {name: '5', value: 5}
    ]
    static PLANNING_MEMBER_SEQUENCE_INDEX={
        1:'First&Last',
        2:'First',
        3:'Last',
        4:'Other'
    }
}