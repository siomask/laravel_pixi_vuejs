var  console = window.console;
export class VConsole {
    static log(...data) {
        console.log(...data);
    }

    static warn(...data) {
        console.warn(...data);
    }
}