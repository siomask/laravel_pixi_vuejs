import invoice from './invoice';
import type from './type';
import summary from './summary';

export default {
	namespaced: true,
	modules: {
        summary,
        invoice,
        type
    }
};
