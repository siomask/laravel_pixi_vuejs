import axios from 'axios';

export default {
    all: async ({commit, state}, opt = {}) => {
        let {link, queries=''} = opt,
            res;
        try {
            // if (!state.isCached || hard) {
            res = await axios.get(link ? link : `${process.env.API_URL}/api/v1/finance/summary?${queries}`);
            commit('ALL', res.data);
            // }
        } catch (error) {
            commit('BAD', {error});
        }
        return res;
    },
    editSite: async ({commit, state}, item) => {
        try {
            let res = await axios.put(`${process.env.API_URL}/api/v1/finance/summary/${item.id}`, item);
            commit('EDIT_SITE', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    }

};
