export default {
    ALL: (state, payload) => {
        for (let i = 0; i < payload.data.length; i++) {
            let shouldAdd = true;
            for (let j = 0; j < state.list.length; j++) {
                if (state.list[j].id == payload.data[i].id) {
                    shouldAdd = false;
                    break;
                }
            }
            if (shouldAdd) state.list.push(payload.data[i]);
        }
    },
    EDIT_SITE: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.data.id) {
                return Object.assign_fields(state.list[i], payload.data, [
                    'total_amount_orders',
                    'total_amount_processed',
                    'date_financial_details'
                ]);
            }
        }
    },
    BAD: (state, payload) => {
        alert(payload.error.response.data.message);
    }
};
