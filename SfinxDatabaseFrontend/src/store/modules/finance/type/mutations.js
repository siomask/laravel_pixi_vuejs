export default {
    ALL: (state, payload) => {
        state.list = payload.data;
        state.isCached = true;
    }
};
