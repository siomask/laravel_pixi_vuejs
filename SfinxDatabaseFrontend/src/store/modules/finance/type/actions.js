import axios from 'axios';

export default {
    all: async ({commit,state}) => {
        try {
            if (!state.isCached) {
                let res = await axios.get(`${process.env.API_URL}/api/v1/finance/type`);
                commit('ALL', res.data);
            }
        } catch (error) {
            commit('BAD', {error});
        }
    }

};
