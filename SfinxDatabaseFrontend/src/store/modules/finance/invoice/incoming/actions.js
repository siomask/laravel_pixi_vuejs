import axios from 'axios';

export default {
    all: async ({commit, state}, {link, queries}={}) => {
        let res;
        try {
            // if (!state.isCached || hard) {
            res = await axios.get(link ? link : `${process.env.API_URL}/api/v1/finance/incoming-invoice?${queries}`);
            commit('ALL', res.data);
            // }
        } catch (error) {
            commit('BAD', {error});
        }
        return res;
    },
    add: async ({commit}, data) => {
        try {
            let res = await axios.post(`${process.env.API_URL}/api/v1/finance/incoming-invoice`, data);
            commit('ADD', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    edit: async ({commit}, data) => {
        try {
            let res = await axios.put(`${process.env.API_URL}/api/v1/finance/incoming-invoice/${data.id}`, data);
            commit('EDIT', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(`${process.env.API_URL}/api/v1/finance/incoming-invoice/${data.id}`);
            commit('DELETE_ITEM', data);
        } catch (error) {
            commit('BAD', {error});
        }
    },


    refresh: async ({state}) => {
        state.isCached = false;
    }

};
