import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import entry from './entry';
import detail from './detail';

export default {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
	modules:{
        detail,
        entry
	}
};
