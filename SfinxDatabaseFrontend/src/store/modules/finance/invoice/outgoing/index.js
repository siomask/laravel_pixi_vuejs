import state from './state';
import actions from './actions';
import getters from './getters';
import mutations from './mutations';
import entry from './entry';
import payment from './payment';

export default {
	namespaced: true,
	state,
	actions,
	mutations,
	getters,
	modules:{
        entry,
        payment
	}
};
