export default {
    ALL: (state, payload) => {
        for (let i = 0; i < payload.data.length; i++) {
            let shouldAdd = true;
            for (let j = 0; j < state.list.length; j++) {
                if (state.list[j].id == payload.data[i].id) {
                    shouldAdd = false;
                    break;
                }
            }
            if (shouldAdd) state.list.push(payload.data[i]);
        }
    },
    ADD: (state, payload) => {
        state.list.push(payload.data);
    },
    DELETE_ITEM: (state, data) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == data.id) {
                return state.list.splice(i, 1);
            }
        }
    },
    EDIT: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.data.id) {
                Object.assign(state.list[i], payload.data);
                break;
            }
        }
    },
    BAD: (state, payload) => {
        alert(payload.error.response.data.message);
    }
};
