import axios from 'axios';

export default {
    all: async ({commit, state}, invoice) => {
        try {
            // if (!state.isCached) {
            let res = await axios.get(`${process.env.API_URL}/api/v1/finance/outgoing-invoice/${invoice.id}/entry`);
            commit('ALL', res.data);
            // }
        } catch (error) {
            commit('BAD', {error});
        }
    },
    addItem: async ({commit}, data) => {
        try {
            let res = await axios.post(`${process.env.API_URL}/api/v1/finance/outgoing-invoice/${data.invoiceId}/entry`, data);
            commit('ADD', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    editItem: async ({commit}, data) => {
        try {
            let res = await axios.put(`${process.env.API_URL}/api/v1/finance/outgoing-invoice/${data.invoiceId}/entry/${data.id}`, data);
            commit('EDIT', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(`${process.env.API_URL}/api/v1/finance/outgoing-invoice/${data.invoiceId}/entry/${data.id}`);
            commit('DELETE_ITEM', data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    refresh: async ({state}) => {
        state.isCached = false;
    }

};
