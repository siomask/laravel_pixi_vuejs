import incoming from './incoming';
import outgoing from './outgoing';
import accounts from './accounts';

export default {
	namespaced: true,
	modules: {
        incoming,
        accounts,
        outgoing
    }
};
