export default {
    ALL: (state, payload) => {
        state.list = payload.data;
    },
    EDIT: (state, payload) => {
        let oldParent = findItemById(state.list, payload.data.id);
        if (!oldParent) throw new Error('This should not happen!!!');
        if (oldParent.item.parent_id == payload.data.parent_id) {

            let newIndex = payload.data.sequence,
                oldIndex = oldParent.item.oldSequence;
            oldParent.item.oldSequence = -1;
            if (oldIndex > -1) {
                for (let di = 0, _p = oldParent.parent; di < _p.length; di++) {
                    let _ediItem = _p[di];
                    if (payload.data.id != _ediItem.id) {
                        if (newIndex < oldIndex) {
                            if (_ediItem.sequence >= newIndex && _ediItem.sequence <= oldIndex) {
                                _ediItem.sequence += 1;
                            }
                        } else if (newIndex > oldIndex) {
                            if (_ediItem.sequence <= newIndex && _ediItem.sequence >= oldIndex) {
                                _ediItem.sequence -= 1;
                            }
                        }
                    }

                }
            }

            Object.assign_fields(oldParent.item, payload.data, ['amount', 'description', 'sequence', 'parent_id']);
        } else {
            let new_parent = findItemById(state.list, null, payload.data.parent_id);
            if (!new_parent) throw new Error('This should not happen!!!');
            //remove from old parent list
            let removedItem = null;
            for (let i = 0, isDec = false, _list = oldParent.parent; i < _list.length; i++) {
                if (_list[i].id == payload.data.parent_id) {
                    removedItem = _list.splice(i--, 1)[0];
                    isDec = true;
                    continue;
                }
                if (isDec) _list[i].sequence--;
            }
            if (removedItem) {
                Object.assign_fields(removedItem, payload.data, ['amount', 'description', 'sequence', 'parent_id']);

                if (!new_parent.children) new_parent.children = [];
                new_parent.children.push(removedItem);
                new_parent.children.sort((a, b) => a.sequence > b.sequence ? 1 : -1);
                new_parent.children.forEach((el) => {
                    if (el.id != payload.data.id && el.sequence >= payload.data.sequence) el.sequence++;
                })
            }
        }

    },
    ADD: (state, payload) => {
        payload.data.children = [];
        if (!payload.data.parent_id) {
            state.list.push(payload.data);
            state.list.sort((a, b) => a.sequence > b.sequence ? 1 : -1);
        } else {
            let new_parent = findItemById(state.list, null, payload.data.parent_id).item;
            if (!new_parent) throw new Error('This should not happen!!!');
            if (!new_parent.children) new_parent.children = [];
            new_parent.children.push(payload.data);
            new_parent.children.sort((a, b) => a.sequence > b.sequence ? 1 : -1);
        }

    },
    DELETE_ITEM: (state, payload) => {
        if (!payload.parent_id) {
            for (let i = 0; i < state.list.length; i++) {
                if (state.list[i].id == payload.id) {
                    for (let di = i; di < state.list.length; di++) {
                        state.list[di].sequence -= 1;
                    }
                    return state.list.splice(i, 1)
                }
            }
        } else {
            let new_parent = findItemById(state.list, null, payload.parent_id);
            if (!new_parent) throw new Error('This should not happen!!!');
            for (let i = 0; i < new_parent.item.children.length; i++) {
                if (new_parent.item.children[i].id == payload.id) {

                    for (let di = i; di < new_parent.item.children.length; di++) {
                        new_parent.item.children[di].sequence -= 1;
                    }
                    return new_parent.item.children.splice(i, 1)
                }
            }
        }

    },
    BAD: (state, payload) => {
        alert(payload.error.response.data.message);
    },

};

function findItemById(list, id = null, parentId = null) {
    let item = null;
    for (let i = 0; i < list.length; i++) {
        if (id) {
            if (list[i].id == id) return {
                parent: list,
                item: list[i],
            }
            if (list[i].children) {
                item = findItemById(list[i].children, id);
                if (item) return item;
            }
        } else if (parentId) {
            if (list[i].id == parentId) return {
                item: list[i],
            }
            if (list[i].children) {
                item = findItemById(list[i].children, null, parentId);
                if (item) return item;
            }
        }

    }
    return item;
}