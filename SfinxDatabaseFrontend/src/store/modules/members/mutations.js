export default {
	ALL_MEMBER: (state, payload) => {
		state.members = payload.data;
	},
	ADD_MEMBER: (state, payload) => {
		state.members.push({
			first_name: payload.first_name,
			last_name: payload.last_name,
			id: payload.id
		});
	},
	DELETE_MEMBER: (state, id) => {
		state.members = state.members.filter(member => {
			return member.id !== id;
		});
	},
	EDIT_MEMBER: (state, payload) => {
		state.members = state.members.map(member => {
			if (member.id === payload.id) {
				return {
					...member,
					first_name: payload.first_name,
					last_name: payload.last_name
				};
			} else {
				return member;
			}
		});
	}
};
