import axios from 'axios';
export default {
	getMembers: async ({ commit }) => {
		const res = await axios.get(`${process.env.API_URL}/api/v1/member`);
		commit('ALL_MEMBER', res.data);
	},
	addMember: async ({ commit }, data) => {
		const res = await axios.post(`${process.env.API_URL}/api/v1/member`, data);
		// if (res.status === 201) {
			commit('ADD_MEMBER', res.data.data);
		// }
	},
	editMember: async ({ commit }, data) => {
		const res = await axios.put(
			`${process.env.API_URL}/api/v1/member/${data.id}`,
			data
		);
		if (res.status == 200) {
			commit('EDIT_MEMBER', data);
		}
	},
	deleteMember: async ({ commit }, id) => {
		const res = await axios.delete(
			`${process.env.API_URL}/api/v1/member/${id}`
		);
		if (res.status === 200) {
			commit('DELETE_MEMBER', id);
		}
	}
};
