import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import settings from './settings';


export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters,
    modules:{
        settings
    }
};
