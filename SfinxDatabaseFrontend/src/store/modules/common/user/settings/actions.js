import axios from 'axios';

export default {
    editItem: async ({commit}, data) => {
        try {
            const res = await axios.post(`${process.env.API_URL}/api/v1/user/settings`, data);
            commit('EDIT_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    }
};
