export default {
    GET_ALL: (state, payload) => {
        state.list = payload.data;
        state.list.push({name:'No Member',id:-1});
    },
    ADD_ITEM: (state, payload) => {
        state.list.push(payload.data);
    },
    EDIT_ITEM: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.data.id) {
                Object.assign(state.list[i], payload.data);
                break;
            }
        }
    },
    DELETE_ITEM: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.id) {
                return state.list.splice(i, 1);
            }
        }
    },
    GET_PLANNINGS: (state, payload) => {
        state.plannings = payload.data;
    },
    BAD: (state, payload) => {
        alert(payload.message || 'Something Bad');
    }
};
