import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import holiday from './holiday';
import member from './member';
import user from './user';
import constructionSiteSettings from './constructionSiteSettings';

export default {
	namespaced: true,
	actions,
	state,
	mutations,
	getters,
    modules:{
        user,
        member,
        constructionSiteSettings,
        holiday
	}
};
