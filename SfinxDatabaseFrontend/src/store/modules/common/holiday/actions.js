import axios from 'axios';

export default {
    all: async ({commit, state}, {link, queries}) => {
        // if(state.isChached)return;
        state.isChached = true;
        let res;
        try {
            res = await axios.get(
                link ? link : `${process.env.API_URL}/api/v1/holiday?${queries}`
            );
            commit('GET_ALL', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
        return res;

    },
    editItem: async ({commit}, data) => {
        try {
            const res = await axios.put(
                `${process.env.API_URL}/api/v1/holiday/${data.id}`,
                data
            );
            commit('EDIT_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(
                `${process.env.API_URL}/api/v1/holiday/${data.id}`);
            commit('DELETE_ITEM', data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    addItem: async ({commit}, data) => {
        try {
            const res = await axios.post(
                `${process.env.API_URL}/api/v1/holiday`,
                data
            );
            commit('ADD_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }

    }
};
