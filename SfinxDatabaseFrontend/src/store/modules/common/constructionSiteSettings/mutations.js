export default {
    GET_ALL: (state, payload) => {
        state.list = payload.data;
    },
    BAD: (state, payload) => {
        alert(payload.message || 'Something Bad');
    }
};
