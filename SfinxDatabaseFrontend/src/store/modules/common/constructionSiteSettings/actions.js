import axios from 'axios';

export default {
    all: async ({commit,state}, id) => {
        if(state.isChached && !id)return;
        state.isChached = true;
        try {
            const res = await axios.get(
                `${process.env.API_URL}/api/v1/construction-site-setting-type`
            );
            commit('GET_ALL', res.data);
        } catch (error) {
            commit('BAD', {error});
        }

    },
};
