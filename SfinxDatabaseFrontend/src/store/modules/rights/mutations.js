export default {
    ALL: (state, payload) => {
        state.list = payload.data;
        state.list.sort((a,b)=>a.sequence>b.sequence?1:-1)
    },
    ADD: (state, payload) => {
        state.list.push(payload.data);
    },
    EDIT: (state, payload) => {
        for(let i=0;i<state.list.length;i++){
            if(state.list[i].id == payload.data.id){
                return Object.assign(state.list[i],payload.data);
            }
        }
    },
    DELETE_ITEM: (state, payload) => {
        for(let i=0;i<state.list.length;i++){
            if(state.list[i].id == payload.id){
                return state.list.splice(i,1)
            }
        }
    },

    BAD: (res) => {
        console.log(res);
        alert(res.message);
    }
};
