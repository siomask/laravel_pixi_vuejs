import axios from 'axios';

export default {
    all: async ({commit}) => {
        const res = await axios.get(`${process.env.API_URL}/api/v1/rights`);
        commit('ALL', res.data);
        return res
    },
    add: async ({commit}, data) => {
        const res = await axios.post(`${process.env.API_URL}/api/v1/rights`, data);
        if (res.status == 201) {
            commit('ADD', res.data);
        } else {
            commit('BAD', res);
        }
    },
    edit: async ({commit}, data) => {
        const res = await axios.put(`${process.env.API_URL}/api/v1/rights/${data.id}`, data);
        if (res.status == 200) {
            commit('EDIT', res.data);
        } else {
            commit('BAD', res);
        }
    },
    deleteItem: async ({commit}, data) => {
        const res = await axios.delete(`${process.env.API_URL}/api/v1/rights/${data.id}`);
        if (res.status == 200) {
            commit('DELETE_ITEM', data);
        } else {
            commit('BAD', res);
        }
    }
};
