export default {
	ALL: (state, payload) => {
		state.list = payload.data;
		state.isCached = true;
	},
	GET_ALL_COLLABORATORS: (state, payload) => {
		state.allCollaborators = payload.data;
	},
    EDIT_SITE: (state, payload) => {

	}
};
