import axios from 'axios';

export default {
    all: async ({commit}, data, id) => {
        if (data) {
            commit('ALL', data);
        } else {
            commit('BAD', {message: 'No Data'});
        }
    },
    editItem: async function ({commit}, data) {
        try {
            const res = await axios.put(
                `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/collaborator/${data.id}`,
                data
            );
            commit('EDIT', res);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(
                `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/collaborator/${data.id}`
            );
            commit('DELETE', data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    addItem: async function ({commit}, data) {
        try {
            let res = await axios.post(
                `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/collaborator`,
                data
            );
            commit('ADD', res);
        } catch (error) {
            commit('BAD', {error});
        }
    }
};
