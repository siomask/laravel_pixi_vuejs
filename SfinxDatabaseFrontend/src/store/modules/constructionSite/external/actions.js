import axios from 'axios';
export default {
	all: async ({ commit }, data) => {
		commit('GET_ALL', data);
	},
	editItem: async({ commit }, data)=>{
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/external/${data.id}`,
            data
        );
        if (res.status === 200) {
            commit('EDIT', res.data);
        }else{
            commit('BAD', res);
		}
	},
	deleteItem: async ({ commit }, data) => {
		const res = await axios.delete(
			`${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/external/${data.id}`
		);
		if (res.status === 200) {
			commit('DELETE', data);
		}else{
            commit('BAD', res);
        }
	},
	addItem: async ({ commit }, data) => {
		const res = await axios.post(
			`${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.siteId}/external`,
			data
		);
		if (res.status === 201) {
			commit('ADD', res.data);
		}else{
            commit('BAD', res);
        }
	}
};
