import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import external from './external';
import projectLeader from './projectLeader';
import estimator from './estimator';
import status from './status';
import collaborator from './collaborator';
export default {
	namespaced: true,
	state,
	mutations,
	getters,
	actions,
	modules:{
        external,
        estimator,
        status,
        collaborator,
        projectLeader
	}
};
