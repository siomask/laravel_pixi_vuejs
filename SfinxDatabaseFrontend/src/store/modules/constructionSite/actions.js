import axios from 'axios';
export default {

    all: async ({commit,state}, {link, queries}={}) => {
        try {
            // if (!state.isCached) {
                let res = await axios.get(link ? link :`${process.env.API_URL}/api/v1/construction-site?${queries}`);
                commit('ALL', res.data);
                return res;
            // }
        } catch (error) {
            commit('BAD', {error});
        }
    },
	getAllCollaboratorsData: async ({ commit }, id) => {
		const res = await axios.get(
			`${
				process.env.API_URL
			}/api/v1/planner/construction-site/${id}/collaborator`
		);
		commit('GET_ALL_COLLABORATORS', res.data);
	},
    editItem: async({ commit }, data)=>{
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.id}`,
            data
        );
        if (res.status === 201) {
            commit('EDIT_SITE', res.data);
        }
        return res.data;
	}
};
