import axios from 'axios';
export default {
	all: async({ commit }, id)=>{
         try{
             let res = await axios.get(
                 `${
                     process.env.API_URL
                     }/api/v1/construction-site-status`
             );
             commit('ALL', res.data);
		 }catch (e) {
             commit('BAD', {message:'No Data'});
         }

	}
};
