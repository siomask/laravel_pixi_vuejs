export default {
    ALL: (state, payload) => {
        state.list = payload.data;
    },
    ADD: (state, payload) => {
        state.list.push(payload.data);
    },
    EDIT: (state, payload) => {
        for(let i=0;i<state.list.length;i++){
            if(state.list[i].id ==payload.data.id){
                Object.assign(state.list[i],payload.data);
                break;
            }
        }
    },
    DELETE: (state, payload) => {
        for(let i=0;i<state.list.length;i++){
            if(state.list[i].id ==payload.id){
                return state.list.splice(i,1);
            }
        }
    },
    BAD: (state, payload) => {
       alert(payload.message ||'Something Bad');
    }
}
