export default {
	setMini: (state, payload) => {
		state.mini = payload;
	},
	setSiteToolsVisibility: (state, payload) => {
		state.siteToolsVisibility = payload;
	},
	setAddContactsDialogVisibility: (state, payload) => {
		state.addContactsDialogVisibility = payload;
	},
	setPLanerViewer: (state, payload) => {
		state.planerViewer = payload;
	},
	setWorkflowSettingsVisibility: (state, payload) => {
		state.workflowSettingsVisibility = payload;
	}
};
