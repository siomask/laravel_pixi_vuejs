export default {
	mini: true,
	siteToolsVisibility: false,
	addContactsDialogVisibility: false,
	planerViewer: false,
	workflowSettingsVisibility: false
};
