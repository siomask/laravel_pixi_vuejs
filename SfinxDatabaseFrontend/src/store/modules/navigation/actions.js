export default {
	changeMini: (context, payload) => {
		context.commit('setMini', payload);
	},
	changeSiteToolsVisibility: (context, payload) => {
		context.commit('setSiteToolsVisibility', payload);
	},
	changeAddContactsDialogVisibility: (context, payload) => {
		context.commit('setAddContactsDialogVisibility', payload);
	},
	changePLanerViewer: (context, payload) => {
		context.commit('setPLanerViewer', payload);
	},
	changeWorkflowSettingsVisibility: (context, payload) => {
		context.commit('setWorkflowSettingsVisibility', payload);
	}
};
