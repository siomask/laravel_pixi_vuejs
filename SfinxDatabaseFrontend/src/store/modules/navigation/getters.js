export default {
	mini: state => {
		return state.mini;
	},
	siteToolsVisibility: state => {
		return state.siteToolsVisibility;
	},
	addContactsDialogVisibility: state => {
		return state.addContactsDialogVisibility;
	},
	planerViewer: state => {
		return state.planerViewer;
	},
	workflowSettingsVisibility: state => {
		return state.workflowSettingsVisibility;
	}
};
