import axios from 'axios';
export default {
	getTeamContacts: async ({ commit }, id) => {
		const res = await axios.get(
			`${process.env.API_URL}/api/v1/contact/${id}/team`
		);
		commit('GET_ALL_TEAM_CONTACTS', res.data);
	}
};
