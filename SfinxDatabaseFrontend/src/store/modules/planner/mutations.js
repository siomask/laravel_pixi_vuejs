import {PlannerHistoryService} from '@/services';
export default {
	WARNING_CONFLICT: (state, payload) => {
		return state.warnings.conflicts.push((payload));
	},
	WARNING_CONFLICT_CLEAN: (state, payload) => {
		return state.warnings.conflicts=[];
	},
	UTIL_EMTY_ROW: (state, payload) => {
		return state.utils.emty_rows=!state.utils.emty_rows;
	},
    HISTORY_RESET: (state, payload) => {
		return state.history = new PlannerHistoryService();
	},
	HISTORY_UNDO: (state, payload) => {
		return state.history.undo();
	},
    HISTORY_REDO: (state, payload) => {
		return state.history.redo();
	},
    SHOW_MAIN_PAGE: (state, payload) => {
		return state.main_page = payload;
	}
}
