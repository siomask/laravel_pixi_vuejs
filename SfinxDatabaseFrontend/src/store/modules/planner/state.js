import {BrowserStorageService,PlannerHistoryService} from '@/services';
export default {
	warnings: {
		conflicts:[]
	},
	history: new PlannerHistoryService(),
	utils:{
		emty_rows:(!!BrowserStorageService.getItem(BrowserStorageService.PREFIX.MAIN + BrowserStorageService.PREFIX.PLANNER + BrowserStorageService.PREFIX.PLANNER_BRANCH))
	},
    main_page:1
}
