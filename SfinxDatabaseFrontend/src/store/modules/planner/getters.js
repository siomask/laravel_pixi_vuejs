export default {
	warnings: state => state.warnings,
	history: state => state.history,
	emty_rows: state => state.utils.emty_rows,
	main_page: state => state.main_page
}
