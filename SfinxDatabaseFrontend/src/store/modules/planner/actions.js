export default {
    toggleEmptyRowsFromAllSites: (context, payload) => {
        context.commit('UTIL_EMTY_ROW', payload);
    },
    showMainPage: (context, payload) => {
        context.commit('SHOW_MAIN_PAGE', payload);
    },
    historyReset: (context, payload) => {
        context.commit('HISTORY_RESET', payload);
    },
    historyUndo: (context, payload) => {
        context.commit('HISTORY_UNDO', payload);
    },
    historyRedo: (context, payload) => {
        context.commit('HISTORY_REDO', payload);
    },
};
