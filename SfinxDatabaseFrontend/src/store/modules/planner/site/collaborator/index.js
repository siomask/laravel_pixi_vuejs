import state     from './state';
import getters   from './getters';
import mutations from './mutations';
import actions   from './actions';
import planning   from './planning';

export default {
	namespaced: true,
	state,
	mutations,
	getters,
	actions,
	modules:{
        planning
	}
};
