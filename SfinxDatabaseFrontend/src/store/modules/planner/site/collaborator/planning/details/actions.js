import axios from "axios";

export default {
    editItem: async ({commit}, data) => {
        try {
            let res = await axios.put(`${process.env.API_URL}/api/v1/planner/construction-site/${data.siteId}/collaborator/${data.collaboratorId}/planning/${data.planningId}/details/${data.id}`, data);
            commit('EDIT', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
};
