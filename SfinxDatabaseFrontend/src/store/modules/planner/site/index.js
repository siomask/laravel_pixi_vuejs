import state     from './state';
import getters   from './getters';
import mutations from './mutations';
import actions   from './actions';
import collaborator   from './collaborator';

export default {
	namespaced: true,
	state,
	mutations,
	getters,
	actions,
	modules:{
        collaborator
	}
};
