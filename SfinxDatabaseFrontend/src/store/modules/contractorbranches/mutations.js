export default {
	GET_ALL_CONTRACTORS_TYPE: (state, payload) => {
		state.contractorTypes = payload.data;
	},
	ADD_CONTRACTOR_TYPE: (state, payload) => {
		state.contractorTypes.push({
			id: payload.id,
			abbreviation: payload.abbreviation,
			name: payload.name,
			sequence: payload.sequence
		});
	},
	DELETE_CONTRACTOR_TYPE: (state, id) => {
		state.contractorTypes = state.contractorTypes.filter(contractor => {
			return contractor.id !== id;
		});
	},
	EDIT_CONTRACTOR_TYPE: (state, payload) => {
		state.contractorTypes = state.contractorTypes.map(contractor => {
			if (contractor.id === payload.id) {
				return {
					...contractor,
					abbreviation: payload.abbreviation,
					name: payload.name
				};
			} else {
				return contractor;
			}
		});
	}
};
