import axios from 'axios';
export default {
	getAllContractorType: async ({ commit }) => {
		const res = await axios.get(
			`${process.env.API_URL}/api/v1/contractor-type`
		);
		commit('GET_ALL_CONTRACTORS_TYPE', res.data);
	},
	addContractorType: async ({ commit }, data) => {
		const res = await axios.post(
			`${process.env.API_URL}/api/v1/contractor-type`,
			data
		);
		if (res.status == 201) {
			commit('ADD_CONTRACTOR_TYPE', res.data.data);
		}
	},
	deleteContractorType: async ({ commit }, id) => {
		const res = await axios.delete(
			`${process.env.API_URL}/api/v1/contractor-type/${id}`
		);
		if (res.status === 200) {
			commit('DELETE_CONTRACTOR_TYPE', id);
		}
	},
	editContractorType: async ({ commit }, data) => {
		const res = await axios.put(
			`${process.env.API_URL}/api/v1/contractor-type/${data.id}`,
			data
		);
		if (res.status === 200) {
			commit('EDIT_CONTRACTOR_TYPE', data);
		}
	}
};
