export default {
    GET_WORKFLOW: (state, payload) => {
        state.workflowTask = payload.tasks;
        state.workflowFunctionGroups = payload.function_groups.sort((a, b) => a.sequence > b.sequence ? 1 : -1);
        state.workflowConstructionSites = payload.selected_construction_sites.sort((a, b) => a.status && b.status && a.status.id > b.status.id ? 1 : -1);
        state.workflowConstructionSitesAll = payload.all_construction_sites;
        state.projectLeaders = payload.projectLeaders;

        let _colors = ['red', 'green', 'yellow', 'purple', 'blue', 'pink'],//TODO: hardcoded list of colors
            groups = {},
            noStatus = -1,
            _els = state.workflowConstructionSites;
        state.workflowConstructionSites = [];
        _els.forEach((el) => {
            if(el.status){
                if (!groups[el.status.id]) {
                    groups[el.status.id] = {
                        color: _colors.shift(),
                        els: []
                    };
                    state.workflowConstructionSites.push(groups[el.status.id].els);
                    state.workflowConstructionSites[state.workflowConstructionSites.length - 1].name = el.status.name;
                }
                if (!el.color) el.color = groups[el.status.id].color;
                groups[el.status.id].els.push(el);
            }else{
                if (!groups[noStatus]) {
                    groups[noStatus] = {
                        color: _colors.shift(),
                        els: []
                    };
                    state.workflowConstructionSites.push(groups[noStatus].els);
                    state.workflowConstructionSites[state.workflowConstructionSites.length - 1].name ="No Status";
                }
                groups[noStatus].els.push(el);
            }

        });
        state.workflowConstructionSites.forEach((el) => {
            el.sort((a, b) => a.sequence < b.sequence ? -1 : 1)
        });
    },
    ADD_WORKFLOW_TASK: (state, payload) => {
        state.workflowTask = Object.values(state.workflowTask);
        if (payload.parent_workflow_task_id !== null) {
            state.workflowTask.map(task => {
                if (task.id === payload.parent_workflow_task_id) {
                    task.sub_tasks &&
                    task.sub_tasks.push({
                        name: payload.name,
                        description: payload.description,
                        sequence: payload.sequence,
                        id: payload.id,
                        parent_workflow_task_id: payload.parent_workflow_task_id
                    });
                } else {
                    task.sub_tasks &&
                    task.sub_tasks.map(sub_task => {
                        if (sub_task.id === payload.parent_workflow_task_id) {
                            sub_task.sub_tasks.push({
                                name: payload.name,
                                description: payload.description,
                                sequence: payload.sequence,
                                id: payload.id,
                                parent_workflow_task_id: payload.parent_workflow_task_id
                            });
                        }
                    });
                }
            });
        } else {
            state.workflowTask.push({
                name: payload.name,
                description: payload.description,
                sequence: payload.sequence,
                id: payload.id
            });
        }
        return state.workflowTask;
    },
    DELETE_WORKFLOW_TASK: (state, id) => {
        state.workflowTask = Object.values(state.workflowTask);
        state.workflowTask = state.workflowTask.filter(task => {
            task.sub_tasks.filter((sub_task, index) => {
                if (sub_task.id === id) {
                    task.sub_tasks.splice(index, 1);
                }
                sub_task.sub_tasks.filter((task, index) => {
                    if (task.id === id) {
                        sub_task.sub_tasks.splice(index, 1);
                    }
                });
            });
            return task.id !== id;
        });
    },
    UPDATE_SETTINGS: (state, payload) => {
        state.settings = payload;
    },
    EDIT_WORKFLOW_TASK: (state, payload) => {
        for (const key in state.workflowTask) {
            if (state.workflowTask.hasOwnProperty(payload.id)) {
                return {
                    ...key,
                    name: payload.name,
                    sequence: payload.sequence,
                    description: payload.description
                };
            }
        }
    },
    ADD_WORKFLOW_FUNCTION_GROUP: (state, payload) => {
        state.workflowFunctionGroups.push({
            name: payload.name
        });
    },
    DELETE_WORKFLOW_FUNCTION_GROUP: (state, id) => {
        state.workflowFunctionGroups = state.workflowFunctionGroups.filter(
            functionGroup => {
                return functionGroup.id !== id;
            }
        );
    },
    EDIT_WORKFLOW_FUNCTION_GROUP: (state, payload) => {
        state.workflowFunctionGroups = state.workflowFunctionGroups.map(
            functionGroup => {
                if (functionGroup.id === payload.id) {
                    return {
                        ...functionGroup
                    };
                } else {
                    return functionGroup;
                }
            }
        );
    },
    GET_WORKFLOW_TASK_CONSTRUCTION_SITE: (state, payload) => {
        state.workflowTaskConstructionSite = payload.data;
    },
    ADD_WORKFLOW_TASK_CONSTRUCTION_SITE: (state, payload) => {
        state.workflowTaskConstructionSite.push({
            name: payload.name
        });
    },
    EDIT_WORKFLOW_TASK_CONSTRUCTION_SITE: (state, payload) => {
        state.workflowConstructionSites = state.workflowConstructionSites.map(
            constructionSite => {
                if (constructionSite.id === payload.id) {
                    return {
                        ...constructionSite,
                        start: payload.start,
                        end: payload.end,
                        task_status: payload.task_status
                    };
                } else {
                    return constructionSite;
                }
            }
        );
    },
    DELETE_WORKFLOW_TASK_CONSTRUCTION_SITE: (state, payload) => {
        state.workflowTaskConstructionSite = state.workflowTaskConstructionSite.filter(
            constructionSite => {
                return constructionSite.id !== id;
            }
        );
    }
};
