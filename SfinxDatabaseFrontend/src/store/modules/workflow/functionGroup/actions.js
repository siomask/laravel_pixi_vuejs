import axios from 'axios';

export default {
    all: async ({commit}, data) => {
        commit('ALL', data);
    },
    editItem: async ({commit}, data) => {
        try {
            let res = await axios.put(
                `${process.env.API_URL}/api/v1/workflow/function-group/${data.id}`,
                data
            );
            commit('EDIT_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    addItem: async ({commit}, data) => {
        try {
            let res = await axios.post(
                `${process.env.API_URL}/api/v1/workflow/function-group`,
                data
            );
            data._originItem =res.data.data;
            commit('ADD_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },

    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(`${process.env.API_URL}/api/v1/workflow/function-group/${data.id}`);
            commit('DELETE_ITEM', data);
        } catch (error) {
            commit('BAD', {error});
        }
    }
};
