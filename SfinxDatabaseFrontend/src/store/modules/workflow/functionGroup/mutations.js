export default {
    ALL: (state, payload) => {
        state.list = payload;
    },
    ADD_ITEM: (state, payload) => {
        state.list.push(payload.data);
    },
    EDIT_ITEM: (state, payload) => {
        for (let i = 0, _l = state.list, _length = _l.length; i < _length; i++) {
            let _item = state.list[i];
            if (_item.id == payload.data.id) {
                if (_item.oldSequence > payload.data.sequence) {//shift to left|top
                    for (let di = 0; di < _length; di++) {
                        if (_l[di].sequence >= payload.data.sequence && _l[di].sequence <= _item.oldSequence) {
                            state.list[di].sequence += 1;
                        }

                    }
                } else if (_item.oldSequence < payload.data.sequence) {//shift to right|bottom
                    for (let di = 0; di < _length; di++) {
                        if (_l[di].sequence >= _item.oldSequence && _l[di].sequence <= payload.data.sequence) {
                            state.list[di].sequence -= 1;
                        }
                    }
                }
                Object.assign(_item, payload.data);
                break;
            }
        }
    },
    DELETE_ITEM: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.id) {
                return state.list.splice(i, 1);
            }
        }
    },
    BAD: (state, payload) => {
        alert(payload.error.response.data.message);
    },

};
