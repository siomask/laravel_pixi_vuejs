import state from './state';
import actions from './actions';
import mutations from './mutations';
import getters from './getters';
import constructionSite from './constructionSite';
import task from './task';
import functionGroup from './functionGroup';

export default {
	namespaced: true,
	state,
	mutations,
	actions,
	getters,
    modules:{
        functionGroup,
        constructionSite,
        task
	}
};
