export default {
    isCached: false,
    settings: null,
    workflowTask: [],
    workflowFunctionGroups: [],
    workflowConstructionSites: [],
    workflowTaskConstructionSite: [],
    projectLeaders: [],
    workflowConstructionSitesAll: []
};
