export default {
	getSettings: state => state.settings,
	getWorkflowTask: state => state.workflowTask,
	getWorkflowFunctionGroups: state => state.workflowFunctionGroups,
	getWorkflowConstructionSites: state => state.workflowConstructionSites,
    allProjectLeaders: state => state.projectLeaders,
    allworkflowConstructionSites: state => state.workflowConstructionSitesAll
};
