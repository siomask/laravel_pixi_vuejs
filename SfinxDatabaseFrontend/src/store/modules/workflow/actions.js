import axios from 'axios';

export default {
    refreshOrder: async ({commit, state}) => {
        state.workflowFunctionGroups = state.workflowFunctionGroups.sort((a, b) => a.sequence > b.sequence ? 1 : -1);
    },
    getWorkFlow: async ({commit, state}) => {
        if (state.isCached) return;
        state.isCached = true;
        const res = await axios.get(`${process.env.API_URL}/api/v1/workflow`);
        commit('GET_WORKFLOW', res.data);
    },
    clearCache: async ({commit, state}) => {
        state.isCached = false;
    },
    addWorkFlowTask: async ({commit}, data) => {
        const res = await axios.post(
            `${process.env.API_URL}/api/v1/workflow/task`,
            data
        );
        if (res.status === 201) {
            commit('ADD_WORKFLOW_TASK', res.data.data);
        }
    },
    editWorkFlowTask: async ({commit}, data) => {
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/workflow/task/${data.id}`,
            data
        );
        if (res.status === 200) {
            commit('EDIT_WORKFLOW_TASK', data);
        }
    },
    deleteWorkFlowTask: async ({commit}, id) => {
        const res = await axios.delete(
            `${process.env.API_URL}/api/v1/workflow/task/${id}`
        );
        if (res.status === 200) {
            commit('DELETE_WORKFLOW_TASK', id);
        }
    },
    updateSettings: async ({commit}, value) => {
        commit('UPDATE_SETTINGS', value);
    },
    addWorkFlowFunctionGroup: async ({commit}, data) => {
        const res = await axios.post(
            `${process.env.API_URL}/api/v1/workflow/task/${id}/function-group`,
            data
        );
        if (res.status === 201) {
            commit('ADD_WORKFLOW_FUNCTION_GROUP', data);
        }
    },
    editWorkFlowFunctionGroup: async ({commit}, data) => {
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/workflow/task/${id}/function-group/${
                data.id
                }`
        );
        if (res.status === 200) {
            commit('EDIT_WORKFLOW_FUNCTION_GROUP', data);
        }
    },
    deleteWorkFlowFunctionGroup: async ({commit}, id) => {
        const res = await axios.delete(
            `${process.env.API_URL}/api/v1/workflow/task/${id}/function-group/${id}`
        );
        if (res.status === 200) {
            commit('DELETE_WORKFLOW_FUNCTION_GROUP', id.id);
        }
    },
    getWorkFlowTaskConstructionSite: async ({commit}, id) => {
        const res = await axios.get(
            `${
                process.env.API_URL
                }/api/v1/workflow/construction-site/${id}/workflow-task`
        );
        commit('GET_WORKFLOW_TASK_CONSTRUCTION_SITE', res.data);
    },
    addWorkFlowTaskConstructionSite: async ({commit}, data) => {
        const res = await axios.post(
            `${
                process.env.API_URL
                }/api/v1/workflow/construction-site/${id}/workflow-task`
        );
        if (res.status === 201) {
            commit('ADD_WORKFLOW_TASK_CONSTRUCTION_SITE', data);
        }
    },
    deleteWorkFlowTaskConstructionSite: async ({commit}, id) => {
        const res = await axios.delete(
            `${
                process.env.API_URL
                }/api/v1/workflow/construction-site/${id}/workflow-task/${id}`
        );
        if (res.status === 200) {
            commit('DELETE_WORKFLOW_TASK_CONSTRUCTION_SITE', id.id);
        }
    },
    editWorkFlowTaskConstructionSite: async ({commit}, data) => {
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/workflow/construction-site/${
                data.id
                }/workflow-task/${data.task_id}`,
            data
        );
        if (res.status === 200) {
            commit('EDIT_WORKFLOW_TASK_CONSTRUCTION_SITE', data);
        }
    }
};
