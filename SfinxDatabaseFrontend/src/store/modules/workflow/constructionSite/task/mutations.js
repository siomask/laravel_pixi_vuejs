export default {
    ALL: (state, payload) => {
        state.list = payload;
    },
    ADD: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            let group = state.list[i];
            for (let di = 0; di < group.length; di++) {
                if (group[di].id == payload.data.site_id) {
                    return group[di].tasks.push(payload.data);
                }
            }
        }

    },
    EDIT: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            let group = state.list[i];
            for (let di = 0; di < group.length; di++) {
                if (group[di].id == payload.data.site_id) {
                    if(group[di].tasks){
                        for (let ids = 0, tasks = group[di].tasks; ids < tasks.length; ids++) {
                            if (tasks[ids].id == payload.data.id) {
                                return Object.assign(tasks[ids], payload.data);
                            }
                        }
                    }
                    group[di].tasks.push(payload.data);
                    break;
                }

            }
        }
    },
    BAD: (state, payload) => {
        console.log(payload);
        alert(payload.error.response.data.message);
    }
};
