import axios from 'axios';

export default {
    all: async ({commit}, data) => {
        commit('ALL', data);
    },
    addItem: async ({commit}, data) => {
        try {
            let res = await axios.post(
                `${process.env.API_URL}/api/v1/workflow/construction-site/${data.site_id}/workflow-task`,
                data
            );
            commit('ADD', res.data);
        } catch (error) {
            commit('BAD',  {error});
        }

    },
    editItem: async ({commit}, data) => {
        try {
            let res = await axios.put(
                `${process.env.API_URL}/api/v1/workflow/construction-site/${data.site_id}/workflow-task/${data.id}`,
                data
            );
            commit('EDIT', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    }
};
