import axios from 'axios';

export default {
    all: async ({commit}, data) => {
        commit('ALL', data);
    },
    editItem: async ({commit}, data) => {
        try {
            let res = await axios.put(
                `${process.env.API_URL}/api/v1/client/${data.clientId}/construction-site/${data.id}`,
                data
            );
            commit('EDIT', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    }
};
