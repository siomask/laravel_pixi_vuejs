export default {
    ALL: (state, payload) => {
        state.list = payload;
    },
    EDIT: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            let group = state.list[i];
            for (let di = 0; di < group.length; di++) {
                if (group[di].id == payload.data.id) {
                    return Object.assign(group[di], payload.data);
                }

            }
        }
    },
    BAD: (state, payload) => {
        alert(payload.error.response.data.message);
    }
};
