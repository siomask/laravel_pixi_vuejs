export default {
	all: state => state.list,
	selected: state => {
		let items = [];
        state.list.forEach((el)=>items=el.concat(items));
        return items;
	}
};
