export default {
    getMainContactsTypes: state => {
    	let _res =state.contactsTypes.filter((el)=>{return el.contact_type_id == null;});
        if(_res.length)_res.splice(0,0, _res.splice(2,1)[0]);
        return _res;
	},
	getAllContactsTypes: state => state.contactsTypes,
	getAllContacts: state => state.contacts,
	all: state => state.contacts,
	getAllCollaborators: state => state.collaborators,
	getAllInternTypeContacts: state => state.contacts_with_intern_team,
	getContacts: state => state.contacts
};
