import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import team from './team';

export default {
	namespaced: true,
	actions,
	state,
	mutations,
	getters,
    modules:{
        team
	}
};
