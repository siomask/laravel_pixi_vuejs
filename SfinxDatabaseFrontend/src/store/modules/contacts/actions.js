import axios from 'axios';

export default {
    getAllContacts: async ({commit, state}) => {
        // if (!state.isCached) {
        // 	const res = await axios.get( `${process.env.API_URL}/api/v1/contact?type=all`);
        // 	commit('GET_ALL_CONTACTS', res.data);
        // }
        // return res;
        console.warn('Depricated: use contacts/all');
        return {};
    },
    all: async ({commit, state}, {link, queries}) => {
        // if (!state.isCached) {
        const res = await axios.get(link ? link : `${process.env.API_URL}/api/v1/contact?${queries}`);
        commit('GET_ALL_CONTACTS', res.data);
        // }
        return res;

    },
    getAllContactsTypes: async ({commit}) => {
        const res = await axios.get(`${process.env.API_URL}/api/v1/contact/types`);
        commit('GET_ALL_CONTACTS_TYPE', res.data);
    },
    getAllInternTypeContacts: async ({commit}) => {
        const res = await axios.get(`${process.env.API_URL}/api/v1/contact?type=9`);
        commit('GET_ALL_CONTACTS_WITH_INTERN_TEAM', res.data);
    },
    create: async ({commit}, data) => {
        try {
            const res = await axios.post(`${process.env.API_URL}/api/v1/contact`, data);
            if (res.status === 201) {
                commit('ADD_CONTACT', res.data.data);
            }
        } catch (error) {
            commit('BAD', {...error});
        }

    }
};
