export default {
	GET_ALL_CONTACTS: (state, payload) => {
		if(state.collaborators.length ==0)state.collaborators=[{name:'No Collaborator',id:-1}];
        for (let i = 0; i < payload.data.length; i++) {
            let shouldAdd = true;
            for (let j = 0; j < state.contacts.length; j++) {
                if (state.contacts[j].id == payload.data[i].id) {
                    shouldAdd = false;
                    break;
                }
            }
            if (shouldAdd) {
            	state.contacts.push(payload.data[i]);
                // state.contacts[state.contacts.length-1].__id=state.contacts.length;
            }

            let shouldAddC = true;
            for (let j = 0; j < state.collaborators.length; j++) {
                if (state.collaborators[j].id == payload.data[i].id) {
                    shouldAddC = false;
                    break;
                }
            }
            if (shouldAddC) {
            	state.collaborators.push(payload.data[i]);
            }
        }
        // state.contacts =state.contacts.concat([]);
	},
    GET_ALL_CONTACTS_TYPE: (state, payload) => {
		state.contactsTypes = payload.map((el)=>{
			el.selected = false;
			return el;
		});
    },
	GET_ALL_CONTACTS_WITH_INTERN_TEAM: (state, payload) => {
		state.contacts_with_intern_team = payload.data;
	},
	ADD_CONTACT: (state, payload) => {
		state.contacts = Object.assign(state.contacts).push(payload.data);
	},
    BAD: (state, payload) => {
        alert(payload.message || 'Something Bad');
    }
};
