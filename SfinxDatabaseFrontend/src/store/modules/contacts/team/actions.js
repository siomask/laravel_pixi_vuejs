import axios from 'axios';

export default {
    all: async ({commit}, id) => {
        try {
            const res = await axios.get(
                `${process.env.API_URL}/api/v1/contact/${id}/team`
            );
            commit('GET_ALL', res.data);
        } catch (error) {
            commit('BAD', {error});
        }

    },
    editItem: async ({commit}, data) => {
        try {
            const res = await axios.put(
                `${process.env.API_URL}/api/v1/contact/${data.contactId}/team/${data.id}`,
                data
            );
            commit('EDIT_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    deleteItem: async ({commit}, data) => {
        try {
            await axios.delete(
                `${process.env.API_URL}/api/v1/contact/${data.contactId}/team/${data.id}`);
            commit('DELETE_ITEM', data);
        } catch (error) {
            commit('BAD', {error});
        }
    },
    addItem: async ({commit}, data) => {
        try {
            const res = await axios.post(
                `${process.env.API_URL}/api/v1/contact/${data.contactId}/team`,
                data
            );
            commit('ADD_ITEM', res.data);
        } catch (error) {
            commit('BAD', {error});
        }

    }
};
