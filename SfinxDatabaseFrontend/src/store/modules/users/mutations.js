export default {
	ALL_USERS: (state, payload) => {
		state.users = payload.data;
	},
	ADD_USER: (state, payload) => {
		state.users.push({
			first_name: payload.first_name,
			name: payload.name,
			username: payload.username,
			password: payload.password,
			id: payload.id
		});
	},
	DELETE_USER: (state, id) => {
		state.users = state.users.filter(user => {
			return user.id !== id;
		});
	},
	EDIT_USER: (state, payload) => {
		state.users = state.users.map(user => {
			if (user.id === payload.id) {
				// return {
				// 	...user,
				// 	name: payload.name,
				// 	first_name: payload.first_name,
				// 	username: payload.username,
				// 	password: payload.password
				// };
                Object.assign(user,payload);
			}
				return user;

		});
	},
    USER_SETTINGS: (state, payload) => {

	}
};
