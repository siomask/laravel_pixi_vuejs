import axios from 'axios';

export default {
    getUsers: async ({commit}) => {
        const res = await axios.get(`${process.env.API_URL}/api/v1/user`);
        commit('ALL_USERS', res.data);
    },
    addUser: async ({commit}, data) => {
        const res = await axios.post(`${process.env.API_URL}/api/v1/user`, data);
        if (res.status === 201) {
            commit('ADD_USER', res.data.data);
        }
    },
    editUser: async ({commit}, data) => {
        const res = await axios.put(
            `${process.env.API_URL}/api/v1/user/${data.id}`,
            data
        );
        if (res.status === 200) {
            commit('EDIT_USER', data);
        }
    },
    editSelf: async ({commit}, data) => {
        let res;
        try {
            res = await axios.put(
                `${process.env.API_URL}/api/v1/self/${data.id}`,
                data
            );
            return res;
            // commit('EDIT_USER_SELF', data);
        } catch (error) {
            return {error};
        }

    },
    deleteUser: async ({commit}, id) => {
        const res = await axios.delete(`${process.env.API_URL}/api/v1/user/${id}`);
        if (res.status === 200) {
            commit('DELETE_USER', id);
        }
    },
    settings: async ({commit}, data) => {
        const res = await axios.post(`${process.env.API_URL}/api/v1/user/settings`, data);
        if (res.status === 200) {
            commit('USER_SETTINGS', data);
        }
    }
};
