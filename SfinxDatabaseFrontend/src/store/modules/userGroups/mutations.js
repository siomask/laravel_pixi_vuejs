export default {
    ALL_USER_GROUPS: (state, payload) => {
        state.list = payload.data;
    },
    ADD: (state, payload) => {
        state.list.push(payload.data);
    },
    EDIT: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.data.id) {
                return Object.assign(state.list[i], payload.data);
            }
        }
    },
    DELETE_ITEM: (state, payload) => {
        for (let i = 0; i < state.list.length; i++) {
            if (state.list[i].id == payload.id) {
                return state.list.splice(i, 1)
            }
        }
    },
    TOGGLE_RIGHT: (state, payload) => {
        if(payload.isDetach){
            for (let i = 0; i < state.list.length; i++) {
                if(state.list[i].id ==payload.group.id){
                    let group = state.list[i];
                    for (let i = 0; i < group.rights.length; i++) {
                        if (group.rights[i].id == payload.right.id) {
                            group.rights.splice(i, 1);
                        }
                    }
                    break;
                }
            }

        }else{
            payload.group.rights.push(payload.right);
        }

    },
    BAD: (res) => {
        alert(res.message);
    }
};
