import axios from 'axios';

export default {
    all: async ({commit}) => {
        const res = await axios.get(`${process.env.API_URL}/api/v1/user-group`);
        commit('ALL_USER_GROUPS', res.data);
        return res
    },
    add: async ({commit}, data) => {
        const res = await axios.post(`${process.env.API_URL}/api/v1/user-group`, data);
        if (res.status == 201) {
            commit('ADD', res.data);
        } else {
            commit('BAD', res);
        }
    },
    edit: async ({commit}, data) => {
        const res = await axios.put(`${process.env.API_URL}/api/v1/user-group/${data.id}`, data);
        if (res.status == 200) {
            commit('EDIT', res.data);
        } else {
            commit('BAD', res);
        }
    },
    deleteItem: async ({commit}, data) => {
        const res = await axios.delete(`${process.env.API_URL}/api/v1/user-group/${data.id}`);
        if (res.status == 200) {
            commit('DELETE_ITEM', data);
        } else {
            commit('BAD', res);
        }
    },
    toggleRight: async ({commit}, {right, group}) => {
        let isDetach = false,
            res;
        for (let i = 0; i < group.rights.length; i++) {
            if (group.rights[i].id == right.id) {
                isDetach = true;
                break;
            }
        }
        if (isDetach) {
            res = await axios.delete(`${process.env.API_URL}/api/v1/user-group/${group.id}/right/${right.id}`);
        } else {
            res = await axios.post(`${process.env.API_URL}/api/v1/user-group/${group.id}/right`,right);
        }

        if (res.status == 200) {
            commit('TOGGLE_RIGHT', {right, group,isDetach});
        } else {
            commit('BAD', res);
        }
    }
};
