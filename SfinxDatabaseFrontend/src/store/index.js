import Vue from 'vue';
import Vuex from 'vuex';

import team from './modules/team';
import rights from './modules/rights';
import planner from './modules/planner';
import navigation from './modules/navigation';
import contractorBranches from './modules/contractorbranches';
import checkoffType from './modules/checkoffType';
import constructionSite from './modules/constructionSite';
import users from './modules/users/';
import members from './modules/members/';
import contacts from './modules/contacts/';
import userGroups from './modules/userGroups/';
import common from './modules/common/';


import finance from './modules/finance/';
import workflow from './modules/workflow/';

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		team,
		rights,
		planner,
		navigation,
		contractorBranches,
		checkoffType,
		constructionSite,
		users,
		contacts,
		members,
		workflow,
        finance,
        userGroups,
        common
	}
});
