import Vue from 'vue'
import App from './App.vue'
import router from './routes/index'
import store from './store/index'

import axios from 'axios'
import VueAxios from 'vue-axios'
import VueAuth from '@websanova/vue-auth'
import Vuetify from 'vuetify'

import 'vuetify/dist/vuetify.min.css'
import '../static/libs/stats.min'
import '../static/libs/pixi.min.js'
import '../static/libs/gifuct-js.min.js'


Vue.config.productionTip = false
Vue.router = router

let customAuth = {
	request: function (req, token) {
		this.options.http._setHeaders.call(this, req, {
			Authorization: 'Bearer ' + token
		})
	},
	response: function (res) {
		let token = res.data.access_token
		if (token) {
			return token
		}
	}
}

Vue.use(Vuetify,{
    theme: {
        // primary: '#4F5370',
        dark: '#4F5370'
    }
})
Vue.use(VueAxios, axios)
Vue.use(VueAuth, {
	auth: customAuth,
	http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
	router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
	refreshData: {url: `${process.env.API_URL}/api/auth/refresh`, method: 'POST'},
	loginData: {url: `${process.env.API_URL}/api/auth/login`, method: 'POST'},
	fetchData: {url: `${process.env.API_URL}/api/auth/me`, method: 'POST'},
	authRedirect: '/login',
	rolesVar: 'rechten'
});

Vue.component('task-tree', require('./components/layout/main/views/+workflow/overview/task.tree/index.vue'));
Vue.component('task-tree-node', require('./components/layout/main/views/+workflow/overview/task.tree/taskTreeNode.vue'));
Vue.component('task-tree-cell', require('./components/layout/main/views/+workflow/overview/task.tree.cell/index.vue'));
Vue.component('task-tree-cell-node', require('./components/layout/main/views/+workflow/overview/task.tree.cell/taskTreeNode.vue'));


Vue.component('item-tree-nested', require('./components/layout/main/utils/list.nested.tree/index.vue'));
Vue.component('item-tree-node-nested', require('./components/layout/main/utils/list.nested.tree/item.nested.vue'));
Vue.component('item-select-load', require('./components/layout/main/utils/filter.select/index'));


/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: {App}
})


