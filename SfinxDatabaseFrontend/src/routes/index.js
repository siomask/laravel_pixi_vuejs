import Vue from 'vue';
import Router from 'vue-router';

import loginComponent from '@/components/layout/login';
import changePswComponent from '@/components/layout/changePsw';
import homeComponent from '@/components/layout/main';
import ErrorComponent from '@/components/layout/main/ErrorComponent';

// Child routes
import { teamRoutes } from './team.routing';
import { invoicesRoutes } from './invoices.routing';
import { contractorbranchesRoutes } from './contractorbranches.routing';
import { checkoffTypeRoutes } from './checkoffType.routing';
import { constructionsiteTodoRoutes } from './constructionsiteTodo.routing';
import { contactsRoutes } from './contacts.routing';
import {financeRoutes} from './finance.routing';
import { workflowRoutes } from './workflow.routing';


// Lazy loaded
const dashboardContainer = () =>
    import('@/components/layout/main/views/+dashboard');
const contactsContainer = () =>
    import('@/components/layout/main/views/+contacts');
const teamContainer = () => import('@/components/layout/main/views/+team');
const invoicesContainer = () =>
    import('@/components/layout/main/views/+invoices');
const rightsContainer = () => import('@/components/layout/main/views/+rights');
const plannerContainer = () =>
    import('@/components/layout/main/views/+planner');
const contractorbranchesContainer = () =>
    import('@/components/layout/main/views/+contractorbranches');
const checkoffTypeContainer = () =>
    import('@/components/layout/main/views/+checkoffType');
const constructionsiteTodoContainer = () =>
    import('@/components/layout/main/views/+constructionsiteTodo');
const holidayContainer = () =>
    import('@/components/layout/main/views/+holidays');
const clientDetailRoutesComponent = () =>
    import('@/components/layout/main/views/+contacts/overview/tabs/clientDetail');
const workflowComponent = () =>
    import('@/components/layout/main/views/+workflow');
// const teamContactComponent = () =>
//     import('@/components/layout/main/views/+team/teamContacts');

const settingsComponent = () =>
    import('@/components/layout/main/views/+settings');

import { settingsRoutes } from './settings.routing';
	// import('@/components/layout/main/views/+team/teamContacts');
const financeComponent = () =>
    import('@/components/layout/main/views/+finance');
import _router from '@/routes';


Vue.use(Router);

const routes = [
	{
		path: '',
		component: homeComponent,
        meta: {
            requiresAuth: true,
            auth: true
        },
        beforeEnter: (to, from, next) => {
			let app = this.a.app;
            app.$auth.fetch({
                success: function (res) {
                    if(res.data.data.shouldRefreshPsw){
                        _router.push('changePsw')
                    }else{
                        next();
					}
                },
                error: function (res) {
                    console.log('Something went wrong');
                }
            });
        },
		children: [
			{
				path: '',
				redirect: '/dashboard'
			},
            {
                path: '/settings',
                name: 'settings',
                component: settingsComponent,
                meta: {
                    auth: true
                },
                children:settingsRoutes
            },
            {
                path: '/finances',
                name: 'finance',
                // redirect: 'finances',
                component: financeComponent,
                meta: {
                    auth: true
                },
                children:financeRoutes
            },
			{
				path: '/workflow',
                name: 'workflow',
				component: workflowComponent,
				meta: { requiresAuth: true,auth: true },
				children: workflowRoutes
			},
			{
				redirect: '/contacts',
				path: '/dashboard',
				name: 'dashboard',
				component: dashboardContainer,
				meta: { auth: true }
			},
			{
				path: '/contacts',
				component: contactsContainer,
				meta: { auth: true },
				children: contactsRoutes
			},
			{
				path: 'client/:id/details',
				meta: { auth: true },
				component: clientDetailRoutesComponent
			},
			/*{
				path: 'contact/:id/teams',
				meta: { auth: true },
				component: teamContactComponent
			},*/
			{
				path: '/invoices',
				component: invoicesContainer,
				meta: { auth: true },
				children: invoicesRoutes
			},
			{
				path: '/teams',
				component: teamContainer,
				meta: { auth: true },
				children: teamRoutes
			},

			{
				path: '/holidays',
				component: holidayContainer,
				meta: { auth: true }
			},
			{
				path: '/planner',
				component: plannerContainer,
				meta: { requiresAuth: true,auth: true }
			},
			{
				path: '/contractorbranches',
				component: contractorbranchesContainer,
				meta: { auth: true },
				children: contractorbranchesRoutes
			},
			{
				path: '/checkofftype',
				component: checkoffTypeContainer,
				meta: { auth: true },
				children: checkoffTypeRoutes
			},
			{
				path: '/constructionsitetodo',
				component: constructionsiteTodoContainer,
				meta: { auth: true },
				children: constructionsiteTodoRoutes
			},
			{
				path: '/not-found',
				name: 'error',
				component: ErrorComponent
			}
		]
	},
	{
		path: '/login',
		name: 'login',
		meta: { auth: false },
		component: loginComponent
	},
	{
		path: '/changePsw',
		name: 'changePsw',
		meta: {
		    requiresAuth: true,
            auth: true
        },
        beforeEnter: (to, from, next) => {
            let app = this.a.app;
            app.$auth.fetch({
                success: function (res) {
                    if(!res.data.data.shouldRefreshPsw){
                        _router.push('dashboard')
                    }else{
                        next();
                    }
                },
                error: function (res) {
                    console.log('Something went wrong');
                }
            });
        },
		component: changePswComponent
	}
];

const router = new Router({
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    if (!to.matched.length) {
        next('/not-found');
    } else {
        next();
    }
});

export default router;
