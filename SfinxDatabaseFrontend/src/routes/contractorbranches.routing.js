const contractorbranchComponent = () =>  import('@/components/layout/main/views/+contractorbranches/overview');
const contractorbranchAddComponent = () => import('@/components/layout/main/views/+contractorbranches/add');

export const contractorbranchesRoutes = [
	{
		path: '',
		component: contractorbranchComponent,
		name: 'aannemingstak'
	},
	{
		path: 'add',
		component: contractorbranchAddComponent,
		name: 'aannemingstak-add'
	}
];
