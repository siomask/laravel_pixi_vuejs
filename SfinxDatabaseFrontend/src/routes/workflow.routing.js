import {taskRoutes} from "./tasks.routing";

const memberContainer = () => import('@/components/layout/main/views/+workflow/+members');
const functionGroup = () => import('@/components/layout/main/views/+workflow/+function.group');
const taskComponent = () => import('@/components/layout/main/views/+workflow/+tasks');
const workFlowOberviewContainer = () =>
    import('@/components/layout/main/views/+workflow/overview/');

export const workflowRoutes = [
    {
        path: '',
        redirect: 'view'
    },
    {
        path: 'view',
        name: 'workflow-overview',
        component: workFlowOberviewContainer,
        meta: {
            auth: true
        }
    },
    {
        path: 'tasks',
        name: 'workflow-tasks',
        component: taskComponent,
        children: taskRoutes,
        meta: {
            auth: true
        }
    },
    {
        path: 'members',
        name: 'workflow-members',
        component: memberContainer,
        meta: {auth: true}
    },
    {
        path: 'function-groups',
        name: 'workflow-function-groups',
        component: functionGroup,
        meta: {auth: true}
    },
];
