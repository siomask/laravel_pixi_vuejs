const settingsComponent = () =>
	import('@/components/layout/main/views/+checkoffType/index');
const userComponent = () => import('@/components/layout/main/views/+settings/+users');
const userGroupComponent = () => import('@/components/layout/main/views/+settings/+usersGroups');
const rightsComponent = () => import('@/components/layout/main/views/+settings/+rights');
const profileComponent = () => import('@/components/layout/main/views/+settings/+profile');
export const settingsRoutes = [
    {
        path: 'users',
        component: userComponent,
        name: 'users'
    },
    {
        path: '',
        redirect: 'profile'
    },
    {
        path: 'user-groups',
        component: userGroupComponent,
        name: 'user-groups'
    },
    {
        path: 'rights',
        component: rightsComponent,
        name: 'rights'
    },
    {
        path: 'profile',
        component: profileComponent,
        name: 'profile'
    }
]
