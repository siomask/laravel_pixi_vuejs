const checkoffTypeComponent = () =>
	import('@/components/layout/main/views/+checkoffType/overview')
const checkoffTypeAddComponent = () =>
	import('@/components/layout/main/views/+checkoffType/add')

export const checkoffTypeRoutes = [
	{
		path: '',
		component: checkoffTypeComponent,
		name: 'checkofftype'
	},
	{
		path: 'add',
		component: checkoffTypeAddComponent,
		name: 'checkofftype-add'
	}
]
