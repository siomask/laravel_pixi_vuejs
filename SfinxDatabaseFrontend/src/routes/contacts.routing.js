const contactsOverviewContainer = () => import('@/components/layout/main/views/+contacts/overview');
const contactsClientsContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/clients');
const contactsContractorsContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/contractors');
const contactsSuppliersContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/suppliers');
const contactsArchitectsContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/architects');
const contactsPreventionOfficersContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/prevention_officers');
const contactsEPBReportersContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/epb_reporters');
const contactsStabilityEngineersContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/stability_engineers');
const contactsConstructionOfficersContainer = () => import('@/components/layout/main/views/+contacts/overview/tabs/construction_officers');
const defContainer = () => import('@/components/layout/main/views/+contacts/default.vue');
const teamContactListOverviewContainer = () => import('@/components/layout/main/views/+contacts/+team/list');
export const contactsRoutes = [
	{
		path: '',
		redirect: 'view',
		component: defContainer,
		children: [

			{
				path: 'view',
				name: 'contacts',
				component: contactsOverviewContainer
			},

			{
				name: 'contacts-detail',
				path: ':id',
				component: defContainer,
				children: [
					{
						name: 'contact-team',
						path: 'team',
						component: teamContactListOverviewContainer
					}
				]
			},
			{
				path: 'clients',
				component: contactsClientsContainer
			},
			{
				path: 'clients',
				component: contactsClientsContainer
			},
			{
				path: 'contractors',
				component: contactsContractorsContainer
			},
			{
				path: 'suppliers',
				component: contactsSuppliersContainer
			},
			{
				path: 'architects',
				component: contactsArchitectsContainer
			},
			{
				path: 'prevention_officers',
				component: contactsPreventionOfficersContainer
			},
			{
				path: 'epb_reporters',
				component: contactsEPBReportersContainer
			},
			{
				path: 'stability_engineers',
				component: contactsStabilityEngineersContainer
			},
			{
				path: 'construction_officers',
				component: contactsConstructionOfficersContainer
			}
		]
	},


	{
		path: '**',
		component: contactsOverviewContainer,
		name: 'catch'
	}
];
