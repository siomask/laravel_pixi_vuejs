const taskContainer = () =>
	import('@/components/layout/main/views/+workflow/+tasks/overview');

export const taskRoutes = [
	{
		path: '',
		component: taskContainer,
		name: 'tasks'
	}
];
