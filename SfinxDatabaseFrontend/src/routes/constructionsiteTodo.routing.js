const constructionsiteTodoComponent = () =>  import('@/components/layout/main/views/+constructionsiteTodo/overview');
const constructionsiteTodoAddComponent = () => import('@/components/layout/main/views/+constructionsiteTodo/add/');


export const constructionsiteTodoRoutes = [
	{
		path: '',
		component: constructionsiteTodoComponent,
		name: 'constructionsitetodo'
	},
	{
		path: 'add',
		component: constructionsiteTodoAddComponent,
		name: 'constructionsitetodo-add'
	}
];
