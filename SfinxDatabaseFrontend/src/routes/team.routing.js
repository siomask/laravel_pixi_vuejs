const teamOverviewContainer = () =>  import('@/components/layout/main/views/+team/overview');
const membertListOverviewContainer = () => import('@/components/layout/main/views/+team/+member');
export const teamRoutes = [
	{
		path: '',
		component: teamOverviewContainer,
		name: 'teams-view'
	},
    {
        name: 'teams-members',
        path: 'teams-members',
        component: membertListOverviewContainer
    },
];
