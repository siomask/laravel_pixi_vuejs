const incomingInvoicesComponent = () => import('@/components/layout/main/views/+finance/+invoice/+incoming');
const incomingInvoiceViewComponent = () => import('@/components/layout/main/views/+finance/+invoice/+incoming/+view');
const outgoingInvoiceViewComponent = () => import('@/components/layout/main/views/+finance/+invoice/+outgoing/+view');

const accountsComponent = () => import('@/components/layout/main/views/+finance/+accounts');
const viewAccountsComponent = () => import('@/components/layout/main/views/+finance/+accounts/overview');
const outgoingInvoicesComponent = () => import('@/components/layout/main/views/+finance/+invoice/+outgoing');
const summaryComponent = () => import('@/components/layout/main/views/+finance/+summary');
const summaryDetailComponent = () => import('@/components/layout/main/views/+finance/+summary/+details');
const summaryMemberComponent = () => import('@/components/layout/main/views/+finance/+summary/+members');

const summarySiteComponent = () => import('@/components/layout/main/views/+finance/+site/');
const summarySiteViewComponent = () => import('@/components/layout/main/views/+finance/+site/view');
const summarySiteViewIncomingInvoicesComponent = () => import('@/components/layout/main/views/+finance/+site/view/incoming.invoice');

const invoiceComponent = () =>
    import('@/components/layout/main/views/+finance/+invoice');
export const financeRoutes = [
    {
        path: '',
        name: 'finances',
        redirect: 'summary',

    },
    {
        path: 'summary',
        name: 'fincance-summary',
        component: summaryComponent,
        children:[
            {
                path: 'details',
                name: 'summary-details',
                component: summaryDetailComponent,
            },
            {
                path: 'members',
                name: 'summary-members',
                component: summaryMemberComponent,
            },
        ]
    },
    {
        path: 'site',
        name: 'fincance-summary-site',
        component: summarySiteComponent,
        children:[
            {
                path: ':id',
                name: 'summary-site-details',
                component: summarySiteViewComponent,
                children:[
                    {
                        path: 'incoming-invoices',
                        name: 'summary-site-details-incoming',
                        component: summarySiteViewIncomingInvoicesComponent,
                    },
                ]
            },
        ]
    },
    {
        path: 'accounts',
        name: 'fincance-summary',
        component: accountsComponent,
        // redirect: 'accounts/',
        children:[
             {
                 path: '',
                 name: 'accounts-view',
                 component: viewAccountsComponent,
             },
        ]
    },
    {
        path: 'invoices',
        name: 'fincance-invoices',
        redirect: 'invoices/incoming',
        component: invoiceComponent,
        children: [
            /* {
                 path: '',
                 redirect: 'incoming',
                 name: 'el-invoices',
             },*/
            {

                path: 'incoming',
                component: incomingInvoicesComponent,
                name: 'incoming-invoices',
                children:[
                    {
                        path: ':id',
                        component: incomingInvoiceViewComponent,
                        name: 'incoming-invoices-view',
                    }
                ]
            },
            {
                path: 'outgoing',
                component: outgoingInvoicesComponent,
                name: 'outgoing-invoices',
                children:[
                    {
                        path: ':id',
                        component: outgoingInvoiceViewComponent,
                        name: 'outgoing-invoices-view',
                    }
                ]
            }
        ]
    }
]
