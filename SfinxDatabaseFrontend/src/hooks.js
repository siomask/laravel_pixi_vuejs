class Hooks {

}

export {Hooks}

Date._toUTCSeconds = function (date1) {
    return typeof date1 == 'number' ? date1 : new Date(date1).getTime() / 1000;
}
Date._toCustomDate = function (date1) {
    let _d = date1;
    if (typeof date1 == 'number') {
        _d = new Date(0);
        _d.setUTCSeconds(date1);
        _d = _d.toLocaleDateString();
    }
    return _d;
}
Date.daysBetween = function (date1, date2) {   //Get 1 day in milliseconds
    let one_day = 1000 * 60 * 60 * 24;    // Convert both dates to milliseconds
    let date1_ms = date1.getTime();
    let date2_ms = date2.getTime();    // Calculate the difference in milliseconds
    let difference_ms = date2_ms - date1_ms;        // Convert back to days and return
    return Math.abs(Math.round(difference_ms / one_day));
};
Date.prototype.getWeek = function () {

    // Create a copy of this date object
    var d = new Date(this.valueOf());

    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
    // ISO week date weeks start on monday, so correct the day number
    /*  var dayNr   = (this.getDay() + 7) % 7;

     // Set the target to the thursday of this week so the
     // target date is in the right year
     target.setDate(target.getDate() - dayNr + 3);

     // ISO 8601 states that week 1 is the week with january 4th in it
     var jan4    = new Date(target.getFullYear(), 0, 4);

     // Number of days between target date and january 4th
     var dayDiff = (target - jan4) / 86400000;

     if(new Date(target.getFullYear(), 0, 1).getDay() < 5) {
     // Calculate week number: Week 1 (january 4th) plus the
     // number of weeks between target date and january 4th
     return 1 + Math.ceil(dayDiff / 7);
     }
     else {  // jan 4th is on the next week (so next week is week 1)
     return Math.ceil(dayDiff / 7);
     }*/
};
Date._formatDate = function (dayUtc) {
    return new Date(new Date(0).setUTCSeconds(dayUtc)).formatDate()
}
Date._formatDateLocal = function (dayUtc) {
    return new Date(new Date(0).setUTCSeconds(dayUtc)).toLocaleDateString()
}
Date.prototype.formatDate = function () {
    var dayNames = [
        "Monday", "Tuesday", "Wednesday",
        "Thurdsday", "Friday", "Saturday", "Sunday"
    ];
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var s = this.toDateString().split(" ");
    for (let i = 0; i < dayNames.length; i++) {
        if (dayNames[i].match(s[0])) {
            s[0] = dayNames[i] + ", ";
            break;
        }
    }
    for (let i = 0; i < monthNames.length; i++) {
        if (monthNames[i].match(s[1])) {
            let _montIndex = i + 1;
            s[1] = _montIndex < 10 ? "0" + _montIndex : _montIndex;
            break;
        }
    }

    return s[0] + s[1] + "-" + s[2] + "-" + s[3];
}
Object.defineProperty(Object.prototype, "customClone", {
    get: function customClone() {
        let s = new this.constructor() , deep = true;
        // Object.assign(s,this);

        if (deep) {
            for (let field in this) {
                let _v = this[field];

                if (_v) {
                    if (typeof _v == 'object') {
                        s[field] = _v.customClone;
                    } else {
                        s[field] = _v;
                    }
                }

            }
        }
        return s;
    }
});
Object.defineProperty(Object.prototype, "customCloneNoDeep", {
    get: function customClone() {
        let s =  new this.constructor(this);

        for (let field in this) {
            s[field] = this[field];
        }
        return s;
    }
});
Object.assign_fields=function(core,source,fields){
    for(let i=0;i<fields.length;i++){
        core[fields[i]] = source[fields[i]];
    }
}